<?php

class Sc_ContentController extends Eh_DefaultController {

	public function init() {
		$this->_helper->layout->setLayout('subpage');

		$rp = $this->_request->getParams();

		$static = true;
		$cId = $rp['action'];

		switch ($rp['action']) {
			case 'about' :
				$this->view->pageTitle = t('TAB_PROGRAM_TITLE');
				break;

			default : 
				$static = false;
				break;
		}

		if ($static) {
			//$this->view->pageContent = '';
			$this->view->pageContent = WX_Content::getInstance($cId)->getContent();
			$this->_helper->viewRenderer('static');
		}

		parent::init();
	}

	public function aboutAction () {}

}
