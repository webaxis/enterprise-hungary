<?php

class Sc_FaqController extends Eh_DefaultController {

	public function indexAction() {
		$this->_helper->layout->setLayout('subpage');

		$this->view->faq = Eh_Model_Faq::getInstance()->fetchListToArray(null, 'order ASC');
	}
}
