<?php

class Sc_IndexController extends Eh_DefaultController {

	public function indexAction() {
		$this->view->slides = Eh_Converter_Slide::toList(Eh_Model_Slide::getInstance()->fetchListToArray('site = 2', 'order ASC'));
		$this->view->events = Eh_Converter_Event::toList(Eh_Model_Event::getInstance()->fetchListToArray('site = 2 || site = 3', 'date DESC'));
		$this->view->timeline = Eh_Converter_Timeline::toList(Eh_Model_Timeline::getInstance()->fetchListToArray(null, 'order ASC'));
		$this->view->news = new WX_List2('newsSc');
	}

	public function applyAction() {
		$this->_redirect('https://docs.google.com/forms/d/1j-Mk7NkekmorUswhpWjZYMy5E_h21TZ3eFedA3EcxbU/viewform?embedded=true');
	}
}
