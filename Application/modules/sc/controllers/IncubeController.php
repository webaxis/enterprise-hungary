<?php

class Sc_IncubeController extends Eh_DefaultController {

	public function indexAction() {
		$this->_helper->layout->setLayout('subpage');
		$this->view->pageTitle = t('TAB_INCUBE_TITLE');

		$this->view->organizations = Eh_Converter_Organization::toList(Eh_Model_Organization::getInstance()->fetchListToArray(null, 'order ASC'));
	}
}
