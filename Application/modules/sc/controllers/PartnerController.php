<?php

class Sc_PartnerController extends Eh_DefaultController {

	public function indexAction() {
		$this->_helper->layout->setLayout('partners');

		$this->view->subPartners = Eh_Converter_Partner::toList(Eh_Model_Partner::getInstance()->fetchListToArray(null, 'order ASC'));

		$this->view->pageTitle = t('TAB_PARTNERS_TITLE');
	}
}
