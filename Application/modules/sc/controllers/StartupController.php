<?php

class Sc_StartupController extends Eh_DefaultController {

	public function indexAction() {
		$this->_helper->layout->setLayout('subpage');

		$rp = $this->_request->getParams();

		if (!isset($rp['startup']) || empty($rp['startup'])) {
			$this->_redirect(Eh_Url::startup());
		}

		$id = explode('-',$rp['startup'])[0];

		$this->view->profile = Eh_Converter_Startup::toProfile(Eh_Model_Startup::getInstance()->find($id)->toArray());

		$this->view->leftSections = array(
			['field' => 'projectmanager', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_PROJECT_MANAGER')],
			['field' => 'businessinformation', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_BUSINESS_INFORMATION')],
			['field' => 'investmentinformation', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_INVESTMENT_INFORMATION')],
			['field' => 'projectteam', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_PROJECT_TEAM')],
		);

		$this->view->rightSections = array(
			['field' => 'businesssummary', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_BUSINESS_SUMMARY')],
			['field' => 'solution', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_SOLUTION')],
			['field' => 'targetmarket', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_TARGET_MARKET')],
			['field' => 'competitors', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_COMPETITORS')],
			['field' => 'progress', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_PROGRESS')],
			['field' => 'businessmodel', 'title' => t('STARTUPS_PROFILE_SECTION_TITLE_BUSINESS_MODEL')],
		);

		if (empty($this->view->profile)) {
			$this->_redirect(Eh_Url::startup());
		}

		$this->view->pageTitle = $this->view->profile['name'];
	}

	public function listAction() {
		$this->_helper->layout->setLayout('subpage');

		$this->view->batches = Eh_Model_Batch::getInstance()->fetchListToArray(null, 'order ASC');
		$this->view->startups = Eh_Converter_Startup::toList(Eh_Model_Startup::getInstance()->fetchListToArray(null, ['batch DESC','order ASC']));

		$this->view->pageTitle = t('TAB_STARTUPS_TITLE');
	}
}
