<?php

class Sc_TeamController extends Eh_DefaultController {

	public function indexAction() {
		$this->_helper->layout->setLayout('subpage');
		
		$this->view->team = Eh_Converter_Team::toList(Eh_Model_Team::getInstance()->fetchListToArray(null, 'order ASC'));

		$this->view->pageTitle = t('TAB_TEAM_TITLE');
	}
}
