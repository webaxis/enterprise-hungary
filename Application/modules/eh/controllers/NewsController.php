<?php

class Eh_NewsController extends Eh_DefaultController {

	public function indexAction() {
		$this->_helper->layout->setLayout('news');

		$rp = $this->_request->getParams();

		$id = explode('-', $rp['news'])[0];

		$this->view->news = Eh_Converter_News::toProfile(Eh_Model_News::getInstance()->find($id)->toArray());
		$this->view->moreNews = Eh_Converter_News::toList(Eh_Model_News::getInstance()->fetchListToArray(
			'(site = 1 || site = 3) && lang like "'.Eh_Locale::getInstance()->getLanguage().'" && id != "'.$id.'"',
			'date DESC',
			3
		));
	}
}
