<?php

class Eh_IndexController extends Eh_DefaultController {

	public function indexAction() {
		$lang = Eh_Locale::getInstance()->getLanguage();
		$this->view->slides = Eh_Converter_Slide::toList(Eh_Model_Slide::getInstance()->fetchListToArray('(site = 1 || site = 3) && lang = "'.$lang.'"'));
		$this->view->news = new WX_List2('news');
		$this->view->events = Eh_Converter_Event::toList(Eh_Model_Event::getInstance()->fetchListToArray('(site = 1 || site = 3) && lang = "'.$lang.'"', 'date DESC'));
	}

    public function langhuAction () {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$rp = $this->_request->getParams();

		$this->_redirect(Eh_Url::index() . '?language=hu');
    }

    public function langenAction () {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$rp = $this->_request->getParams();
		$lang = $rp['lang'];

		$this->_redirect(Eh_Url::index() . '?language=en');
    }
}
