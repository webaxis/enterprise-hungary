<?php

class Admin_TranslateehController extends Eh_AdminController {
	/**
	 * Ajax function
	 */
	public function importtranslatefileAction() {
		//Ajax check
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		if (!$this->_request->isXmlHttpRequest()) {
			die('Not valid AJAX call!');
		}

		//A fordito tabla uritese
		//$this->_emptyTranslateTable();

		$languages = Eh_Locale::getInstance()->getAvailableLanguages();
		$tags = $this->_getTagsFromTable();

		foreach ($languages as $language) {
			$translateFile = APPLICATION_PATH . 'modules/eh/languages/' . $language . '/site.wxt';
			$this->_processFile($translateFile, $language, $tags);
		}

		$response = WX_Ajax_Response_Json::success();
		die($response);
	}

	public function exporttranslatefileAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		if (!$this->_request->isXmlHttpRequest()) {
			die('Not valid AJAX call!');
		}

		self::_deleteTranslateFiles();

		$model = new Eh_Model_Translateeh();

		$tags = $model->fetchAllToArray();

		foreach ($tags as $tag) {
			$languages = Eh_Locale::getInstance()->getAvailableLanguages();
			foreach ($languages as $language) {
				$translateFile = APPLICATION_PATH . 'modules/eh/languages/' . $language . '/site.wxt';
				$line = $tag['label'] . '|||' . preg_replace( "/\r|\n/", "", nl2br($tag[strtolower($language)])) . "\n";
				file_put_contents($translateFile, $line, FILE_APPEND);
			}
		}

		$response = WX_Ajax_Response_Json::success();
		die($response);
	}

	protected function _getTagsFromTable () {
		$db = Zend_Registry::get('db');

		$query = $db->select()
					->from('translateeh');

		$tags = array();
		foreach ($db->query($query)->fetchAll() as $one) {
			foreach (Eh_Locale::getInstance()->getAvailableLanguages() as $lang) {
				$tags[$one['label']][$lang] = $one[$lang];
			}
		}

		return $tags;
	}

	protected function _emptyTranslateTable () {
		Eh_Model_Translateeh::deleteAll();
	}

	protected function _processFile ($file, $language = 'hu', $tags) {
		$fh = fopen($file,'r');
		while ($line = fgets($fh)) {
			//Remove newlines
			$line =  trim(preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $line));

			$parts = explode('|||', $line);
			$parts[1] = nl2br($parts[1]);
			//DX($parts);
			//Save tag
			if (!array_key_exists($parts[0],$tags) || ($tags[$parts[0]][$language] == null)) {
				Eh_Model_Translateeh::saveTag($parts, $language);
			}

		}
		fclose($fh);
	}

	/**
	 * Generate translation files from DB
	 */
	static public function generateFiles() {
		//A letezo forditofajlok torlese
		self::_deleteTranslateFiles();

		$model = new Eh_Model_Translateeh();

		$tags = $model->fetchAllToArray();

		foreach ($tags as $tag) {
			$languages = Eh_Locale::getInstance()->getAvailableLanguages();
			foreach ($languages as $language) {
				$translateFile = APPLICATION_PATH . 'modules/eh/languages/' . $language . '/site.wxt';
				$line = $tag['label'] . '|||' . preg_replace( "/\r|\n/", "",  nl2br($tag[strtolower($language)])) . "\n";
				file_put_contents($translateFile, $line, FILE_APPEND);
			}
		}
	}

	static protected function _deleteTranslateFiles() {
		$languages = Eh_Locale::getInstance()->getAvailableLanguages();
		foreach ($languages as $language) {
			$translateFile = APPLICATION_PATH . 'modules/eh/languages/' . $language . '/site.wxt';
			if (is_file($translateFile)) {
				unlink($translateFile);
			}
		}
	}
}
