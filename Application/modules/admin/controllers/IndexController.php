<?php

class Admin_IndexController extends Eh_Controller {

    public function indexAction() {

		$this->_helper->layout->setLayout('login');

		if (Eh_Admin::getInstance()->isLoggedIn()) {
			$this->_redirect(Eh_Url::admin());
		}

		$r = $this->_request;
		$rp = $r->getParams();

		$isSubmit = WX_Tools::getValue($rp, 'isSubmit');

		$this->view->form = WX_DForm::getInstance('adminlogin')->get($rp);
		//DX($this->view->form);

				//DX($r->isPost());
		if ($r->isPost()) {
			if ($this->view->form->isValid($rp)) {
				$login = Eh_Admin::getInstance()->login($rp['login'], $rp['password']);
				if ($login === true) {
					$this->_redirect(Eh_Url::admin());
				}
	
				$this->view->error	= 'Hibás felhasználónév vagy jelszó.';
			}
		}
    }
}
