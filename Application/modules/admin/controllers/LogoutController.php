<?php

class Admin_LogoutController extends Eh_AdminController {

    public function indexAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		Eh_Admin::getInstance()->logout();
		$this->_redirect(Eh_Url::admin());
    }

}
