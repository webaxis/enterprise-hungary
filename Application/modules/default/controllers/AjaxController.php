<?php

class AjaxController extends WX_Controller_Ajax {

	public function changecontentlanguageAction() {
		$rp = $this->_request->getParams();

		Eh_Admin_ContentLanguage::getInstance()->setLanguage($rp['lang']);

		$response = WX_Ajax_Response_Json::success();
		die($response);
	}
	
	public function listmoreAction () {
		$rp = $this->_request->getParams();

		$descriptor = WX_Tools::getValue($rp, 'descriptor', false);
		$page = WX_Tools::getValue($rp, 'page', false);
		$refresh = WX_Tools::getValue($rp, 'refresh', false);
		$params = WX_Tools::getValue($rp, 'params', false);

		$list = new WX_List2($descriptor, $page, $params);

		if (!$list->isPermitted()) {
			$this->_forward('error', null, null, array('errorCode' => 901));
			return false;
		}

		if (!empty($refresh)) {
			$list->refresh();
		}

		if (!empty($ahead)) {
			$list->ahead($ahead);
		}

		$response['html'] = $list->fetch();

		die(json_encode($response));
	}

	public function listAction () {
		$rp = $this->_request->getParams();

		$descriptor = WX_Tools::getValue($rp, 'descriptor', false);
		$page = WX_Tools::getValue($rp, 'page', false);
		$params = WX_Tools::getValue($rp, 'params', false);

		$list = new WX_List2($descriptor, $page, $params);

		if (!$list->isPermitted()) {
			$this->_forward('error', null, null, array('errorCode' => 901));
			return false;
		}

		$response['html'] = $list->fetch();

		die(json_encode($response));
	}
}
