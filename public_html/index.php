<?php

ini_set('default_charset', 'UTF-8' );

define('APP_NAME', 					'Eh');
define('PROJECT_NAME', 				strstr($_SERVER['HTTP_HOST'], 'campus') !== false || strstr($_SERVER['HTTP_HOST'], 'sc') !== false ? 'sc' : 'eh');
define('APP_KEY', 					'f4357a39108602a792aer36b8d8a06877dfb3c23'); # http://online-code-generator.com/sha1-hash-with-optional-salt.php
define('APP_ENV', 					isset($_SERVER['APPLICATION_ENV']) ? $_SERVER['APPLICATION_ENV'] : 'production');

define('CACHE_VERSION',				'11');

define('BASE_URL', 					'/');
define('BASE_HOST',					(!empty($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http') . '://' . $_SERVER['HTTP_HOST'] . BASE_URL);
define('BASE_PATH', 				preg_replace('/\\\\/','/',realpath(dirname(__FILE__)) . '/../'));

define('APPLICATION_PATH', 			BASE_PATH . 'Application/');
define('PUBLIC_PATH', 				realpath(dirname(__FILE__)  .  '/../public_html/') . '/');
define('CORE_PATH', 				BASE_PATH . 'Core/');
define('LIBRARIES_PATH',			BASE_PATH . 'Library/');

// CORE folders
define('CORE_APP_PATH', 			BASE_PATH . 'Core/' . APP_NAME . '/');
define('ZEND_PATH', 				CORE_PATH . 'Zend/');
define('ZENDX_PATH', 				CORE_PATH . 'ZendX/');
define('WX_PATH', 					CORE_PATH . 'WX/');

// APPLICATION folders
define('MODULES_PATH',				APPLICATION_PATH . 'modules/');
define('DEFAULT_MODULE_PATH',		MODULES_PATH . 'default/');
define('VIEWS_PATH',				DEFAULT_MODULE_PATH . 'views/');
define('SCRIPTS_PATH',				VIEWS_PATH . 'scripts/');
define('LANGUAGES_PATH',			APPLICATION_PATH . 'modules/' . PROJECT_NAME . '/languages/');
define('MAIL_TEMPLATES_PATH',		APPLICATION_PATH . 'mails/');

// PUBLIC relative
define('JS_RELATIVE', 				BASE_URL . 'js/dist/');
define('PLUGIN_RELATIVE',			BASE_URL . 'js/plugins/');
define('NODE_RELATIVE', 			BASE_URL . 'node_modules/');
define('CSS_RELATIVE', 				BASE_URL . 'css/dist/');
define('IMAGE_RELATIVE',			BASE_URL . 'images/dist/');
define('FONT_RELATIVE',				BASE_URL . 'fonts/');
define('CACHE_RELATIVE',			BASE_URL . 'cache/');
define('UPLOAD_RELATIVE',			BASE_URL . 'uploads/');
define('UPLOAD_TEMP_RELATIVE',		BASE_URL . 'uploads/temp/');

// PUBLIC urls
define('JS_URL', 					BASE_HOST . 'js/dist/');
define('PLUGIN_URL',				BASE_HOST . 'js/plugins/');
define('NODE_URL', 					BASE_HOST . 'node_modules/');
define('CSS_URL', 					BASE_HOST . 'css/dist/');
define('IMAGE_URL',					BASE_HOST . 'images/dist/');
define('FONT_URL',					BASE_HOST . 'fonts/');
define('CACHE_URL',					BASE_HOST . 'cache/');
define('UPLOAD_URL',				BASE_HOST . 'uploads/');
define('UPLOAD_TEMP_URL',			BASE_HOST . 'uploads/temp/');

// PUBLIC paths
define('JS_PATH', 					PUBLIC_PATH . 'js/dist/');
define('PLUGIN_PATH',				PUBLIC_PATH . 'js/plugins/');
define('NODE_MODULE_PATH', 			PUBLIC_PATH . 'node_modules/');
define('CSS_PATH', 					PUBLIC_PATH . 'css/dist/');
define('IMAGE_PATH',				PUBLIC_PATH . 'images/dist/');
define('FONT_PATH',					PUBLIC_PATH . 'fonts/');
define('CACHE_PATH',				PUBLIC_PATH . 'cache/');
define('UPLOAD_PATH',				PUBLIC_PATH . 'uploads/');
define('UPLOAD_TEMP_PATH',			PUBLIC_PATH . 'uploads/temp/');

// ADMIN urls
define('ADMIN_URL',					BASE_HOST . 'admin/');

set_include_path(implode(PATH_SEPARATOR, array(
    CORE_PATH,
    APPLICATION_PATH,
    get_include_path(),
)));

include CORE_PATH . "Zend/Loader/Autoloader.php";

/* Initialize the application autoloader */
$autoloaderApp = Zend_Loader_Autoloader::getInstance();
$autoloaderApp->registerNamespace(APP_NAME . '_');


/* Initialize WX autoloader */
$autoloaderWX = Zend_Loader_Autoloader::getInstance();
$autoloaderWX->registerNamespace('WX_');

require_once ZEND_PATH . 'Application.php';
require_once WX_PATH . 'Functions.php';

$cnfFile = APPLICATION_PATH . 'config/application.xml';

Zend_Registry::set("config_file_path", $cnfFile);

$application = new Zend_Application(
	APP_ENV,
	$cnfFile
);

$application->bootstrap();

$application->run();
