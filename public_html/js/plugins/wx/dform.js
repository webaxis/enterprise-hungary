;(function($) {
	$.fn.dynamicForm = function(options){

		// if no elements associated
		if (this.length == 0) return this;

		// support mutltiple elements
		if (this.length > 1){
			this.each(function(){$(this).dynamicForm(options)});
			return this;
		}

		var element = $(this);

		// must have id
		if (!element.attr('id').length) {
			D('Missing ID! You can init DynamicForm only with ID! ','e');
			return;
		}

		namespace = element.attr('id');

		var defaults = {
			// variables
			namespace : namespace,
			submitButton : false,
			cloneGroups : {
				enable : true,
				settings : {
					addMoreButton : '[+] add more',
					removeButton : '[-] remove'
				}
			},
			decorators : {
				enable : true,
				datepicker : {
					enable : true,
					message : 'words left',
					settings : {}, // see further options in counter.js
				},
				charlimit : {
					enable : true,
					message : 'characters left',
					settings : {} // see further options in counter.js
				},
				wordlimit : {
					enable : true,
					settings : { // see further options in counter.js
						inlineAttributes : {
							goal: namespace + '-wordlimit-max'
						}
					}
				},
				url : {
					enable : true,
					types : { // see further options in validation.js
						basic : {
							message : 'Wrong url'
						}
					}
				},
				email : {
					enable : true,
					message : 'enter a valid email',
					settings : {} // see options in validation.js
				}
			},

			imageUploaders : {
				enable : true,
				default : {
					enable : true,
					type : null,
					loader : '/images/wx/image-loader.gif',
					size : null,
					uploadUrl : false, // if its true use defaults by 'urls'
					urls : {
						upload : '/ajax/uploadimage',
						uploadTemp : '/ajax/uploadtempimage'
					},
					temp : true,
					delete : {
						softDelete : false,
						enable : true,
						url : '/ajax/deleteimage'
						//defaultImage : false
					},
					params : {},
					loadInAnimation : function(element) {
						element.fadeIn();
					}
				}
			},

			// callbacks
			beforeSubmit : function() {},
			afterSubmit : function() {},
			onSubmit : function() { submitForm(); } // you can override submit
		};

		var decorators = {
			init : function() {
				if (options.decorators.enable !== true) {
					return;
				}

				var config = options.decorators;

				// datepicker
				if (config.datepicker.enable == true) {
					// @todo : fast solution for language
					$.datepicker.regional['hu'] = {
						closeText: 'bezárás',
						prevText: '&laquo;&nbsp;vissza',
						nextText: 'előre&nbsp;&raquo;',
						currentText: 'ma',
						monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június',
						'Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
						monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',
						'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],
						dayNames: ['Vasámap', 'Hétfő', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
						dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],
						dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
						dateFormat: 'yy-mm-dd', 
						firstDay: 1,
						isRTL: false
					};
					$.datepicker.setDefaults($.datepicker.regional['hu']);
					$('input[' + namespace + '-decorator*="datepicker"]').datepicker($.datepicker.regional[ "hu" ]);
				}

				// wordlimit
				if (config.wordlimit.enable == true) {
					$('textarea[' + namespace + '-decorator*="wordlimit"], input[' + namespace + '-decorator*="wordlimit"]').counter({
						type: 'word',
						msg: config.wordlimit.message,
						inlineAttributes: config.wordlimit.settings.inlineAttributes
					});
				}

				// charlimit
				if (config.charlimit.enable == true) {
					$('textarea[' + namespace + '-decorator*="charlimit"], input[' + namespace + '-decorator*="charlimit"]').counter({
						type: 'char',
						msg: config.charlimit.message,
						inlineAttributes: config.charlimit.settings.inlineAttributes
					});
				}

				// url
				if (config.url.enable == true) {
					$('input[' + namespace + '-decorator*="url"]').validation_url({
						nspace : namespace,
						types : config.url.types
					});
				}

				// email
				if (config.email.enable == true) {
					$('input[' + namespace + '-decorator*="email"]').validation_email({
						msg: config.email.message
					});
				}
			}
		}

		cloneGroup = function() {
			var cloneGroupContainer = [];

			var config = {};

			var namespace = '';

			this.init = function() {
				if (options.cloneGroups.enable !== true) {
					return;
				}
				
				config = options.cloneGroups;

				collectCloneGroups();
				bindCloneGroups();
			}

			var getSelector = function(name) {
				var selector = '[clone-group="' + name + '"]';

				return selector;
			}

			var collectCloneGroups = function() {
				$('[clone-group]').each(function(){
					name = $(this).attr('clone-group');
					if ($.inArray(name, cloneGroupContainer) == -1) {
						cloneGroupContainer.push(name);
					}
				});
			}

			var bindCloneGroups = function() {
				$.each(cloneGroupContainer, function(idx, name) { 
					var selector = getSelector(name);
					addRemoveButtons(selector);
					bindRemoveButtons(selector);
					addAndBindMoreButton(selector, name);
				});
			}

			var addAndBindMoreButton = function(selector, name) {
				var lastCloneGroupItem = getCloneGroupLastGroup(selector),
					addMoreId = name + '-add-more';
				// append
				lastCloneGroupItem.after('<a clone-group-name="' + name + '" id="' + addMoreId + '" href="javascript:void(0);">' + config.settings.addMoreButton + '</a>');
				// bind
				$('#' + addMoreId).unbind('click').bind('click', function(e){
					e.preventDefault();
					var name = $(this).attr('clone-group-name'),
						selector = getSelector(name),
						clone = getCloneGroupFirstElementClean(selector),
						lastCloneGroupItem = getCloneGroupLastGroup(selector);

					lastCloneGroupItem.after(clone);
					bindRemoveButtons(selector);
					decorators.init();
				});
			}

			var getCloneGroupFirstElementClean = function(selector) {
				// get element
				var clone = $('html').find(selector + ':first').clone();
				// add remove button also
				addRemoveButton(clone);
				// clean values
				clone.find('input[type="text"], textarea').each(function(idx, element){
					if ($(element).is('input[type="text"]')) {
						$(element).attr('value', '');
					} else {
						$(element).html('');
					}
				});

				return clone.outerHTML();
			}

			var getCloneGroupLastGroup = function(selector) {
				var lastElement = $('html').find(selector + ':last');

				return lastElement;
			}

			var addRemoveButtons = function(selector) {
				$(selector).each(function(idx, value){
					if (idx > 0) {
						addRemoveButton($(this));
					}
				});
			}

			var addRemoveButton = function(element) {
				element.append('<a class="button-remove" href="javascript:void(0);">' + config.settings.removeButton + '</a></div>');
			}

			var bindRemoveButtons = function(selector) {
				$(selector + ' a.button-remove').unbind('click').bind('click', function() {
					$(this).parent().remove();
					decorators.init();
				});
			}
		}

		imgUploader = function() {

			var uploaderSelector = '.image-uploader';

			var deleteSelector = '.image-uploader-delete';

			this.init = function() {
				if (options.imageUploaders.enable !== true) {
					return;
				}

				$(uploaderSelector).each(function() {
					var element = $(this),
						id = $(this).attr('data-id'),
						type = $(this).attr('data-type'),
						feedbackSize = $(this).attr('feedback-size')
						loader = element.find('.loader');
						loaderImg = loader.find('img');

					// if element has 
					if (options.imageUploaders.hasOwnProperty(id)) {
						var currentUploader = $.extend(true, {}, options.imageUploaders[id]);
						var defaultUploader = options.imageUploaders.default;
						$.extend(true, options.imageUploaders[id], defaultUploader, currentUploader);
					} else {
						var defaultUploader = options.imageUploaders.default;
						options.imageUploaders[id] = {};
						$.extend(true, options.imageUploaders[id], defaultUploader);
					}

					// set type
					options.imageUploaders[id].type = type;
					
					// set upload url
					if (options.imageUploaders[id].uploadUrl == false) {
						if (options.imageUploaders[id].temp == true) {
							options.imageUploaders[id].uploadUrl = options.imageUploaders.default.urls.uploadTemp;
						} else {
							options.imageUploaders[id].uploadUrl = options.imageUploaders.default.urls.upload;
						}
					}
					// further settings for uploader
					options.imageUploaders[id].params.type = options.imageUploaders[id].type;
					options.imageUploaders[id].imageSelector = '#' + id + '-image';
					if (options.imageUploaders[id].size != null) {
						options.imageUploaders[id].params.size = options.imageUploaders[id].size;
					}

					var imageSelector = options.imageUploaders[id].imageSelector,
						imageSelectorDefault = imageSelector + '-default',
						deleteElement = element.find('.image-uploader-delete');

					// check delete option is enabled
					if (options.imageUploaders[id].delete.enable == true) {
						// set delete link event
						deleteElement.unbind('click').bind('click', function(e) {
							e.preventDefault();
							var dElement = $(this);

							var hash = $('#' + id).val();

							if (options.imageUploaders[id].delete.softDelete == true) {
								// hide img
								$(imageSelector).hide();
								// set img src attribute to empty
								$(imageSelector).attr('src', '');
								// set main form's hidden input value to empty
								$('#' + id).val('');
								// show default image - if exists
								if ($(imageSelectorDefault).length) {
									$(imageSelectorDefault).show();
								}
								// hide delete link
								deleteElement.hide();
							} else {
								// collect variables for delete
								var vars = 'type=' + options.imageUploaders[id].type + '&hash=' + hash + '&temp=' + (options.imageUploaders[id].temp == true ? '1' : '0');

								// call delete
								wx.ajax(options.imageUploaders[id].delete.url, vars, {
									afterSuccess : function(response) {
										// hide img
										$(imageSelector).hide();
										// set img src attribute to empty
										$(imageSelector).attr('src', '');
										// set main form's hidden input value to empty
										$('#' + id).val('');
										// show default image - if exists
										if ($(imageSelectorDefault).length) {
											$(imageSelectorDefault).show();
										}
										// hide delete link
										deleteElement.hide();
									}
								});
							}
						});
					} else {
						// remove delete
						if (deleteElement.length) {
							deleteElement.remove();
						}
					}

					// load loader into loaderImg
					loaderImg.attr('src', options.imageUploaders[id].loader);

					// Before the image upload start hook
					options.imageUploaders[id].onBeforeUploadDForm = function() {
						// hide img
						$(imageSelector).hide();
						// set img src attribute to empty
						$(imageSelector).attr('src', '');
						// show loader
						$('div[data-id=' + id + ']').find('.loader').show();
					}

					// After the image successfully uploaded hook
					options.imageUploaders[id].afterUploadSuccessDForm = function(response) {
						// add hash to main form
						$('#' + id).val(response.data.id);

						$(imageSelectorDefault).hide();
						$(imageSelector).hide().load(function(){
							// hide loader
							loader = $('div[data-id=' + id + ']').find('.loader').hide();
							options.imageUploaders[id].loadInAnimation($(this));
						});
						deleteElement.show();
					}

					$(this).find(uploaderSelector + '-container').imageUploader(options.imageUploaders[id]);
				});
			}

		}

		var options = $.extend(true, defaults, options);

		var init = function() {
			if (options.submitButton !== false) {
				bindSubmitButton();
			}
		}

		var bindSubmitButton = function() {
			$(options.submitButton).unbind('click').bind('click', function(e) {
				e.preventDefault();
				options.beforeSubmit();
				options.onSubmit();
				options.afterSubmit();
			});
		}

		var submitForm = function() {
			$('#' + namespace).submit();
		}

		// init main system
		init();

		// init decorators
		decorators.init();

		// init clonegroups
		var cloneGroups = new cloneGroup();
		cloneGroups.init();

		// init uploaders
		var imgUploaders = new imgUploader();
		imgUploaders.init();

		return this;
	};
})(jQuery);