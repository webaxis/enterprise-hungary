/* GOOGLE MAPS IMPLEMENTATION COLLECTION */

if (typeof wx == 'undefined') {
	var wx = {};
}

wx.maps = {};

// one map with one marker and without any interaction
wx.maps.simple = function(canvId, options) {

	var canvasId = null;

	var map = null;

	var mapSettings = null;

	var defaults = {
		/* REQUIRED PARAMS */
		cordLat: null,
		cordLong: null,

		/* OPTIONAL PARAMS */
		map: {
			zoom: 18,
			maxZoom: 18,
			styles: null,
			scrollwheel: false,
		},

		marker: {
			title : null,
			icon: null,
			cordLat: null,
			cordLong: null,
		}
	}

	var canvasId = canvId;
	var settings = $.extend( true, defaults, options );

	// MAP
	var mapLatLng = new google.maps.LatLng(settings.cordLat, settings.cordLong);
	mapSettings = {
		center: mapLatLng,
		zoom: settings.map.zoom,
		scrollwheel: settings.map.scrollwheel
	};
	if (settings.map.styles != null) {
		mapSettings.styles = settings.map.styles;
	}

	map = new google.maps.Map(document.getElementById(canvasId), mapSettings);

	// MARKER
	var markerSettings = {
		map: map
	};
	if (settings.marker.cordLat == null || settings.marker.cordLong == null) {

		markerSettings.position = mapLatLng;
	} else {

		markerSettings.position = new google.maps.LatLng(settings.marker.cordLat, settings.marker.cordLong)
	}
	if (settings.marker.title != null) {
		markerSettings.title = settings.marker.title;
	}
	if (settings.marker.icon != null) {
		markerSettings.icon = settings.marker.icon;
	}

	var marker = new google.maps.Marker(markerSettings);
}