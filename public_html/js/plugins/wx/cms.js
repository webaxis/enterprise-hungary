if (typeof wx == 'undefined' || wx === undefined) {
	var wx = {}
}

wx.cms = {};
wx.cms.list = {
	init : function (cmsDescriptorName) {
		$('.wx-delete-entity').click(function(e){
			e.preventDefault();
			
			if (!confirm(t('JS_CMS_CONFIRM_DELETE_ROW')))
				return false;
			
			var that = this;

			wx.ajax('/cms/deleteentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});
					
					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		})
		
		$('.wx-activate-entity').click(function(e){
			e.preventDefault();
			
			var that = this;
			
			wx.ajax('/cms/activateentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});
					
					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		})
		
		$('.wx-inactivate-entity').click(function(e){
			e.preventDefault();
			
			var that = this;
			
			wx.ajax('/cms/inactivateentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		})

		$('.wx-archive-entity').click(function(e){
			e.preventDefault();

			var that = this;

			wx.ajax('/cms/archiveentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		})

		$('.wx-dearchive-entity').click(function(e){
			e.preventDefault();

			var that = this;

			wx.ajax('/cms/dearchiveentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		})

		$('.wx-change-order').change(function(e){
			e.preventDefault();
			
			var that = this;
			
			wx.ajax('/cms/changeorder',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName,order:$(this).val()},{
				beforeSend : function() {
					$('.wx-change-order').attr('disabled',true);
				},
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');

					wx.list.refresh(listId, {
						beforeSend : function () {
							$('.wx-change-order').attr('disabled',false);
						},
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});
					
					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		})
	}
}

wx.cms.form = {
	init : function (options) {
		$('.ckeditor-content').ckeditor({
			customConfig : '/js/admin/ckeditor_config.js',
			height: 400
		});

		$('#' + options.formId).dynamicForm({
			submitButton : options.saveButton,
			onSubmit : function() {
				wx.form.validate(options.formDescriptorName,options.formId,{
					url : options.saveUrl,
					params : {
						id : options.entityId
					},
					onResponse : function() {
						wx.cms.form.init(options);
					},
					onSuccess : function (response) {
						if (typeof options.onSaveSuccess == 'function') {
							options.onSaveSuccess();
						}
						wx.feedback.init('success', response.data.message, '', 1500);
					},
					onError : function() {
						if (typeof options.onError == 'function') {
							options.onError();
						}
					}
				})
			},
			imageUploaders : options.imageUploaders
		}); 
	}
}

wx.cms.wysiwyg = {
	init : function (options) {
		$('.page-tabs a').click(function(e){
			e.preventDefault();
			$('.page-tabs a').removeClass('active');
			
			$(this).addClass('active');
			
			var rel = $(this).attr('rel');
			
			$('div[id^="page-tab-"]').hide();
			$('#page-tab-'+rel).show();
			$('input[name="language"]').val(rel);
		});

		$('.ckeditor.ck-full').ckeditor({
			customConfig : '/js/admin/ckeditor_config.js',
			height: wx.cms.wysiwyg.getWysiwygHeight()
		});

		
		$('#' + options.submitButton).click(function(e){
			e.preventDefault();

			var vars = $('#' + options.formId).serialize();
			
			wx.ajax(options.saveUrl, vars, function(response) {
				wx.feedback.init('success', response.data.message, '', 1500);
			})
		})
	},
	getWysiwygHeight : function () {
		var documentH 	= $(document).height(),
			leftH		= $('.left-side').outerHeight(true),
			minus		= $('h1').outerHeight(true) + $('.page-tabs').outerHeight(true) + 72,
			height		= documentH - minus;

		if (height < leftH - minus)
			height = leftH - minus;
			
		return height;
	},
	setActiveTab : function () {
		var tab = $('.page-tabs a.active').attr('rel');
		$('#page-tab-'+tab).show();
	}
}