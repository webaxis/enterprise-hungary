var facebook = {
	connect : function (callback) {
		loadStart();
		facebook.auth(callback);
	},
	auth : function (callback) {
		FB.login(function(response){
			if (response.status === 'connected') {
				loadEnd();
				
				if (typeof callback == 'function')
					callback(response);
			} else {
				endButtonLoad();
				loadEnd();
			}
		},{scope:'email,user_photos'});//@TODO A facebook jogosultsagokat majd egy configon keresztul kellne betolteni ide
	},
	share : function () {
		FB.ui({
			method: 		'share',
			href:			document.URL
		});
	}
}