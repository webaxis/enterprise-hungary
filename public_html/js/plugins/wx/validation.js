;(function($) {
	$.fn.validation_email = function(options){
		// if no elements associated
		if (this.length == 0) return this;

		// support mutltiple elements
		if(this.length > 1){
			this.each(function(){$(this).validation_email(options)});
			return this;
		}

		var defaults = {
			msg: 'not valid content'
		};

		options = $.extend(true, defaults, options);

		var element = $(this);
		
		var errorContainer = '';

		var type = 'email';

		var init = function() {
			initError();
			element.bind('focus', function(){
				errorContainer.hide();
			});
			element.bind('focusout', function(){
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if (!emailReg.test(element.val())) {
					errorContainer.show();
				} 
			});
		}

		var initError = function() {
			// Remove element if it was initialized before
			if ($(element).parent().find('.error-' +  type).length) {
				$(element).parent().find('.error-' +  type).remove();
			}
			element.after('<span class="error-' +  type + '">' + options.msg + '</span>');
			errorContainer = element.next();
			errorContainer.hide();
		}
		
		var email = function() {
			initError();
			element.bind('focus', function(){
				errorContainer.hide();
			});
			element.bind('focusout', function(){
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if (!emailReg.test(element.val())) {
					errorContainer.show();
				} 
			});
		}
		
		init();

		return this;
	};
})(jQuery);

;(function($) {
	$.fn.validation_url = function(options){
		// if no elements associated
		if (this.length == 0) return this;

		// support mutltiple elements
		if( this.length > 1) {
			this.each(function(){$(this).validation_url(options)});
			return this;
		}

		var defaults = {
			nspace: 'validation',
			types : {
				basic : {
					pattern : /^(http:\/\/|https:\/\/)?(www.)?[a-zA-Z0-9]+\.[a-z]{2,4}/,
					message : 'not valid url!'
				},
				youtube : {
					pattern : /^(?:https?:\/\/|\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/)/,
					message : 'not valid youtube url!'
				},
				vimeo : {
					pattern : /^(https?:\/\/)?(www.)?(player.)?vimeo.com\//,
					message : 'not valid vimeo url!'
				},
				youtubeOrVimeo : {
					pattern : /^((https?:\/\/)?(www.)?(player.)?vimeo.com\/)|^(?:https?:\/\/|\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/)/,
					message : 'not youtube nor vimeo url!'
				},
				linkedin : {
					pattern : /^https?:\/\/((www|[a-zA-Z_]{1,2})\.)?linkedin.com\/((in\/[^/]+\/?)|(pub\/[^/]+\/((\w|\d)+\/?){3}))$/,
					message : 'not valid linkedin url!'
				}
			}
		};

		options = $.extend(true, defaults, options);

		var element = $(this);

		var errorContainer = '';

		var type = 'basic';

		var message = '';

		var pattern = '';

		var init = function() {
			setType();
			setMessage();
			setPattern();
			initError();
			element.bind('focus', function(){
				errorContainer.hide();
			});
			element.bind('focusout', function(){
				if (element.val() != '') {
					if (!pattern.test(element.val())) {
						errorContainer.show();
					}
				}
			});
		}

		var setType = function() {
			elType = element.attr(options.nspace + '-url-type');
			if (elType !== undefined) {
				type = elType;
			}
		}

		var setMessage = function() {
			message = options.types[type].message;
		}

		var setPattern = function() {
			pattern = options.types[type].pattern;
		}

		var initError = function() {
			// Remove element if it was initialized before
			if ($(element).parent().find('.error-' +  type).length) {
				$(element).parent().find('.error-' +  type).remove();
			}
			element.after('<span class="error-' +  type + '">' + message + '</span>');
			errorContainer = element.next();
			errorContainer.hide();
		}

		init();

		return this;
	};
})(jQuery);