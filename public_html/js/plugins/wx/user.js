if (typeof wx == 'undefined' || wx === undefined) {
	var wx = {}
}

wx.user = {
	newpassword : function() {
		$('#button-save-newpassword').unbind('click').bind('click', function(e){
			e.preventDefault();
			$('#new-password-form').submit();
		});
	}
}

wx.user.modal = {

	facebook : function() {
		$('.button-facebook-connect').unbind('click').click(function(e){
			e.preventDefault();

			facebook.connect(function(response){
				wx.ajax('/ajax/fbconnect',{accessToken:response.authResponse.accessToken},function(){
					// @todo : ez drot
					location.href = '/kereses';
				});
			});
		})
	},

	googleplus : function() {
		$('.button-googleplus-connect').unbind('click').click(function(e){
			e.preventDefault();

			googleplus.connect(function(response){
				wx.ajax('/ajax/gpconnect',{accessToken:response.access_token, code: response.code, expires:response.expires_at},function(response){
					// @todo : ez drot
					location.href = '/kereses';
				});
			});
		})
	},

	login : function() {

		$('#button-login').unbind('click').bind('click', function(e){
			e.preventDefault();
			wx.form.validate('login','login-form',({
				save : true,
				onSuccess : function(response) {
					//window.location.href = wx.config.base_host;
					// @todo : ez drot
					window.location.href = '/kereses';
				},
				onError : function(response) {
					$('#login-error').html(response.data.message);
					$('#login-error').show();
				}
			}));
		});
	},

	registration : function() {

		$('#login-to-registration').unbind('click').bind('click', function(e) {
			e.preventDefault();
			wx.modal.closeAll();
			wx.form.clearFields('#modal-registration');
			$('#modal-registration').modal("show").focus();
		});

		$('#registration-to-login').unbind('click').bind('click', function(e) {
			e.preventDefault();
			wx.modal.closeAll();
			wx.form.clearFields('#modal-login');
			$('#modal-login').modal("show");
		});

		$('#button-registrate').unbind('click').bind('click', function(e){
			e.preventDefault();
			wx.form.validate('registration','registration-form',({
				onSuccess : function(response) {
					wx.modal.closeAll();
					wx.modal.success(
						t('JS_DIALOG_BODY_FEEDBACK_SUCCESS_REGISTRATION_SUCCESS'), 
						({title: t('JS_DIALOG_TITLE_REGISTRATION_SUCCESS')})
					);
				}
			}));
		});
	},

	forgottenpassword : function() {

		$('#login-to-forgotten-password').unbind('click').bind('click', function(e) {
			e.preventDefault();
			wx.modal.closeAll();
			wx.form.clearFields('#modal-forgotten-password');
			$('#modal-forgotten-password').modal("show");
		});

		$('#forgotten-password-to-login').unbind('click').bind('click', function(e) {
			e.preventDefault();
			wx.modal.closeAll();
			wx.form.clearFields('#modal-login');
			$('#modal-login').modal("show");
		});

		$('#button-forgotten-password').unbind('click').bind('click', function(e){
			e.preventDefault();
			wx.form.validate('forgottenpassword','forgotten-password-form',({
				save : true,
				onSuccess : function () {
					wx.modal.closeAll();
					wx.modal.message(
						t('JS_DIALOG_BODY_FEEDBACK_SUCCESS_FORGOTTEN_PASSWORD_SENT'),
						({title: t('JS_DIALOG_TITLE_FORGOTTEN_PASSWORD_SENT')})
					);
				}
			}));
		});
	},

	resendactivation : function() {

		$('#login-to-resend-activation').unbind('click').bind('click', function(e) {
			e.preventDefault();
			wx.modal.closeAll();
			wx.form.clearFields('#modal-resend-activation');
			$('#modal-resend-activation').modal("show");
		});

		$('#resend-activation-to-login').unbind('click').bind('click', function(e) {
			e.preventDefault();
			wx.modal.closeAll();
			wx.form.clearFields('#modal-login');
			$('#modal-login').modal("show");
		});

		$('#button-resend-activation').unbind('click').bind('click', function(e){
			e.preventDefault();
			wx.form.validate('resendactivation','resend-activation-form',({
				save : true,
				onSuccess : function () {
					wx.modal.closeAll();
					wx.modal.message(
						t('JS_DIALOG_BODY_FEEDBACK_SUCCESS_RESEND_ACTIVATION_SENT'),
						({title: t('JS_DIALOG_TITLE_RESEND_ACTIVATION_SENT')})
					);
				}
			}));
		})
	},

	changepassword : function() {
		$('#button-change-password').unbind('click').bind('click', function(e){
			e.preventDefault();
			wx.modal.closeAll();
			$('#dialog-change-password').modal('open');
		});

		$('#button-save-change-password').unbind('click').bind('click', function(e){
			e.preventDefault();
			wx.form.validate('changepassword','change-password-form',({
				onSuccess : function(response) {
					wx.modal.closeAll();
					wx.modal.success(
						t('JS_DIALOG_BODY_FEEDBACK_SUCCESS_PASSWORD_CHANGED'), 
						({title: t('JS_DIALOG_TITLE_PASSWORD_CHANGED')})
					);
				}
			}));
		});
	}
}