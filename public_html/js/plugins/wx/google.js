var googleplus = {
	connect : function (callback) {
		loadStart();
		googleplus.auth(callback);
	},
	auth : function (callback) {
		gapi.auth.authorize({
			'client_id': '137768234608-vbotprs93h6b1n1t2s806gj80a7qu8p5.apps.googleusercontent.com',
			'approval_prompt':'force',
			'access_type':'offline',
			'response_type':'code',
			'scope':'https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.google.com/m8/feeds/'
		},function(response){
			loadEnd();
			if (response['error']) {
				switch (response['error']) {
					case 'access_denied':
						// do nothing
						break;

					default:
						// @todo: átállítani wxes dialogra
						alert('An unknown error occured during connecting to Google. Please try again later.');
						break;
				}
			} else {
				callback(response);
			}
		});
	}
}