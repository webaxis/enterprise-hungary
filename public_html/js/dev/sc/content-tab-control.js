$(function() {
	'use strict';

	$('#startup-batches a').click(function (e) {
		e.preventDefault();

		var tab = $(this).attr('href').replace('#','');

		$('#startup-batches').find('li').removeClass('active');

		$(this).parent().addClass('active');

		$('.tab-pane').hide();

		$('#' + tab).show();
	});
});
