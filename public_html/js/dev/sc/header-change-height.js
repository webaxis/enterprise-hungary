$(function() {

	'use strict';

	$(window).scroll(function() {

		var st = $(document).scrollTop();

		var page = 'index';

		if ($('.page-news').length > 0) {
			page = 'news';
		}

		if ($('.subpage').length > 0) {
			page = 'subpage';
		}

		switch (page) {
			case 'index':
				var from = $('.section-program').offset().top;
				break;

			case 'news':
				var from = 1;
				break;

			case 'subpage':
				var from = 400;
				break;
		}

		if ($(window).width() < 480) {
			from = 1;
		}

		if (st < from) {
			$('.header-animated .navbar-default').removeClass('header-change-height navbar-fixed-top animated slideInDown');
		} else {
			$('.header-animated .navbar-default').addClass('header-change-height navbar-fixed-top animated slideInDown');
		}
	});
});
