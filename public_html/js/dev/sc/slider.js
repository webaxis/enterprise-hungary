$(function() {

	'use strict';

	$('.bxslider').show().bxSlider({
		mode: 'fade',
		auto: true,
		//adaptiveHeight: true,
		infiniteLoop: true,
		hideControlOnEnd: false,
		nextText: '',
		prevText: '',
		pager: false
	});

	if ($('.programs-slider .card-iconic').length === 1) {
		$('.programs-slider').bxSlider({
			minSlide: 1,
			maxSlide: 1,
			slideMargin: 20,
			responsive: true,
			nextText: '',
			prevText: '',
			autoReload: true,
			pager: false
		});

	} else if ($('.programs-slider .card-iconic').length === 2) {
		$('.programs-slider').bxSlider({
			minSlide: 2,
			maxSlide: 2,
			infiniteLoop: false,
			slideMargin: 20,
			responsive: true,
			nextText: '',
			prevText: '',
			autoReload: true,
			pager: false,
			hideControlOnEnd: true,
			breaks: [
				{
					screen: 0,
					slides: 1
				},
				{
					screen: 479,
					slides: 1
				},
				{
					screen:767,
					slides: 2
				},
				{
					screen: 1024,
					slides: 2
				}
			]
		});

	} else if ($('.programs-slider .card-iconic').length === 3) {

		$('.programs-slider').bxSlider({
			minSlide: 3,
			maxSlide: 3,
			slideMargin: 20,
			responsive: true,
			infiniteLoop: false,
			nextText: '',
			prevText: '',
			autoReload: true,
			hideControlOnEnd: true,
			pager: false,
			breaks: [
				{
					screen: 0,
					slides: 1
				},
				{
					screen: 479,
					slides: 1
				},
				{
					screen:767,
					slides: 2
				},
				{
					screen: 1024,
					slides: 3
				}
			]
		});
	} else if ($('.programs-slider .card-iconic').length > 3) {

		$('.programs-slider').bxSlider({
			slideMargin: 20,
			moveSlides: 1,
			responsive: true,
			infiniteLoop: false,
			nextText: '',
			prevText: '',
			pager: true,
			autoReload: true,
			hideControlOnEnd: true,
			breaks: [
				{
					screen: 0,
					slides: 1
				},
				{
					screen: 479,
					slides: 1
				},
				{
					screen:767,
					slides: 2
				},
				{
					screen: 1024,
					slides: 3
				}
			]
		});
	}
});
