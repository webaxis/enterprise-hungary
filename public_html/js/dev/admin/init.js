var init = {
	common : function () {
		// left menu
		$('.tabs > ul > li > a').click(function(e){
			if ($(this).parent().find('ul').length === 0)
				return;

			e.preventDefault();

			var hasClass = $(this).parent().hasClass('active');

			$('.tabs > ul > li').removeClass('active');

			if (!hasClass) {
				$(this).parent().addClass('active');
			}
		});
	},

	loggedIn : function () {
	},

	unloggedIn : function () {
	},

	page : {
		//...
	}
};

var plugins = {

	getDateTimePickerSettings : function() {
		return {
			firstDay: 1,

			timeOnlyTitle: 'Csak idő',
			timeText: 'Idő',
			hourText: 'Óra',
			minuteText: 'Perc',
			secondText: 'Másodperc',
			currentText: 'Most',
			closeText: 'Kész',

			dateFormat: 'yy-mm-dd',
			timeFormat: 'hh:mm',

			monthNames: ['Január','Február','Március','Április','Május','Június','Július','Augusztus','Szeptember','Október','November','December'],
			monthNamesShort: ['Jan','Feb','Már','Ápr','Máj','Jún','Júl','Aug','Sze','Okt','Nov','Dec'],
			dayNames: ['Vasárnap', 'Héftő','Kedd','Szerda','Csütörtök','Péntek','Szombat'],
			dayNamesShort: ['Va','Hé','Ke','Sze','Csü','Pé','Szo'],
			dayNamesMin: ['V','H','K','Sze','Сs','P','Szo']
		};
	}
};
