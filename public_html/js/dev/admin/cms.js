if (typeof wx === 'undefined' || wx === undefined) {
	var wx = {};
}

wx.cms = {};
wx.cms.list = {
	init : function (cmsDescriptorName) {
		$('.wx-delete-entity').click(function(e){
			e.preventDefault();

			if (!confirm(t('JS_CMS_CONFIRM_DELETE_ROW')))
				return false;

			var that = this;

			wx.ajax('/cms/deleteentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		});

		$('.wx-activate-entity').click(function(e){
			e.preventDefault();

			var that = this;

			wx.ajax('/cms/activateentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		});

		$('.wx-inactivate-entity').click(function(e){
			e.preventDefault();

			var that = this;

			wx.ajax('/cms/inactivateentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		});

		$('.wx-archive-entity').click(function(e){
			e.preventDefault();

			var that = this;

			wx.ajax('/cms/archiveentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		});

		$('.wx-dearchive-entity').click(function(e){
			e.preventDefault();

			var that = this;

			wx.ajax('/cms/dearchiveentity',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName},{
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');
					wx.list.refresh(listId, {
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		});

		$('.wx-change-order').change(function(e){
			e.preventDefault();

			var that = this;

			wx.ajax('/cms/changeorder',{id:$(this).attr('data-id'),descriptor:cmsDescriptorName,order:$(this).val()},{
				beforeSend : function() {
					$('.wx-change-order').attr('disabled',true);
				},
				afterSuccess : function(response){
					var listId = $(that).closest('table').parent().attr('id');

					wx.list.refresh(listId, {
						beforeSend : function () {
							$('.wx-change-order').attr('disabled',false);
						},
						onSuccess : function() {
							wx.cms.list.init(cmsDescriptorName);
						}
					});

					wx.feedback.init('success', response.data.message, '', 1500);
				}
			});
		});
	}
};

wx.cms.form = {
	init : function (options) {
		/*$('.ckeditor-content').ckeditor({
			//customConfig : '/js/plugins/ckeditor/profiles/admin/ckeditor_config.js',
			height: 400
		});*/

		$('.ckeditor-content').each(function(){
			var profile = $(this).attr('class').replace('ckeditor-content','').trim().replace('textarea','').trim(),
				id = $(this).attr('id');

			if (profile === undefined) {
				profile = 'editor-content';
			}


			$('#' + id).ckeditor({
				height: 400,
				language : 'hu',
				filebrowserBrowseUrl : '/js/plugins/ckeditor/plugins/kcfinder/browse.php?type=files',
				filebrowserImageBrowseUrl : '/js/plugins/ckeditor/plugins/kcfinder/browse.php?type=images',
				filebrowserFlashBrowseUrl : '/js/plugins/ckeditor/plugins/kcfinder/browse.php?type=flash',
				filebrowserUploadUrl : '/js/plugins/ckeditor/plugins/kcfinder/upload.php?type=files',
				filebrowserImageUploadUrl : '/js/plugins/ckeditor/plugins/kcfinder/upload.php?type=images',
				filebrowserFlashUploadUrl : '/js/plugins/ckeditor/plugins/kcfinder/upload.php?type=flash',
				fontSize_sizes : '11/11px,12/12px,13/13px,14/14px,15/15px,16/16px,17/17px,18/18px,19/19px,20/20px,21/21px,22/22px,23/23px,24/24px',
				format_tags : 'p;h2;h3;h4;h5',
				toolbar : 'Toolbar',
				entities : false,
				basicEntities : false,
				entities_greek : false,
				entities_latin : false,
				entities_additional : '',
				htmlEncodeOutput : false,
				allowedContent : true,
				extraPlugins : 'font,table,youtube,letterspacing',
				contentsCss  : '/js/plugins/ckeditor/profiles/'+profile+'.css',
				forcePasteAsPlainText : true,

				font_names : 'Arial,Times New Roman,Verdana,' + 'Open Sans/Open Sans,',

				toolbar_Toolbar :
				[
					[ 'Format' ],
					[ 'Bold', 'Italic', 'Underline', 'Strike'],
					['BulletedList','NumberedList','Outdent','Indent'],
					['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
					['Image','Youtube',',Table','Link','Unlink'],
					['Undo','Redo'],
					[ 'Source' ]
				],
			});
		});

		$('#' + options.formId).dynamicForm({
			submitButton : options.saveButton,
			onSubmit : function() {
				wx.form.validate(options.formDescriptorName,options.formId,{
					url : options.saveUrl,
					params : {
						id : options.entityId
					},
					onResponse : function() {
						wx.cms.form.init(options);
					},
					onSuccess : function (response) {
						if (typeof options.onSaveSuccess === 'function') {
							options.onSaveSuccess();
						}
						wx.feedback.init('success', response.data.message, '', 1500);
					},
					onError : function() {
						if (typeof options.onError === 'function') {
							options.onError();
						}
					}
				});
			},
			imageUploaders : options.imageUploaders
		});
	}
};

wx.cms.wysiwyg = {
	init : function (options) {
		$('.page-tabs a').click(function(e){
			e.preventDefault();
			$('.page-tabs a').removeClass('active');

			$(this).addClass('active');

			var rel = $(this).attr('rel');

			$('div[id^="page-tab-"]').hide();
			$('#page-tab-'+rel).show();
			$('input[name="language"]').val(rel);
		});

		/*$('.ckeditor.ck-full').ckeditor({
			customConfig : '/js/admin/ckeditor_config.js',
			height: wx.cms.wysiwyg.getWysiwygHeight()
		});*/

		var profile = 'editor-content';

		$('.ckeditor.ck-full').ckeditor({
			height: wx.cms.wysiwyg.getWysiwygHeight(),
			language : 'hu',
			filebrowserBrowseUrl : '/js/plugins/ckeditor/plugins/kcfinder/browse.php?type=files',
			filebrowserImageBrowseUrl : '/js/plugins/ckeditor/plugins/kcfinder/browse.php?type=images',
			filebrowserFlashBrowseUrl : '/js/plugins/ckeditor/plugins/kcfinder/browse.php?type=flash',
			filebrowserUploadUrl : '/js/plugins/ckeditor/plugins/kcfinder/upload.php?type=files',
			filebrowserImageUploadUrl : '/js/plugins/ckeditor/plugins/kcfinder/upload.php?type=images',
			filebrowserFlashUploadUrl : '/js/plugins/ckeditor/plugins/kcfinder/upload.php?type=flash',
			fontSize_sizes : '11/11px,12/12px,13/13px,14/14px,15/15px,16/16px,17/17px,18/18px,19/19px,20/20px,21/21px,22/22px,23/23px,24/24px',
			format_tags : 'p;h2;h3;h4;h5',
			toolbar : 'Toolbar',
			entities : false,
			basicEntities : false,
			entities_greek : false,
			entities_latin : false,
			entities_additional : '',
			htmlEncodeOutput : false,
			allowedContent : true,
			extraPlugins : 'font,table,youtube,letterspacing',
			contentsCss  : '/js/plugins/ckeditor/profiles/'+profile+'.css',
			forcePasteAsPlainText : true,

			font_names : 'Arial,Times New Roman,Verdana,' + 'Open Sans/Open Sans,',

			toolbar_Toolbar :
			[
				[ 'Format' ],
				[ 'Bold', 'Italic', 'Underline', 'Strike'],
				['BulletedList','NumberedList','Outdent','Indent'],
				['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
				['Image','Youtube',',Table','Link','Unlink'],
				['Undo','Redo'],
				[ 'Source' ]
			],
		});


		$('#' + options.submitButton).click(function(e){
			e.preventDefault();

			var vars = $('#' + options.formId).serialize();

			wx.ajax(options.saveUrl, vars, function(response) {
				wx.feedback.init('success', response.data.message, '', 1500);
			});
		});
	},
	getWysiwygHeight : function () {
		var documentH 	= $(document).height(),
			leftH		= $('.left-side').outerHeight(true),
			minus		= $('h1').outerHeight(true) + $('.page-tabs').outerHeight(true) + 72,
			height		= documentH - minus;

		if (height < leftH - minus)
			height = leftH - minus;

		return height;
	},
	setActiveTab : function () {
		var tab = $('.page-tabs a.active').attr('rel');
		$('#page-tab-'+tab).show();
	}
};
