if (typeof wx === 'undefined' || wx === undefined) {
	var wx = {};
}

wx.list = {
	init : function (id, options) {
		var defaults = {
			// variables
			params : {},

			// runs on every paging/refreshing
			onSuccess : function() {  },

			// runs only on init
			onLoad : function () {  }
		};

		var settings = $.extend(true, defaults, options);
//D($('#'+id+' .wx-list-paginator a'));
		$('#'+id+' .wx-list-paginator a').unbind('click').click(function(e){
			e.preventDefault();
			var id = $(this).attr('rel');

			if ($(this).hasClass('wx-list-link-more-results')) {
				D('1');
				$('#'+id+' .wx-list-paginator .wx-list-more-loader').show();
				$('#'+id+' .wx-list-paginator .wx-list-more-results').hide();

				wx.list.loadMore(id, settings);
			} else {
				D('2');
				settings.params.page = wx.list.getParam('page',$(this).attr('href'));
				D(settings);
				wx.list.refresh(id, settings);
			}
		});

		settings.onLoad();
	},
	refresh : function (id, settings) {
		var container = $('#'+id),
			url  = '/index/wxlist/',
			params = {
				descriptor : $('#wx-list-'+id+'-descriptor').val(),
				page : parseInt($('#wx-list-'+id+'-page').val()),
				refresh : 1
			};

		if (settings.hasOwnProperty('params')) {
			$.each(settings.params, function(k,v){
				params[k] = v;
			});
		}

		if (settings.hasOwnProperty('beforeSend')) {
			settings.beforeSend();
		}

		$.post(url, params, function(response){
			container.replaceWith(response);

			if (settings.hasOwnProperty('onSuccess')) {
				settings.onSuccess();
			}

			wx.list.init(id, settings);

		});
	},
	loadMore : function (id, settings) {
		/*
			settings.prepend = false;
		*/

		$('#'+id+' .wx-list-link-more-results').remove();

		var container = $('#'+$('#wx-list-'+id+'-more-container').val()),
			url  = '/index/wxlist/',
			params = {
				descriptor : $('#wx-list-'+id+'-descriptor').val(),
				page : parseInt($('#wx-list-'+id+'-page').val()) + 1
			};

		$.each(settings.params, function(k,v){
			if (k !== 'page') {
				params[k] = v;
			}
		});

		$.post(url, params, function(response){
			if (response.substr(0,1) === '{') {
				response = $.parseJSON(response);
				if (response.unexpectedError !== undefined) {
					switch (parseInt(response.errorCode)) {
						default:
							wx.dialog.error(response.error);
					}
				}
			} else {
				$('#'+id+' .wx-list-paginator').remove();
				$('#wx-list-'+id+'-inputs').remove();

				if (settings.prepend !== undefined && settings.prepend === true) {
					container.prepend(response);
				} else {
					container.append(response);
				}

				settings.onSuccess();

				wx.list.init(id, settings);
			}
		});
	},
	getParam : function (param, url) {
		var urlParts = url.split('/'),
			urlPartsLength = urlParts.length;

		for (var i = 0; i < urlPartsLength; i++) {
			if (urlParts[i] === param) {
				var nextI = i + 1;
				return urlParts[nextI];
			}
		}
		return false;
	}
};
