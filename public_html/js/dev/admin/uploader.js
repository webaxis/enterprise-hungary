var imageUploaderId = 1;

;(function($){
	$.fn.imageUploader = function(options, id){

		if (this.length == 0 || this.length > 1) {
			return this;
		}

		var defaults = {
			/* REQUIRED PARAMS */
			uploadUrl : '', // example: /ajax/image/upload,
			type : '', // example: gallery

			/* OPTIONAL */
			imageSelector : '', // example: #image-id or #image-container img
			size : '', // example: 'normal'
			params : null,

			/* GENERAL */
			prefix : 'wx2-image-uploader-',
			//loaderImage : '/images/WX2/loader.gif',
			uploadText : t('JS_UPLOADER_BUTTON_UPLOAD'),

			/* VALIDATORS */
			validators : {
				extension : { 
					allowed : ['gif', 'png', 'jpg', 'jpeg'], // empty array : no constraint
					errorMessage : t('JS_UPLOADER_ERROR_EXTENSION')
				},
				size : {
					allowed : 5242880, // 0 : no limit
					errorMessage : t('JS_UPLOADER_ERROR_SIZE')
				}
			},

			/* CALLBACKS */
			loader : function() { 
				loadStart(); 
			},
			onBeforeUpload : function() {},
			onBeforeUploadDForm : function() {},
			afterUploadSuccess : function() {},
			afterUploadSuccessDForm : function(response) {}, // special gateway for dform! do not override it!
			afterUploadError : function() {}
		};

		var settings = $.extend( true, defaults, options );
		var container = this;

		var uploader = {
			id : imageUploaderId,
			container : '.' + settings.prefix + imageUploaderId,
			uploaderInput : '.' + settings.prefix + imageUploaderId + ' input[type="file"]'
		};

		var init = function() {
			var isValid = validateParams();
			if (isValid === true) {
				// add unique class to container
				container.addClass(settings.prefix + uploader.id);
				imageUploaderId++;

				appendFormToContainer();
				bindUploadButton();
			} else {
				var error = 'WX2Uploader error: ' + isValid;
				alert(error);
			}
		}

		var validateParams = function() {
			var messages = '';
			if (settings.uploadUrl == '') {
				messages+= 'Missing parameter: uploadUrl; ';
			}
			if (settings.type == '') {
				messages+= 'Missing parameter: type; ';
			}
			if (messages == '' ) {
				return true;
			}

			return messages;
		}

		var appendFormToContainer = function() {
			var form = getForm();
			container.append(form);
		}

		var getForm = function() {
			//var html = '<div id="' + uploader.selectors.container + '">';
			var html = '<form method="post" enctype="multipart/form-data" action="javascript:void(0);">';
				html+= '<div class="button-container">';
				html+= '<span>' + settings.uploadText + '</span>';
				//html+= '<input type="file" name="upload" multiple="true">';
				html+= '<input type="file" name="upload">';
				html+= '<input name="hash" type="hidden" value="" />';
				html+= '<input name="type" type="hidden" value="' + settings.type +'" />';
				html+= '<input name="size" type="hidden" value="' + settings.size + '" />';
				html+= '</div>';
				html+= '</form>';
				//html+= '</div>';

			return html;
		}

		var bindUploadButton = function() {
			$(uploader.uploaderInput).unbind('change').bind('change', function() {
				var isValid = validate($(this));
				if (isValid === true) {
					var formData = new FormData;
					formData.append('upload-image', this.files[0]);
					upload(formData);
				} else {
					wx.dialog.error(isValid);
				}
			});
		}

		var upload = function (formData) {
			settings.onBeforeUpload();
			settings.onBeforeUploadDForm();
			wx.ajax(settings.uploadUrl, formData, {
				isImageAjax : true,
				params : settings.params,
				afterSuccess : function(response) {
					if (response.status == 'success') {
						changeImage(response.data.url, response.data.id);
						// store current image hash
						$(uploader.uploaderInput).val('');
						$(uploader.container + 'form input[name="hash"]').val(response.id);
						
						settings.afterUploadSuccess();
						settings.afterUploadSuccessDForm(response);
					} else {
						settings.afterUploadError();
					}
				}
			});
		}

		var changeImage = function(url, hash) {
			if (settings.imageSelector != '') {
				$(settings.imageSelector).attr('src', url);
			}

			$(uploader.container + ' input[name="hash"]').val(hash);			
		};

		var validate = function(fileObj) {
			var file = fileObj.prop("files")[0];
			var errors = [];
			var valid = true;

			/* validate size - if it has constraint */
			if (settings.validators.hasOwnProperty('size') && settings.validators.size.allowed != 0) {
				size = file.size;
				var maxSize = settings.validators.size.allowed;
				if (size > maxSize) {
					valid = false;
					errors.push(settings.validators.size.errorMessage);
				}
			}

			/* validate extensions - if it has constraint */
			if (settings.validators.hasOwnProperty('extension') && settings.validators.extension.allowed != []) {
				var ext = fileObj.val().split('.').pop().toLowerCase();
				if ($.inArray(ext, settings.validators.extension.allowed) == -1) {
					errors.push(settings.validators.extension.errorMessage);
				}
			}

			if (errors.length === 0) {
				return true;
			}
			return errors;
		};

		// HERE START THE PROCESS!
		init();

		return this;
	}

})(jQuery);