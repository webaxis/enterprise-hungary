;(function($){
	$.fn.multiImageUploader = function(options, id){
		if (this.length == 0 || this.length > 1) {
			return this;
		}

		var defaults = {
			/* REQUIRED PARAMS */
			id : '', // (hidden) input id; example: #images
			//button : '', // uploader button selector; example: '#files'
			container : '', // selector, where images displayed; '.image-container' or '#image-container'
			uploadUrl : '',
			deleteUrl : '',
			progressContainer : '',
			temp : false,

			type : '', // type of image (wx specified requirement; property in descriptor); example: 'gallery'
			size : '', // size of image (wx specified requirement; property in descriptor); example: 'thumbnail'

			/* OPTIONAL */
			usePrettyPhoto : false,
			useImageTitle : false,
			imageSelector : '', // example: #image-id or #image-container img
			params : null,
			warning : false, // create warning when you navigate to other page without save
			warningText : 'Are you sure leave this page?',

			/* GENERAL */
			//prefix : 'wx-image-multiuploader-',
			imageWrapper : 'one-image',

			/* VALIDATORS */
			validators : {
				extension : { 
					allowed : ['gif', 'png', 'jpg', 'jpeg'], // empty array : no constraint
					errorMessage : t('JS_UPLOADER_ERROR_EXTENSION')
				},
				size : {
					allowed : 26214400, // 0 : no limit
					errorMessage : t('JS_UPLOADER_ERROR_SIZE')
				}
			},

			/* CALLBACKS */
			loader : function() { 
				loadStart(); 
			},

			/*
			onBeforeUpload : function() {},
			onBeforeUploadDForm : function() {}, // special gateway for dform! do not override it!
			
			afterUploadSuccessDForm : function(response) {}, // special gateway for dform! do not override it!
			afterUploadError : function() {}
			*/
		};

		var settings = $.extend( true, defaults, options );

		var button = this;

		var inputValue = {}; // here stored the json encoded image object

		var init = function() {
			bindButton();
			buildImages();
			initProgressBar();
			bindDeleteButtons();
			bindTitles();
			initSortable();
		}

		var initProgressBar = function() {
			var container = $(settings.progressContainer),
				frame = $('<div class="progress push-left"><div class="progress-bar"></div></div>'),
				percent = $('<div class="progress-percent push-left"></div>');

			container.append(frame);
			container.append(percent);

			hideProgressBar();
			resetProgressBar();
		}

		var hideProgressBar = function() {
			$(settings.progressContainer).hide();
		}
		
		var fadeOutProgressBar = function() {
			$(settings.progressContainer).fadeOut('slow', function() {
				resetProgressBar();
			});
		}

		var showProgressBar = function() {
			if (!$(settings.progressContainer).is('visible')) {
				$(settings.progressContainer).show();
			}
		}

		var resetProgressBar = function () {
			$(settings.progressContainer + ' .progress-percent').html('0 %');
			$(settings.progressContainer + ' .progress-bar').css('width', '0%');
		}

		var bindButton = function() {
			$('#' + button.attr('id')).unbind('change').bind('change', function() {
				showProgressBar();
				// collect images
				var form = new FormData;
				for (i = 0; i < this.files.length; i++) {
					form.append('img' + i, this.files[i]);
				}

				form.append('type', settings.type);
				form.append('size', settings.size);

				// send with ajax
				$.ajax({
					xhr: function() {
						var xhr = new window.XMLHttpRequest();
						xhr.addEventListener('progress', function(e) {
							var done = e.position || e.loaded, total = e.totalSize || e.total;
						}, false);
						if ( xhr.upload ) {
							xhr.upload.onprogress = function(e) {
								var done = e.position || e.loaded, total = e.totalSize || e.total;
								var percent = Math.floor(done/total*1000)/10;
								$('.progress-percent').html(percent + "%");
								$('.progress-bar').css('width', percent + "%");
							};
						}

						return xhr;
					},
					type: 'POST',
					url: settings.uploadUrl,
					data: form,
					processData: false,
					contentType: false,
					beforeSend : function (){
						//loadStart();
					},
					success: function(data) {
						fadeOutProgressBar();
						handleUploadResponse(data);
					}
				});
			});
		}

		var bindDeleteButtons = function() {
			$('a.delete').unbind('click').bind('click', function() {
				var that = $(this),
					id = that.attr('data-id');

				// collect variables for delete
				var vars = 'type=' + settings.type + '&hash=' + id + '&temp=' + (settings.temp == true ? '1' : '0');

				// call delete
				if (settings.temp) {
					wx.ajax(settings.deleteUrl, vars, {
						afterSuccess : function(response) {
							removeImage(id);
							removeFromInput(id);
							bindBeforeunload();
						}
					});
				} else {
					removeImage(id);
					removeFromInput(id);
					bindBeforeunload();
				}
				return false;

			});
		}

		var removeImage = function(id) {
			$('.' + settings.imageWrapper + '[data-id="' + id + '"]').remove();
		}

		var removeFromInput = function(id) {
			delete inputValue[id];
			refreshInputValue();
		}

		var handleUploadResponse = function(response) {
			var response = JSON.parse(response);
			for (i = 0; i < response.data.length; i++) {
				appendImage(response.data[i]['id'], response.data[i]['url']);
			}

			bindDeleteButtons();
			bindTitles();
			initSortable();
			bindBeforeunload();
		}

		var initSortable = function() {
			$(settings.container).sortable({
				placeholder: "ui-state-highlight",
				start: function( event, ui ) {
					if (settings.usePrettyPhoto) {
						$("a[rel^='prettyPhoto']").unbind('click');
					}
				},
				update: function( event, ui ) {
					reorderImages();
					setTimeout(function(){
						prettyPhotoLoad();
					},300);
				},
			});

		}

		var reorderImages = function() {
			var oldInputValue = inputValue;
			inputValue = {};

			$.each($(settings.container + ' .' + settings.imageWrapper), function(idx, item) {
				var id = $(item).attr('data-id');
				inputValue[id] = oldInputValue[id];
			});
			refreshInputValue();
			bindBeforeunload();
		}

		var bindTitles = function() {
			$('.' + settings.imageWrapper + ' .text').unbind('keyup').bind('keyup', function(){
				var that = $(this),
					id = that.attr('data-id'),
					title = that.val();

				inputValue[id]['title'] = title;
				refreshInputValue();
			});
			
		}

		var refreshTitlesInInputValue = function() {
			$.each($('.' + settings.imageWrapper + ' .text'), function(idx, item) {
				var that = $(item),
					id = that.attr('data-id'),
					title = that.val();

				inputValue[id]['title'] = title;
				refreshInputValue();
			});
		}
		
		var refreshInputValue = function() {
			$(settings.id).val(JSON.stringify(inputValue));
		}

		var prettyPhotoLoad = function() {
			if (settings.usePrettyPhoto) {
				$("a[rel^='prettyPhoto']").prettyPhoto({
					social_tools: ''
				});
			}
		}

		var appendImage = function(id, url, text) {
			var div = $('<div class="' + settings.imageWrapper + '" data-id="' + id + '"></div>'),
				background = 'background: url(\'' + url + '\') repeat scroll center center / cover  rgba(0, 0, 0, 0)',
				frame = $('<a href="' + url + '" rel="prettyPhoto[uploader]"><div class="frame" style="' + background + '"></div></a>'),
				text = typeof text != 'undefined' ? text : '';
				if (settings.useImageTitle) {
					linkDelete = $('<div class="controls full-width push-left"><input type="text" class="text" data-id="' + id + '" value="' + text +'" placeholder="Kép aláírás"/><a class="delete btn" data-id="' + id + '" href=""><i class="fa fa-trash"></i></a></div>');
				} else {
					linkDelete = $('<div class="controls full-width push-left"><a class="delete btn" data-id="' + id + '" href="" style="float:right; margin-right: 5px;"><i class="fa fa-trash"></i></a></div>');
				}

			div.append(frame);
			div.append(linkDelete);
			$(settings.container).append(div);

			inputValue[id] = {'title' : '', 'url' : url};
			refreshInputValue();

			prettyPhotoLoad();
		}

		// rebuild images by input's value
		var buildImages = function() {
			var images = $(settings.id).val();
			if (images != '') {
				$.each($.parseJSON(images), function(idx, item) {
					appendImage(idx, item.url, item.title);
				});
			}
			refreshTitlesInInputValue();
		}

		var bindBeforeunload = function() {
			if (settings.warning) {
				$(window).bind('beforeunload', function(e){
					e.preventDefault();
					return settings.warningText;
				});
			}
		}

		unbindBeforeunload = function() {
			if (settings.warning) {
				$(window).unbind('beforeunload');
			}
		}

		// HERE START THE PROCESS!
		init();

		return this;
	}

})(jQuery);