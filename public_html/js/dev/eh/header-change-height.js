$(function() {

	'use strict';

	$(window).scroll(function() {

		var st = $(document).scrollTop();

		var page = 'index';

		if ($('.page-news').length > 0) {
			page = 'news';
		}

		switch (page) {
			case 'index':
				var from = $('.section-offers').offset().top;
				break;

			case 'news':
				var from = 1;
				break;
		}

		if ($(window).width() < 480) {
			from = 1;
		}

		if (st < from) {
			$('.navbar-default').removeClass('header-change-height navbar-fixed-top');
		} else {
			$('.navbar-default').addClass('header-change-height navbar-fixed-top');
		}
	});
});
