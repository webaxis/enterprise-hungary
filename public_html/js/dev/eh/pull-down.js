$(function() {

'use strict';

	$('.pull-down').click(function(e) {
		e.preventDefault();

		var to = $(this).attr('rel');

		$('html, body').animate({
			scrollTop: $('#'+to).offset().top - 1
		}, 500);
	});
});
