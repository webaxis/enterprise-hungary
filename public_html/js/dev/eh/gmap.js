function initMap() {

	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 14,
		scrollwheel: false,
		draggable: false,
		styles: [{'featureType':'administrative','elementType':'labels.text.fill','stylers':[{'color':'#444444'}]},{'featureType':'landscape','elementType':'all','stylers':[{'color':'#f2f2f2'}]},{'featureType':'poi','elementType':'all','stylers':[{'visibility':'off'}]},{'featureType':'road','elementType':'all','stylers':[{'saturation':-100},{'lightness':45}]},{'featureType':'road.highway','elementType':'all','stylers':[{'visibility':'simplified'}]},{'featureType':'road.arterial','elementType':'labels.icon','stylers':[{'visibility':'off'}]},{'featureType':'transit','elementType':'all','stylers':[{'visibility':'off'}]},{'featureType':'water','elementType':'all','stylers':[{'color':'#1b75bc'},{'visibility':'on'}]}]
	});

	geocoder = new google.maps.Geocoder();

	geocoder.geocode({
		'address':  t('JS_LANDING_MAP_ADDRESS_TO_SHOW')
	}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
				map.setCenter(results[0].geometry.location);

				var windowWidth = $(window).width(),
					infoboxLeft = $('.map-infobox').offset().left,
					center = windowWidth / 2,
					place = infoboxLeft / 2,
					correctX = (center - place) * -1,
					correctY = 172;

				if (windowWidth > 767) {
					correctY = 0;
				} else {
					correctX = 0;
				}

				offsetCenter(results[0].geometry.location, correctX, correctY)

				image = '/images/dist/marker.png';

				var marker = new google.maps.Marker({
					icon: {
						url: image
					},
					map: map,
					position: results[0].geometry.location
				});
			}
		}
	});
}


function offsetCenter(latlng,offsetx,offsety) {

	// latlng is the apparent centre-point
	// offsetx is the distance you want that point to move to the right, in pixels
	// offsety is the distance you want that point to move upwards, in pixels
	// offset can be negative
	// offsetx and offsety are both optional

	var scale = Math.pow(2, map.getZoom());
	var nw = new google.maps.LatLng(
	    map.getBounds().getNorthEast().lat(),
	    map.getBounds().getSouthWest().lng()
	);

	var worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latlng);
	var pixelOffset = new google.maps.Point((offsetx/scale) || 0,(offsety/scale) ||0)

	var worldCoordinateNewCenter = new google.maps.Point(
	    worldCoordinateCenter.x - pixelOffset.x,
	    worldCoordinateCenter.y + pixelOffset.y
	);

	var newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

	map.setCenter(newCenter);

}