/* ------------------- Declare Libraries ------------------- */

var	gulp 			= require('gulp'),
	postcss 		= require('gulp-postcss'),
	atImport 		= require("postcss-import"),
	simplevars 		= require('postcss-simple-vars'),
	nestedcss 		= require('postcss-nested'),
	colorFunction 	= require("postcss-color-function"),
	zindex			= require('postcss-zindex'),
	responsivetype	= require('postcss-responsive-type'),
	jshint 			= require('gulp-jshint'),
	concat 			= require('gulp-concat'),
	uglify 			= require('gulp-uglify'),
	imagemin 		= require('gulp-imagemin'),
	minifyCss 		= require('gulp-minify-css'),
	less 			= require('gulp-less'),
	pngquant 		= require('imagemin-pngquant'),
	cssnext 		= require('cssnext');

var src = {
	js: 		'js/dev/**/*.js',
	commonjs: 	'js/dev/common/*.js',
	ehjs: 		'js/dev/eh/*.js',
	scjs: 		'js/dev/sc/*.js',
	adminjs: 	[
		'js/dev/admin/core.js',
		'js/dev/admin/counter.js',
		'js/dev/admin/dform.js',
		'js/dev/admin/init.js',
		'js/dev/admin/list.js',
		'js/dev/admin/multiuploader.js',
		'js/dev/admin/uploader.js',
		'js/dev/admin/validation.js',
		'js/dev/admin/cms.js'
	],

	ehcss: 			'css/src/eh-style.css',
	sccss: 			'css/src/sc-style.css',
	admincss: 		'css/src/admin-style.css',
	ehbootstrapcss: 'css/src/themes/enterprise/bootstrap/bootstrap.less',
	scbootstrapcss: 'css/src/themes/sc/bootstrap/bootstrap.less',
	rawcss: 		'css/src/**/*.*',
	image: 			'images/src/**/**/*'
};

var dist = {
	js: 	'js/dist/',
	css: 	'css/dist/',
	image: 	'images/dist'
};

/* ------------------- Bootstrap Stylesheet tasks ------------------- */


gulp.task('css-bootstrap-eh', function() {
	return gulp.src(src.ehbootstrapcss)
	.pipe(less())
	.pipe(minifyCss())
	.pipe(gulp.dest(dist.css));
});

gulp.task('css-bootstrap-sc', function() {
	return gulp.src(src.scbootstrapcss)
	.pipe(less())
	.pipe(minifyCss())
	.pipe(gulp.dest(dist.css));
});

/* ------------------- EH Stylesheet tasks ------------------- */

gulp.task('css-eh', function() {

	var processors = [
		atImport,
		simplevars,
		nestedcss,
		responsivetype,
		cssnext({
	 		'browers': ['last 2 version', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
			'compress': false
		}),
		zindex
	];

	gulp.src(src.ehcss)
		.pipe(postcss(processors))
		.pipe(minifyCss())
		.pipe(gulp.dest(dist.css))
});

/* ------------------- SC Stylesheet tasks ------------------- */

gulp.task('css-sc', function() {

	var processors = [
		atImport,
		simplevars,
		nestedcss,
		responsivetype,
		cssnext({
	 		'browers': ['last 2 version', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
			'compress': false
		}),
		zindex
	];

	gulp.src(src.sccss)
		.pipe(postcss(processors))
		.pipe(minifyCss())
		.pipe(gulp.dest(dist.css))
});

/* ------------------- Admin Stylesheet tasks ------------------- */

gulp.task('css-admin', function() {

	var processors = [
		atImport,
		simplevars,
		nestedcss,
		responsivetype,
		cssnext({
	 		'browers': ['last 2 version', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'],
			'compress': false
		}),
		zindex
	];

	gulp.src(src.admincss)
		.pipe(postcss(processors))
		.pipe(minifyCss())
		.pipe(gulp.dest(dist.css))
});

/* ------------------- JavaScript hint ------------------- */

gulp.task('js-hint', function() {
	return gulp.src(src.js)
	.pipe(jshint('.jshintrc'))
	.pipe(jshint.reporter('jshint-stylish'));
});

/* ------------------- EH JavaScript tasks ------------------- */

gulp.task('js-compress-eh', ['js-hint'], function() {
	return gulp.src(src.ehjs)
	.pipe(concat('eh.min.js'))
	.pipe(uglify({
		mangle: false,
		compress: {
			drop_console: true
		}
	}))
	.pipe(gulp.dest(dist.js))
});

/* ------------------- SC JavaScript tasks ------------------- */

gulp.task('js-compress-sc', ['js-hint'], function() {
	return gulp.src(src.scjs)
	.pipe(concat('sc.min.js'))
	.pipe(uglify({
		mangle: false,
		compress: {
			drop_console: true
		}
	}))
	.pipe(gulp.dest(dist.js))
});

/* ------------------- Admin JavaScript tasks ------------------- */

gulp.task('js-compress-admin', ['js-hint'], function() {
	return gulp.src(src.adminjs)
	.pipe(concat('admin.min.js'))
	.pipe(uglify({
		mangle: false,
		compress: {
			sequences: true,
			dead_code: true,
			conditionals: true,
			booleans: true,
			unused: true,
			if_return: true,
			join_vars: true,
			drop_console: true
		}
	}))
	.pipe(gulp.dest(dist.js))
});

/* ------------------- Image task------------------- */

gulp.task('opt-img', function () {
    return gulp.src(src.image)
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()]
        }))

        .pipe(gulp.dest(dist.image));
});

/* ------------------- Watch task------------------- */
gulp.task('watch:css-eh', function() {
	gulp.watch(src.rawcss, ['css-eh']);
});

gulp.task('watch:css-sc', function() {
	gulp.watch(src.rawcss, ['css-sc']);
});



gulp.task('watch:js-eh', function() {
	gulp.watch(src.js,['js-compress-eh']);
});

gulp.task('watch:js-sc', function() {
	gulp.watch(src.js,['js-compress-sc']);
});


/* ------------------- Deploy task ------------------- */

gulp.task('web', ['js-compress' , 'css' , 'opt-img'], function() {});
