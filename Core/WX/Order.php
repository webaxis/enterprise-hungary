<?php

class WX_Order {
	
	private $_id			= null;
	
	// feature is basically the identifier
	private $_feature		= false;
	
	private $_totalCount	= 0;
	
	public $_searchParams	= array();
	
	protected $_descriptor	= array();
	
	public function __construct ($feature = false) {
		$this->_feature = $feature;
	}
	
	public function getInstance ($feature = false) {
		return new Sxsw_Order($feature);
	}

	public function getDescriptor () {
		$descriptor = $this->_descriptor;

		if (isset($descriptor[$this->_feature]))
			return $descriptor[$this->_feature];
			
		return false;
	}
	
	public function getVar ($var) {
		$descriptor = $this->getDescriptor();
		
		if (isset($descriptor[$var]))
			return $descriptor[$var];
	
		return false;
	}
	
	public function getFeature () {
		return $this->_feature;
	}
	
	public function featureExists () {
		return (bool) $this->getDescriptor();
	}
	
	public function setSearchParams ($params) {
		$this->_searchParams 	= $params;
		
		// have to turn down total count, as it may has changed do to extra params
		$this->_totalCount		= 0;
		
		return $this;
	}

	public function getSearchParamList() {
		return $this->getVar('searchParams');
	}

	public function getSearchParams() {
		return $this->_searchParams;
	}
	
	private function _getSearchParams() {
		// if has been set
		if (!empty($this->_searchParams))
			return $this->_searchParams;
			
		// if not required
		if (!$this->getVar('searchParams'))
			return $this->_searchParams;
			
		// if not possible
		if (is_null($this->_id))
			return $this->_searchParams;
			
		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from($this->getVar('table'), $this->getVar('searchParams'))
					->where('id = "'.$this->_id.'"');
					
		$result = $db->query($query)->fetch();
		
		$gatheredSearchParams = array();
		foreach ($this->getVar('searchParams') as $one) {
			$gatheredSearchParams[$one] = $result[$one];
		}
		$this->setSearchParams($gatheredSearchParams);
		
		return $this->_searchParams;
	}
	
	public function createSelect ($id, $order, $direction = 'ASC') {
		$this->_id = $id;
		
		$total = $this->_getTotalCount();
		
		$select = '<select class="wx-change-order" data-id="'.$id.'" data-feature="'.$this->getFeature().'">';
		
		if ($direction == 'ASC') {
			for ($i = 1; $i <= $total; $i++) {
				$select .= "<option value='$i'".($i == $order ? ' selected="selected"' : '').">$i</option>";
			}
		} else {
			for ($i = $total; $i >= 1; $i--) {
				$select .= "<option value='$i'".($i == $order ? ' selected="selected"' : '').">$i</option>";
			}
		}
		
		
		$select .= '</select>';
		
		return $select;
	}
	
	public function changeOrder ($id, $newOrder) {
		$this->_id = $id;
		
		$oldOrder	= $this->_getElementOrder($id);
				
		if ($oldOrder > $newOrder) {
			$idsToChange = $this->_getIdsBetweenOrder($newOrder, $oldOrder - 1);
			
			$db = Zend_Registry::get('db');
			$db->update($this->getVar('table'),array($this->getVar('orderColumn') => $newOrder), 'id = "'.$id.'"');
			
			$otherNewOrder = $newOrder;
			foreach ($idsToChange as $one) {
				$otherNewOrder++;
				$db->update($this->getVar('table'),array($this->getVar('orderColumn') => $otherNewOrder), 'id = "'.$one.'"');
			}
		} else {
			$idsToChange = $this->_getIdsBetweenOrder($oldOrder + 1, $newOrder);
			
			$db = Zend_Registry::get('db');
			$db->update($this->getVar('table'),array($this->getVar('orderColumn') => $newOrder), 'id = "'.$id.'"');
			
			$otherNewOrder = $oldOrder - 1;
			foreach ($idsToChange as $one) {
				$otherNewOrder++;
				$db->update($this->getVar('table'),array($this->getVar('orderColumn') => $otherNewOrder), 'id = "'.$one.'"');
			}
		}
	}
	
	public function removeOrder ($id) {
		$this->_id = $id;
		
		$order 			= $this->_getElementOrder($id);
		$idsToChange	= $this->_getIdsAfterOrder($order);

		$db = Zend_Registry::get('db');
		
		$otherNewOrder = $order - 1;
		foreach ($idsToChange as $one) {
			$otherNewOrder++;
			$db->update($this->getVar('table'),array($this->getVar('orderColumn') => $otherNewOrder), 'id = "'.$one.'"');
		}
		
		$db->update($this->getVar('table'),array($this->getVar('orderColumn') => 0), 'id = "'.$id.'"');
	}
	
	private function _getTotalCount () {
		if (!$this->_totalCount)
			$this->_setTotalCount();
		
		return $this->_totalCount;
	}
	
	private function _setTotalCount () {
		$this->_totalCount = $this->_getElementCount();
	}
	
	public function getNextOrder () {
		$db = Zend_Registry::get('db');
		$sql = $db->select()
                  ->from($this->getVar('table'),'*')
				  ->order($this->getVar('orderColumn') . ' DESC')
				  ->limit(1);
				  
		$sql = $this->_addSearchParams($sql);
				  
		$stmt = $db->query($sql);
		$result = $stmt->fetch();
		
		return $result['order'] + 1;
	}
	
	private function _getIdsAfterOrder ($order) {
		$db = Zend_Registry::get('db');
		$sql = $db->select()
                  ->from($this->getVar('table'),'*')
				  ->order($this->getVar('orderColumn') . ' ASC')
				  ->where("`".$this->getVar('orderColumn')."` > '$order'");
				  
		$sql = $this->_addSearchParams($sql);
		
		$stmt = $db->query($sql);
		
		$return = array();
		
		foreach ($stmt->fetchAll() as $one) {
			$return[] = $one['id'];
		}
		
		return $return;
	}
	
	private function _getIdsBetweenOrder ($from, $to) {
		$db = Zend_Registry::get('db');
		$sql = $db->select()
                  ->from($this->getVar('table'),'*')
				  ->order($this->getVar('orderColumn') . ' ASC')
				  ->limit($to - $from + 1, $from - 1);
				  
		$sql = $this->_addSearchParams($sql);
				  
		$stmt = $db->query($sql);
		
		$return = array();
		
		foreach ($stmt->fetchAll() as $one) {
			$return[] = $one['id'];
		}
		
		return $return;
	}
	
	private function _getElementOrder ($id) {
		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from($this->getVar('table'))
					->where("id = '$id'");
		$result = $db->query($query)->fetch();
		
		return $result[$this->getVar('orderColumn')];
	}
	
	private function _getElementCount () {
		$db = Zend_Registry::get('db');
		$sql = $db->select()
                  ->from($this->getVar('table'),'*');
				  
		$sql = $this->_addSearchParams($sql);
		
		$stmt = $db->query($sql);
		return count($stmt->fetchAll());
	}
	
	private function _addSearchParams ($sql) {
		foreach ($this->_getSearchParams() as $key => $value) {
			$sql->where("$key like '$value'");
		}
		return $sql;
	}
}