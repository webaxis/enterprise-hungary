<?php

class WX_Image {

	protected $_type 		= null;

	protected $_config 		= null;

	protected $_descriptor 	= array();

	protected $_errors 		= array();
	
	protected $_imageObj 	= null;

	static public function type($type) {
		return new WX_Image($type);
	}

	public function __construct ($type) {
		$this->_type = $type;
		$this->_initConfig();
		$this->_initDescriptor();
	}

	static public function getInstance ($type) {
		return new WX_Image($type);
	}

	protected function _initConfig() {
		if (class_exists(APP_NAME.'_Image_Config')) {
			$class = APP_NAME.'_Image_Config';
		} else {
			$class = 'WX_Image_Config';
		}
		$this->_config = $class::getInstance();
	}

	protected function _initDescriptor() {
		$class = APP_NAME.'_Image_Descriptor';
		$method = $this->_type;
		
		$this->_descriptor = $class::$method();
	}

	protected function _getProperty($property) {
		$value = '';
		switch ($property) {
			case 'path' :
			case 'url' :
				if (isset($this->_descriptor[$property])) {
					$root = $this->_descriptor[$property];
				} else if ($this->_config->get($property) != NULL) {
					$root = $this->_config->get($property);
				}
				if (isset($this->_descriptor['folder'])) {
					$folder = $this->_descriptor['folder'];
				} else if ($this->_config->get('folder') != NULL) {
					$folder = $this->_config->get('folder');
				}
				$value = $root . $folder . '/';
				break;
			case 'tempPath' :
			case 'tempUrl' :
				if (isset($this->_descriptor[$property])) {
					$value = $this->_descriptor[$property];
				} else if ($this->_config->get($property) != NULL) {
					$root = $this->_config->get($property);
					if (isset($this->_descriptor['folder'])) {
						$folder = $this->_descriptor['folder'];
					} else if ($this->_config->get('folder') != NULL) {
						$folder = $this->_config->get('folder');
					}
					$value = $root . $folder . '/';
				}
				break;
			default :
				if (isset($this->_descriptor[$property])) {
					$value = $this->_descriptor[$property];
				} else if ($this->_config->get($property) != NULL) {
					$value = $this->_config->get($property);
				}
				break;
		}
		return $value;
	}

	public function hasErrors () {
		return (bool)count($this->_errors);
	}

	public function getErrors () {
		return $this->_errors;
	}

	public function get($size, $hash = false, $disableDefault = false) {
		$settings = array(
			'path' => $this->_getProperty('path'),
			'url' => $this->_getProperty('url'),
			'sizes' => $this->_getProperty('sizes')
		);

		if ($disableDefault === false && isset($this->_descriptor['defaultImage'])) {
			$settings['defaultImage'] = $this->_descriptor['defaultImage'];
		}

		if ($this->_imageObj) {
			$image = $this->_imageObj->initGetter()->getImageWithId($size);
		} elseif ($hash === '' || $hash === NULL) { # no uploaded image
			$image = $settings['defaultImage']['url'];
		} elseif ($hash !== false) {
			$imageObj = new WX_Image_Image($settings, $hash);
			$image = $imageObj->initGetter()->getImageUrl($size);
		} else {
			$message = 'Please load the image object or add hash!';
			throw new Exception($message);
		}

		return $image;
	}
	public function getPath($size, $hash = false, $disableDefault = false) {
		$settings = array(
			'path' => $this->_getProperty('path'),
			'url' => $this->_getProperty('url'),
			'sizes' => $this->_getProperty('sizes')
		);

		if ($disableDefault === false && isset($this->_descriptor['defaultImage'])) {
			$settings['defaultImage'] = $this->_descriptor['defaultImage'];
		}

		if ($this->_imageObj) {
			$image = $this->_imageObj->initGetter()->getImageWithId($size);
		} elseif ($hash === '' || $hash === NULL) { # no uploaded image
			$image = $settings['defaultImage']['url'];
		} elseif ($hash !== false) {
			$imageObj = new WX_Image_Image($settings, $hash);
			$image = $imageObj->initGetter()->getImageUrl($size);
		} else {
			$message = 'Please load the image object or add hash!';
			throw new Exception($message);
		}

		return $image;
	}

	public function getTemp($size = false, $hash = false, $disableDefault = false) {
		$settings = array(
			'path' => $this->_getProperty('tempPath'),
			'url' => $this->_getProperty('tempUrl'),
			'sizes' => $this->_getProperty('sizes')
		);

		if ($disableDefault === false && isset($this->_descriptor['defaultImage'])) {
			$settings['defaultImage'] = $this->_descriptor['defaultImage'];
		}

		if ($this->_imageObj) {
			$image = $this->_imageObj->initGetter()->getImageWithId($size);
		} elseif ($hash !== false) {
			$imageObj = new WX_Image_Image($settings, $hash);
			$image = $imageObj->initGetter()->getImageUrl($size);
		} else {
			$message = 'Please load the image object or add hash!';
			throw new Exception($message);
		}

		return $image;
	}

	public function getDefaultImageUrl($hash) {
		$settings = array(
			'path' => $this->_getProperty('tempPath'),
			'url' => $this->_getProperty('tempUrl'),
			'sizes' => $this->_getProperty('sizes')
		);

		if (!isset($this->_descriptor['defaultImage'])) {
			$message = 'You cannot get default image bacause is not defined!';
			WX_Exception::create($message);
		}

		return $this->_descriptor['defaultImage']['url'];
	}

	public function getUploadedOrTemp($size = false, $hash = false) {
		$image = $this->getTemp($size, $hash, true);
		if ($image === '') {
			$image = $this->get($size, $hash, true);
			if ($image === '') {
				$image = $this->getDefaultImageUrl($size, $hash, true);
			}
		}

		return $image;
	}

	public function upload($file) {
		$settings = array(
			'path' => $this->_getProperty('path'),
			'url' => $this->_getProperty('url'),
			'sizes' => $this->_getProperty('sizes'),
			'uploader' => array(
				'errorMessage' => $this->_getProperty('uploadError'),
				'validation' => array(
					'size' => array(
						'maxFileSize' => $this->_getProperty('maxFileSize'),
						'errorMessage' => $this->_getProperty('uploadErrorSize')
					),
					'extensions' => array(
						'types' => $this->_getProperty('validExtensions'),
						'errorMessage' => $this->_getProperty('uploadErrorExtension')
					)
				)
			)
		);

		if (isset($this->_descriptor['defaultImage'])) {
			$settings['defaultImage'] = $this->_descriptor['defaultImage'];
		}

		$image = new WX_Image_Image($settings);
		$image->initUploader($file)->upload();

		$this->_imageObj = $image;

		if ($image->getUploader()->hasError()) {
			$this->_errors = $image->getErrors();
		}
	}

	public function uploadTemp($file) {
		$settings = array(
			'path' => $this->_getProperty('tempPath'),
			'url' => $this->_getProperty('tempUrl'),
			'sizes' => $this->_getProperty('sizes'),
			'uploader' => array(
				'errorMessage' => $this->_getProperty('uploadError'),
				'validation' => array(
					'size' => array(
						'maxFileSize' => $this->_getProperty('maxFileSize'),
						'errorMessage' => $this->_getProperty('uploadErrorSize')
					),
					'extensions' => array(
						'types' => $this->_getProperty('validExtensions'),
						'errorMessage' => $this->_getProperty('uploadErrorExtension')
					)
				)
			)
		);

		if (isset($this->_descriptor['defaultImage'])) {
			$settings['defaultImage'] = $this->_descriptor['defaultImage'];
		}

		$image = new WX_Image_Image($settings);
		$image->initUploader($file)->upload();

		$this->_imageObj = $image;

		if ($image->getUploader()->hasError()) {
			$this->_errors = $image->getUploader()->getErrors();
		}
	}

	public function delete($hash) {
		$settings = array(
			'path' => $this->_getProperty('path'),
			'url' => $this->_getProperty('url'),
			'sizes' => $this->_getProperty('sizes')
		);

		$image = new WX_Image_Image($settings, $hash);
		$image->initDelete()->deleteAll();
	}

	public function deleteTemp($hash) {
		$settings = array(
			'path' => $this->_getProperty('tempPath'),
			'url' => $this->_getProperty('tempUrl'),
			'sizes' => $this->_getProperty('sizes')
		);

		$image = new WX_Image_Image($settings, $hash);
		$image->initDelete()->deleteAll();
	}

	public function resize($hash) {
		$settings = array(
			'path' => $this->_getProperty('path'),
			'url' => $this->_getProperty('url'),
			'sizes' => $this->_getProperty('sizes')
		);

		$image = new WX_Image_Image($settings, $hash);
		$image->initResizer()->generateImages();
	}

	public function moveFromTempToLive($hash) {
		$settings = array(
			'path' => $this->_getProperty('tempPath'),
			'url' => $this->_getProperty('tempUrl'),
			'sizes' => $this->_getProperty('sizes'),
			'copy' => array(
				'targetPath' => $this->_getProperty('path')
			)
		);

		$image = new WX_Image_Image($settings, $hash);
		$image->initCopy()->copy();
		$image->initDelete()->deleteAll();
	}

	public function moveFromTempToLiveAndResize($hash) {
		$this->moveFromTempToLive($hash);
		$this->resize($hash);
	}
}