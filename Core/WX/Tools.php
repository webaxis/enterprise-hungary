<?php

class WX_Tools {

	static public function getValue ($array, $key, $default = false) {
		return isset($array[$key]) && !empty($array[$key]) ? $array[$key] : $default;
	}

	static public function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	static public function setIfExists(&$target, $source, $value) {
		if (self::getValue($source, $value) !== false) {
			$target[$value] = $source[$value];
		}
	}

	static public function setIfExistsMulti(&$target, $source, $values) {
		foreach($values as $value) {
			self::setIfExists($target, $source, $value);
		}
	}

	static public function generateRandomString ($length = 6, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') {
		$count = mb_strlen($chars);

		for ($i = 0, $result = ''; $i < $length; $i++) {
			$index = rand(0, $count - 1);
			$result .= mb_substr($chars, $index, 1);
		}

		return $result;
	}

	static public function sortByOneKey(array $array, $key, $asc = true) {
		$result = array();
		   
		$values = array();
		foreach ($array as $id => $value) {
			$values[$id] = isset($value[$key]) ? $value[$key] : '';
		}
 
		if ($asc) {
			asort($values);
		}
		else {
			arsort($values);
		}
		   
		foreach ($values as $key => $value) {
			$result[$key] = $array[$key];
		}
		   
		return $result;
	}
	


	static public function truncate($str, $maxLen, $suffix = '...') {
	 	if (strlen($str) <= $maxLen) {
	 		return $str;
		}

		mb_internal_encoding('UTF-8');

		$substr = mb_substr($str, 0, $maxLen - strlen($suffix)).$suffix;
	 	return $substr;
	}

	static public function linkRewrite($str, $connector = '-', $utf8Decode = false) {
		$str = rtrim(strtolower($str));
		$convertTo = array(
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
			"v", "w", "x", "y", "z", "à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
			"ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
			"з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
			"ь", "э", "ю", "я"
		);
		$convertFrom = array(
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
			"V", "W", "X", "Y", "Z", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï",
			"Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
			"З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
			"Ь", "Э", "Ю", "Я"
		);

		$str = str_replace($convertFrom, $convertTo, $str);
		$from = explode(",","á,é,í,ö,ő,ó,ü,ű,ú");
		$to = explode(",","a,e,i,o,o,o,u,u,u");
		$str = str_replace($from, $to, $str);
		$purified = '';
		$length = strlen($str);

		if ($utf8Decode) {
			$str = utf8Decode($str);
		}

		for ($i = 0; $i < $length; $i++) {
			$char = substr($str, $i, 1);
			if (strlen(htmlentities($char)) > 1) {
				$entity = htmlentities($char, ENT_COMPAT, 'UTF-8');
				$purified .= @$entity{1};
			} elseif (preg_match('|[[:alpha:]]{1}|u', $char)) {
				$purified .= $char;
			} elseif (preg_match('<[[:digit:]]|-{1}>', $char)) {
				$purified .= $char;
			}  elseif ($char == ' ') {
				$purified .= $connector;
			}
		}

		return $purified;
	}

	static public function createCaptcha ($wordLen, $font, $height = 80, $width = 120, $timeout = 3600, $imgDir = 'images/captcha') {
		$captcha = new Zend_Captcha_Image();

		$captcha->setTimeout($timeout)
		->setWordLen($wordLen)
		->setHeight($height)
		->setWidth($width)
		->setFont($font)
		->setDotNoiseLevel(0)
		->setLineNoiseLevel(0)
		->setImgDir($imgDir);

		$captcha->generate();

		return $captcha;
	}

	static public function encrypt($str, $key) {
		$block = mcrypt_get_block_size('des', 'ecb');
		$pad = $block - (strlen($str) % $block);
		$str .= str_repeat(chr($pad), $pad);

		return mcrypt_encrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB);
	}

	static public function decrypt($str, $key) {  
		@$str = mcrypt_decrypt(MCRYPT_DES, $key, $str, MCRYPT_MODE_ECB);

		$block = mcrypt_get_block_size('des', 'ecb');
		$pad = ord($str[($len = strlen($str)) - 1]);
		return substr($str, 0, strlen($str) - $pad);
	}

	static public function isValidInArrayMulti(array $whatArray,array $where) {
		foreach ($whatArray as $what) {
			self::isValidInArray($what,$where);
		}
		return true;
	}

	static public function isValidInArray($what, array $where) {
		if (array_key_exists($what,$where)) {
			if ($where[$what] == NULL) {
				throw new Exception('Following parameter is NULL: '.$what);
			}
		} else {
			throw new Exception('Missing or empty paramater: '.$what);
		}
		return true;
	}

	static public function getNowDatetime() {
		$date = new DateTime();
		$date->setTimestamp(time());
		$datetime = $date->format('Y-m-d H:i:s');
		
		return $datetime;
	}

	static public function getYoutubeVideoId($url) {
		$pattern = '/^(?:https?:\/\/|\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=))([\w-]{10,12})$/';
		if (preg_match($pattern, $url, $match)) {
			return $match[1];
		} else {
			return false;
		}
	}

	static public function getVimeoVideoId($url) {
		$pattern = '/^(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*$/';
		if (preg_match($pattern, $url, $match)) {
			return $match[5];
		} else {
			return false;
		}
	}

	static public function getRomanicNumber($integer, $upcase = true) { 
		$table = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
		$return = ''; 
		while ($integer > 0) { 
			foreach ($table as $rom => $arb) { 
				if ($integer >= $arb) { 
					$integer -= $arb; 
					$return .= $rom; 
					break; 
				}
			}
		}

		return $return; 
	}

	static public function isValidImage($image, $allowedTypes = array()) {
		if (empty($allowedTypes)) {
			$allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
		}
		$detectedType = exif_imagetype($image);
		if (in_array($detectedType, $allowedTypes)) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function jsStr($s) {
		return '"' . addcslashes($s, "\0..\37\"\\") . '"';
	}

	# Php array-t javascript array-e konvertalja
	static public function toJsArray($array) {
		$temp = array_map('self::jsStr', $array);
		return '[' . implode(',', $temp) . ']';
	}
	
}