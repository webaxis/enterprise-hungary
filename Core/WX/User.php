<?php

class WX_User {

	# states
	const USER_DELETED 				= 'deleted';
	const USER_INACTIVE				= 'inactive';
	const WRONG_LOGIN_DATA			= 'wrong_login_data';
	const AUTH_FAILED				= 'auth_failed';
	const UNKNOWN_IDENTITY			= 'unknown_identity';
	const SUCCESS					= true;
	const NOT_EXISTS_OR_ACTIVATED	= 'not_exists_or_activated';

	protected $_namespace 			= 'user';

	protected $_table				= 'user';
	protected $_identity			= 'email';
	protected $_credential			= 'password';

	protected $_expiration			= 86400;

	# Example: 'Example_Model_User' instance
	protected $_dbUser 			= null;

	protected $_useSalt 		= true;

	/**
	 * It contains db stored fields.
	 *
	 * Required fields: id, email, password, tempPassword, hash, date, lastLogin, deleted;
	 * Other fields are described in $_defaultFields array.
	 *
	 * It can be extended (or overridden) by defining the derived class's
	 * $_fields protected array. It will be merged with the $_defaultFields.
	 * 
	 * Example:
	 *
	 * <verbatim>
	 * 		protected $_fields = array(
	 * 			'other' => array(
	 *				'facebook' => array(
	 *					'enabled' => true
	 *				)
	 * 			)
	 *		);
	 * </verbatim>
	 */
	protected $_defaultFields = array(
		'groups' => array(
			 # DB: fbId, fbAccessToken
			'facebook' => array(
				'enabled' => false,
				# List of Facebook related fields.
				'requiredFields' => array(
					'fbId',
					'fbAccessToken'
				),
				'fields' => array(
					'fbUrl'
				)
			),
			 # DB: gpId, gpAccessToken
			'googleplus' => array(
				'enabled' => false,
				# List of Google plus related fields.
				'requiredFields' => array(
					'gpId',
					'gpAccessToken',
					'gpAccessTokenExpiration'
				),
				'fields' => array(
					'gpUrl',
					'gpRefreshToken'
				)
			),
			# DB: activationCode, activated
			'activation' => array(
				'enabled' => true,
				'sendActivatedEmail' => true
			),
		),
		'others' => array(
			'name',
			'firstname',
			'lastname',
			'birthday', 
			'gender',
			'timezone',
			'locale'
		)
	);

	# You can load user model by these fields.
	protected $_allowedBy = array(
		'id', 
		'email', 
		'login', 
		'username', 
		'fbId', 
		'fbAccessToken', 
		'hash', 
		'activationCode', 
		'tempPassword'
	);

	public function setDbUser($dbUser) {
		$this->_dbUser = $dbUser;
	}

	public function getDbUser() {
		return $this->_dbUser;
	}

	protected function _extendFields() {
		if (isset($this->_fields)) {
			if (isset($this->_fields['others'])) {
				$others = $this->_fields['others'];
			} else {
				$others = array();
			}

			$defaultFieldsOthers = $this->_defaultFields['others'];
			$this->_fields = array_replace_recursive($this->_defaultFields, $this->_fields);
			$this->_fields['others'] = array_merge($defaultFieldsOthers, $others);
		} else {
			$this->_fields = $this->_defaultFields;
		}
	}

	public function __construct($id = false) {
		$this->_extendFields();
		if ($id !== false) {
			$dbClass = APP_NAME.'_Model_' . ucfirst($this->_table);
			$this->_dbUser = new $dbClass();
			$this->_dbUser->find($id);
		}
	}

	static public function getInstance($identifier = false, $by = 'id') {
		$userClass 	= get_called_class();
		$userObj	= new $userClass($identifier, $by);

		$userObj->setUser($identifier, $by);

		return $userObj;
	}

	public function setUser($identifier, $by = 'id') {
		if (!in_array($by, $this->_allowedBy)) {
			$message = 'You can load a user only by: "'.implode(', ', $this->_allowedBy).'"';
			WX_Exception::create($message);
		}

		$dbClass = APP_NAME . '_Model_' . ucfirst($this->_table);

		$this->_dbUser = new $dbClass;

		if ($by == 'id') {
			$find = 'find';
		} else {
			$find = 'findOneBy'.ucfirst($by);
		}
		$this->_dbUser->$find($identifier);
	}

	static public function emailExistsStatic ($email) {
		$userObj = self::getMimicClass(get_called_class());
		return $userObj->emailExists($email);
	}

	public function emailExists ($email) {
		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from($this->_table)
					->where('email like "'.$email.'"');
		return (bool) count($db->query($query)->fetchAll());
	}

	public function exists () {
		if (is_null($this->_dbUser)) {
			return false;
		}

		if (is_null($this->_dbUser->getId())) {
			return false;
		}

		return true;
	}

	public function loginIfActive ($identity, $credential) {
		$this->setUser($identity, $this->_identity);
		if (!$this->isActive()) {
			return self::USER_INACTIVE;
		}

		return $this->login($identity, $credential);
	}

	public function isActive() {
		if ($this->getDbUser()->getActivated() == 1) {
			return true;
		}

		return false;
	}

	public function login($identity, $credential, $hashPassword = false) {
		$this->setUser($identity, $this->_identity);

		# Check user exists
		if (!$this->exists()) {
			return self::WRONG_LOGIN_DATA;
		}

		# Check if user is deleted.
		if ($this->isDeleted()) {
			return self::USER_DELETED;
		}

		# Check if user is activated.
		if ($this->_fields['groups']['activation']['enabled']) {
			if (!$this->isActivated()) {
				return self::USER_INACTIVE;
			}
		}

		$auth = new WX_Authentication($this->_namespace, $this->_expiration);

		$auth->setAdapter(array(
			'table'			=> $this->_table,
			'identity' 		=> $this->_identity,
			'credential' 	=> $this->_credential
		));

		$auth->setLoginDetails(array(
			'identity' 		=> $identity,
			'credential' 	=> $credential,
			'salt' 			=> $this->_useSalt
		), $hashPassword);

		$isAuthenticated = $auth->authenticate();

		# Check if user credentials are valid.
		if (!$isAuthenticated) {
			# That means that user authentication failed
			# due to incorrect credentials.
			return self::WRONG_LOGIN_DATA;
		}
		
		if ($auth->login()) {
			# User is logged in.
			return true;
		} else {
			# User couldn't be logged in.
			return self::AUTH_FAILED;
		}
	}

	public function isDeleted () {
		if (method_exists($this->_dbUser, 'getDeleted')) {
			if ($this->_dbUser->getDeleted() == 1) {
				return true;
			}
		}

		return false;
	}

	public function isActivated () {
		return (bool) $this->getDbUser()->getActivated();
	}

	public function loginInstance () {
		$dbModel = $this->getDbUser();

		$identityFuncName = 'get' . ucfirst($this->_identity);
		$credentialFuncName = 'get' . ucfirst($this->_credential);

		return $this->login($dbModel->$identityFuncName(), $dbModel->$credentialFuncName(), true);
	}

	public function logout () {
		WX_Authentication::getInstance($this->_namespace, $this->_expiration)->logout();
	}

	public function isLoggedIn () {
		return WX_Authentication::getInstance($this->_namespace, $this->_expiration)->isLoggedIn();
	}

	public function getLoggedInUser () {
		if (Zend_Registry::isRegistered($this->_namespace.'Identity')) {
			return Zend_Registry::get($this->_namespace.'Identity');
		}

		$id = WX_Authentication::getInstance($this->_namespace, $this->_expiration)->getIdentity();
		$user = self::getInstance($id);

		Zend_Registry::set($this->_namespace.'Identity', $user);

		return $user;
	}

	public function getLoggedInParams () {
		$user = $this->getLoggedInUser();

		return $user->_dbUser->toArray();
	}

	public function getLoggedInParam ($key) {
		$params = $this->getLoggedInParams();
		
		return $params[$key];
	}

	public function register($params) {
		$class = APP_NAME.'_Model_' . ucfirst($this->_table);
		$setIdentityFuncName = 'set' . ucfirst($this->_identity);
		$setCredentialFuncName = 'set' . ucfirst($this->_credential);

		$model = new $class;
		$model->$setIdentityFuncName($params[$this->_identity]);

		# required fields
		if (isset($params[$this->_credential]) && !empty($params[$this->_credential])) {
			$model->$setCredentialFuncName($this->_createPassword($params[$this->_credential]));
		}

		if (method_exists($model, 'setDate')) {
			$model->setDate(date('Y-m-d H:i:s'));
		}

		if (method_exists($model, 'setHash')) {
			$model->setHash(md5(rand(0,9999) . $params['email'] . mktime()));
		}

		if (method_exists($model, 'setDeleted')) {
			$model->setDeleted(0);
		}

		$sendActivationEmail = false;
		$sendPasswordEmail = false;

		# set activation if it is enabled
		if ($this->_fields['groups']['activation']['enabled'] === true) {
			if (
				($this->_fields['groups']['facebook']['enabled']
				&& isset($params['fbId']) 
				&& !empty($params['fbId']))
				||
				($this->_fields['groups']['googleplus']['enabled']
				&& isset($params['gpId']) 
				&& !empty($params['gpId']))
			) {
				$model->setActivated(1);
				$sendPasswordEmail = true;
			} else {
				$model->setActivated(0);
				$model->setActivationCode(md5(mktime() . $params['email'] . rand(0,9999)));
				$sendActivationEmail = true;
			}
		} else {
			$model->setActivated(1);
		}

		$model->save();

		$this->setDbUser($model);

		$this->updateDetails($params);

		$model = $this->getDbUser();

		if ($sendActivationEmail) {
			$this->_sendRegistrationEmail();
		}

		if ($sendPasswordEmail) {
			$this->_sendPasswordEmail($params[$this->_credential]);
		}

		return true;
	}

	public function updateDetails ($params) {
		$model = $this->getDbUser();

		foreach ($this->_fields['others'] as $field) {
			if (isset($params[$field]) && !empty($params[$field])) {
				$funcName = 'set' . ucfirst($field);
				$model->$funcName($params[$field]);
			}
		}

		if ($this->_fields['groups']['facebook']['enabled']) {
			foreach ($this->_fields['groups']['facebook']['requiredFields'] as $field) {
				if (isset($params[$field]) && !empty($params[$field])) {
					$funcName = 'set' . ucfirst($field);
					$model->$funcName($params[$field]);
				}
			}
			foreach ($this->_fields['groups']['facebook']['fields'] as $field) {
				if (isset($params[$field]) && !empty($params[$field])) {
					$funcName = 'set' . ucfirst($field);
					$model->$funcName($params[$field]);
				}
			}
		}

		if ($this->_fields['groups']['googleplus']['enabled']) {
			foreach ($this->_fields['groups']['googleplus']['requiredFields'] as $field) {
				if (isset($params[$field]) && !empty($params[$field])) {
					$funcName = 'set' . ucfirst($field);
					$model->$funcName($params[$field]);
				}
			}
			foreach ($this->_fields['groups']['googleplus']['fields'] as $field) {
				if (isset($params[$field]) && !empty($params[$field])) {
					$funcName = 'set' . ucfirst($field);
					$model->$funcName($params[$field]);
				}
			}
		}

		$model->save();
	}

	protected function _sendRegistrationEmail() {
		$class = APP_NAME . '_Locale';
		$language = $class::getInstance()->getLanguage();

		if ($this->_fields['groups']['activation']['enabled'] === true) {
			$mailParams = array(
				'addTo' => $this->getDbUser()->getEmail(),
				'subject' => t('EMAIL_SUBJECT_REGISTRATION_AND_ACTIVATION')
			);
			$templateVars = array(
				'name' =>  $this->_getNameToEmail(),
				'activationCode' => $this->getDbUser()->getActivationCode()
			);

			WX_Mail::getInstance()->send('activation', $mailParams, $templateVars);
		} else {
			$mailParams = array(
				'addTo' => $this->getDbUser()->getEmail(),
				'subject' => t('EMAIL_SUBJECT_REGISTRATION')
			);
			$templateVars = array(
				'name' => $this->_getNameToEmail()
			);

			WX_Mail::getInstance()->send('registration', $mailParams, $templateVars);
		}
	}

	protected function _sendPasswordEmail($password) {
		$mailParams = array(
			'addTo' => $this->getDbUser()->getEmail(), 
			'subject' => t('EMAIL_SUBJECT_REGISTRATION')
		);

		$templateVars = array(
			'name' => $this->_getNameToEmail(),
			'password' => $password
		);

		WX_Mail::getInstance()->send('password', $mailParams, $templateVars);
	}

	protected function _getNameToEmail() {
		if (method_exists($this->getDbUser(), 'getName')) {
			$name = $this->getDbUser()->getName();
		} else {
			$name = $this->getDbUser()->getEmail();
		}

		return $name;
	}

	public function activate($activationCode, $login = false) {
		if ($this->getDbUser()->getId() === NULL) {
			return false;
		}

		$userObj = $this->getDbUser();
		$userObj->setActivationCode(NULL);
		$userObj->setActivated(1);
		$userObj->save();

		if ($this->_fields['groups']['activation']['sendActivatedEmail']) {
			$this->_sendActivatedEmail();
		}

		if ($login) {
			$this->loginInstance();
		}

		return true;
	}

	protected function _sendActivatedEmail() {
		$mailParams = array(
			'addTo' => $this->getDbUser()->getEmail(),
			'subject' => t('EMAIL_SUBJECT_ACTIVATION')
		);

		$templateVars = array(
			'name' => $this->_getNameToEmail()
		);

		WX_Mail::getInstance()->send('activated', $mailParams, $templateVars);
	}

	public function newPassword($email) {
		$user = self::getInstance($email, 'email');

		if ($user->getDbUser()->getId() === NULL) {
			return self::UNKNOWN_IDENTITY;
		}

		$userObj = $user->getDbUser();
		$tempPassword = md5(mktime() . $userObj->getName() . rand(0,9999));
		$userObj->setTempPassword($tempPassword);
		$userObj->save();

		$user->_sendNewPassword();

		return self::SUCCESS;
	}

	protected function _sendNewPassword() {
		$mailParams = array(
			'addTo' => $this->getDbUser()->getEmail(),
			'subject' => t('EMAIL_SUBJECT_NEWPASSWORD')
		);

		$templateVars = array(
			'name' => $this->_getNameToEmail(), 
			'password' => $this->getDbUser()->getTempPassword(), 
		);

		WX_Mail::getInstance()->send('newpassword', $mailParams, $templateVars);
	}

	public function isValidTempPassword($tempPassword) {
		$user = self::getInstance($tempPassword, 'tempPassword');
		if ($user->getDbUser()->getId() === NULL) {
			return false;
		}

		return true;
	}

	public function newPasswordByTempPassword($newPassword, $tempPassword) {
		$setCredentialFuncName = 'set' . ucfirst($this->_credential);
		
		$user = self::getInstance($tempPassword, 'tempPassword');
		$userObj = $user->getDbUser();
		$userObj->$setCredentialFuncName($user->_createPassword($newPassword));
		$userObj->setTempPassword(NULL);
		$userObj->save();
	}

	static public function checkPasswordStatic ($password) {
		$userObj = self::getMimicClass(get_called_class());
		return $userObj->checkPassword($password);
	}

	public function checkPassword ($password) {
		$user = $this->getLoggedInUser();

		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from($this->_table)
					->where('id = "'.$user->getDbUser()->getId().'" AND '.$this->_credential.' like "'.$user->_createPassword($password).'"');
		return !(bool) count($db->query($query)->fetchAll());
	}

	public function changeLoggedInUserPassword($newPassword) {
		$user = $this->getLoggedInUser();
		$user->_changePassword($newPassword);

		return true;
	}

	protected function _changePassword($newPassword) {
		$setCredentialFuncName = 'set' . ucfirst($this->_credential);

		$userObj = $this->getDbUser();
		$userObj->$setCredentialFuncName($this->_createPassword($newPassword));
		$userObj->save();
	}

	protected function _createPassword($password) {
		if ($this->_useSalt) {
			$password = md5(APP_KEY . $password);
		} else {
			$password = md5($password);
		}

		return $password;
	}

	static protected function getMimicClass ($userClass) {
		$userObj	= new $userClass();

		return $userObj;
	}

	public function resendActivation($email) {
		$user = self::getInstance($email, 'email');

		if ($user->getDbUser()->getId() === NULL) {
			return self::NOT_EXISTS_OR_ACTIVATED;
		}

		$userObj = $user->getDbUser();
		if ($userObj->getActivated() == 1) {
			return self::NOT_EXISTS_OR_ACTIVATED;
		}

		$user->_sendResendActivation();

		return true;

	}

	protected function _sendResendActivation() {
		$this->_sendRegistrationEmail();
	}
}