<?

class WX_Admin_Menu {

	protected $_descriptor = array();

	protected $_url = null;

	protected $_controller = null;

	protected $_action = null;

	protected $_selectedMain = null;

	protected $_selectedSub = null;

	protected $_structure = array();

	/**
	 * $params contains: controller, action, url
	 */
	public function __construct($descriptor, $params) {
		$this->_url = $params['url'];
		$this->_controller = $params['controller'];
		$this->_action = $params['action'];
		$this->_descriptor = $descriptor;
	}

	static public function getInstance($descriptor, $params) {
		return new WX_Admin_Menu($descriptor, $params);
	}

	protected function _build() {
		foreach ($this->_descriptor as $mainName => $mainData) {
			if (isset($mainData['subtabs'])) {
				$branchFound = false;
				foreach ($mainData['subtabs'] as $subName => $subData) {
					if (isset($subData['alias'])) {
						if (substr($subData['alias'], -1) == '*') {
							$alias = substr($subData['alias'], 0, -1);
							$aliasLength = strlen($alias);
							$url = substr($this->_url, 0, $aliasLength);
							if ($alias == $url) {
								$this->_selectedSub = $subName;
								$branchFound = true;
							}
						} else if ($subData['alias'] == $this->_url) {
							$this->_selectedSub = $subName;
							$branchFound = true;
						}
					} elseif ($subName == $this->_controller) {
						$this->_selectedSub = $subName;
						$branchFound = true;
					}
					if ($branchFound) {
						$this->_selectedMain = $mainName;
					}
				}
			} else {
				if (isset($mainData['alias'])) {
					if ($mainData['alias'] == $this->_url) {
						$this->_selectedMain = $mainName;
					} 
				} elseif ($mainName == $this->_controller) {
					$this->_selectedMain = $mainName;
					$branchFound = true;
				}
			}
		}
	}

	protected function _render() {
		$html = '';
		foreach ($this->_descriptor as $mainName => $mainData) {
			if ($mainName == $this->_selectedMain) {
				$html.= '<li class="active">' . PHP_EOL;
			} else {
				$html.= '<li>' . PHP_EOL;
			}

			if (isset($mainData['subtabs'])) {
				$html.= '<a href="' . $mainData['url'] . '"><span class="' . $mainData['icon'] . '"></span>' . $mainData['label'] . '<i class="fa fa-chevron-right"></i><i class="fa fa-chevron-down"></i></a>' . PHP_EOL;
				$html.= '<ul>';
				foreach ($mainData['subtabs'] as $subName => $subData) {
					if ($subName == $this->_selectedSub) {
						$html.= '<li class="active">' . PHP_EOL;
					} else {
						$html.= '<li>' . PHP_EOL;
					}

					$html.= '<a href="' . $subData['url'] . '">' . $subData['label'] . '</a>' . PHP_EOL;
					$html.= '</li>' . PHP_EOL;
				}
				$html.= '</ul>';
			} else {
				$html.= '<a href="' . $mainData['url'] . '"><span class="' . $mainData['icon'] . '"></span>' . $mainData['label'] . '</a>' . PHP_EOL;
			}

			$html.= '</li>' . PHP_EOL;
		}

		return $html;
	}
	
	public function render() {
		$this->_build();
		return $this->_render();
	}
}

?>