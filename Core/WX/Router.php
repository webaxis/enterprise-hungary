<?php

class WX_Router extends Zend_Controller_Plugin_Abstract
{
	protected $_route				= array();
	protected $_uri					= false;
	protected $_method				= false;
	protected $_routes				= array();
	protected $_routesParams		= array();
	protected $_params				= array();
	protected $_gotParams			= array();
	protected $_requestVars			= array();
	
	protected $_languages			= array();
	
	protected $_defaultRoute		= '/';
	
	protected $_variableType		= 'a-z0-9-';
	
	public function routeShutdown(Zend_Controller_Request_Abstract $request) {
		$this->_request = $request;
		
		if($request->isPut()  || $request->isDelete()) {
            parse_str($request->getRawBody(), $params);
            foreach($params as $key => $value) {
                $request->setParam($key, $value);
            }
        }
		
		$gotParams = array();
		foreach ($_REQUEST as $key => $one) {
			if (!in_array($key,array('module','controller','action')))
				$gotParams[$key] = $one;
		}
		
		$this->_uri 		= preg_replace('/^\/*/','',preg_replace('/\/+/','/',str_replace(strstr($request->getRequestUri(),'?'),'',$request->getRequestUri())));
		$this->_method 		= $request->getMethod();
		$this->_gotParams 	= $gotParams;
		
		$this->addRoute(array('cms/:action','GET,POST','cms',':action'));
		$this->addRoute(array(':controller/saveform','GET,POST',':controller','saveform'));
		$this->addRoute(array('admin/:controller/saveform','GET,POST',':controller','saveform','admin'));
		$this->addRoute(array('form/validate','GET,POST','form','validate'));
		$this->addRoute(array('wxlist','GET,POST','index','wxlist'));
		$this->addRoute(array('index/wxlist','GET,POST','index','wxlist'));
		$this->addRoute(array('ajax/:function','GET,POST','ajax',':function'));
		$this->addRoute(array('admin/ajax/:function','GET,POST','ajax',':function','admin'));
	
		$this->_resolve();
		
		$this->route();
	}
	
	/**
	 *
	 * @route (array)
	 *   0 - url
	 *   1 - type of request (GET, POST, PUT, DELETE)
	 *   2 - controller to set
	 *   3 - action to set
	 *
	 * @routeParams (array)
	 *   variableType - allowed regex characters of the joker element (eg. 0-9) (joker element is ':id' for example)
	 **/
	public function addRoute ($route, $routeParams = array()) {
		$appLocaleClassName = APP_NAME . '_Locale';
		$appLocaleClass = $appLocaleClassName::getInstance();

		if (empty($this->_languages)) {
			$this->_languages = $appLocaleClass->getAvailableLanguages();
		}

		if (empty($this->_languages)) {
			$this->_routes[] = $route;
			$this->_routesParams[] = $routeParams;
		} else {	
			$originalLocale = $appLocaleClass->getLocale();
			
			$originalRoutePath = $route[0];
			foreach ($this->_languages as $lang) {
				$locale = new Zend_Locale($lang);
				Zend_Registry::set('Zend_Locale', $locale);
				
				$routePathParts = explode('/', $originalRoutePath);
				
				$routePath = '';
				foreach ($routePathParts as $key => $one) {
					$routePath .= WX_Translator::router($one) . (count($routePathParts) > ($key + 1) ? '/' : '');
				}
				
				$route[0] = $routePath;
				
				$this->_routes[] = $route;
				$this->_routesParams[] = $routeParams;
			}
			
			$locale = new Zend_Locale($originalLocale);
			Zend_Registry::set('Zend_Locale', $locale);
		}
		
		return $this;
	}
	
	public function route ($a = false) {
		if ($this->validateRoute()) {
			$this->_request->setModuleName($this->getModule());
			$this->_request->setControllerName($this->getController());
			$this->_request->setActionName($this->getAction());
			$this->_request->setParams(array_merge(array('module'=>$this->getModule(),'controller'=>$this->getController(),'action'=>$this->getAction()),$this->getParams()));
		} else {
			$this->_uri = $this->_defaultRoute;
			$this->_route = null;
			$this->_params = null;
			$this->route(1);
		}
	}
	
	public function validateRoute () {
		$this->getRequestVars();
		
		if ($this->_requestVars['controller'] !== false)
			return true;
			
		return false;
	}
	
	public function setLanguages ($languages) {
		$this->_languages = $languages;
	}
	
	public function getRequestVars () {
		if (empty($this->_params))
			$this->_setParams();
			
		$this->_requestVars = array(
			'module'		=> isset($this->_route[4]) && !empty($this->_route[4]) ? $this->_route[4] : 'default',
			'controller'	=> !empty($this->_route) ? $this->_route[2] : false,
			'action'		=> !empty($this->_route) ? $this->_route[3] : false,
			'params'		=> $this->_params
		);
		
		return $this->_requestVars;
	}
	
	protected function _setParams () {
		if (empty($this->_route)) {
			$this->_chooseRoute();
		}
			
		if (empty($this->_route))
			return $this->_params = $this->_gotParams;
	
		$routeParts = explode('/',$this->_route[0]);
		$uriParts = explode('/',$this->_uri);
		
		$params = array();
		foreach ($routeParts as $key => $one) {
			if (strstr($one,':') !== false)
				$params[str_replace(':','',$one)] = $uriParts[$key];
		}
		
		$this->_params = array_merge($params,$this->_gotParams);
	}
	
	protected function _chooseRoute () {
		foreach ($this->_routes as $key => $one) {
			if (preg_match('/^(\/*)(' . str_replace('/','\/', $one[0]) . ')(\/*)(((\?).*)|$)/', $this->_uri)) {
				if (in_array(strtoupper($this->_method),explode(',',strtoupper($one[1])))) {
					return $this->_route = $this->_routes[$key];
				}
			}
		}
		
		foreach ($this->_routes as $key => $one) {
			$route = $one[0];
			if (preg_match('/(:[a-zA-Z0-9]+)/', $route, $matches)) {
			
				$variableType = isset($this->_routesParams[$key]['variableType']) ? $this->_routesParams[$key]['variableType'] : $this->_variableType;
				$regex = '/^'.str_replace('/','\/',preg_replace('/(:[a-zA-Z0-9]+)/','(['.$variableType.']+)',$route)).'(\/*)(((\?).*)|$)/';
				
				if (preg_match($regex,$this->_uri)) {
					if (in_array(strtoupper($this->_method),explode(',',strtoupper($one[1])))) {
						return $this->_route = $one;
					}
				}
			}
		}
	}
	
	public function getModule () {
		return $this->getRequestPart('module');
	}
	
	public function getController () {
		$controller = $this->getRequestPart('controller');

		if (substr($controller,0,1) == ':') {
			return $this->getJokerValue(str_replace(':','',$controller));
		} else {
			return $controller;
		}

		return $this->getRequestPart('controller');
	}
	
	public function getAction () {
		$action = $this->getRequestPart('action');
		
		if (substr($action,0,1) == ':') {
			return $this->getJokerValue(str_replace(':','',$action));
		} else {
			return $action;
		}
		return $this->getRequestPart('action');
	}
	
	public function getParams () {
		return $this->getRequestPart('params');
	}
	
	public function getRequestPart ($key) {
		$request = $this->getRequestVars();
		
		return $request[$key];
	}
	
	public function getJokerValue ($joker) {
		$uri 	= $this->_uri;
		$route	= $this->_route[0];

		$jokerKey = false;
		foreach (explode('/',$route) as $key => $value) {
			if ($jokerKey != false)
				break;

			if ($value == ':'.$joker) {
				$jokerKey = $key;
			}
		}

		if ($jokerKey === false) {
			return false;
		}
			
		$uriParts = explode('/',$uri);
		
		return $uriParts[$jokerKey];
	}
	
	public function setDefaultRoute ($defaultRoute) {
		$this->_defaultRoute = $defaultRoute;
	}
}