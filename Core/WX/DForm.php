<?php

class WX_DForm {

	static protected $WX_DForm;

	protected $_mode = null;

	protected $_descriptor = array();

	protected $_descriptorClass = null;

	protected $_descriptorMethod = null;

	protected $_form = null;

	protected $_formArguments = array();

	public static function getInstance($id) {
		return new self($id);
	}

	public function __construct($descriptorMethod) {
		$this->_descriptorClass = APP_NAME . '_DForm_Descriptor';
		$this->_descriptorMethod = $descriptorMethod;
		$this->_defaultTemplateDirectory = CORE_APP_PATH . 'Form/Templates/Standards/';
	}

	public function setFormArguments($args = array()) {
		$this->_formArguments = $args;

		return $this;
	}

	public function get($rp = false, $templateDirectory = false, $mode = false) {
		$this->_descriptor = call_user_func_array($this->_descriptorClass . '::' . $this->_descriptorMethod, array($this->_formArguments));
		$templateDirectory = $this->_getTemplateDirectory($templateDirectory);

		$descriptor = array(
			'templateDirectory' => $templateDirectory,
			'url' => $this->_getFormUrl(),
			'id' => $this->_getFormId(),
			'method' => $this->_getFormMethod(),
			'globalFilters' => array(
				0 => array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'items' => $this->getFormDescriptor($rp, $mode)
		);

		$jsonDescriptor = json_encode($descriptor);
		$jsonDescriptor = str_replace('"', "'", $jsonDescriptor);

		$this->_form = new WX_Form_Form_Json($jsonDescriptor);
		return $this->_form;
	}

	public function isValid($rp) {
		return $this->_form->isValid($rp);
	}

	protected function _getTemplateDirectory($templateDirectory = false) {
		if ($templateDirectory !== false) {
			return $templateDirectory;
		}
		if (isset($this->_descriptor['templateDirectory'])) {
			return $this->_descriptor['templateDirectory'];
		}

		return $this->_defaultTemplateDirectory;
	}

	protected function _getFormUrl() {
		if (!isset($this->_descriptor['url'])) {
			WX_Exception::create('You must set "url" in form descriptor');
		}

		return $this->_descriptor['url'];
	}

	protected function _getFormId() {
		if (!isset($this->_descriptor['url'])) {
			WX_Exception::create('You must set "id" in form descriptor');
		}

		return $this->_descriptor['id'];
	}
	
	protected function _getFormMethod() {
		if (isset($this->_descriptor['method'])) {
			return $this->_descriptor['method'];
		}

		return 'POST';
	}

	/**
	 * FormData : vagy DB-bol jon, vagy request-bol
	 */
	public function getFormDescriptor($formData, $mode = null) {
		$this->_mode = $mode;
		# ez a legtagabb descriptor tarolo
		$formDescriptor = array();
		if (isset($this->_descriptor['groups'])) {
			foreach ($this->_descriptor['groups'] as $group) {
				if (!isset($group['items']) || !count($group['items'])) {
					throw new Exception('You cannot create group without items. Please add least one item to "'.$group['name'].'" clone group items!');					
				}
				if ($this->_isVisible($group)) {
					$dGroup = array(
						'vtype' => 'group',
						'label' => $group['label']
					);

					if (isset($group['template'])) {
						$dGroup['template'] = $group['template'];
					}

					$description = isset($group['description']) ? $group['description'] : false;

					if ($description !== false) {
						$dGroup['description'] = $description;
					}

					$dItems = array();
					foreach ($group['items'] as $item) {
						if (!isset($item['type'])) {
							$item['type'] = 'text';
						}
						$type = $item['type'];

						switch ($type) {
							# a cloneGroup elemeket kulon kell kezelni
							case 'cloneGroup' :
								if ($this->_isVisible($item)) {
									if (!isset($item['items']) || !count($item['items'])) {
										throw new Exception('You cannot create clone group without items. Please add least one item to "'.$group['name'].'" clone group items!');
									}
									# Megnezem, hogy hany darab van a leiroban (request vagy db data) a 
									# group-ban talalhato elso elembol. Mivel ez csoport, ezert a tobbi
									# elembol is ennyinek kell lennie.
									$firstItemName = $item['items'][0]['name'];

									# megnezzuk, hogy a request-ben van a db adatokban van e ilyen sor
									if (WX_Tools::getValue($formData, $firstItemName) !== false) {
										# ha van, megszamoljuk hogy mennyi van belole
										$countElements = count(WX_Tools::getValue($formData, $firstItemName));	
									} else {
										# egyebkent meg automatikusan 1 -re allitjuk
										$countElements = 1;	
									}

									# annyiszor szurja be a cloneGroup szekciot, ahanyszor megtalalta a leiroban
									for ($i = 0; $i < $countElements; $i++ ) {
										$dItem = array(
											'vtype' => 'group',
											'label' => WX_Tools::getValue($item, 'label', ''),
											'cloneGroup' => $item['id'],
											'template' => WX_Tools::getValue($item, 'template', 'groupclone.tpl.php')
										);
										$dItem['items'] = array();
										foreach ($item['items'] as $cloneGroupItem) {
											$parameters = $cloneGroupItem;
											# kiszedjuk az eredeti nevet
											$name = $parameters['name'];
											# atkonvertaljuk multiple elemme
											$parameters['name'] = $parameters['name'].'[]';
											# megnezi, hogy van e a request-ben (vagy mentett adatok kozott) kitoltott ertek.
											if (isset($formData[$name])) {
												# ...ha multiple input - vagyis lett klonozva
												if (is_array($formData[$name])) {
													$value = $formData[$name][$i];
												} else {
													$value = $formData[$name];
												}
											}
											$dItem['items'][] = $this->_createElement($parameters, $value);
										}
										$dItems[] = $dItem;
									}
								}
							break;
							case 'group' :
								if ($this->_isVisible($item)) {
									if (!isset($item['items']) || !count($item['items'])) {
										throw new Exception('You cannot create clone group without items. Please add least one item to "'.$group['name'].'" clone group items!');
									}

									$dItem = array(
										'vtype' => 'group',
										'label' => WX_Tools::getValue($item, 'label', '')
									);
									$dItem['items'] = array();
									foreach ($item['items'] as $groupItem) {
										if ($this->_isVisible($groupItem)) {
											$parameters = $groupItem;
											$value = $this->_getValue($formData, $parameters);
											$dItem['items'][] = $this->_createElement($parameters, $value);
										}
									}
									$dItems[] = $dItem;
								}
							break;
							# minden egyeb elemet az alabbi logika szerint epit fel
							default:
								if ($this->_isVisible($item)) {
									$parameters = $item;
									$value = $this->_getValue($formData, $parameters);
									$dItems[] = $this->_createElement($parameters, $value);
								}
							break;
						}
					}
					$dGroup['items'] = $dItems;
					$formDescriptor[] = $dGroup;
				}
			}
		} else {
			DX('If you want to use the descriptor without groups you need implement');
		}

		/*
		$jsonFormDescriptor = json_encode($formDescriptor);
		$jsonFormDescriptor = str_replace('"', "'", $jsonFormDescriptor);

		return $jsonFormDescriptor;
		*/

		return $formDescriptor;
	}

	/**
	 * Az allowedEmptyValues-ban taroljuk azokat az elemeket, 
	 * amiket nem szeretnenk false-kent visszakapni validalasnal.
	 *
	 * Erre pelda: a "0" ertek. Mivel a WX_Tools::getValue-ban empty()
	 * fuggvenyt hasznalunk - es az empty a "0" erteket semminek veszi
	 * ezert kellett ezt bevezetni, amellett, hogy a WX_Tools::getValue
	 * mukodese is frissult.
	 */
	protected function _getValue($formData, $parameters) {
		if (!empty($parameters['allowedEmptyValues'])) {
			$value = WX_Tools::getValue($formData, $parameters['name'], false, $parameters['allowedEmptyValues']);
		} else {
			$value = WX_Tools::getValue($formData, $parameters['name']);
		}

		return $value;
	}

	protected function _createElement($descriptor, $value) {
		if (is_array($value)) {
			$values = array();
			foreach ($value as $item) {
				$values[] = htmlspecialchars($item, ENT_QUOTES);
			}
			$value = $values;
		} else {
			$value = htmlspecialchars($value, ENT_QUOTES);
		}

		# Azert kell, mert egyebkent a WX_Form nem tudja feldolgozni a value-kat,
		# igy oldal ujratoltesnel eltunnek az ertekek. Ezzel a megoldassal
		# pedig jol mukodik
		if ($descriptor['type'] == 'checkboxgroup') {
			foreach ($descriptor['items'] as $idx => $item) {
				$fieldLabel = $item['fieldLabel'];
				if (!empty($value)) {
					if (in_array($fieldLabel, $value)) {
						$descriptor['items'][$idx]['checked'] = 'checked';
					}
				}
			}
		}

		# Ugyanazert kell, mint a checkboxgroupnal
		if ($descriptor['type'] == 'checkbox') {
			if ($value != '') {
				if ($descriptor['name'] == $value) {
					$descriptor['checked'] = 'checked';
				}
			}
		}

		$this->_viewMode($descriptor, $value);
		$className = 'WX_DForm_Element_' . ucfirst(strtolower($descriptor['type']));
		$element = call_user_func_array(
			array($className, "create"), 
			array($descriptor, $value)
		);
		$this->_setDecorators($element, $descriptor);			
		//convert specified contents
		$this->_convertContent($element);

		return $element;
	}

	protected function _viewMode(&$descriptor, &$value) {
		//return if not exists viewMode
		if (!$this->_mode == 'viewMode') {
			return;
		}
		if (!isset($descriptor['viewMode'])) {
			return;
		}

		//set view template
		$viewMode = $descriptor['viewMode'];
		if (isset($viewMode['template'])) {
			$descriptor['template'] = $viewMode['template'];
		}

		unset($descriptor['viewMode']);
	}

	protected function _convertContent(&$element) {
		//return if not exists viewMode
		if (!$this->_mode == 'viewMode') {
			return;
		}

		// create linked url from url
		$decoratorKey = $this->_descriptor['id'].'-decorator';
		if (isset($element['attributes'][$decoratorKey])) {
			$decorators = $element['attributes'][$decoratorKey];
			if (is_array($decorators)) {
				if (array_key_exists('url', $decorators)) {
					$value = $element['defaultValue'];
					$element['defaultValue'] = '<a href="' . $value . '">' . $value . '</a>';
				}
			} else {
				if ($decorators == 'url') {
					$value = $element['defaultValue'];
					$element['defaultValue'] = '<a href="' . $value . '" target="_blank">' . $value . '</a>';
				}
			}
		}
	}

	protected function _isVisible($descriptor) {
		if (
			$this->_mode == 'viewMode'
			&& isset($descriptor['viewMode']['hide'])
			&& $descriptor['viewMode']['hide'] === true
		) {
			return false;
		} else {
			return true;
		}
	}

	protected function _setDecorators(&$formDataArray, $itemDescriptor) {
		if (isset($itemDescriptor['decorators'])) {
			foreach ($itemDescriptor['decorators'] as $decorator => $settings) {
				# $decorator === 0 means: decorator doesn't have settings
				if ($decorator === 0) {
					$decorator = $settings;
					$settings = array();
				}
				$className = 'WX_DForm_Decorator_'.ucfirst(strtolower($decorator));
				$id = $this->_descriptor['id'];
				$formDataArray = call_user_func_array(
					array($className, "add"), 
					array($settings, $formDataArray, $id)
				);
			}
		}
	}

}