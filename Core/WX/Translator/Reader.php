<?php

class WX_Translator_Reader {

	const _FILE_EXPRESSION_SPLITTER_ = '|||';

	const _FILE_SEGMENT_SPLITTER_ = '###';

	static public $reversedLanguages = array('hu');

	private static $_wxTranslatorReader;

    public static function getInstance() {
        if (self::$_wxTranslatorReader == null) {
            self::$_wxTranslatorReader = new self();
        }

        return self::$_wxTranslatorReader;
    }

	private function _removeNewLine($string) {
		$string = preg_replace("/\n\r|\r\n|\n|\r/", "", $string);
		$string = preg_replace("/\t/", "", $string);

		return $string;
	}

	public function readFileSimple($file) {
		$lines = file($file);
		$result = array();

		foreach($lines as $line_num => $line) {
			$line = $this->_removeNewLine($line);
			# omit empty rows AND segment splitter
			if ($line != '' && substr($line,0,3) !== self::_FILE_SEGMENT_SPLITTER_) {
				$exploded = explode(self::_FILE_EXPRESSION_SPLITTER_, $line);
				$result[$exploded[0]] = $exploded[1];
			}
		}

		return $result;
	}

	public function readFile($file) {
		$lines = file($file);
		$result = array();

		foreach($lines as $line_num => $line) {
			if (substr($line,0,3) === self::_FILE_SEGMENT_SPLITTER_) {
				$segment = substr($line,3);
				$segment = $this->_removeNewLine($segment);
				$result[$segment] = array();
			} else {
				$line = $this->_removeNewLine($line);
				$exploded = explode(self::_FILE_EXPRESSION_SPLITTER_,$line);
				$result[$segment][$exploded[0]] = $exploded[1];
			}
		}

		return $result;
	}
	
	static public function isNameReversed (){
		if (in_array(Zend_Registry::get('SESSION')->lang, self::$reversedLanguages)) {
			return true;
		}

		return false;
	}
}