<?php

/**
 * Directory creator class
 *
 * With this class you can:
 * - create directory with selected permission
 */
class WX_Directory_Writer {
	/**
	 * Singleton instance
	 *
	 * @var object
	 */
	protected static $WX_Directory_Writer;

	private $_defaultPermission = 0755;

	/**
	 * Create singleton instance
	 *
	 * @return object
	 */
	public static function getInstance() {
		if (self::$WX_Directory_Writer == null) {
			self::$WX_Directory_Writer = new self();
		}

		return self::$WX_Directory_Writer;
	}

	public function makeDirectory ($directory, $permission = false) {
		if ($permission === false) {
			$permission = $this->_defaultPermission;
		}
		mkdir($directory, $permission, true);
		if (!is_dir($directory)) {
			$message = 'This directory doesn\'t created: "'.$directory.'"';
			WX_Exception::create($message);
		}
	}

	public function makeDirectoryIfNotExists ($directory, $permission = false) {
		if (!is_dir($directory)) {
			$this->makeDirectory($directory, $permission);
		}
	}

	public function deleteDirectory($directory) {
		if (is_dir($directory)) {
			rmdir($directory);
		} else {
			$message = 'Try to delete directory, but this not exists: '.$directory;
			WX_Exception::create($message);
		}		
	}

	public function emptyDirectory($directory) {
		if (is_dir($directory)) {
			$it = new RecursiveDirectoryIterator($directory);
			$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($files as $file) {
				if ($file->getFilename() === '.' || $file->getFilename() === '..') {
					continue;
				}
				if ($file->isDir()){
					rmdir($file->getRealPath());
				} else {
					unlink($file->getRealPath());
				}
			}
			rmdir($directory);
		} else {
			$message = 'Try to delete directory, but this not exists: '.$directory;
			WX_Exception::create($message);
		}
	}

}


?>