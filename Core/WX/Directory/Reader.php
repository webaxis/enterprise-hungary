<?php

/**
 * Read files and directories
 *
 * With this class you can:
 * - read files and directories from a directory
 */
class WX_Directory_Reader {
	/**
	 * Get the first occurence by pattern.
	 *
	 * @param string $pattern Search pattern e.g /var/www/directory/jellyfish
	 *
	 * @return string|bool
	 */
	public static function getFirstResultByPattern($pattern) {
		$files = glob($pattern);
		if (count($files)) {
			return $files[0];
		}
		return false;
	}

	/**
	 * Get files by pattern
	 *
	 * @param string $pattern Search pattern e.g /var/www/directory/jellyfish
	 * @param string $sort Found elements order
	 *
	 * @return array
	 */
	public static function getFilesByPattern($pattern, $sort = 'date') {
		$files = glob($pattern);
		if (count($files)) {
			switch ($sort) {
				case 'date' :
					usort($files, function ($b, $a) {
						return filemtime($b) - filemtime($a);
					});
					break;
				default :
					$message = 'Unknown sort type: "'.$sort.'"';
					break;
			}
		}
		return $files;
	}
}


?>