<?php

class WX_Mail_Descriptor {

	static public function defaults($method) {

		$descriptor = array(
			'layout' => array(
				'html' => 'layout.phtml',
				'text' => 'layout.txt'
			),
			'template' => array(
				'html' => $method . '.phtml',
				'text' => $method . '.txt'
			),
			'embedImages' => true
		);

		return $descriptor;
	}	

}