<?php

class WX_Controller_Ajax extends Zend_Controller_Action {

	/**
	 * Add authentication required actions
	 */
	protected $_authRequiredActions = array();

	protected $_userClass = null;

	public function init() {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		if (!$this->_request->isXmlHttpRequest()) {
			die('Not valid AJAX call!');
		}

		if (in_array($this->_request->getActionName(),$this->_authRequiredActions)) {
			$this->_userClass = $this->_userClass == null ? APP_NAME . '_User' : $this->_userClass;
			$instance = call_user_func($this->_userClass .  '::getInstance');
			if (!$instance->isLoggedIn()) {
				$this->_forward('error', null, null, array('errorCode' => 901));
				return false;
			}
		}
	}

	public function errorAction () {
		$rp = $this->_request->getParams();

		$errorCode = isset($rp['errorCode']) ? $rp['errorCode'] : '';

		switch ($errorCode) {
			case 901:
				$message = t('ERROR_NOT_LOGGED_IN');
				break;
			default :
				$message = t('ERROR_UNEXPECTED');
				break;
		}

		$response = WX_Ajax_Response_Json::unexpectedError(array(
			'unexpectedError' => true, 
			'errorCode' => $errorCode,
			'message' => $message
		));

		die($response);
	}

	public function deletefileAction() {
		$rp = $this->_request->getParams();

		$file = UPLOAD_PATH . $rp['type'] . '/' . $rp['hash'] . '.' . $rp['ext'];
		$fileTemp = UPLOAD_TEMP_PATH . $rp['type'] . '/' . $rp['hash'] . '.' . $rp['ext'];

		if (file_exists($file)) {
			unlink($file);
		} else if (file_exists($fileTemp)) {
			unlink($fileTemp);
		}

		if (isset($rp['id'])) {
			$model = $rp['dbModel'];
			$object = new $model();
			$object->find($rp['id']);
			$setField = 'set' . $rp['dbField'];
			$object->$setField(null);
			$object->save();
		}

		$response = WX_Ajax_Response_Json::success();
		die($response);
	}

	public function uploadfileAction() {
		$rp = $this->_request->getParams();

		if (isset($_FILES['upload-file'])) {
			$file = $_FILES['upload-file'];
			$fileinfo = pathinfo($file['name']);

			$name = $fileinfo['filename'];
			$ext = $fileinfo['extension'];
			$hash = MD5(rand(5, 15) . time() . $name);

			$directory = $rp['directoryPath'];

			if (!file_exists($directory)) {
				WX_Directory_Writer::getInstance()->makeDirectory($directory, 0777);
			}

			if (move_uploaded_file($file['tmp_name'], $directory . '/' . $hash . '.' . $ext)) {
				$type = $rp['type'];
				$file = $name . '.' .$ext;
				$path = UPLOAD_TEMP_URL . $type;
				$responseData = array(
					'file' 			=> $file,
					'name'			=> $name,
					//'downloadLink'=> $rp['downloadScript'] . '/' . $type . '/' . $hash . '/' . $name . '/' . $ext,
					'downloadLink'	=> $path . '/' . $hash . '.' . $ext,
					'hash' 			=> $hash,
					'ext'			=> $ext
				);

				$response = WX_Ajax_Response_Json::success($responseData);
			} else {
				$message = t('ERROR_UNEXPECTED');
				$response = WX_Ajax_Response_Json::error($message);
			}

			die($response);
		}
	}

	public function uploadtempimageAction() {
		$this->uploadimageAction(true);
	}

	public function uploadimageAction($isTemp = false) {
		$rp = $this->_request->getParams();

		$type = WX_Tools::getValue($rp, 'type');

		$response = WX_Ajax_Response_Json::error('-1');
		
		if (isset($_FILES['upload-image'])) {
			$file 	= $_FILES['upload-image'];
			if (WX_Tools::isValidImage($file['tmp_name'])) {
				$image 	= new WX_Image($type);

				if ($isTemp) {
					$image->uploadTemp($file);
				} else {
					$image->upload($file);
				}

				if ($image->hasErrors()) {
					$response = WX_Ajax_Response_Json::error($image->getErrors());
				} else {
					$feedbackSize = isset($rp['size']) ? $rp['size'] : false;
					$responseData = $image->get($feedbackSize);
					$response = WX_Ajax_Response_Json::success($responseData);
				}
			}

			die($response);
		}
	}

	public function uploadmultitempimageAction() {
		$this->uploadmultiimageAction(true);
	}

	public function uploadmultiimageAction($isTemp = false) {
		$rp = $this->_request->getParams();

		$type = $rp['type'];
		$feedbackSize = $rp['size'];

		$response = array();

		foreach ($_FILES as $file) {
			if (WX_Tools::isValidImage($file['tmp_name'])) {
				$image 	= new WX_Image($type);

				if ($isTemp) {
					$image->uploadTemp($file);
				} else {
					$image->upload($file);
				}

				if (!$image->hasErrors()) {
					$responseData = $image->get($feedbackSize);
					$response[] = $responseData;
				}
			}
		}

		if (count($response)) {
			$responseObj = WX_Ajax_Response_Json::success($response);
		} else {
			$responseObj = WX_Ajax_Response_Json::error();
		}

		die($responseObj);
	}

	public function deleteimageAction() {
		$rp = $this->_request->getParams();
		$response = WX_Ajax_Response_Json::success();

		if (WX_Tools::getValue($rp, 'type', false) === false) {
			$message = 'Missing "type" param';
			WX_Exception::create($message);

			return;
		}
		if (WX_Tools::getValue($rp, 'hash', false) === false) {
			$message = 'Missing "hash" param';
			WX_Exception::create($message);

			return;
		}
		$type = WX_Tools::getValue($rp, 'type');
		$hash = WX_Tools::getValue($rp, 'hash');
		$temp = WX_Tools::getValue($rp, 'temp', false);

		$image = WX_Image::getInstance($type);

		if ($temp === false) {
			$image->delete($hash);
		} else {
			$image->deleteTemp($hash);
		}

		$response = WX_Ajax_Response_Json::success();

		die($response);
	}

	public function fbconnectAction () {
		$rp = $this->_request->getParams();

		$fb = new WX_Facebook;
		$fb->setAccessToken($rp['accessToken']);

		$class = APP_NAME . '_User';
		$user = new $class();
		if ($fb->isConnected()) {
			$fb->setExtendedAccessToken();
			$accessToken = $fb->getAccessToken();

			$details = $fb->getDetails();

			if (empty($details)) {
				$message = t('CONTROLLER_ERROR_COULDNT_CONNECT_TO_FB');
				$response = WX_Ajax_Response_Json::error($message);
			} else {
				$user->setUser($details['email'], 'email');

				if ($user->emailExists($details['email'])) {
					$isNew = 0;
					$user->updateDetails(array(
						'fbId'			=> $details['id'],
						'firstname'		=> $details['first_name'],
						'lastname'		=> $details['last_name'],
						'gender'		=> $details['gender'] == 'male' ? 1 : 2,
						'fbUrl'			=> $details['link'],
						'locale'		=> $details['locale'],
						'timezone'		=> $details['timezone'],
						'activated'		=> 1
					));
				} else {
					$isNew = 1;
					$user->register(array(
						'fbId'			=> $details['id'],
						'email'			=> $details['email'],
						'name'			=> $details['name'],
						'firstname'		=> $details['first_name'],
						'lastname'		=> $details['last_name'],
						'gender'		=> $details['gender'] == 'male' ? 1 : 2,
						'fbUrl'			=> $details['link'],
						'locale'		=> $details['locale'],
						'timezone'		=> $details['timezone'],
						'password'		=> WX_Tools::generateRandomString()
					));
					$user->setUser($details['email'], 'email');
				}

				$login = $user->loginInstance();

				if ($login !== true) {
					$error = 1;
					$message = t('CONTROLLER_ERROR_COULDNT_CONNECT_TO_FB');
					$response = WX_Ajax_Response_Json::error($message);
				} else {
					$success = 1;
					$response = WX_Ajax_Response_Json::success();
				}
			}
		} else {
			$message = t('CONTROLLER_ERROR_COULDNT_CONNECT_TO_FB');
			$response = WX_Ajax_Response_Json::error($message);
		}

		die(json_encode($response));
	}
}