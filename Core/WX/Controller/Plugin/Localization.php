<?php

class WX_Controller_Plugin_Localization extends Zend_Controller_Plugin_Abstract {

	public function preDispatch (Zend_Controller_Request_Http $request) {
		$localeClass = APP_NAME . '_Locale';
		$localeClassInstance = $localeClass::getInstance();

		$rp = $request->getParams();

		$refresh = false;

		# if cookie exists and it's not a language change
		if (isset($_COOKIE['language']) && !isset($rp['language'])) {
			$language = $_COOKIE['language'];
			$locale = new Zend_Locale($language);
		} else {
			# if it's a language change
			if (isset($rp['language']) && !empty($rp['language'])) {
				$language = $rp['language'];
				
				# set uri for refreshing (because we don't want keep &language=... in the url)
				$refresh = true;
				$uri = Zend_Uri::factory(BASE_HOST . $request->getRequestUri());

				$queryParts = explode('&',$uri->getQuery());

				$query = '';
				if (count($queryParts) > 1) {
					$query = '?';
					$newQueryParts = array();
					foreach ($queryParts as $part) {
						if (!strstr($part, 'language')) {
							$newQueryParts[] = $part;
						}
					}
					$query .= implode('&', $newQueryParts);
				}
				DX($query);

				$url = str_replace(array('http:/','https:/'),'http://',preg_replace('#/+#','/',BASE_HOST . $uri->getPath() . $query));

			# if it's just a simple load
			} else {
				$language = 'auto';
			}

			# we set the zend locale
			$locale = new Zend_Locale($language);
		
			# if that locale is not available in our project we set to default
			if (!$localeClassInstance->languageExists($locale->getLanguage())) {
				$locale = new Zend_Locale($localeClassInstance->getDefaultLanguage() . '_' . $locale->getRegion());
			}
			
			# save the language to a cookie
			setcookie('language', $locale->getLanguage(), time() + (3600 * 24 * 365), '/');
		}
		
		# passing locale to registry
		Zend_Registry::set('Zend_Locale',$locale);
		
		# setting locale to Zend_Locale_Format
		Zend_Locale_Format::setOptions(array(
			'locale' => $locale->toString()
		));
		
		# if it was a language change we refresh the page
		if ($refresh) {
			header("Location: $url");
		}
	}
}