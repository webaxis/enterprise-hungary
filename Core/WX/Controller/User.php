<?php

class WX_Controller_User extends Zend_Controller_Action {

	public function init() {
		$rp = $this->_request->getParams();

		$userClassName	= APP_NAME. '_User';

		if ($userClassName->isLoggedIn()) {
			$this->view->changepasswordForm = WX_DForm::getInstance('changepassword')->get($rp);
		} else {
			$this->view->registrationForm = WX_DForm::getInstance('registration')->get($rp);
			$this->view->loginForm = WX_DForm::getInstance('login')->get($rp);
			$this->view->forgottenpasswordForm = WX_DForm::getInstance('forgottenpassword')->get($rp);
			$this->view->resendactivationForm = WX_DForm::getInstance('resendactivation')->get($rp);
		}
	}

	public function activationAction() {
		$rp = $this->_request->getParams();

		$userClassName	= APP_NAME. '_User';

		$activationSuccess = $userClassName::getInstance($rp['activationCode'],'activationCode')->activate($rp['activationCode'], false);

		if ($activationSuccess) {
			$this->renderScript('user/activation-success.phtml');
		} else {
			$this->renderScript('user/activation-failed.phtml');
		}
	}

	public function newpasswordAction() {
		$userClassName	= APP_NAME. '_User';

		$rp = $this->_request->getParams();

		if ($this->_request->isPost()) {
			$form =  WX_DForm::getInstance('newpassword')->get($rp);
			if ($form->isValid($rp)) {
				$valid = $userClassName::getInstance()->isValidTempPassword($rp['tempPassword']);
				if ($valid) {
					$userClassName::getInstance()->newPasswordByTempPassword($rp['password'], $rp['tempPassword']);
					$url = call_user_func(APP_NAME. '_Url::index');
					$this->_redirect($url);
				} else {
					$this->view->newpasswordForm = $form;	
				}
			} else {
				$this->view->newpasswordForm = $form;
			}
		} else {
			$valid = $userClassName::getInstance()->isValidTempPassword($rp['tempPassword']);
			$this->view->newpasswordForm = WX_DForm::getInstance('newpassword')->get($rp);
			if ($valid) {
				$this->renderScript('user/newpassword.phtml');
			} else {
				$this->renderScript('user/newpassword-failed.phtml');
			}
		}
	}

	public function logoutAction() {
		$userClassName	= APP_NAME. '_User';
		
		$userClassName::getInstance()->logout();
		
		$url = call_user_func(APP_NAME. '_Url::index');
		$this->_redirect($url);
	}
}