<?php

class WX_Controller_Form extends Zend_Controller_Action {

	protected $_authRequiredDescriptors = array();

	protected $_userClass = null;

	public function init() {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();
		if (!$this->_request->isXmlHttpRequest()) {
			die('Not valid AJAX call!');
		}

		$rp = $this->_request->getParams();
		$descriptor = WX_Tools::getValue($rp,'d');

		if (!array_key_exists($descriptor, $this->_allowed)) {
			die(WX_Ajax_Response_Json::response(array('message'=>'Not allowed action')));
		}

		if (in_array($descriptor,$this->_authRequiredDescriptors)) {
			$this->_userClass = $this->_userClass == null ? APP_NAME . '_User' : $this->_userClass;
			if (!$this->_userClass . '::' . getInstance()->isLoggedIn()) {
				$this->_forward('error', null, null, array('errorCode' => 901));
				return false;
			}
		}
	}

	public function errorAction () {
		$rp = $this->_request->getParams();

		$errorCode = isset($rp['errorCode']) ? $rp['errorCode'] : '';

		switch ($errorCode) {
			case 901:
				$message = t('ERROR_NOT_LOGGED_IN');
				break;
			default :
				$message = t('ERROR_UNEXPECTED');
				break;
		}

		$response = WX_Ajax_Response_Json::unexpectedError(array(
			'unexpectedError' => true, 
			'errorCode' => $errorCode,
			'message' => $message
		));

		die($response);
	}

	public function validateAction() {
		$rp = $this->_request->getParams();
		$descriptor = WX_Tools::getValue($rp,'d');
		$isReturn = WX_Tools::getValue($rp,'r');

		$validate = new WX_Form_Validate();
		$validate->setDescriptor($this->_allowed[$descriptor])
				 ->setFormDescriptorName($descriptor)
				 ->setParams($rp)
				 ->setIsReturn($isReturn);

		$response = $validate->validate($descriptor);

		die(WX_Ajax_Response_Json::response($response));
	}

}