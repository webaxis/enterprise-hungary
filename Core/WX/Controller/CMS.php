<?php

class WX_Controller_CMS extends WX_Controller {

	protected $_descriptor = array();

	protected $_userClass = null;

	protected $_checkAuth = false;

	public function init() {
		if ($this->_checkAuth) {
			$this->_userClass = $this->_userClass == null ? APP_NAME . '_User' : $this->_userClass;
			if (!$this->_userClass . '::' . getInstance()->isLoggedIn()) {
				$this->_forward('error', null, null, array('errorCode' => 901));
				return false;
			}
		}
		parent::init();
	}

	public function errorAction () {
		$rp = $this->_request->getParams();

		$errorCode = isset($rp['errorCode']) ? $rp['errorCode'] : '';

		switch ($errorCode) {
			case 901:
				$message = t('ERROR_NOT_LOGGED_IN');
				break;
			default :
				$message = t('ERROR_UNEXPECTED');
				break;
		}

		$response = WX_Ajax_Response_Json::unexpectedError(array(
			'unexpectedError' => true, 
			'errorCode' => $errorCode,
			'message' => $message
		));

		die($response);
	}

	public function listAction () {
		$rp = $this->_request->getParams();

		$cms = new WX_CMS(($rp['module'] == 'default' ? '' : $rp['module']) . $rp['controller']);
		$cms->setRequestParams($rp);

		if (!$cms->isTemplate('list')) {
			$this->view->list = $cms->fetchList();
			return;
		}

		$this->_helper->viewRenderer->setNoRender(true);
		$cms->renderListPage();
	}

	public function editAction () {
		$rp = $this->_request->getParams();

		$cms = new WX_CMS(($rp['module'] == 'default' ? '' : $rp['module']) . $rp['controller']);
		$cms->setRequestParams($rp);
		$cms->setId(WX_Tools::getValue($rp, 'id', null));

		if (!$cms->isTemplate('form')) {
			$this->view->form = $cms->fetchForm();
			return;
		}

		$this->_helper->viewRenderer->setNoRender(true);
		$cms->renderFormPage();
	}

	public function saveformAction() {
		$rp = $this->_request->getParams();
		$descriptorName = WX_Tools::getValue($rp,'d');
		$isReturn = WX_Tools::getValue($rp,'r');
		$id = WX_Tools::getValue($rp,'id');

		$cms = new WX_CMS(($rp['module'] == 'default' ? '' : $rp['module']) . $rp['controller']);
		$cms->setRequestParams($rp);
		if (!$cms->isWysiwyg()) {
			$validate = new WX_Form_Validate();
			$validate->setFormDescriptorName($descriptorName)
					 ->setParams($rp)
					 ->setEmptyFormOnSuccess($id ? false : true)
					 ->setIsReturn($isReturn);

			$response = $validate->validate();
		}

		if (isset($response) && $response['status'] == 'error') {
			die(WX_Ajax_Response_Json::error($response['data']));
		} else {
			// implement save here
			$cms->setId($id);
			$cms->saveEntity();

			/** 
			 * @todo: image hack
			 *
			 * Ez a szekcio azert kell, mert a kep feltoltese a $response
			 * kikerese utan tortenik, es ezert a response-ban levo image
			 * meg a regi image-re mutat - ami menet kozben torolve lesz
			 */

			if (!$cms->isWysiwyg()) {
				$validate = new WX_Form_Validate();
				$validate->setFormDescriptorName($descriptorName)
						 ->setParams($rp)
						 ->setEmptyFormOnSuccess($id ? false : true)
						 ->setIsReturn($isReturn);
				$response = $validate->validate();
			}

			if ($id) {
				$response['data']['message'] = $cms->getFormFeedback('edit');
			} else {
				$response['data']['message'] = $cms->getFormFeedback('add');
			}

			if ($cms->isTranslate()) {
				$options = $cms->getOptions();

				if (isset($options['fileGeneratorMethod']) && isset($options['fileGeneratorMethod'])) {
					$options['fileGeneratorClass']::$options['fileGeneratorMethod']();
				}
			}
			
			die(WX_Ajax_Response_Json::success($response['data']));
		}
	}
	
	public function deleteentityAction () {
		$this->_helper->viewRenderer->setNoRender(true);

		$rp = $this->_request->getParams();
		
		$id 			= WX_Tools::getValue($rp,'id');
		$descriptor 	= WX_Tools::getValue($rp,'descriptor');

		if (!$id || !$descriptor) {
			return false;
		}

		$cms = new WX_CMS($descriptor);
		$cms->setId($id);
		$cms->deleteEntity();

		die(WX_Ajax_Response_Json::success(array('message'=>t('FEEDBACK_SUCCESS_CMS_DELETE'))));
	}
	
	public function activateentityAction () {
		$rp = $this->_request->getParams();
		
		$id 			= WX_Tools::getValue($rp,'id');
		$descriptor 	= WX_Tools::getValue($rp,'descriptor');

		if (!$id || !$descriptor) {
			return false;
		}

		$cms = new WX_CMS($descriptor);
		$cms->setId($id);
		$cms->activateEntity();

		die(WX_Ajax_Response_Json::success(array('message'=>t('FEEDBACK_SUCCESS_CMS_ACTIVATION'))));
	}
	
	public function inactivateentityAction () {
		$rp = $this->_request->getParams();
		
		$id 			= WX_Tools::getValue($rp,'id');
		$descriptor 	= WX_Tools::getValue($rp,'descriptor');

		if (!$id || !$descriptor) {
			return false;
		}

		$cms = new WX_CMS($descriptor);
		$cms->setId($id);
		$cms->inactivateEntity();

		die(WX_Ajax_Response_Json::success(array('message'=>t('FEEDBACK_SUCCESS_CMS_INACTIVATION'))));
	}

	public function archiveentityAction () {
		$rp = $this->_request->getParams();

		$id 			= WX_Tools::getValue($rp,'id');
		$descriptor 	= WX_Tools::getValue($rp,'descriptor');

		if (!$id || !$descriptor) {
			return false;
		}

		$cms = new WX_CMS($descriptor);
		$cms->setId($id);
		$cms->archiveEntity();

		die(WX_Ajax_Response_Json::success(array('message'=>t('FEEDBACK_SUCCESS_CMS_ARCHIVATION'))));
	}

	public function dearchiveentityAction () {
		$rp = $this->_request->getParams();

		$id 			= WX_Tools::getValue($rp,'id');
		$descriptor 	= WX_Tools::getValue($rp,'descriptor');

		if (!$id || !$descriptor) {
			return false;
		}

		$cms = new WX_CMS($descriptor);
		$cms->setId($id);
		$cms->dearchiveEntity();

		die(WX_Ajax_Response_Json::success(array('message'=>t('FEEDBACK_SUCCESS_CMS_DEARCHIVATION'))));
	}

	public function changeorderAction () {
		$rp = $this->_request->getParams();
		
		$id 			= WX_Tools::getValue($rp,'id');
		$descriptor 	= WX_Tools::getValue($rp,'descriptor');
		$newOrder 		= WX_Tools::getValue($rp,'order');

		if (!$id || !$descriptor || !$newOrder) {
			return false;
		}

		$cms = new WX_CMS($descriptor);
		$cms->setId($id);
		$cms->changeOrder($newOrder);
		
		die(WX_Ajax_Response_Json::success(array('message'=>t('FEEDBACK_SUCCESS_CMS_ORDER'))));
	}
}