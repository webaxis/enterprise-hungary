<?php

class WX_Model {

	static protected $_defaults = [
		'order' => '', // sima stringkent megadott sql order parancs
		'where' => '', // sima stringkent megadott sql where parancs			
	];

	static public function getInstance () {
		$class = get_called_class();

		return new $class();
	}

	static public function getAllQuery ($options = array()) {
		$settings = array_replace_recursive(self::$_defaults, $options);

		$modelClass = get_called_class();
		$model = new $modelClass();

		$name = $model->getMapper()->getDbTable()->_name;

		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from($name);

		if ($settings['where'] != '') {
			$query->where($settings['where']);
		}
		if ($settings['order'] != '') {
			$query->order($settings['order']);
		}

		//DX($query->__toString());
		
		return $query; 
	}

	static public function getAll($options = array()) {
		$db = Zend_Registry::get('db');
		$query = self::getAllQuery($options);

		$stmt = $db->query($query);
		$result = $stmt->fetchAll();

		return $result; 
	}

}