<?php

class WX_Application_Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

	protected function _initAll() {
		$this->_iSession();
		$this->_iConfig();
		$this->_iDatabase();
		$this->_iViewHelpers();
		$this->_iOwnRouter();
		$this->_iCompressors();
		$this->_iLocale();
		$this->_iTranslator();
	}

	protected function _iConfig() {
		$config = new Zend_Config_Xml(
			Zend_Registry::get("config_file_path"),
			APP_ENV
		);
		Zend_Registry::set('config', $config);
		return $config;
	}

    protected function _iSession() {
        $req = new Zend_Controller_Request_Http();
        if ($req->get('PHPSESSIONID')) {
        	Zend_Session::setId($req->get('PHPSESSIONID'));
        }
        $session = new Zend_Session_Namespace(APP_NAME);
        Zend_Registry::set('SESSION', $session);
    }

	protected function _iViewHelpers() {
		$this->bootstrap('layout');
		$layout = $this->getResource('layout');
		$view = $layout->getView();
		$view->addHelperPath('WX/View/Helper', 'WX_View_Helper');
		ZendX_JQuery::enableView($view);
	}

	protected function _iOwnRouter() {
		$this->bootstrap('FrontController');
		$front = $this->getResource('FrontController');
		$router = APP_NAME.'_Router';
		$front->registerPlugin(new $router(),1);
    }

	protected function _iDatabase() {
		$cfg = Zend_Registry::get('config');
		if ($cfg->resources->db) {
			$this->bootstrap('db');
			$db = $this->getResource('db');

			Zend_Registry::set('db', $db);
			$db->query( 'SET NAMES UTF8;');
		}
	}

	protected function _iCompressors() {
		$config = Zend_Registry::get('config');
		// if not available the compressor, return
		if ($config->compressor == null) {
			return;
		}

		// check js or css compressor is active
		if ($config->compressor->js != NULL || $config->compressor->css != NULL) {
			$this->bootstrap('layout');
			$layout = $this->getResource('layout');
			$view = $layout->getView();
			// check js compressor is active
			if ($config->compressor->js == "true" ) {
				$view->getHelper('ScriptCompressor')
					->activate()
					->setCacheDir(CACHE_PATH . 'js')
					->setDocRoot(PUBLIC_PATH)
					->setUrlPrefix(CACHE_URL . 'js/')
						/* @TODO: THESE FUNCTIONS ARE CURRENTLY UNAVAILABLE!
							->setUseMinify(true)
							->setMinifyCommand('java -jar yuicompressor -o :filename')
							->setUseGzip(true)
							->setGzipLevel(9)
						*/
				;
			}
			if ($config->compressor->css == "true" ) {
				$view->getHelper('LinkCompressor')
					->activate()
					->setCacheDir(CACHE_PATH . 'stylesheets')
					->setDocRoot(PUBLIC_PATH)
					->setUrlPrefix(CACHE_URL . 'stylesheets/');
			}
			$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
			$viewRenderer->setView($view);
		}
	}

	protected function _iLocale() {
        $request = new Zend_Controller_Request_Http();
		$localeClass = APP_NAME . '_Locale';
		$localeClassInstance = $localeClass::getInstance();

		$rp = $request->getParams();

		$refresh = false;

		# if cookie exists and it's not a language change
		if (isset($_COOKIE['language']) && !isset($_GET['language'])) {
			$language = $_COOKIE['language'];
			$locale = new Zend_Locale($language);
		} else {
			# if it's a language change
			if (isset($_GET['language']) && !empty($_GET['language'])) {
				$language = $_GET['language'];

				# set uri for refreshing (because we don't want keep &language=... in the url)
				$refresh = true;
				$uri = Zend_Uri::factory(BASE_HOST . $request->getRequestUri());
				$path = implode('/',array_unique(explode('/', $uri->getPath()))); # remove "//" duplication
				if (substr($path, 0, 1) == '/') {
					$path = substr($path, 1);
				}
				$paths = explode('/', $path);

				$currentLanguage = isset($_COOKIE['language']) ? $_COOKIE['language'] : $localeClassInstance->getDefaultLanguage();

				foreach ($paths as &$one) {
					$one = WX_Translator::translateUrl($one, $currentLanguage, $language);
				}

				$path = implode('/', $paths);

				$queryParts = explode('&',$uri->getQuery());

				$query = '';
				if (count($queryParts) > 1) {
					$query = '?';
					$newQueryParts = array();
					foreach ($queryParts as $part) {
						if (!strstr($part, 'language')) {
							$newQueryParts[] = $part;
						}
					}
					$query .= implode('&', $newQueryParts);
				}

				$url = str_replace(array('http:/','https:/'),'http://',preg_replace('#/+#','/',BASE_HOST . $path . $query));
				//$url = str_replace(array('http:/','https:/'),'http://',preg_replace('#/+#','/',BASE_HOST . $uri->getPath() . $query));

			# if it's just a simple load
			} else {
				$class = APP_NAME . '_Locale';
    			$language = $class::getInstance()->getDefaultLanguage();
			}

			# we set the zend locale
			$locale = new Zend_Locale($language);

			# if that locale is not available in our project we set to default
			if (!$localeClassInstance->languageExists($locale->getLanguage())) {
				$locale = new Zend_Locale($localeClassInstance->getDefaultLanguage() . '_' . $locale->getRegion());
			}

			# save the language to a cookie
			setcookie('language', $locale->getLanguage(), time() + (3600 * 24 * 365), '/');
		}

		# passing locale to registry
		Zend_Registry::set('Zend_Locale', $locale);

		# passing language to registry
		Zend_Registry::set('language', $language);

		# setting locale to Zend_Locale_Format
		Zend_Locale_Format::setOptions(array(
			'locale' => $locale->toString()
		));

		# if it was a language change we refresh the page
		if ($refresh) {
			header("Location: $url");
		}
	}

	protected function _iTranslator() {
		$appLocale = APP_NAME . '_Locale';
        $language = Zend_Registry::get('language');

		WX_Translator::setToRegistry($language);
	}

}