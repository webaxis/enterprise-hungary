<?php

class WX_Converter {

	/**
	 * Az adatok kozul kiszedi a kulcsokat, es azokat allitja be 
	 * a visszateresi adatok kulcs => adat formaban
	 */
	static public function idToKey($data, $id = 'id') {
		$return = array();

		if (!empty($data)) {
			foreach ($data as $one) {
				$return[$one[$id]] = $one;
			}
		}

		return $return;
	}

	/**
	 * Az adathalmazbol <option id="xxx">yyy</option> tombot formaz.
	 *
	 * $data: maga a nyers tomb
	 * $value: a $data tombben az a kulcs, ami majd az "yyy" erteket kepviseli
	 * $id: a $data tombben az a kulcs, ami majd az id lesz ("xxx")
	 */
	static public function createOptionItems($data, $value, $id = 'id') {
		$return = '';

		foreach ($data as $one) {
			$return.= '<option value="' . $one[$id] . '">' . $one[$value] . '</option>';
		}

		return $return;
	}

}