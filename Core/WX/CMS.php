<?php

class WX_CMS {

	protected $_module = null;

	protected $_controller = null;

	protected $_descriptorName = null;

	protected $_descriptor = array();

	protected $_id = null;

	protected $_requestParams = array();

	protected $_requiredDescriptorListFields = array(
		'listDescriptorName',
		'listId'
	);

	// @todo: megcsinálni, hogy wysiwyg esetében ne legyen kötelező
	protected $_requiredDescriptorFormFields = array(
		//'formDescriptorName',
		//'formId',
		'data' => array(
			'dbTable',
			//'params'
		)
	);

	// @todo: megcsinálni, hogy wysiwyg esetében ne legyen kötelező
	protected $_requiredDescriptorWysiwygFields = array(
		'data' => array(
			'dbTable',
			'columnName'
		)
	);

	protected $_defaults = array();

	public function __construct($descriptorName = null) {
		$this->_descriptorName = $descriptorName;

		$this->_initDefaults();

		if ($this->_descriptorName) {
			$descriptorClassName = APP_NAME . '_CMS_Descriptor';
			$descriptorMethodName = lcfirst($this->_descriptorName);

			$this->setDescriptor(call_user_func(array($descriptorClassName,$descriptorMethodName)));
		}
	}

	protected function _initDefaults() {
		$this->_defaults = array(
			'list' => array(
				'buttons' => array(),

	    		# false or array with params,
	    		# if false action template is used
				'template' => array(
	    			# false or title name, if false it's hidden
	    			'title' => '',
	    			# if false wx list will not be inited
	    			'initList' => true,
	    			'directory' => WX_PATH . 'CMS/templates',
	    			'template' => 'list.phtml'
	    		)
			),
			'form' => array(
				# form or wysiwyg
				'type' => 'form',

				'buttons' => array(),

				# this is the url that wx.form.validate calls to validate data,
				# MUST end with /saveform - to use CMS saving
				# default is module/controller/saveform
				# if module is 'default' only /controller/saveform
				'saveUrl' => false,

	    		# false or array with params,
	    		# if false action template is used
				'template' => array(
	    			# false or title name, if false it's hidden
	    			'title' => array(
	    				'add' => 'Add',
	    				'edit' => 'Edit'
	    			),
	    			# if false wx list will not be inited
	    			'initForm' => true,
	    			'saveButtonId' => 'save-button',
	    			'directory' => WX_PATH . 'CMS/templates',
	    			'template' => false,
	    			'ckconfig' => JS_URL . 'admin/ckeditor_config.js'
				),
				'data' => array(
					'params' => array(
						# Only column names should be listed.
						# Exceptions, when it should be an array:
						# - order: array(
						#	'type' => 'order',
						#	'orderDescriptorName' => '...', (name of the descriptor)
						#	'orderColumn' => '...' (name of the column that represents the order)
						# )
						#
						# @todo: a többit is kitalálni
					),
					'feedback' => array(
						# popup or notification
						'type' => 'popup',
						'add' => t('FEEDBACK_SUCCESS_SAVE_ADD_FORM'),
						'edit' => t('FEEDBACK_SUCCESS_SAVE_EDIT_FORM'),
					),
					'toFormConverter' => false,
					'toDbConverter' => false
				)
			)
		);
	}

	public function setDescriptor ($descriptor) {
		if (isset($descriptor['list'])) {
			$this->_checkRequiredFields($this->_requiredDescriptorListFields, $descriptor['list'], 'list');
		}

		if (isset($descriptor['form'])) {
			if (isset($descriptor['form']['type']) && $descriptor['form']['type'] == 'wysiwyg') {
				$this->_checkRequiredFields($this->_requiredDescriptorWysiwygFields, $descriptor['form'], 'form');
			} else {
				$this->_checkRequiredFields($this->_requiredDescriptorFormFields, $descriptor['form'], 'form');
			}
		}

		$this->_descriptor = array_replace_recursive($this->_defaults, $descriptor);
	}

	public function setRequestParams ($requestParams) {
		$this->_requestParams = $requestParams;
	}

	public function setId ($id) {
		$this->_id = $id;
	}

	public function isTemplate($type) {
		if (
			(isset($this->_descriptor[$type]['template'])
			&& !empty($this->_descriptor[$type]['template']))
			||
			(!isset($this->_descriptor[$type]['template']))
		) {
			return true;
		}

		return false;
	}

	public function fetchList() {
		$listClassName = APP_NAME . '_List_Descriptors_' . ucfirst($this->_descriptor['list']['listDescriptorName']);
		return $listClassName::get($this->_requestParams);
	}

	public function renderListPage() {
		$list = $this->fetchList();

		$templateDir = $this->_descriptor['list']['template']['directory'];
		$template = $this->_descriptor['list']['template']['template'];

		echo WX_Html::get($template,array(
			'list' => $list,
			'listId' => $this->_descriptor['list']['listId'],
			'title' => $this->_descriptor['list']['template']['title'],
			'buttons' => $this->_descriptor['list']['buttons'],
			'init' => $this->_descriptor['list']['template']['initList'],
			'cmsDescriptorName' => $this->_descriptorName
		),$templateDir);
	}

	public function fetchForm() {
		$formParams = $this->_requestParams;

		if ($this->_id) {
			$model = $this->_getModel();
			$entityDetails = $model->find($this->_id)->toArray();
			$formParams = $this->_convertEntityDetailsToForm($entityDetails);
		}

		$formDescriptorName = $this->_descriptor['form']['formDescriptorName'];
		return WX_DForm::getInstance($formDescriptorName)->get($formParams);
	}

	public function renderFormPage() {
		$templateDir = $this->_descriptor['form']['template']['directory'];
		if ($this->_descriptor['form']['type'] == 'form') {
			$template = $this->_descriptor['form']['template']['template'] == false ? 'form.phtml' : $this->_descriptor['form']['template']['template'];
		} else {
			$template = $this->_descriptor['form']['template']['template'] == false ? 'wysiwyg.phtml' : $this->_descriptor['form']['template']['template'];
		} 

		$saveUrl = $this->_descriptor['form']['saveUrl'];

		if (!$saveUrl) {
			$saveUrl = '/';
			if ($this->_requestParams['module'] != 'default') {
				$saveUrl .= $this->_requestParams['module'] . '/';
			}
			$saveUrl .= $this->_requestParams['controller'] . '/saveform';
		} else {
			if (strstr($saveUrl, 'saveform')) {
				WX_Exception::create('SaveUrl param must end with \'saveform\' to perform CMS saving.');
			}
		}

		$localeClassName = APP_NAME . '_Locale';
		$locale = $localeClassName::getInstance();
		$languages = $locale->getLanguages();
		$language = $locale->getDefaultLanguage();

		$templateParams = array(
			'buttons' => $this->_descriptor['form']['buttons'],
			'init' => $this->_descriptor['form']['template']['initForm'],
			'entityId' => $this->_id,
			'saveUrl' => $saveUrl,
			'title' => $this->_descriptor['form']['template']['title'],
			'id' => $this->_id,
			'saveButtonId' => $this->_descriptor['form']['template']['saveButtonId']
		);

		switch ($this->_descriptor['form']['type']) {
			case 'form':
				$templateParams['form'] = $this->fetchForm();
				$templateParams['formId'] = $this->_descriptor['form']['formId'];
				$templateParams['formDescriptorName'] = $this->_descriptor['form']['formDescriptorName'];
				$templateParams['imageFields'] = $this->_getImageFields();
				break;

			case 'wysiwyg':
				$templateParams['formId'] = isset($this->_descriptor['form']['formId']) ? $this->_descriptor['form']['formId'] : 'wysiwyg-form';
				$templateParams['languages'] = $languages;
				$templateParams['language'] = $language;
				$templateParams['fieldName'] = $this->_descriptor['form']['data']['columnName'];
				$templateParams['contents'] = $this->_getWysiwygContents();
				$templateParams['ckconfig'] = $this->_descriptor['form']['template']['ckconfig'];
				break;
		}

		echo WX_Html::get($template,$templateParams,$templateDir);
	}

	public function isWysiwyg () {
		if ($this->_descriptor['form']['type'] == 'wysiwyg') {
			return true;
		}

		return false;
	}

	public function isTranslate () {
		if (isset($this->_descriptor['type']) && $this->_descriptor['type'] == 'translate') {
			return true;
		}

		return false;
	}

	public function getOptions() {
		if (isset($this->_descriptor['options'])) {
			return $this->_descriptor['options'];
		}
	}

	protected function _getWysiwygContents () {
		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from($this->_descriptor['form']['data']['dbTable'])
					->where('id = ' . $this->_id);
		$result = $db->query($query)->fetch();

		$localeClassName = APP_NAME . '_Locale';
		$locale = $localeClassName::getInstance();
		$languages = $locale->getLanguages();

		$return = array();
		foreach ($languages as $iso => $one) {
			$return[$iso] = $result[$this->_descriptor['form']['data']['columnName'] . '_' . $iso];
		}

		return $return;
	}

	public function _getImageFields () {
		$imageFields = $this->_searchParamType('image');

		$return = array();
		foreach ($imageFields as $field) {
			$return[$field] = $this->_descriptor['form']['data']['params'][$field];
		}

		return $return;
	}

	public function getFormFeedback($type) {
		return $this->_descriptor['form']['data']['feedback'][$type];
	}

	public function saveEntity () {
		$entityDetails = $this->_convertEntityDetailsToDb($this->_requestParams);

		$model = $this->_getModel();

		if ($this->_id) {
			$model->find($this->_id);
		}

		if ($this->isWysiwyg()) {
			$columnName = $this->_descriptor['form']['data']['columnName'];

			$localeClassName = APP_NAME . '_Locale';
			$locale = $localeClassName::getInstance();
			$languages = $locale->getLanguages();

			foreach ($languages as $iso => $one) {
				$setFunctionName = 'set' . ucfirst($columnName) . ucfirst($iso);
				$model->$setFunctionName($entityDetails[$columnName . '_' . $iso]);
			}
		}

		foreach ($this->_descriptor['form']['data']['params'] as $key => $param) {
			if (!is_array($param)) {
				if (array_key_exists($param, $entityDetails)) {
					$setFunctionName = 'set' . ucfirst($param);
					$model->$setFunctionName($entityDetails[$param]);
				}
			} else {
				switch ($param['type']) {

					case 'order':
						$orderClassName = APP_NAME . '_Order';
						$order = new $orderClassName($this->_descriptor['form']['data']['params'][$key]['orderDescriptorName']);
						$orderDescriptor = $order->getDescriptor();
						$searchParamList = $order->getSearchParamList();
						$searchParamsArray = array();

						if (!empty($searchParamList)) {
							if ($this->_id) {

								// if it's an existing item and one of the search params changed
								// we have to align the elements orders
								$searchParamChanged = false;

								$originalModelName = APP_NAME . '_Model_' . ucfirst($this->_descriptor['form']['data']['dbTable']);
								$originalModel = new $originalModelName();
								$originalModel->find($this->_id);

								foreach ($searchParamList as $searchParam) {
									$paramGetFunctionName = 'get' . ucfirst($searchParam);
									if ($entityDetails[$searchParam] != $originalModel->$paramGetFunctionName()) {
										$searchParamChanged = true;
									}
								}

								if ($searchParamChanged) {
									foreach ($searchParamList as $searchParam) {
										$paramGetFunctionName = 'get' . ucfirst($searchParam);
										$searchParamsArray[$searchParam] = $originalModel->$paramGetFunctionName();
									}
									$order->setSearchParams($searchParamsArray);

									$order->removeOrder($this->_id);
								}
							}

							$searchParamsArray = array();
							foreach ($searchParamList as $searchParam) {
								$searchParamsArray[$searchParam] = $entityDetails[$searchParam];
							}

							$order->setSearchParams($searchParamsArray);
						}

						if (!$this->_id || (isset($searchParamChanged) && $searchParamChanged)) {
							$orderSetFunctionName = 'set' . ucfirst($orderDescriptor['orderColumn']);
							$model->$orderSetFunctionName($order->getNextOrder());
						}

						break;

					case 'image':
						if (!$this->_id) {
							if (isset($this->_requestParams[$key]) && !empty($this->_requestParams[$key])) {
								WX_Image::getInstance($param['imageDescriptorName'])->moveFromTempToLiveAndResize($this->_requestParams[$key]);
								$imageSetter = 'set' . ucfirst($key);
								$model->$imageSetter($this->_requestParams[$key]);
							}
							break;
						} else {
							$getImageFuncName = 'get' . ucfirst($key);
							$setImageFuncName = 'set' . ucfirst($key);

							if ($model->$getImageFuncName() != $this->_requestParams[$key]) {
								# if old existed delete it
								if (!is_null($model->$getImageFuncName())) {
									WX_Image::getInstance($param['imageDescriptorName'])->delete($model->$getImageFuncName());
									$model->$setImageFuncName(null);
								}

								# if new exists set it
								if (!empty($this->_requestParams[$key])) {
									WX_Image::getInstance($param['imageDescriptorName'])->moveFromTempToLiveAndResize($this->_requestParams[$key]);
									$model->$setImageFuncName($this->_requestParams[$key]);
								}
							}
						}
						break; //@todo: eddig nem volt benne break

					case 'images':
						if (!$this->_id) {
							if (isset($this->_requestParams[$key]) && !empty($this->_requestParams[$key])) {
								$images = json_decode($this->_requestParams[$key], true);
								$imageIds = array_keys($images);
								foreach ($imageIds as $hash) {
									WX_Image::getInstance($param['imageDescriptorName'])->moveFromTempToLiveAndResize($hash);
								}
								
								// clear descriptor data
								$result = array();
								foreach ($images as $hash => $image) {
									# @todo: ezt lehetne altalanosabban is
									$result[$hash] = array(
										'title' => $image['title']
									);
								}
								
								if (empty($result)) {
									$result = null;
								} else {
									$result = json_encode($result);
								}

								$setImageFuncName = 'set' . ucfirst($key);
								$model->$setImageFuncName($result);

							}
							break;
						} else {
							$getImageFuncName = 'get' . ucfirst($key);
							$setImageFuncName = 'set' . ucfirst($key);

							// collect images data
							$oldImages = $model->$getImageFuncName();
							$newImages = $this->_requestParams[$key];
							if ($oldImages != '') {
								$oldImagesIds = array_keys(json_decode($oldImages, true));
							} else {
								$oldImagesIds = array();
							}
							if ($newImages != '') {
								$newImagesIds = array_keys(json_decode($newImages, true));
							} else {
								$newImagesIds = array();
							}

							// has change (delete or update) - do physical delete or updata
							$diff = array_diff($oldImagesIds, $newImagesIds);
							if (!empty($diff)) {
								// image Ids for delete
								$imagesDelete = array_diff($oldImagesIds, $newImagesIds);
								// images Ids for upload
								$uploadImages = array_diff($newImagesIds, $oldImagesIds);	

								if (!empty($imagesDelete)) {
									foreach ($imagesDelete as $hash) {
										WX_Image::getInstance($param['imageDescriptorName'])->delete($hash);
									}
								}

								if (!empty($uploadImages)) {
									foreach ($uploadImages as $hash) {
										WX_Image::getInstance($param['imageDescriptorName'])->moveFromTempToLiveAndResize($hash);
									}
								}
							}

							if (empty($newImages)) {
								$newImages = '{}';
							}

							// clear descriptor data
							$result = array();
							foreach (json_decode($newImages, true) as $hash => $image) {
								# @todo: ezt lehetne altalanosabban is
								$result[$hash] = array(
									'title' => $image['title']
								);
							}
							
							if (empty($result)) {
								$result = null;
							} else {
								$result = json_encode($result);
							}

							$model->$setImageFuncName($result);
						}
						break;

					case 'file':
						if (!$this->_id) {
							if (isset($this->_requestParams[$key]) && !empty($this->_requestParams[$key])) {
								$fileArray = json_decode($this->_requestParams[$key], true);

								$file = UPLOAD_PATH . $fileArray['type'] . '/' . $fileArray['hash'] . '.' . $fileArray['ext'];
								$fileTemp = UPLOAD_TEMP_PATH . $fileArray['type'] . '/' . $fileArray['hash'] . '.' . $fileArray['ext'];

								if (file_exists($fileTemp)) {
									$uploadDir = UPLOAD_PATH . $fileArray['type'] . '/';
									if (!file_exists($uploadDir)) {
										WX_Directory_Writer::getInstance()->makeDirectory($uploadDir, 0777);
									}

									copy($fileTemp, $file);
									unlink($fileTemp);
								}

								$fileSetter = 'set' . ucfirst($key);
								$model->$fileSetter($this->_requestParams[$key]);
							}
							break;
						} else {
							$getFileFuncName = 'get' . ucfirst($key);
							$setFileFuncName = 'set' . ucfirst($key);

							if ($model->$getFileFuncName() != $this->_requestParams[$key]) {

								# if old existed delete it
								$funcName = $model->$getFileFuncName();
								if (!empty($funcName)) {
									$fileArray = json_decode($model->$getFileFuncName(), true);

									$file = UPLOAD_PATH . $fileArray['type'] . '/' . $fileArray['hash'] . '.' . $fileArray['ext'];
									
									if (file_exists($file)) {
										unlink($file);
									}

									$model->$setFileFuncName(null);
								}

								# if new exists set it
								if (!empty($this->_requestParams[$key])) {
									$fileArray = json_decode($this->_requestParams[$key], true);

									$file = UPLOAD_PATH . $fileArray['type'] . '/' . $fileArray['hash'] . '.' . $fileArray['ext'];
									$fileTemp = UPLOAD_TEMP_PATH . $fileArray['type'] . '/' . $fileArray['hash'] . '.' . $fileArray['ext'];

									if (file_exists($fileTemp)) {
										$uploadDir = UPLOAD_PATH . $fileArray['type'] . '/';
										if (!file_exists($uploadDir)) {
											WX_Directory_Writer::getInstance()->makeDirectory($uploadDir, 0777);
										}

										copy($fileTemp, $file);
										unlink($fileTemp);
									}

									$model->$setFileFuncName($this->_requestParams[$key]);
								}
							}
						}
						break;

					default:
						if (array_key_exists($key, $entityDetails)) {
							$setFunctionName = 'set' . ucfirst($key);
							$model->$setFunctionName($entityDetails[$key]);
						}
						break;
				}
			}
		}

		$model->save();
	}

	public function deleteEntity() {
		$db = Zend_Registry::get('db');
		
		$orderParam = $this->_searchParamType('order');

		if ($orderParam) {
			$orderDescriptorName = WX_Tools::getValue($this->_descriptor['form']['data']['params'][$orderParam], 'orderDescriptorName');
			$orderClassName = APP_NAME . '_Order';
			$order = new $orderClassName($orderDescriptorName);

			if ($order->featureExists()) {
				$order->removeOrder($this->_id);
			}
		}

		# delete all images related to row
		foreach ($this->_searchParamType('image') as $fieldKey) {
			//DX($fieldParams);
			$query = $db->select()
						->from($this->_descriptor['form']['data']['dbTable'])
						->where('id = "'.$this->_id.'"');
			$result = $db->query($query)->fetch();

			if (!empty($result[$fieldKey])) {
				$image = new WX_Image($this->_descriptor['form']['data']['params'][$fieldKey]['imageDescriptorName']);
				$image->delete($result[$fieldKey]);
			}
		}
		
		$db->delete($this->_descriptor['form']['data']['dbTable'], 'id = "'.$this->_id.'"');
	}

	public function activateEntity () {
		$activateParam = $this->_searchParamType('activate');

		$db = Zend_Registry::get('db');
		$db->update($this->_descriptor['form']['data']['dbTable'], array($this->_descriptor['form']['data']['params'][$activateParam]['columnName'] => 1), 'id = "'.$this->_id.'"');
	}

	public function inactivateEntity () {
		$activateParam = $this->_searchParamType('activate');
		
		$db = Zend_Registry::get('db');
		$db->update($this->_descriptor['form']['data']['dbTable'], array($this->_descriptor['form']['data']['params'][$activateParam]['columnName'] => 0), 'id = "'.$this->_id.'"');
	}
	
	public function archiveEntity () {
		$archiveParam = $this->_searchParamType('archive');

		$db = Zend_Registry::get('db');
		$db->update($this->_descriptor['form']['data']['dbTable'], array($this->_descriptor['form']['data']['params'][$archiveParam]['columnName'] => 1), 'id = "'.$this->_id.'"');
	}

	public function dearchiveEntity () {
		$archiveParam = $this->_searchParamType('archive');

		$db = Zend_Registry::get('db');
		$db->update($this->_descriptor['form']['data']['dbTable'], array($this->_descriptor['form']['data']['params'][$archiveParam]['columnName'] => 0), 'id = "'.$this->_id.'"');
	}

	public function changeorder($newOrder) {
		$orderParam = $this->_searchParamType('order');
		
		$orderDescriptorName = WX_Tools::getValue($this->_descriptor['form']['data']['params'][$orderParam], 'orderDescriptorName');
		$orderClassName = APP_NAME . '_Order';
		$order = new $orderClassName($orderDescriptorName);
		
		$order->changeOrder($this->_id, $newOrder);
	}

	protected function _searchParamType($type) {
		$singles = array('order','activate', 'archive');

		$return = array();
		foreach ($this->_descriptor['form']['data']['params'] as $key => $param) {
			if (is_array($param) && $param['type'] == $type) {
				if (in_array($param['type'], $singles)) {
					return $key;
				} else {
					$return[] = $key;
				}
			}
		}

		return $return;
	}

	protected function _getModel() {
		$modelName = APP_NAME . '_Model_' . ucfirst($this->_descriptor['form']['data']['dbTable']);
		return new $modelName();
	}

	protected function _convertEntityDetailsToForm($entityDetails) {
		$toFormConverter = $this->_descriptor['form']['data']['toFormConverter'];
		if ($toFormConverter) {
			$toFormConverterClass 	= APP_NAME . '_Converter_' . ucfirst($toFormConverter[0]);
			$toFormConverterMethod 	= $toFormConverter[1];
		}
		return $toFormConverter ? call_user_func(array($toFormConverterClass,$toFormConverterMethod),$entityDetails) : $entityDetails;
	}

	protected function _convertEntityDetailsToDb($entityDetails) {
		$toDbConverter = $this->_descriptor['form']['data']['toDbConverter'];
		if ($toDbConverter) {
			$toDbConverterClass 	= APP_NAME . '_Converter_' . ucfirst($toDbConverter[0]);
			$toDbConverterMethod 	= $toDbConverter[1];
		}
		return $toDbConverter ? call_user_func(array($toDbConverterClass,$toDbConverterMethod),$entityDetails, $this->_id) : $entityDetails;
	}

	protected function _checkRequiredFields($required, $haystack, $section = array()) {
		if ($section && !is_array($section)) {
			$section = array($section);
		}

		foreach ($required as $key => $field) {
			if (!is_array($field)) {
				if (!array_key_exists($field, $haystack)) {
					$pathToField = '';
					foreach ($section as $one) {
						$pathToField .= $one . '/';
					}
					WX_Exception::create('The following field value is missing from the CMS descriptor: ' . $pathToField . $field);
				}
			} else {
				$section[] = $key;
				return $this->_checkRequiredFields($field, $haystack[$key], $section);
			}
		}

		return true;
	}
}