<?php

require_once APPLICATION_PATH . 'library/instagram/Instagram.php';
use MetzWeb\Instagram\Instagram;

/**
 * Handles Facebook realted functions.
 */
class WX_Instagram {
	/**
	 * Contains the Facebook object.
	 */
	public $_instagram = false;

	/**
	 * Contains the config data for Facebook.
	 */
	protected $_cfg = false;

	/**
	 * Details of the user.
	 */
	protected $_me = false;
    
    /**
     * User object
     * @var object 
     */
    protected $_user = false;

	/**
	 * Initalizing the Facebook object.
	 * 
	 * @method __construct
	 */
	public function __construct() {
		# Get the config data from xml.
		$cfg = Zend_Registry::get('config');

		# Set the config data.
        /*
		$this->_cfg = array(
			'appId' => $cfg->facebook->appid,
			'secret' => $cfg->facebook->appsecret
		);

		# Instantiate Facebook object and set it to variable.
		$this->_fb = new Facebook(array(
			'appId' => $this->_cfg['appId'],
			'secret' => $this->_cfg['secret']
		));
        */
        
        // initialize class
        //@TODO át kell vinni a configba az instagram app adatait
        $this->_instagram = new Instagram(array(
          'apiKey'      => '7deca03357164967aaf3e4a4cdaf35ce',
          'apiSecret'   => 'a426da73467748b2bc9cb20eeb260078',
          'apiCallback' => BASE_HOST.'instagram-photos/' // must point to success.php
        ));
	}
    
    public function getLoginUrl()
    {
        return $this->_instagram->getLoginUrl();
    }
    
    public function checkLogin($requestParams)
    {
            // check whether the user has granted access
            if (array_key_exists('code', $requestParams)) {
              // receive OAuth token object
              $data = $this->_instagram->getOAuthToken($requestParams['code']);
              
              // store user access token
              $this->_instagram->setAccessToken($data);
              // now you have access to all authenticated user methods
              
              if (!empty($data->user))
              {
                  $this->_user = $data->user;
                  return true;
              }
              
            } else {
              // check whether an error occurred
              if (array_key_exists('error', $requestParams)) {
                  //@TODO ide egy rendes kivételkezelést kell írni
                  DX ('An error occurred: ' .$requestParams['error_description']);
                
              }
            }
    }
    
    public function getUser()
    {
        return $this->_user;
    }
    
    public function getUserMedia()
    {
        return $this->_instagram->getUserMedia();
    }

}