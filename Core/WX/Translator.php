<?php

class WX_Translator {

	const REFLECTION_CHARS = '@@@';

	static public function setToRegistry($language) {
		$translations = self::getTranslationByLanguage($language);

		Zend_Registry::set('translations', $translations);
	}

	static public function getTranslationByLanguage($language) {
		$translations = array();

		$directory = LANGUAGES_PATH . $language . '/';
		$files = array_diff(scandir($directory), array('..', '.')); # avoid "." and ".." system dirs

		if (empty($files)) {
			$message = 'Translation file not found in the directory: "' . $directory . '"';
			WX_Exception::create($message);
		}

		foreach ($files as $file) {
			$path = $directory . $file;
			$expressions = WX_Translator_Reader::getInstance()->readFileSimple($path);
			# check duplicated expressions
			$duplicatedExpressions = array_intersect_key($expressions, $translations);
			if (!empty($duplicatedExpressions)) {
				$message = 'The following expressions are duplicated: ';
				$message.= implode(', ', array_keys($duplicatedExpressions));
				WX_Exception::create($message);
			}
			$translations = array_merge($translations, $expressions);
		}

		return $translations;
	}

	static public function t() {
		$args = func_get_args();
		$argsNum = func_num_args();
		if ($argsNum == 0) {
			WX_Exception::create('First parameter must be a string');
		}
		$expression = $args[0];

		$translations = Zend_Registry::get('translations');
		//D($translations);

		if (isset($translations[$expression])) {
			//handle reflection
			if (substr($translations[$expression],0,3) == self::REFLECTION_CHARS) {
				//handle reflection with args
				return call_user_func_array(
					't',
					array_merge(
						array(substr($translations[$expression],3)),
						array_slice($args, 1)
					)
				);
			}
			$args[0] = $translations[$expression];
			return call_user_func_array('sprintf', $args);
		} else {
			return $expression;
		}
	}

	static public function getJS() {
		$translations = Zend_Registry::get('translations');

		$result = 'TRANSLATOR = {';
		foreach ($translations as $idx => $expression) {
			if (substr($idx, 0, 3) == 'JS_') {
				$result.= PHP_EOL;
				$result.= "\t\t\t".$idx . " : '" . $expression . "',";
			}
		}
		$result = substr($result, 0, -1);
		$result.= PHP_EOL;
		$result.= "\t\t}" . PHP_EOL;

		return $result;
	}

	static public function router() {
		$args = func_get_args();
		$argsNum = func_num_args();
		if ($argsNum == 0) {
			WX_Exception::create('First parameter must be a string');
		}
		$expression = $args[0];

		$translations = Zend_Registry::get('translations');

		if (isset($translations[$expression])) {
			$args[0] = $translations[$expression];
			return call_user_func_array('sprintf', $args);
		} else {
			return $expression;
		}
	}

	static public function translateUrl($value, $sourceLanguage, $targetLanguage) {
		$key =  self::findKeyByValue($value, $sourceLanguage);

		if ($key == false) { # nincs forditas hozza, visszaadjuk az eredeti url-t
			return $value;
		}

		$targetTranslation = self::getTranslationByLanguage($targetLanguage);

		$url = $targetTranslation[$key];
		if ($url == false) {
			$message = 'Unknown translateUrl in "' . $targetLanguage . '" : "' . $value . '"';
			throw new Exception($message);
		}
		
		return $url;
	}

	static public function findKeyByValue($value, $sourceLanguage) {
		$sourceTranslation = self::getTranslationByLanguage($sourceLanguage);

		#$key = array_search($value, $sourceTranslation);
		$key = false;
		# vegigmegyunk a forras nyelve translation tombjen
		foreach ($sourceTranslation as $tKey => $tVal) {
			# ha a szoveg megegyzik, meg nem biztos, hogy az amit kerestunk...
			if ($tVal == $value) {
				# ... meg kell nezni, hogy a kulcs ugy kezdodik e, hogy URL_
				if (substr($tKey, 0, 4) === 'URL_') {
					$key = $tKey;
				}
			}
		}

		/*
		if ($key === false) { # Ha nincs meg, akkor valoszinuleg nem lett felvive a wxt fajlba
			$message = 'No translation found for "' . $value  . '" in "' . $sourceLanguage .'" language!';
			throw new Exception($message);
		}
		*/

		# Visszaadjuk a a kulcsot, amiben a kifejezes megtalalhato. 
		# Vagy egy kulcs megy vissza, vagy false.
		return $key;
	}

}