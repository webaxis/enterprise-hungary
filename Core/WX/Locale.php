<?php

# @todo: ez az osztaly nincs meg refaktorizalva. Adam csinalja
class WX_Locale {

	static protected $_instances 	= array();

	protected $_defaultLanguage 	= 'en';
	
	protected $_defaultRegion 		= 'US';
	
	protected $_availableLanguages	= array();

	public static function getInstance() {
		$class = get_called_class();
		if (!isset(self::$_instances[$class])) {
			self::$_instances[$class] = new $class();
		}
		return self::$_instances[$class];
	}

	protected function _getLocaleClass() {
		return Zend_Registry::get('Zend_Locale');
	}

	public function getLocale() {
		return $this->_getLocaleClass()->toString();
	}

	public function getLanguage() {
		return $this->_getLocaleClass()->getLanguage();
	}

	public function getRegion() {
		return $this->_getLocaleClass()->getRegion();
	}
	
	public function getDefaultLanguage() {
		return strtolower($this->_defaultLanguage);
	}
	
	public function getDefaultRegion() {
		return strtoupper($this->_defaultRegion);
	}
	
	public function getDefaultLocale() {
		return strtolower($this->_defaultLanguage) . '-' . strtoupper($this->_defaultRegion);
	}
	
	public function languageExists($language) {
		return in_array(strtolower($language), $this->getAvailableLanguages());
	}

	public function getLanguageUrl($language) {
        $request = new Zend_Controller_Request_Http();
		$uri = Zend_Uri::factory(BASE_HOST . $request->getRequestUri());

		$query = $uri->getQuery();
		if (!empty($query)) {
			$query .= '&';
		}
		$query .= 'language=' . $language;

		$url = str_replace(array('http:/','https:/'),'http://',preg_replace('#/+#','/',BASE_HOST . $uri->getPath() . '?' . $query));

		return $url;
	}

	public function getLanguages() {
		$currentLanguage = $this->getLanguage();
	
		$return = array();
		foreach ($this->getAvailableLanguages() as $language) {
			$return[$language] = array(
				'language'		=> $language,
				'name'			=> $this->_getLocaleClass()->getTranslation($language, 'Language', $this->getLocale()),
				'nameLocale'	=> $this->_getLocaleClass()->getTranslation($language, 'Language', $language),
				
			);
		}
	
		return $return;
	}
	
	public function getAvailableLanguages() {
		$return = array();
		
		foreach ($this->_availableLanguages as $one) {
			$return[] = strtolower($one);
		}
		
		return $return;
	}
}