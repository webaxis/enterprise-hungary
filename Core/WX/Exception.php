<?php

class WX_Exception {

	static public function create($message) {
		$cfg = Zend_Registry::get('config');
		$cfg->phpSettings->display_errors;
		if ($cfg->phpSettings->display_errors == 1) {
			throw new Exception($message);
		} else {
			error_log($message, 3);
		}
	}

}