<?php

class WX_Static {

	public function listYesNo() {
		return [
			1 => t('YES'),
			2 => t('NO')
		];
	}

}