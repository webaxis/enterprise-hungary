<?php

require_once APPLICATION_PATH . 'library/facebook/facebook.php';

/**
 * Handles Facebook realted functions.
 */
class WX_Facebook {
	/**
	 * Contains the Facebook object.
	 */
	public $_fb = false;

	/**
	 * Contains the config data for Facebook.
	 */
	protected $_cfg = false;

	/**
	 * Details of the user.
	 */
	protected $_me = false;

	/**
	 * Initalizing the Facebook object.
	 * 
	 * @method __construct
	 */
	public function __construct() {
		# Get the config data from xml.
		$cfg = Zend_Registry::get('config');

		# Set the config data.
		$this->_cfg = array(
			'appId' => $cfg->facebook->appid,
			'secret' => $cfg->facebook->appsecret
		);

		# Instantiate Facebook object and set it to variable.
		$this->_fb = new Facebook(array(
			'appId' => $this->_cfg['appId'],
			'secret' => $this->_cfg['secret']
		));
	}

	/**
	 * Returns App ID.
	 *
	 * @method getAppId
	 * @return {Integer} App ID
	 */
	static public function getAppId () {
		$cfg = Zend_Registry::get('config');
		return $cfg->facebook->appid;
	}

	/**
	 * Longers the durability of access token.
	 *
	 * @method setExtendedAccessToken
	 */
	public function setExtendedAccessToken() {
		$this->_fb->setExtendedAccessToken();
	}

	/**
	 * Sets access token.
	 *
	 * @method setAccessToken
	 */
	public function setAccessToken($accessToken) {
		$this->_fb->setAccessToken($accessToken);
	}

	/**
	 * Check if the user is connected to our app and logged in Facebook.
	 *
	 * @method isConnected
	 * @return {Bool} Connected of not
	 */
	public function isConnected() {
		# First try to get the Facebook ID of the user.
		$user = $this->getUser();

		# If the ID is not available that means it has not authorized our app yet.
		if (empty($user)) {
			return false;

		# We can only move on if we have a real access token.
		} elseif ($this->_fb->getAccessToken() !== $this->_fb->getAppId().'|'.$this->_fb->getAppSecret()) {
			$return = true;
			# Let's try to get all the deatils of the user - that shows clearly if we are allowed to access or not.
			try {
				$me = $this->_fb->api('/me');
			} catch (FacebookApiException $e) {
				# If no luck, we are not considered as logged in.
				$return = false;
			}

			# If we got the data save it for later usage.
			if ($return == true)
				$this->_me = $me;

			# Than say we are happy.
			return $return;
		} else {
			# If we have a "fake" access token, we are definetly not connected. 
			return false;
		}
	}
	
	/**
	 * Get the details of the user.
	 *
	 * @method getDetails
	 * @return {Array} Details if available, false if not
	 */
	public function getDetails () {
		# We can only get the details if we are connected.
		if ($this->isConnected()) {
			# Try to get the user details. If it's already set it's better.
			try {
				$return = $this->_me;

				# If the details were not set yet, save it now for later usage.
				if ($this->_me === false)
					$return = $this->_me = $this->_fb->api('/me');
					
			} catch (FacebookApiException $e) {
				# Return false if we are not able to gather the details.
				$return = false;
			}
			return $return;
		} else {
			# If we are not connected say false.
			return false;
		}
	}
	
	/**
	 * Get the Facebook ID of the user if it's authorized.
	 *
	 * @method getUser
	 * @return {Integer} ID of user
	 */
	public function getUser () {
		return $this->_fb->getUser();
	}
	
	/**
	 * Get the saved access token of the user from Facebook.
	 *
	 * @method getAccessToken
	 * @return {String} Access token.
	 */
	public function getAccessToken () {
		$accessToken = $this->_fb->getAccessToken();

		if ($accessToken == $this->_cfg['appId'].'|'.$this->_cfg['secret']) {
			$user = new Sxsw_User;
			$accessToken = $user->getAccessToken();
		}

		return $accessToken;
	}

	/**
	 * Enables to send Facebook API request.
	 *
	 * @method api
	 * @request {String} The query of request.
	 * @params {Array} The params related to request.
	 * @return {Array} Result of request.
	 */
	public function api ($request, $params = array(), $type = 'GET', $accessToken = false) {
		if (!$accessToken)
			$accessToken = $this->getAccessToken();
			
		$newParams = array_merge($params, array(
			'access_token' 	=> $accessToken,
			'locale'		=> 'en_US'
		));
		return $this->_fb->api($request,$type,$newParams);
	}

	/**
	 * Executes a Facebook FQL query.
	 *
	 * @method fql
	 * @query {String} The query to be executed.
	 * @accessToken {String} Access token that enables system to run the query.
	 */
	public function fql ($query, $accessToken = false) {
		if (empty($query))
			return array();
			
		$accessToken = !$accessToken ? $this->getAccessToken() : $accessToken;
		
		return $this->_fb->api(array(
			'method' => 'fql.query',
			'query' => $query,
			'access_token' => $accessToken,
			'locale' => 'en_US'));
	}
	
	public function fqlBatch ($queryArray, $accessToken = false) {
		if (empty($queryArray))
			return array();
			
		$queries = array();
		foreach ($queryArray as $one) {
			$queries[] = array(
				'method'=>'GET', 'relative_url'=>'method/fql.query?query='.str_replace(' ','+',$one)
			);
		}
		
		$results = $this->api('/?batch='.json_encode($queries), array('access_token'=>$accessToken), 'POST');
		
		$firstBody = json_decode($results[0]['body'],true);
		$errorMsg = isset($firstBody['error_msg']) ? $firstBody['error_msg'] : false;
		if ($errorMsg && (strstr($errorMsg,'expired') || strstr($errorMsg,'Service temporarily unavailable') || strstr($errorMsg,'access token') || strstr($errorMsg,'appsecret_proof'))) {
			return false;
		}
		
		
		$return = array();
		foreach ($results as $one) {
			$return[] = json_decode($one['body'],true);
		}
		
		return $return;
	}
	
	public function validateAccessToken ($accessToken) {
		try {
			$result = $this->api('debug_token',array(
				'input_token'	=> $this->_cfg['appId'].'|'.$this->_cfg['secret']
			),'GET',$accessToken);
		} catch (Exception $e) {
			if (strstr($e->getMessage(),'expired') || strstr($e->getMessage(),'access token') || strstr($e->getMessage(),'appsecret_proof'))
				return false;
		}
		
		if ($result['data']['is_valid'] == true)
			return true;
			
		return false;
	}
	
	public function checkPermission ($permission) {
		$user = Sxsw_User::getInstance()->getFacebook();
		$data = $this->fql('SELECT '.$permission.' FROM permissions WHERE uid = "'.$user.'"');
		return (bool) $data[0][$permission];
	}
}