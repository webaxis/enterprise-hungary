<?php

class WX_DForm_Option {

	/**
	 * $data: tomb, amiben a nyersadatok vannak
	 * $options: beallitasok;
	 *
	 * Visszaad egy formatozz tombot:
	 * [['0', 'Valassz'], ['1', 'Opcio 1'], ['2', 'Opcio 2'] ....]
	 */
	static public function getItems($data, $options = array()) {
		$defaults = [
			# az adatok milyen formaban erkeznek (array|list); 
			# array: tomb, amiben minden elem egy tovabbi tomb; 
			# list: egy tomb, ahol kulcs => value alapjan szedjuk ki az adatokat
			'mode' => 'array', # array a default ertek
			'array' => [ # array mod beallitasai
				'key' => 'id', # option id-ja
				'value' => 'name', # option value-ja, amit ki lehet valasztani az optin box-bol 
			],
			'list' => [], # list mod beallitasai
			'useEmptyItem' => true, # legyen e "Kerjuk valassz" az elejen
			// lehetne boviteni egy 'defaultSelected' parameterrel
			'emptyItem' => [ # a "Kerjuk valassz" elem tulajdonsagai
				'key' => 0,
				'value' => t('FORM_ELEMENT_OPTION_CHOOSE')
			]
		];

		$settings = array_replace_recursive($defaults, $options);

		$items = [];
 
		if ($settings['useEmptyItem']) {
			$items[] = [$settings['emptyItem']['key'], $settings['emptyItem']['value']];
		}

		switch ($settings['mode']) {
			case 'array' :
				foreach ($data as $id => $row) {
					$key = $settings['array']['key'];
					$value = $settings['array']['value'];
					if (!isset($row[$key])) {
						$message = 'Not exist the following "key" key in $data: "' . $key . '"';
						throw new Exception($message);
					}
					if (!isset($row[$value])) {
						$message = 'Not exist the following "value" key in $data: "' . $VALUE . '"';
						throw new Exception($message);
					}
					$items[] = [$row[$key], $row[$value]];
				}
				break;
			case 'list' :
				foreach ($data as $key => $value) {
					$items[] = [$key, $value];
				}
				break;
			default :
				throw new Exception('Unknown mode: "' . $settings['mode'] . '"');
				break;
		}

		return $items;
	}

}