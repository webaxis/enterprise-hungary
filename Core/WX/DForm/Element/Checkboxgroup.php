<?php

class WX_DForm_Element_Checkboxgroup extends WX_DForm_Element_Abstract {

	static public function create($options, $value = '') {
		# required settings
		$descriptor = array(
			'vtype' => 'checkboxgroup',
			'name' => $options['name'],
			'items' => $options['items'],
			'fieldLabel' => WX_Tools::getValue($options, 'label', ''),
			'allowBlank' => WX_Tools::getValue($options, 'allowBlank', false),
			'error' => WX_Tools::getValue($options, 'error', array('message' => 'You must choose one.'))
		);

		# optional settings 
		WX_Tools::setIfExistsMulti(
			$descriptor, 
			$options, 
			array('class', 'readonly', 'enable', 'id', 'templateDirectory', 'template', 'validators', 'properties')
		);

		return $descriptor;
	}

}