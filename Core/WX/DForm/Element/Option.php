<?php

class WX_DForm_Element_Option extends WX_DForm_Element_Abstract {

	static public function create($options, $value = '') {
		# required settings
		$descriptor = array(
			'name' => $options['name'],
			'fieldLabel' => WX_Tools::getValue($options, 'label', ''),
			'vtype' => 'option',
			'defaultValue' => $value,
			'allowBlank' => WX_Tools::getValue($options, 'allowBlank', false),
			'error' => WX_Tools::getValue($options, 'error', array('message' => t('FORM_ERROR_CHOOSE')))
		);
		
		$emptyText = isset($options['emptyText']) ? $options['emptyText'] : false;
		
		if ($emptyText !== false) {
			$descriptor['emptyText'] = $emptyText;
		}
		
		$description = isset($options['description']) ? $options['description'] : false;
		
		if ($description !== false) {
			$descriptor['description'] = $description;
		}

		# optional settings 
		WX_Tools::setIfExistsMulti(
			$descriptor, 
			$options, 
			array(
				'class', 'readonly', 'enable', 'id', 'templateDirectory', 'template', 'validators', 'properties', 
				'emptyText', 'items', 'groups'
			)
		);

		return $descriptor;
	}
}