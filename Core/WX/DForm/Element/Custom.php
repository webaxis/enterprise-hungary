<?php

class WX_DForm_Element_Custom extends WX_DForm_Element_Abstract {

	static public function create($options, $value = '') {
		# required settings
		$descriptor = array(
			'vtype' => 'custom',
			'name' => $options['name'],
			'fieldLabel' => WX_Tools::getValue($options, 'label', ''),
			'defaultValue' => $value,
			'allowBlank' => WX_Tools::getValue($options, 'allowBlank', false),
			'error' => WX_Tools::getValue($options, 'error', array('message' => 'You must fill the input.'))
		);
		
		$description = isset($options['description']) ? $options['description'] : false;
		
		if ($description !== false) {
			$descriptor['description'] = $description;
		}
		
		# optional settings 
		WX_Tools::setIfExistsMulti(
			$descriptor, 
			$options, 
			array('class', 'readonly', 'enable', 'id', 'templateDirectory', 'template', 'validators', 'properties')
		);

		return $descriptor;
	}
}