<?php

class WX_DForm_Element_Image extends WX_DForm_Element_Abstract {

	static public function create($options, $value = '') {
		# required settings
		$descriptor = array(
			'vtype' => 'image',
			'name' => $options['name'],
			'fieldLabel' => WX_Tools::getValue($options, 'label', ''),
			'defaultValue' => $value,
			'allowBlank' => WX_Tools::getValue($options, 'allowBlank', false),
			'error' => WX_Tools::getValue($options, 'error', array('message' => 'You must fill the input.')),
			'options' => WX_Tools::getValue($options, 'options', array())
		);

		$description = isset($options['description']) ? $options['description'] : false;

		if ($description !== false) {
			$descriptor['description'] = $description;
		}

		# optional settings 
		WX_Tools::setIfExistsMulti(
			$descriptor, 
			$options, 
			array('class', 'readonly', 'enable', 'id', 'templateDirectory', 'template', 'validators', 'properties')
		);

		return $descriptor;
	}
}