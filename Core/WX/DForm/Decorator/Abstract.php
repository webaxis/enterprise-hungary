<?php

class WX_DForm_Decorator_Abstract {

	static public function add($attributes, $descriptor, $namespace = 'dynamic-form') {
		# Example: get "datepicker" from "WX_DForm_Decorator_Datepicker"
		$className = strtolower(get_called_class());
		$decorator = substr(strrchr($className, "_"), 1);

		if (!isset($descriptor['attributes'])) {
			$descriptor['attributes'] = array();
		}

		if (isset($descriptor['attributes'][$namespace.'-decorator'])) {
			$descriptor['attributes'][$namespace.'-decorator'].= ' '.$decorator;
		} else {
			$descriptor['attributes'][$namespace.'-decorator'] = $decorator;
		}

		if (count($attributes)) {
			foreach ($attributes as $key => $value) {
				$descriptor['attributes'][$namespace.'-'.$decorator.'-'.$key] = $value;
			}
		}

		return $descriptor;
	}

}