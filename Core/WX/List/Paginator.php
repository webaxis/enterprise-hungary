<?php

/**
 * The paginator must be in a div with class 'wx-list-paginator'
 */

class WX_List_Paginator
{
	private $_r;
	private $_paginator;
	
	public function __construct ($registry) {
		$this->_setRegistry($registry);
		
		switch ($this->_r->getDataType()) {
			case 'array':
				$data = $this->_r->getRawData();
				
				if ($this->_r->getItemCount() != false)
					$data = range(1, $this->_r->getItemCount());
					
				$adapter = new Zend_Paginator_Adapter_Array($data);
				break;
				
			case 'dbSelect':
				$this->_r->getDbSelect()->reset(Zend_Db_Select::LIMIT_COUNT)->reset(Zend_Db_Select::LIMIT_OFFSET);
				$adapter = new Zend_Paginator_Adapter_DbSelect($this->_r->getDbSelect());
				break;
				
			default:
				
		}
		
		$paginator = new Zend_Paginator($adapter);
		$paginator->setCurrentPageNumber($this->_r->getPage());
		$this->_setPaginator($paginator);
		
		if ($this->_r->getItemCount() == false)
			$this->_r->setItemCount($paginator->getTotalItemCount());
	}
	
	public function getPaginator () {
		return $this->_paginator;
	}
	
	private function _setPaginator ($paginator) {
		$this->_paginator = $paginator;
	}
	
	private function _setRegistry ($registry) {
		$this->_r = $registry;
	}
}