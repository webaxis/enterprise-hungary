<?php

class WX_List_Helpers_GetWXTemplateDirectory extends Zend_View_Helper_Abstract
{
	public function getWXTemplateDirectory () {
		return $this->view->r->getWXTemplateDirectory();
	}
}