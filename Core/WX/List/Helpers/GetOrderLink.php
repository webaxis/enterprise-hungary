<?php

class WX_List_Helpers_GetOrderLink extends Zend_View_Helper_Abstract
{
	public function getOrderLink ($html, $order, $by, $attributes = array()) {
		$attributesString = '';
		foreach ($attributes as $attribute => $value) {
			$attributesString .= $attribute.'="'.$value.'"';
		}
		$href = WX_List_Helpers_CreateUrl::createUrl(1,$order,$by); 
		$link = '<a href="'.$href.'" '.$attributesString.' class="wx-list-link">'.$html.'</a>';
		if ($order == $this->view->r->getOrder() && $by == $this->view->r->getBy())
			$link = '<span class="wx-list-order-active">'.$html.'</span>';
		return $link;
	}
}