<?php

class WX_List_Helpers_GetLabels extends Zend_View_Helper_Abstract
{
	public function getLabels () {
		$return = array();
		if ($this->view->r->getNewMode()) { # new mode
			$structure = $this->view->r->getStructure();
			$structure = $structure['fields'];
		} else { # old mode
			$structure = $this->view->r->getStructure();
		}

		foreach ($structure as $key => $one) {
			$return[$key] = $one['label'];
		}

		return $return;
	}
}