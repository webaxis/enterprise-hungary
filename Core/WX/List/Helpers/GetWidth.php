<?php

class WX_List_Helpers_GetWidth extends Zend_View_Helper_Abstract
{
	public function getWidth ($field) {
		$widths = array();
		foreach ($this->view->r->getStructure() as $key => $one) {
			$widths[$key] = $one['width'];
		}
		
		if (array_key_exists($field,$widths))
			return $widths[$field];
			
		return false;
	}
}