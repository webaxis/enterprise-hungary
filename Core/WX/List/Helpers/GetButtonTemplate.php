<?php

class WX_List_Helpers_GetButtonTemplate extends Zend_View_Helper_Abstract {

	protected $_defaultTemplates = [
		/* Pelda:
		'tipus' => [ # a kulcsa a tipus
			'template' => 'tipus.phtml', # maga a template file
			'directory' => PATH_TO_DIRECTORY, # a template file utvonala
		],
		*/
		'edit' => [
			'template' => 'edit.phtml',
		],
		'delete' => [
			'template' => 'delete.phtml',
		]
	];

	public function getButtonTemplate ($type = null, $templateOptions = null) {
		if ($type == null) {
			$type = 'edit';
		}

		if (!array_key_exists($type, $this->_defaultTemplates)) {
			$message = 'Undefined template type: "' . $type . '"';
			throw new Exception($message);
		}

		# itt van megadva a button template-ek default konyvtara konyvtara!
		$templateDirectory = WX_PATH . 'List/Templates/Buttons/';

		$default = $this->_defaultTemplates[$type];

		$default['directory'] = $templateDirectory;

		if ($templateOptions != null) {
			$templateDescriptor = array_replace_recursive($default, $templateOptions);
		} else {
			$templateDescriptor = $default;
		}

		$template = [
			'file' => $templateDescriptor['template'],
			'directory' => $templateDescriptor['directory']
		];

		return $template;
	}
}