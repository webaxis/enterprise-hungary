<?php

class WX_List_Helpers_GetDescOrderLink extends Zend_View_Helper_Abstract
{
	public function getDescOrderLink ($html, $by, $attributes = array(), $disabledElements = array()) {
		if (!in_array($by, $disabledElements)) {
			$attributesString = '';
			foreach ($attributes as $attribute => $value) {
				$attributesString .= $attribute.'="'.$value.'"';
			}
			$href = WX_List_Helpers_CreateUrl::createUrl(1,'DESC',$by); 
			$link = '<a href="'.$href.'" '.$attributesString.' class="wx-list-link">'.$html.'</a>';
			if ('DESC' == $this->view->r->getOrder() && $by == $this->view->r->getBy())
				$link = '<span class="wx-list-order-active">'.$html.'</span>';
			return $link;
		}
	}
}