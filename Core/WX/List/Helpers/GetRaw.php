<?php

class WX_List_Helpers_GetRaw extends Zend_View_Helper_Abstract
{
	public function getRaw ($elem,$key) {
		$raw = $this->view->r->getRawData();
		return $raw[$elem][$key];
	}
}