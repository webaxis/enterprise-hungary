<?php

class WX_List_Helpers_CreateUrl extends Zend_View_Helper_Abstract
{
	public function createUrl ($page = 1, $order = false, $by = false) {
		return WX_List_Helper::createUrl($this->view->r, $page, $order, $by);
	}
}