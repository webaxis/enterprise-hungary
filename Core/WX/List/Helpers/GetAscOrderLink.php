<?php

class WX_List_Helpers_GetAscOrderLink extends Zend_View_Helper_Abstract
{
	public function getAscOrderLink ($html, $by, $attributes = array(), $disabledElements = array()) {
		if (!in_array($by, $disabledElements)) {
			$attributesString = '';
			foreach ($attributes as $attribute => $value) {
				$attributesString .= $attribute.'="'.$value.'"';
			}
			$href = WX_List_Helpers_CreateUrl::createUrl(1,'ASC',$by); 
			$link = '<a href="'.$href.'" '.$attributesString.' class="wx-list-link">'.$html.'</a>';
			if ('ASC' == $this->view->r->getOrder() && $by == $this->view->r->getBy())
				$link = '<span class="wx-list-order-active">'.$html.'</span>';
			return $link;
		}
	}
}