<?php

class WX_List_Helpers_GetButtons extends Zend_View_Helper_Abstract {

	public function getButtons () {
		$return = [];
		$structure = $this->view->r->getStructure();
		if (!empty($structure['buttons'])) { # nem feltelten kell egy listahoz gomb, ennyivel le van kezelve vegig a folyamat
			$return = $structure['buttons'];
		}

		return $return;
	}
}