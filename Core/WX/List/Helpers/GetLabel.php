<?php

class WX_List_Helpers_GetLabel extends Zend_View_Helper_Abstract
{
	public function getLabel ($field) {
		$labels = array();
		foreach ($this->view->r->getStructure() as $key => $one) {
			$labels[$key] = $one['label'];
		}
		
		if (array_key_exists($field,$labels))
			return $labels[$field];
			
		return false;
	}
}