<?php

class WX_List_Helpers_GetDescriptorName extends Zend_View_Helper_Abstract
{
	public function getDescriptorName () {
		return $this->view->r->getDescriptorName();
	}
}