<?php

class WX_List_Helpers_GetFilterBehaviours extends Zend_View_Helper_Abstract
{
	public function getFilterBehaviours () {
		return $this->view->r->getFilterBehaviours();
	}
}