<?php

class WX_List_Helpers_CreateSimpleUrl extends Zend_View_Helper_Abstract
{
	public function createSimpleUrl ($page = 1) {
		return WX_List_Helper::createSimpleUrl($this->view->r, $page);
	}
}