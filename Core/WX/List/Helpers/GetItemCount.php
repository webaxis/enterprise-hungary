<?php

class WX_List_Helpers_GetItemCount extends Zend_View_Helper_Abstract
{
	public function getItemCount () {
		return $this->view->r->getItemCount();
	}
}