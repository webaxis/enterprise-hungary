<?php

class WX_List_Helpers_GetFieldTemplate extends Zend_View_Helper_Abstract {

	protected $_defaultTemplates = [
		/* Pelda:
		'tipus' => [ # a kulcsa a tipus
			'template' => 'tipus.phtml', # maga a template file
			'directory' => PATH_TO_DIRECTORY, # a template file utvonala
		],
		*/
		'text' => [
			'template' => 'text.phtml'
		],
		'image' => [
			'template' => 'image.phtml'
		]
	];

	public function getFieldTemplate ($type = null, $templateOptions = null) {
		# ha nincs type, akkor automatikusan 'type' -ra allitja
		if ($type == null) {
			$type = 'text';
		}

		if (!array_key_exists($type, $this->_defaultTemplates)) {
			$message = 'Undefined template type: "' . $type . '"';
			throw new Exception($message);
		}

		# itt van megadva a field template-ek default konyvtara konyvtara!
		$templateDirectory = WX_PATH . 'List/Templates/Fields/';

		$default = $this->_defaultTemplates[$type];

		$default['directory'] = $templateDirectory;

		if ($templateOptions != null) {
			$templateDescriptor = array_replace_recursive($default, $templateOptions);
		} else {
			$templateDescriptor = $default;
		}

		$template = [
			'file' => $templateDescriptor['template'],
			'directory' => $templateDescriptor['directory']
		];

		return $template;
	}
}