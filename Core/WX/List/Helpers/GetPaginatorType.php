<?php

class WX_List_Helpers_GetPaginatorType extends Zend_View_Helper_Abstract
{
	public function getPaginatorType () {
		return $this->view->r->getPaginatorType();
	}
}