<?php

class WX_List_Helpers_GetFilterBlock extends Zend_View_Helper_Abstract
{
	protected $_defaultType		= 'text';

	/**
	 * @field is the key of structure elements, that contains all information about the item (e.g. label, filter)
	 */
	public function getFilterBlock ($field) {
		$structure = $this->view->r->getStructure();
		
		if (!isset($structure[$field]['filter']))
			return;
			
		$properties 	= $structure[$field];
		$filter 		= $properties['filter'];
		$type 			= isset($filter['type']) ? $filter['type'] : $this->_defaultType;
		$attributes 	= isset($filter['attributes']) ? $filter['attributes'] : array();
		$filters 		= $this->view->r->getFilters();
		$value 			= $filters[$field];

		$attributesString = '';
		foreach ($attributes as $attr => $val) {
			$attributesString .= ' '.$attr.'="'.$val.'"';
		}

		$view = new WX_List_Anonym(array(
			'attributes' => $attributesString,
			'id' => $this->view->r->getId(),
			'field' => $field,
			'value' => $value
		)) ; 
		$view->setScriptPath($this->_getFilterTemplatePath($type));
		
		switch ($type) {
			default:
				$return = $view->render(strtolower($type) . '.phtml');
		}
		
		return $return;
	}
	
	public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }
	
	private function _getFilterTemplatePath ($type) {
		$filterDirectory = $this->view->r->getConfig()->templateDirectory . 'Filters/';
		if (file_exists($filterDirectory . strtolower($type) . '.phtml'))
			return $filterDirectory;
		return $this->view->r->getWXTemplateDirectory() . 'Filters/';
			
	}
}