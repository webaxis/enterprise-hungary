<?php

class WX_List_Helpers_GetPaginatorMoreText extends Zend_View_Helper_Abstract
{
	public function getPaginatorMoreText () {
		return $this->view->r->getPaginatorMoreText();
	}
}