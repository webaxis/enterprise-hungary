<?php

class WX_List_Helpers_GetPaginator extends Zend_View_Helper_Abstract
{
	public function getPaginator () {
		return $this->view->r->getPaginator();
	}
}