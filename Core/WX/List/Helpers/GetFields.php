<?php

class WX_List_Helpers_GetFields extends Zend_View_Helper_Abstract {

	public function getFields () {
		if ($this->view->r->getNewMode()) { # new mode
			$structure = $this->view->r->getStructure();
			$return = $structure['fields'];
		} else { # old mode
			$return = array();
			foreach ($this->view->r->getStructure() as $field => $one) {
				$return[] = $field;
			}
		}

		return $return;
	}
}