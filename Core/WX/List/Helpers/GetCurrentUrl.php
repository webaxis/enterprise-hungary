<?php

class WX_List_Helpers_GetCurrentUrl extends Zend_View_Helper_Abstract
{
	public function getCurrentUrl () {
		return $this->view->r->getCurrentUrl();
	}
}