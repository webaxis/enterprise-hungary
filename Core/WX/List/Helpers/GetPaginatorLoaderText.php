<?php

class WX_List_Helpers_GetPaginatorLoaderText extends Zend_View_Helper_Abstract
{
	public function getPaginatorLoaderText () {
		return $this->view->r->getPaginatorLoaderText();
	}
}