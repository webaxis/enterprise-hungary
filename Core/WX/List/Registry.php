<?php

class WX_List_Registry
{
	private $_registry = array();
	
	public function __call ($method, $params) {
		$firstThree = substr($method, 0, 3);
		if ($firstThree == 'set') {
			$this->set(str_replace('set', '', $method), $params[0]);
		} elseif ($firstThree == 'get') {
			return $this->get(str_replace('get', '', $method));
		}
	}
	
	public function set ($key, $value) {
		$this->_registry[ucfirst($key)] = $value;
	}
	
	public function get ($key) {
		if (isset($this->_registry[ucfirst($key)]))
			return $this->_registry[ucfirst($key)];
		return false;
	}
}