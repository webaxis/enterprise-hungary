<?php

class WX_List_Static {

	static public function get($method, $id) {
		$class = APP_NAME . '_Static';
		$list = call_user_func($class . '::' . $method);

		return $list[$id];
	}
}

?>