<?php

class WX_List_Create
{
	private $_r;
	private $_paginator;
	
	public function __construct ($params, $descriptor) {
		$trace 				= debug_backtrace();
		
		// Vars related to list params.
		$config		= array(
			# EZ EGY FONTOS KAPCSOLO!
			#
			# Ha TRUE: lehet hasznalni altalanos template-t (lista template, field template, button template stb), viszont
			# akkor a Dt_List_Descriptors_Adminpresspackage.php fajlban lathatoak szerint kell elkesziteni a strukturat
			#
			# Ha FALSE: a regi forma szerint lehet kesziteni a listakat, es a fent leirtakat nem lehet hasznalni
			'newMode'			=> false,


			'id'				=> null,			// *Id of list. (Eg: user-list)
			'template'			=> 'admin.phtml',	// *Template of the list. (Eg: list.phtml) Will look for it in the project's list template folder.
			'listDataType'		=> 'array', 		// Data can be passed to the template as an array of data, or as an object instantiated by the row's ID. (Values: array|object)
			'dataClass'			=> false,			// The object which should be instantiated when using object listDataType (Eg: App_Book)
			'dbSelect'			=> false,			// DBSelect is a Zend DB Select object (Eg: $db->select()->from('user')) - either dbSelect or array (below) should be used.
			'array'				=> array(),			// Array is an array of elements to list - either dbSelect or array (below) should be used.
			'converter'			=> false,			// Name of content converter class and function, separated by :: (Eg: App_ContentConverter_User::dbToList).
			'convertAll'		=> true,			// If true it will convert all the rows, if false only those which are listed.
			'pages'				=> 5,				// Number of page numbers to show before and after the current page.
			'paginator'			=> array(			// Details of the paginator
				'type'				=> 'pages', 			// Type of the paginator. Values: 'pages' - will display separate pages to paginate, 'more' - will append results to the list.
				'template'			=> 'paginator.phtml',	// Template of the paginator. (Eg: paginator.phtml) Will look for it in the project's list template folder.
				'moreText'			=> 'Load more',			// The text of load more link in case of 'more' type.
				'loaderText'		=> 'Loading...',		// The text displayed while loading more results in case of 'more' type.
			),
			'moreContainer'		=> false,			
			'structure'			=> array()					// Structure of table to display (if it's a table). (Eg. array('id'=>array('label'=>'ID')))
		);
		
		// Vars that are related to data display.
		$paramVars	=  array(
			'page'				=> 1,		// Number of page to display from data.
			'limit'				=> 10,		// Number of elements to list on one page.
			'order'				=> false,	// Type of ordering. (Values: DESC, ASC) Can be only used with 'by'
			'by'				=> false,	// Key of ordering. (Eg: id) Can be only used with 'order'.
			'refresh'			=> false,	// Determines if the request is a refresh (related to 'more' type list - if it's a refresh we have to load the list until the current page)
			'itemCount'			=> false	// If you have the count value of all the items number you can set it if it would be too resource consuming to set it on every list event.
		);
		
		// Vars set up automatically
		$systemVars	= array(
			'module'			=> null,	// The name of the module. Filled by the system automatically.
			'controller'		=> null		// The name of the controller. Filled by the system automatically.
		);
		
		$paginatorTypes		= array('pages','more');

		$this->_r = new WX_List_Registry();
		//DX(strtolower(end(explode('_',$trace[1]['class']))));
		@$this->_r->setDescriptorName(strtolower(end(explode('_',$trace[1]['class']))));
		$this->_r->setConfig(Zend_Registry::get('config')->list);
		$this->_r->setWXTemplateDirectory(WX_PATH . 'List/Templates/');
		
		foreach ($paramVars as $key => $default) {
			switch ($key) {
				case 'limit':
					// Search for limit in $params, if not found set by descriptor, if not found, set default
					$value = WX_Tools::getValue($params, $key, WX_Tools::getValue($descriptor, $key, $default));
					break;
					
				default:
					// Search for var in $params, if not found set defaul
					$value = WX_Tools::getValue($params, $key, $default);
					break;
			}
			
			$this->_r->set($key, $value);
		}
		
		foreach ($systemVars as $key => $default) {
			$value = WX_Tools::getValue($params, $key, $default);
			
			$this->_r->set($key, $value);
		}		
		
		foreach ($config as $key => $default) {
			$value = false;
		
			switch ($key) {
				case 'paginator':
					if (isset($descriptor[$key])) {
						foreach ($default as $paginatorKey => $paginatorDefault) {
							$paginatorValue = isset($descriptor['paginator'][$paginatorKey]) ? $descriptor['paginator'][$paginatorKey] : $paginatorDefault;
							
							if ($paginatorKey == 'type' && !in_array($paginatorValue, $paginatorTypes)) {
								$paginatorValue = $paginatorDefault;
							}
							
							$this->_r->set('paginator' . ucfirst($paginatorKey), $paginatorValue);
						}
					} else {
						$this->_r->set('paginatorType', $default['type']);
						$this->_r->set('paginatorTemplate', $default['template']);
					}
					break;

				case 'moreContainer':
					$value = WX_Tools::getValue($descriptor, $key, $descriptor['id'] . '-container');
					break;
					
				case 'newMode' :
					if (isset($descriptor[$key])) {
						$value = $descriptor[$key];
					} else {
						$value = $default;
					}

					break;
					
				default:
					$value = WX_Tools::getValue($descriptor, $key, $default);
					break;
			}
			
			if ($value) {
				$this->_r->set($key, $value);
			}
		}
		
		$this->_r->setParams($params);
		
		$data = $this->_createData();
		$this->_r->setData($data['formatted']);
		$this->_r->setRawData($data['raw']);
		$paginator = new WX_List_Paginator($this->_r);
		$this->_setPaginator($paginator->getPaginator());
	}
	
	/*
	 * When this class is called with a static function
	 * that should be equivalent to the descriptor name
	 * the descriptor is automatically called.
	 */
	static public function __callStatic ($method, $params) {
		$config = Zend_Registry::get('config')->list;
		$path = $config->descriptorDirectory;
		$pathParts = explode('/',str_replace(trim("\ "), "/", $path));
		$className = '';
		foreach ($pathParts as $part) {
			if ($part == APP_NAME)
				$className = APP_NAME;
			else
				$className .= '_' . $part;
		}
		$className .= ucfirst($method);
		return call_user_func_array(array($className,'get'),$params);
	}
	
	private function _createHtml() {
		$view = $this->_createView();
		
		if ($this->_r->getPaginatorType() == 'more' && $this->_r->getPage() > 1 && !$this->_r->getRefresh()) {
			$html = $view->render('more-' . $this->_r->getTemplate());
			$view->setScriptPath($this->_r->getWXTemplateDirectory());
			$html.= $view->render('inputs.phtml');
		} else {
			$view->setScriptPath($this->_r->getWXTemplateDirectory());
			$html = $view->render('container.phtml');
		}
		
		return $html;
	}
	
	/*
	 * Renders the list via an extended view class
	 */
	public function render () {
		echo $this->_createHtml();
	}
	
	/*
	 * Fetches the list via an extended view class
	 */
	public function fetch () {
		return $this->_createHtml();
	}

	private function _createView () {
		$view = new Zend_View;
		$view->setScriptPath($this->_r->getWXTemplateDirectory());
		$view->setHelperPath(WX_PATH . 'List/Helpers/', 'WX_List_Helpers');
		$view->r = $this->_r;
		Zend_Paginator::setDefaultScrollingStyle('Sliding');
		
		$view->setScriptPath(WX_List_Helper::getTemplatePath($this->_r->getPaginatorTemplate(),$this->_r));
		Zend_View_Helper_PaginationControl::setDefaultViewPartial($this->_r->getPaginatorTemplate());
		$this->_paginator->setItemCountPerPage($this->_r->getLimit());
		$this->_paginator->setView($view);
		$this->_paginator->setPageRange($this->_r->getPages());
		$this->_r->setPaginator($view->paginationControl($this->_paginator,null,null,array('r' => $this->_r)));
		$this->_r->setPaginatorObj($this->_paginator);
		
		return $view;
	}

	/*
	 * Sets the data the should be listed and sets paginator
	 */
	private function _createData () {
		$type = 'array';
		if ($this->_r->getDbSelect() != false) {
			$type = 'dbSelect';
		} elseif ($this->_r->getQuery() != false) {
			$type = 'query';
		}
		
		$this->_r->setDataType($type);
		switch ($type) {
			case 'array':
				$raw = $data = $this->_r->getArray();
				
				if ($this->_r->getConvertAll() === true) {
					$data = $this->_convertData($data, $raw);
				}
				
				if ($this->_r->getOrder() != false && $this->_r->getBy() != false) {
					$data = WX_Tools::sortByOneKey($data, $this->_r->getBy(),($this->_r->getOrder() == 'ASC' ? true : false));
				}
				
				if (!$this->_r->getRefresh()) {
					$paginator = Zend_Paginator::factory($data);
					$paginator->setCurrentPageNumber($this->_r->getPage());
					$paginator->setItemCountPerPage($this->_r->getLimit());
				} else {
					$paginator = Zend_Paginator::factory($data);
					$paginator->setCurrentPageNumber(1);
					$paginator->setItemCountPerPage($this->_r->getLimit() * $this->_r->getPage());
				}
				
				$data = array();
				foreach($paginator as $one) {
					$data[] = $one;
				}
				
				if ($this->_r->getConvertAll() === false) {
					$data = $this->_convertData($data, $raw);
				}
				
				break;
				
			case 'dbSelect':
				$db = Zend_Registry::get('db');
				if ($this->_r->getRefresh() && $this->_r->getPaginatorType() == 'more') {
					$limit = $this->_r->getPage() * $this->_r->getLimit();
					$query = $this->_r->getDbSelect()->limit($limit,'0');
				} else {
					$query = $this->_r->getDbSelect()->limitPage($this->_r->getPage(),$this->_r->getLimit());
				}
				if ($this->_r->getOrder() != false && $this->_r->getBy() != false && $this->_r->getOrder() != 'false' && $this->_r->getBy() != 'false') {
					$oldOrders = $query->getPart('order');
					$query->reset(Zend_Db_Select::ORDER);
					$query->order($this->_r->getBy().' '.$this->_r->getOrder());
					if (!empty($oldOrders)) {
						foreach ($oldOrders as $one) {
							$query->order($one[0].' '.$one[1]);
						}
					}
				}
				
				$stmt = $db->query($query);
				$data = $raw = $stmt->fetchAll();
				
				$data = $this->_convertData($data, $raw);
				break;
				
			default:
				
		}
		
		$return = array(
			'formatted' => $data,
			'raw' => $raw
		);
		if (count($data) == 0 && $this->_r->getPage() > 1) {
			$this->_r->setPage($this->_r->getPage() - 1);
			$return = $this->_createData();
		}
		
		return $return;
	}
	
	private function _convertData ($data, $raw) {
		if ($this->_r->getListDataType() == 'object') {
			$data = array();
			foreach ($raw as $one) {
				$className = $this->_r->getDataClass();
				$data[] = new $className($one['id']);
			}
		} elseif ($this->_r->getConverter()) {
			$converter = $this->_r->getConverter();
			if (strstr($converter,'::')) {
				$converterParts = explode('::',$converter);
				$converterClass = $converterParts[0];
				$converterMethod = $converterParts[1];
		
				$data = call_user_func(array($converterClass,$converterMethod),$data,$this->_r->getParams());
			} else {
				$data = call_user_func($converter.'::all',$data,$this->_r->getParams());
			}
		}
		return $data;
	}

	private function _setPaginator ($paginator) {
		$this->_paginator = $paginator;
	}
}