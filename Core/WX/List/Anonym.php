<?php

/**
 * Class used to extend Zend_View with setter and getter methods from an associative array passed to the constructor
 */
class WX_List_Anonym extends Zend_View
{
	private $_anonym = array();
	
	public function __construct ($params = array()) {
		foreach ($params as $key => $value) {
			$this->set($key, $value); 
		}
	} 
	
	public function __call ($method, $params) {
		$firstThree = substr($method, 0, 3);
		if ($firstThree == 'set') {
			$this->set(lcfirst(str_replace('set', '', $method)), $params[0]);
		} elseif ($firstThree == 'get') {
			return $this->get(lcfirst(str_replace('get', '', $method)));
		}
	}
	
	public function set ($key, $value) {
		$this->_anonym[$key] = $value;
	}
	
	public function get ($key) {
		if (isset($this->_anonym[$key]))
			return $this->_anonym[$key];
		return false;
	}
}