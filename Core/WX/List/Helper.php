<?php

class WX_List_Helper
{
	private $_r;
	
	public function createUrl ($r, $page = 1, $order = false, $by = false) {
		$options = array();
		
		$options['page'] = $page;
		
		if (($r->getOrder() != false && $r->getBy() != false) || ($order != false && $by != false)) {
			$options['by'] = $r->getBy();
			$options['order'] = $r->getOrder();
			
			if ($by != false)
				$options['by'] = $by;
			
			if ($order != false)
				$options['order'] = $order;
			
		}
		if ($r->getPaginatorType() == 'ajax') {
			$options['wx-list-id'] = $r->getId();
			$options['controller'] = $r->getController();
			$module = $r->getModule() != 'default' ? $r->getModule() . '/' : false;
			$url = BASE_HOST . $module . $r->getController() . '/wxlist';
			
			foreach ($options as $key => $option) {
				$url .= '/' . $key . '/' . $option;
			}
		} else {
			$router = Zend_Controller_Front::getInstance()->getRouter();
			$url = $router->assemble($options);
		}
		
		return $url;
	}
	
	public function createSimpleUrl ($r, $page = 1) {
		$options = array();
		
		$urlParts = explode('/',$_SERVER['REQUEST_URI']);
		
		$url = BASE_PATH;
		$pageKey = false;
		
		foreach ($urlParts as $key => $part) {
			if ($part == 'page')
				$pageKey = $key + 1;
				
			if ($pageKey === $key && $page !== false) {
				$url .= $page . '/';
			} elseif ($part != '') {
				if ($part == 'page' && $page === false) {
					$url .= '';
				} elseif ($key === $pageKey && $page === false) {
					$url .= '';
				} else {
					$url .= $part . '/';
				}
			}
		}
		
		if ($pageKey === false && $page !== false)
			$url .= 'page/' . $page;
			
		
		return $url;
	}
	
	public function getFilterTemplatePath ($template, $registry) {
		$filterDirectory = $registry->getConfig()->templateDirectory . 'Filters/';
		if (file_exists($filterDirectory . strtolower($template)))
			return $filterDirectory;
		return $registry->getWXTemplateDirectory() . 'Filters/';
	}
	
	static public function getTemplatePath ($template, $registry) {
		$filterDirectory = $registry->getConfig()->templateDirectory;
		if (file_exists($filterDirectory . strtolower($template)))
			return $filterDirectory;
		return $registry->getWXTemplateDirectory();
	}
	
	public function getTemplate ($template, $registry) {
		$path = self::getTemplatePath($template, $registry);
		return $path . $template;
	}
}