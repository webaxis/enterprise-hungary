<?php

class WX_Form_Validate {
	
	protected $_descriptor = array();
	
	protected $_descriptorName = null;

	protected $_params = array();

	protected $_isReturn = false;

	protected $_emptyFormOnSuccess = true;

	public function __construct() {

	}

	public function setDescriptor($descriptor) {
		$this->_descriptor = $descriptor;
		return $this;
	}

	public function setFormDescriptorName($descriptorName) {
		$this->_descriptorName = $descriptorName;
		return $this;
	}

	public function setParams($params) {
		$this->_params = $params;
		return $this;
	}

	public function setIsReturn($isReturn) {
		$this->_isReturn = $isReturn;
		return $this;
	}

	public function setEmptyFormOnSuccess($emptyFormOnSuccess) {
		$this->_emptyFormOnSuccess = $emptyFormOnSuccess;
		return $this;
	}

	public function createReturn($data = array(), $status) {
		$return = array(
			'status' => $status
		);

		if (!is_array($data)) {
			$return['data'] = array(
				'message' => $data
			);
		} elseif (!empty($data)) {
			$return['data'] = $data;
		}

		return $return;
	}

	public function createError($data = array()) {
		return $this->createReturn($data, 'error');
	}

	public function createSuccess($data = array()) {
		return $this->createReturn($data, 'success');
	}

	public function validate() {
		$rp = $this->_params;

		# the key in the $_descriptor array
		$descriptor = $this->_descriptor;

		# need return or not
		$return = $this->_isReturn;

		# empty form on success
		$emptyFormOnSuccess = $this->_emptyFormOnSuccess;

		# form descriptor class
		$form = WX_DForm::getInstance($this->_descriptorName)->get($rp);

		# validate form and its false, return with fetched form
		if (!$form->isValid($rp)) {
			$data = array();
			if ($return) {
				$data['html'] = $form->fetch();
			}
			return $this->createError($data);
		}

		# when form valid and do not need to run anithing else
		if (!isset($descriptor['onFormSuccess'])) {
			$data = array();
			if ($return) {
				$formParams = array();
				if (!$emptyFormOnSuccess) {
					$formParams = $rp;
				}
				$form = WX_DForm::getInstance($this->_descriptorName)->get($formParams);
				$data['html'] = $form->fetch();
			}
			return $this->createSuccess($data);
		}

		$class = $descriptor['onFormSuccess']['class'];
		$method = $descriptor['onFormSuccess']['method'];
		# call a static function
		$static = WX_Tools::getValue($descriptor['onFormSuccess'], 'static');
		# create an instance and call from the method
		$createInstance = WX_Tools::getValue($descriptor['onFormSuccess'], 'createInstance');

		# when the form valid and need to call the onFormSuccess hook with params...
		if (isset($descriptor['onFormSuccess']['params'])) {
			$params = array();
			foreach ($descriptor['onFormSuccess']['params'] as $param) {
				$params[$param] = $rp[$param];
			}
			if ($static) {
				$result = call_user_func_array($class.'::'.$method, $params);
			} else {
				if ($createInstance) {
					$object = call_user_func($class.'::getInstance');
					$result = call_user_func_array(array($object,$method), $params);
				} else {
					$result = call_user_func_array(array($class,$method), $params);
				}
			}
		# ... without params
		} else {
			if ($static) {
				$result = call_user_func($class.'::'.$method, $rp);
			} else {
				if ($createInstance) {
					$object = call_user_func($class.'::getInstance');
					$result = $object->$method($rp);
				} else {
					$result = call_user_func(array($class,$method), $rp);
				}
			}
		}

		# if no need check the answer of the called class
		if (!isset($descriptor['onFormSuccess']['result'])) {
			$data = array();
			$data['message'] = t(WX_Tools::getValue($descriptor['onFormSuccess']['result'][$result],'message'));
			if ($return) {
				$form = WX_DForm::getInstance($this->_descriptorName)->get($rp);
				$data['html'] = $form->fetch();
			}
			return $this->createError($data);
		}
		
		# check the descriptor: is the result in result cases?
		if (!array_key_exists((string) $result, $descriptor['onFormSuccess']['result'])) {
			$message = 'Unexpected response: "'.$result.'"';
			WX_Exception::create($message);
		}

		# check descriptor: has valid key?
		if (!isset($descriptor['onFormSuccess']['result'][$result]['valid'])) {
			$message = 'You most define the "valid" key (true|false) at "'.$result.'"';
			WX_Exception::create($message);
		}

		# is it a valid answer?
		if ($descriptor['onFormSuccess']['result'][$result]['valid'] !== true) {
			$data = array();
			$data['message'] = t(WX_Tools::getValue($descriptor['onFormSuccess']['result'][$result],'message'));
			if ($return) {
				$form = WX_DForm::getInstance($this->_descriptorName)->get($rp);
				$data['html'] = $form->fetch();
			}
			return $this->createError($data);
		}

		# valid answer
		$data = array();
		$data['message'] = t(WX_Tools::getValue($descriptor['onFormSuccess']['result'][$result],'message'));
		if ($return) {
			$formParams = array();
			if (!$emptyFormOnSuccess) {
				$formParams = $rp;
			}
			$form = WX_DForm::getInstance($this->_descriptorName)->get($formParams);
			$data['html'] = $form->fetch();
		}

		return $this->createSuccess($data);
	}
}