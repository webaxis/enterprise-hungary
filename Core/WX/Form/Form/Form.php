<?php
/**
 * Form class from JSON.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 */
/**
 * The form class.
 *
 * Valid JSON descriptor
 * <code>
 * {
 *    'templateDirectory':'<the directory of the templates>',
 *    'template':'<the template file of the form>',
 *    'label':'<the label of the group>',
 *    'description':'<the description of the group>',
 *    'url':'<the form submit url>',
 *    'method':'<get|post>',
 *    'defaultType':'the default input type of the form (if not set default is text)',
 *    'items': [],
 *    'buttons': []
 * }
 * </code>
 *
 * @package WX_Form_Form
 **/
class WX_Form_Form_Form extends WX_Form_Form_Element_Group {
    /**
     * Construct the form.
     *
     **/
    public function __construct(){
        $this->setTemplate('form.tpl.php');
    }
    /**
     * var array The buttons sotre.
     **/
    protected $buttons = array();
    /**
     * Add a button to the form.
     *
     * @param Element_Form The button instance
     *
     * @return Form_Form The instance.
     **/
    public function addButton(WX_Form_Form_Element $value){
        //@TODO: Ha nem alapertelmezett a templateDirectory??
        if (isset($value)) {
            $value->setTemplateDirectory($this->getTemplateDirectory());
            $this->buttons[] = $value;
        }
        return $this;
    }
    /**
     * Try to show the form buttons.
     *
     * @return void
     **/
    public function renderButtons(){
        foreach ($this->buttons as $element) {
            if (method_exists($element, 'render')) {
                $element->render();
            }
        }
    }
    /**
     * Get the actual form buttons.
     *
     * @return array The array of the form's buttonss.
     **/
    public function getButtons(){
        return $this->buttons;
    }

    /**
     * Get the actual form elements.
     *
     * @return array The array of the form's elements.
     **/
    public function getElements(){
        return $this->elements;
    }

    /**
     * Return the form method attribute.
     *
     * @return string The form mehtod attribute (GET, POST)
     **/
    public function getMethod(){
        return $this->getAttribute('method');
    }
    /**
     * Set the form method attibute.
     *
     * @param string $value The method attribute value (get, post).
     *
     * @return Form_Form The configured Form instance.
     **/
    public function setMethod($value){
        $this->setAttribute('method', $value);
        return $this;
    }
    /**
     * Set form method to post.
     *
     * @return Form_Form The configured Form instance.
     **/
    public function setPostMethod(){
        $this->setMethod('post');
        return $this;
    }
    /**
     * Set form method to get.
     *
     * @return Form_Form The configured Form instance.
     **/
    public function setGetMethod(){
        $this->setMethod('get');
        return $this;
    }
    /**
     * Get the form action attribute.
     *
     * @return string The action attribute.
     **/
    public function getAction(){
        return $this->getAttribute('action');
    }
    /**
     * Set form action attribute.
     *
     * @param string $value The action attribute value.
     *
     * @return Form_Form The configured Form instance.
     **/
    public function setAction($value){
        $this->setAttribute('action', $value);
        return $this;
    }
    /**
     * Partial Validating the form element. And return the result an object to Ajax JSON formatter.
     *
     * @param array $context The input value for validating the element value.
     * @param staring $name The element name.
     *
     * @return Object The result of the validation.
     **/
    public function partialValidation($context, $name){
        /*$ret = true;
        foreach ($this->elements as $element) {
            $ret = ($element->isValid($context) and $ret);
        }
        $this->validated = true;
        return $ret;
        */
    }
    /**
    * var bool True if the element was validated otherwise false.
    **/
    protected $validated = false;
    /**
     * Throw an Exception if the validation process is not called.
     *
     * Not used
     *
     * @return void
     **/
    protected function checkValidation(){
        if ( !$this->validated ) {
            throw new Exception('isValid() method should have been called!');
        }
    }
    /**
     * Validating the form. And return the result boolean value.
     *
     * @param array  $context The input value for validating the element value.
     * @param string $value   The value of the element (using in checkbox group).
     * @param string $name    The name of the element (using in checkbox group).
     *
     * @return boolean The form value is valid.
     **/
    public function isValid(array $context){
        $this->validated = true;
		if (parent::isValid($context)) {
			$this->isFeedback = true;
			return true;
		} else {
			return false;
		}
    }
    /**
     * Get the element's error messages of the gorup element after the validation.
     *
     * @return array The error messages.
     **/
    public function getMessages(){
        return parent::getMessages();
    }
    /**
     * Get the element's error codess of the gorup element after the validation.
     *
     * @return array The error codes.
     **/
    public function getCodes(){
        return parent::getCodes();
    }
}