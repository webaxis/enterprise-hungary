<?php
/**
 * Email validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Email validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_NotAllowedString implements WX_Form_Form_IValidator
{
    protected $message = 'Not allowed string!';
    protected $code = 203;
    protected $options = array();
    
    public function __construct(array $descriptor = array())
    {
    	if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
   	}

    public function isValid($value, array $context, $name)
    {
    	if (!isset($value)) {
            return true;
        }
        
        foreach($this->options['string'] as $string){
        	if($string==$value){
        		return false;	
        	}
        }
        return true;
		

	}
	
	public function getMessages(){
        return array($this->message);
    }
     
    public function getErrors(){
        return array($this->code);
    }
}