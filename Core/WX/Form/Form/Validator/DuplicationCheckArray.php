<?php
/**
 * Array duplication check validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Array dupliaction check validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */
class WX_Form_Form_Validator_DuplicationCheckArray implements WX_Form_Form_IValidator{

	const WX_Form_FORM_VALIDATOR_DUPLICATIONCHECKARRAY_KEY = 'key';
	
	const WX_Form_FORM_VALIDATOR_DUPLICATIONCHECKARRAY_VALUE = 'value';

	/**
	 * @var int Error code
	 */
	protected $_code = 1;
	
	/**
	 * @var string Error message
	 */
	protected $_message = '';

	/**
	 * @var array Contain validator options
	 */
	protected $_options = array();

	/**
	 * @var array Scan array
	 */
	protected $_array = array();

	/**
	 * @var string Array search mode: by key or values; key|value
	 */
	protected $_mode = '';
	
	/**
	 * The constructor.
	 *
	 * @param array $description The description array.
	 *
	 * @return void
	 **/
	public function __construct(array $descriptor = array()){
		if (isset($descriptor['message'])){
			$this->_message = $descriptor['message'];
		}

		if (isset($descriptor['options'])){
			$this->_options = $descriptor['options'];
			
			if (array_key_exists('instance',$this->_options) &&  $this->_options['instance'] == 'getInstance') {
				if (!isset($this->_options['class'])) {
					throw new Exception('Missing "class" definition at options descriptor');
				}
				if (!isset($this->_options['method'])) {
					throw new Exception('Missing "method" definition at options descriptor');
				}
				$instance = call_user_func($this->_options['class'].'::'.$this->_options['instance']);
				$this->_array = $instance->{$this->_options['method']}();
			} else {
				DX('not singleton mode not implemented');
			}
			
			if (!isset($this->_options['mode'])) {
				throw new Exception('Missing "mode" definition at options descriptor');
			} else {
				if (!in_array($this->_options['mode'],array(self::WX_Form_FORM_VALIDATOR_DUPLICATIONCHECKARRAY_KEY,self::WX_Form_FORM_VALIDATOR_DUPLICATIONCHECKARRAY_VALUE))) {
					throw new Exception('Unknown "mode" at options descriptor: '.$this->_options['mode']);
				} else {
					$this->_mode = $this->_options['mode'];
					
				}
			}
		}
	}
	 /**
	 * The form validation method.
	 *
	 * @param array $context The data to validate.
	 * @param mixed $value   The reference value for validating.
	 *
	 * @return boolean If valid true.
	 **/
   public function isValid($value, array $context, $name)
   {
		if ($value == '') {
			return true;
		}
		switch ($this->_mode) {
			case self::WX_Form_FORM_VALIDATOR_DUPLICATIONCHECKARRAY_KEY :
				if (array_key_exists($value,$this->_array)) {
					return false;
				} else {
					return true;
				}
				break;
			case self::WX_Form_FORM_VALIDATOR_DUPLICATIONCHECKARRAY_VALUE :
				if (in_array($value,$this->_array)) {
					return false;
				} else {
					return true;
				}
				break;
			default:
				throw new Exception('Unknown error at validation!');
				break;
		}

		/*
		if (empty($value)) {
			return true;
		}
		//Pass options
		if (!empty($this->options)) {
			$validator->setOptions($this->options);
		}
		*/

	}
	 /**
	 * Get the error messages of the validation process.
	 *
	 * @return array The error messages array.
	 **/
   public function getMessages(){
		return array($this->_message);
	}
	/**
	 * Get the error codes of the validation process.
	 *
	 * @return array The error code array.
	 **/
	public function getErrors(){
		return array($this->_code);
	}
}