<?php
/**
 * Password check in Db validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Password check in Fb validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_PasswordCheckInDb implements WX_Form_Form_IValidator
{
    protected $message = 'Not valid password!';
    protected $code = 201;
    protected $requiredOptions 
        = array('credential','credentialValue','password','dbClass','dbMethod','dbPassword');
    protected $options = array(
        'credential'      => NULL,
        'credentialValue' => NULL,
        'password'        => NULL,
        'dbClass'         => NULL,
        'dbMethod'        => NULL,
        'dbPassword'      => NULL
    );
    
    public function __construct(array $descriptor = array())
    {
    	if (isset($descriptor['message'])) {
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])) {
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])) {
            $this->options = $descriptor['options'];
        }
   	}

    public function isValid($value, array $context, $name)
    {
    	if (!isset($value)) {
            return true;
        }

        foreach ($this->requiredOptions as $key) {
            if (!array_key_exists($key,$this->options) || @$this->options[$key] == NULL) {
                throw new Exception(ucfirst($this->requiredOptions[$key]).' is empty. Check the options in descriptor!');
            }
        }
        $pwd = $this->options['password'];
        return call_user_func($this->options['dbClass'].'::'.$this->options['dbMethod'],$this->options['credentialValue'],$context[$pwd]);
	}
	
	public function getMessages(){
        return array($this->message);
    }
     
    public function getErrors(){
        return array($this->code);
    }
}