<?php
/**
 * Birthdate validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Birthdate validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_Birthdate implements WX_Form_Form_IValidator {
    protected $message = array(
		'year' => 'Year must be filled',
		'month' => 'Month must be filled',
		'day' => 'Day must be filled'
	);
    protected $code = array(
		'year' => 1,
		'month' => 2,
		'day' => 3
	);
    protected $options = array();
	
	protected $messages = array();
    /**
     * The constructor.
     *
     * @param array $description The description array.
     *
     **/
    public function __construct(array $descriptor = array()){
        if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
    }
     /**
     * The form validation method.
     *
     * @param array $context The data to validate.
     * @param mixed $value   The reference value for validating.
     *
     * @return boolean If valid true.
     **/
    public function isValid($value, array $context, $name){
		foreach ($value as $key => $value) {
			if (empty($value))
				$this->messages[$this->code[$key]] = $this->message[$key];
		}
		
		if (empty($this->messages))
			return true;
			
		return false;
    }
     /**
     * Get the error messages of the validation process.
     *
     * @return array The error messages array.
     **/
   public function getMessages(){
        return array($this->messages);
    }
    /**
     * Get the error codes of the validation process.
     *
     * @return array The error code array.
     **/
    public function getErrors(){
        return array($this->code);
    }
}