<?php
/**
 * AllowedChars validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * AllowedChars validator.
 *
 * Default enable: a-zA-Z0-9
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_AllowedChar implements WX_Form_Form_IValidator
{
    protected $message = 'Not permitted character in input field!';
    protected $code = 202;
    protected $options = array(
		'allowedChars' => 'a-zA-Z '
	);
    /**
    *  Create AllowedChars Validator
    *  
    * $description {
    *    'message' => 'Error message',
    *    'code'    => 'Error code',
    *    'options' => array(
    *       'allowedChars' => '<characters>',
    *       'regExp' => '<regExp>'
    *    )
    * }
    *
    * @param array $description Configuration
    */
    public function __construct(array $descriptor = array())
    {
    	if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
   	}

    public function isValid($value, array $context, $name)
    {
    	if (empty($value)) {
            return true;
        }
		
		$regExp = '/(^['.$this->options['allowedChars'].'])/';
		
        if(preg_match($regExp, $value)){
        	return true;	
        }	
        return false;
	}
	
	public function getMessages(){
        return array($this->message);
    }
     
    public function getErrors(){
        return array($this->code);
    }
}