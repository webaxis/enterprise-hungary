<?php
/**
 * Alnum validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Alnum validator.
 *
 * Default enable: a-zA-Z0-9
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_Alnum implements WX_Form_Form_IValidator
{
    protected $message = 'Not permitted character in input field!';
    protected $code = 202;
    protected $options = array();
    protected $alnum = 'a-zA-Z0-9 ';
    /**
    *  Create AlNum Validator
    *  
    * $description {
    *    'message' => 'Error message',
    *    'code'    => 'Error code',
    *    'options' => array(
    *       'allowedChars' => '<Special characters expect a-zA-Z0-9>'
    *    )
    * }
    *
    * @param array $description Configuration
    */
    public function __construct(array $descriptor = array())
    {
    	if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
   	}

    public function isValid($value, array $context, $name)
    {
    	if (empty($value)) {
            return true;
        }
        
        if(preg_match('/(^['.$this->options['allowedChars'].$this->alnum.'])/', $value)){
        	return true;	
        }	
        return false;
	}
	
	public function getMessages(){
        return array($this->message);
    }
     
    public function getErrors(){
        return array($this->code);
    }
}