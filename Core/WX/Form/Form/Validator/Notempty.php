<?php
/**
 * Not empty validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Not empty validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_Notempty implements WX_Form_Form_IValidator {
    protected $message = 'This field must be filled!';
    protected $code = 1;
    protected $options = array();
    /**
     * The constructor.
     *
     * @param array $description The description array.
     *
     **/
    public function __construct(array $descriptor = array()){
        if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
    }
     /**
     * The form validation method.
     *
     * @param array $context The data to validate.
     * @param mixed $value   The reference value for validating.
     *
     * @return boolean If valid true.
     **/
    public function isValid($value, array $context, $name){
        $validator = new Zend_Validate_NotEmpty();

        //Pass options
        if (!empty($this->options)) {
            $validator->setOptions($this->options);
        }

        if ($validator->isValid($value)) {
            return true;
        } else {
            return false;
        }
    }
     /**
     * Get the error messages of the validation process.
     *
     * @return array The error messages array.
     **/
   public function getMessages(){
        //@todo: Ez rugalmasabb kell, hogy legyen
        return array($this->message);
    }
    /**
     * Get the error codes of the validation process.
     *
     * @return array The error code array.
     **/
    public function getErrors(){
        return array($this->code);
    }
}