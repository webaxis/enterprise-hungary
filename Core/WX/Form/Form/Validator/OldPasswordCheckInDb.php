<?php
/**
 * Password check in Db validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Password check in Fb validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_OldPasswordCheckInDb implements WX_Form_Form_IValidator
{
    protected $message = 'Not valid password!';
    protected $code = 201;
    protected $requiredOptions 
        = array('credential','credentialValue','password','dbClass','dbMethod','dbPassword');
    protected $options = array(
        'credential'      => NULL,
        'credentialValue' => NULL,
        'password'        => NULL,
        'dbClass'         => NULL,
        'dbMethod'        => NULL
    );
    
    public function __construct(array $descriptor = array())
    {
    	if (isset($descriptor['message'])) {
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])) {
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])) {
            $this->options = $descriptor['options'];
        }
   	}

    public function isValid($value, array $context, $name)
    {
		
	}
	
	public function getMessages(){
        return array($this->message);
    }
     
    public function getErrors(){
        return array($this->code);
    }
}