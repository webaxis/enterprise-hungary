<?php
/**
 * DB duplication check validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form	
 * @subpackage Validators
 */

/**
 * DB duplication check validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_DuplicationCheckDb implements WX_Form_Form_IValidator
{
	protected $message = 'This item is already exists!';
	protected $code = 204;
	protected $options = array('model'=>NULL, 'function'=>NULL);
	protected $allowedValues = array();
	
	public function __construct(array $descriptor = array())
	{
		if (isset($descriptor['message'])) {
			$this->message = $descriptor['message'];
		}
		if (isset($descriptor['code'])) {
			$this->code = $descriptor['code'];
		}
		if (isset($descriptor['options'])) {
			$this->options = $descriptor['options'];
			if (isset($descriptor['options']['allowedValues'])){
				$this->allowedValues = $descriptor['options']['allowedValues'];
			}
		}
   	}

	public function isValid($value, array $context, $name)
	{
		if (empty($value))
			return true;
			
		if (in_array($value,$this->allowedValues)) {
			return true;
		}

		if(empty($this->options['model']) || empty($this->options['function'])){
			return false;
		}
 
 		$isExist = call_user_func($this->options['model'].'::'.$this->options['function'],$value);
 
 		if(!$isExist){
 			return true;
 		}
 		return false;
	}
	
	public function getMessages(){
		return array($this->message);
	}

	public function getErrors(){
		return array($this->code);
	}
}