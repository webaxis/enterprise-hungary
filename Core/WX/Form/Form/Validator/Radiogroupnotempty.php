<?php
/**
 * Radio group not empty validator class.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */
/**
 * Radio group not empty validator class.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */
class WX_Form_Form_Validator_RadioGroupNotEmpty implements WX_Form_Form_IValidator{
    /**
     * var string Error message.
     **/
    protected $message = 'This field must be filled!';
    /**
     * var integer Erroro code.
     **/
    protected $code = 1;
    /**
     * The constructor.
     *
     * @param array $description The description array.
     *
     **/
    public function __construct($descriptor = array()){
        if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
    }
     /**
     * The form validation method.
     *
     * @param array $context The data to validate.
     * @param mixed $value   The reference value for validating.
     *
     * @return boolean If valid true.
     **/
    public function isValid($value, array $context, $name){
        //If $context contain group name value the input parameter $value contains the name of group

        if ( strlen($name) && isset($context[$name])){
            return true;
        }else{
            return false;
        }
    }
     /**
     * Get the error messages of the validation process.
     *
     * @return array The error messages array.
     **/
    public function getMessages(){
        //@todo: Ez rugalmasabb kell, hogy legyen
        return array($this->message);
    }
    /**
     * Get the error codes of the validation process.
     *
     * @return array The error code array.
     **/
    public function getErrors(){
        return array($this->code);
    }
}