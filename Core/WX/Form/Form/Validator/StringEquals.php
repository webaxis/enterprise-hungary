<?php
/**
 * Email validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Email validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_StringEquals implements WX_Form_Form_IValidator
{
    //protected $message = 'Strins are not equal!';
    protected $code = 201;
    //protected $options = array('string1'=>NULL, 'string2'=>NULL);
    
    public function __construct(array $descriptor = array())
    {
    	if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
   	}

    public function isValid($value, array $context, $name)
    {
    	if (!isset($value)) {
            return true;
        }
        if (!empty($context[$this->options['string1']]) || !empty($context[$this->options['string2']])) {
        	if($context[$this->options['string1']] === $context[$this->options['string2']]){
        		return true;	
        	} else {
				return false;
			}
        }
        //var_dump($this->options['string2']);die;
        return true;
	}
	
	public function getMessages(){
        return array($this->message);
    }
     
    public function getErrors(){
        return array($this->code);
    }
}