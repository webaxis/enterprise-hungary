<?php
/**
 * LengthBetween validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * LengthBetween validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_Length implements WX_Form_Form_IValidator{
    protected $message = array(
		'min' => 'This input is too short',
		'max' => 'This input is too long'
	);
    protected $code = 200;
    protected $options = array(
		'min' => NULL,
		'max' => NULL
	);
    /**
     * The constructor.
     *
     * @param array $description The description array.
     *
     **/
     
    public function __construct(array $descriptor = array()){
        if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
    }
    
     /**
     * The form validation method.
     *
     * @param array $context The data to validate.
     * @param mixed $value   The reference value for validating.
     *
     * @return boolean If valid true.
     **/
     
   public function isValid($value, array $context, $name){
		$msg = $this->message;
		$this->message = '';
		
        if ($value == '') {
            return true;
        }
		
		if (isset($this->options['min']) && !is_null($this->options['min']) && strlen($value) < $this->options['min']) {
			$this->messages = $this->message = $msg['min'];
			return false;
		} elseif (isset($this->options['max']) && !is_null($this->options['max']) && strlen($value) < $this->options['max']) {
			$this->messages = $this->message = $msg['max'];
			return false;
		}
		if (empty($this->messages))
			return true;
    }
    
     /**
     * Get the error messages of the validation process.
     *
     * @return array The error messages array.
     **/
     
   public function getMessages(){
        //@todo: Ez rugalmasabb kell, hogy legyen
        return array($this->message);
    }
    
    /**
     * Get the error codes of the validation process.
     *
     * @return array The error code array.
     **/
     
    public function getErrors(){
        return array($this->code);
    }
} 