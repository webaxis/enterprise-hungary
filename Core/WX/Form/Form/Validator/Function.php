<?php
/**
 * Free function check validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form	
 * @subpackage Validators
 */

/**
 * Free function check validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_Function implements WX_Form_Form_IValidator
{
    protected $message = 'This is not correct!';
    protected $code = 204;
    protected $options = array(
		'class' 	=> NULL,
		'function'	=> NULL
	);
    
    public function __construct(array $descriptor = array())
    {
    	if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
   	}

    public function isValid($value, array $context, $name)
    {
		if (empty($value))
			return true;
			
    	if(empty($this->options['class']) || empty($this->options['function'])){
        	return false;
        }
 		
 		return call_user_func($this->options['class'].'::'.$this->options['function'],$value);
	}
	
	public function getMessages(){
        return array($this->message);
    }
     
    public function getErrors(){
        return array($this->code);
    }
}