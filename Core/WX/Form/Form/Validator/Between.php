<?php
/**
 * Email validator.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

/**
 * Email validator.
 *
 * @package WX_Form_Form
 * @subpackage Validators
 */

class WX_Form_Form_Validator_Between implements WX_Form_Form_IValidator{
    protected $message = 'This input is not valid';
    protected $code = 200;
    protected $options = array('min'=>3,'max'=>50);
    /**
     * The constructor.
     *
     * @param array $description The description array.
     *
     **/
     
    public function __construct(array $descriptor = array()){
        if (isset($descriptor['message'])){
            $this->message = $descriptor['message'];
        }
        if (isset($descriptor['code'])){
            $this->code = $descriptor['code'];
        }
        if (isset($descriptor['options'])){
            $this->options = $descriptor['options'];
        }
    }
    
     /**
     * The form validation method.
     *
     * @param array $context The data to validate.
     * @param mixed $value   The reference value for validating.
     *
     * @return boolean If valid true.
     **/
     
   public function isValid($value, array $context, $name){
        if (!isset($value)) {
            return true;
        }
		//var_dump($this->options);die;
        $validator = new Zend_Validate_Between($this->options);

        //Pass options
        if (!empty($this->options)) {
        	$validator->setMin($this->options['min']);
        	$validator->setMax($this->options['max']);
        }
        //var_dump($value);die;

        if ($validator->isValid(mb_strlen($value))) {
            return true;
        } else {
            return false;
        }
    }
    
     /**
     * Get the error messages of the validation process.
     *
     * @return array The error messages array.
     **/
     
   public function getMessages(){
        //@todo: Ez rugalmasabb kell, hogy legyen
        return array($this->message);
    }
    
    /**
     * Get the error codes of the validation process.
     *
     * @return array The error code array.
     **/
     
    public function getErrors(){
        return array($this->code);
    }
} 