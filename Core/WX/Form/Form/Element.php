<?php

class WX_Form_Form_Element extends WX_Form_Form_Element_Abstract {
    /**
     * var string The label text of the element.
     **/
    protected $label;
    /**
     * Set the element label.
     *
     * @param string value
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setLabel($value){
        $this->label = $value;
        return $this;
    }
    /**
     * Return the label string.
     *
     * @return string The label.
     **/
    public function getLabel(){
        return $this->label;
    }
    /**
    * var boolean The field is allowed empty.
    **/
    protected $allowBlank = true;
    /**
     * Set the element allow blank parameter.
     *
     * @param boolean value
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setAllowBlank($value){
        $this->allowBlank = $value;
        return $this;
    }
    /**
     * Return the allow blank parameter.
     *
     * @return boolean The parameter value.
     **/
    public function getAllowBlank(){
        return $this->allowBlank;
    }
    /**
    * var boolean The field is allowed empty.
    **/
    protected $options = array();
    /**
     * Set the element options parameter.
     *
     * @param array value
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setOptions($value){
        $this->options = $value;
        return $this;
    }
    /**
     * Return the options parameter.
     *
     * @return boolean The parameter value.
     **/
    public function getOptions(){
        return $this->options;
    }
    /**
     * Get the options string of the element.
     *
     * @return string The options string.
     **/
    protected function getOptionsString(){
        $ret = ' ';
        foreach ($this->options as $name => $value) {
            $ret .= 'data-' . $name .'="'.str_replace('"',"'",$value).'" ';
        }
        return $ret;
    }
    /**
     * Enable element
     *
     * @return Element_Form The instance.
     **/
    public function enable(){
        $this->setProperty('enable', true);
        unset($this->attributes['disabled']);
        return $this;
    }
    /**
     * Disable element.
     *
     * @return Element_Form The instance.
     **/
    public function disable(){
        $this->setProperty('enable', false);
        $this->setAttribute('disabled', '');
        return $this;
    }
    /**
     * Set the element readonly.
     *
     * @return Element_Form The instance.
     **/
    public function readonly(){
        $this->setAttribute('readonly', '');
        return $this;
    }
    /**
     * var string The element value.
     **/
    protected $value ='';
    /**
     * Initialize the element value.
     *
     * This method is called when the element need a init value before setValue() call.
     * For example: Checkbox, radio button value initialize this method call and the setValue() set the checked.
     *
     * @param string $value The element value.
     *
     * @return Element_Form The instance.
     *
     **/
    public function initValue($value){
        $this->value = $value;
        return $this;
    }
    /**
     * Return the form Element type attibute (text, textarea, password, submit).
     *
     * @return string The Element type attribute
     **/
    public function getType(){
        return $this->getAttribute('type');
    }
    /**
     * Set the Element type attibute (text, textarea, password, submit).
     *
     * @param string $value The type attribute value.
     *
     * @return Form_Element The configured Form instance.
     **/
    public function setType($value){
        $this->setAttribute('type', $value);
        return $this;
    }
    /**
     * Return the form Element value.
     *
     * @return string The Element value attribute
     **/
    public function getValue($arrayFormat = false){
        if ( $arrayFormat ) {
            return array($this->getName() => $this->value);
        }else{
            return $this->value;
        }
    }
    /**
     * Set the Element value attibute.
     *
     * @param string $value The value attribute value.
     *
     * @return Element_Form The configured Form instance.
     **/
    public function setValue($context){
        if ( isset($context[$this->getName()]) ){
            //@TODO Element Filter
            $this->value = $context[$this->getName()];
			$this->filter();
        }
        return $this;
    }
    /**
     * Try to show the element.
     *
     * @return void
     **/
    public function render(){
        include($this->getTemplateDirectory().$this->getTemplate());
    }
    /**
     * var array The validator object store.
     **/
    protected $validators = array();
    /**
     * Add a validator object ot the form.
     *
     * @param WX_Form_Form_IValidator $validator The validator object.
     *
     * @return Element_Form The instance.
     **/
    public function addValidator(WX_Form_Form_IValidator $validator){
        if (is_object($validator)) {
            $this->validators[] = $validator;
        } else {
			throw new Exception('Validator isn\'t an object!');
		}
        return $this;
    }
    /**
     * Add a filter object ot the form.
     *
     * @param WX_Form_Form_IValidator $vallidator The validator object.
     *
     * @return Element_Form The instance.
     **/
    public function addFilter($filter){
        //var_dump("------------- ADD FILTER");
        if (is_object($filter)) {
            $this->filters[] = $filter;
        }
        return $this;
    }

    /**
     * Return true is any error after the validation.
     *
     * @return boolean Is an error in element validation.
     **/
    public function isError(){
        return count($this->messages) or count($this->codes) ? true : false;
    }
    /**
     * Validating the element. And return the result boolean value.
     *
     * @param array  $context The input value for validating the element value.
     * @param string $value   The value of the element (using in checkbox group).
     * @param string $name    The name of the element (using in checkbox group).
     *
     * @return boolean The form value is valid.
     **/
    public function isValid(array $context){
        $ret = true;
        $this->messages = array();
        $this->codes = array();
        foreach($this->validators as $k => $validator) {
            if (method_exists($validator,'isValid')) {
                //@todo: Itt meg megfelelo parameteratadas kell
	//D($this->getValue());
                $v = $validator->isValid($this->getValue(), $context, $this->getName());
                $ret = $ret && $v;
                if (!$v) {
                    $this->messages = array_merge($this->messages, $validator->getMessages());
                    $this->codes = array_merge($this->codes, $validator->getErrors());
                }
            }
        }
       $this->validated = true;
       return $ret;
    }

    /**
     * Calling the filters
     *
     * It is have to be called before
     */
    public function filter() {
        if (!empty($this->filters)) {
            foreach ($this->filters as $filter) {
                if (method_exists($filter,'filter')) {
                    $val = $this->getValue();
					if (is_array($val)) {
						foreach ($val as $k => $v){
							$filteredValue = $filter->filter($v);
							$this->value[$k] = $filteredValue;
						}
					} else {
						$filteredValue = $filter->filter($val);
						$this->value = $filteredValue;
					}
                }
            }
        }
    }
	
    /**
     * Set Element data.
     *
     * @param mixed $value String, array or object; Data for element e.g. month names.
     *
     * @return Element_Form The configured Form instance.
     **/
    public function setData($data){
		$this->data = $data;
	}
}