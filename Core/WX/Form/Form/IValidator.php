<?php

interface WX_Form_Form_IValidator {
    /**
     * The form validation method.
     *
     * @param array $context The data to validate.
     * @param mixed $value   The reference value for validating.
     *
     * @return boolean If valid true.
     **/
    function isValid($value, array $context, $name);
    /**
     * Get the error messages of the validation process.
     *
     * @return array The error messages array.
     **/
    function getMessages();
    /**
     * Get the error codes of the validation process.
     *
     * @return array The error code array.
     **/
    function getErrors();
}
