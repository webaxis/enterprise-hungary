<?php
/**
 * Checked_Element class.
 *
 * PHP version 5
 *
 * @package WX_Form
 * @subpackage Elements
 */
/**
 * Checked_Element class.
 *
 * @package WX_Form
 * @subpackage Elements
 */
class WX_Form_Form_Element_Checked extends WX_Form_Form_Element{
    /**
    * Set the element checked.
    *
    * @return CheckedElement_Form The instance.
    **/
    public function checked(){
        $this->setProperty('checked', true);
        return $this;
    }
    /**
    * Set the element unchecked.
    *
    * @return CheckedElement_Form The instance.
    **/
    public function unchecked(){
        $this->setProperty('checked', false);
        return $this;
    }
    /**
    * Get the element is checked.
    *
    * @return boolean Is checked.
    **/
    public function isChecked(){
        return $this->getProperty('checked');
    }
    /**
    * Get the element checked string.
    *
    * @return bool Is checked.
    **/
    public function getChecked(){
        return  $this->isChecked() ? 'checked' : '';
    }
}
