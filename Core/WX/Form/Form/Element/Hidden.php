<?php
/**
 *
 * @package WX_Form_Form
 * @subpackage Elements
 *
 **/
/**
 * Hidden element.
 *
 * Valid JSON descriptor
 * <code>
 * {
 *    'name':'<the input name (required)>',
 *    'vtype':'hidden',
 *    'fieldLabel':'<the label of the field>',
 *    'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
 *    'class':'<the css class to input filed>',
 *    'id':'<the id of the input tag>'
 *    'templateDirectory':'<the directory of the templates>',
 *    'template':'<the template file of the form>',
 *    'validators':[{ //Validators }],
 *    'properties':[{ //properties }]
 * }
 * </code>
 *
 * @package WX_Form_Form
 * @subpackage Elements
 **/
class WX_Form_Form_Element_Hidden extends WX_Form_Form_Element {
    /**
    * Constructor set the type default text and template teyt.tpl.php.
    **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_HIDDEN);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->hiddenTemplate);
    }
}