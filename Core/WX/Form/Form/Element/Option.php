<?php
/**
 * Option Element class.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Elements
 */
/**
 * Option Element class.
 *
 * @package WX_Form_Form
 * @subpackage Elements
 */
class WX_Form_Form_Element_Option extends WX_Form_Form_Element {
    /**
     * Construct the element.
     *
     * Set the tamplate.
     **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_OPTION);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->optionTemplate);
    }
	
	public function getExtraField($field)
	{
		//D($field);
		$ef = $this->getProperty('extraFields');
		//DX($ef);
		if (isset($ef[$field])) {
			return $ef[$field];
		} else {
			throw new Exception('Not valid extraField: '.$field);
		}
	}
	
    /**
     * Get the options string.
     *
     * @return string The options string.
     **/
    public function getOptionsString(){
        $ret = '';
        if ( $this->isProperty('emptyText') ){
            if ( !$this->getValue() ){
                $ret .= '<option selected value="">'.$this->getProperty('emptyText').'</option>';
            } else {
                $ret .= '<option value="">'.$this->getProperty('emptyText').'</option>';
            }

        }
        if ($this->isProperty('items') ) {
            foreach($this->getProperty('items') as $item) {
                if ( is_array($item)) {
                    if ( $this->getValue() == $item[0] ) {
                        $ret .= '<option selected value="'.$item[0].'">'.$item[1].'</option>';
                    } else {
                        $ret .= '<option value="'.$item[0].'">'.$item[1].'</option>';
                    }
                } else {
                    if ( $this->getValue() == $item ) {
                        $ret .= '<option selected value="'.$item.'">'.$item.'</option>';
                    } else {
                        $ret .= '<option value="'.$item.'">'.$item.'</option>';
                    }
                }

            }
        }
        if ($this->isProperty('groups') ) {
            foreach ($this->getProperty('groups') as $group){
                $ret .= '<optgroup label="'.$group['name'].'">';
                foreach($group['items'] as $item) {
                    if ( is_array($item)) {
                        if ( $this->getValue() == $item[0] ) {
                            $ret .= '<option selected value="'.$item[0].'">'.$item[1].'</option>';
                        } else {
                            $ret .= '<option value="'.$item[0].'">'.$item[1].'</option>';
                        }
                    } else {
                        if ( $this->getValue() == $item ) {
                            $ret .= '<option selected value="'.$item.'">'.$item.'</option>';
                        } else {
                            $ret .= '<option value="'.$item.'">'.$item.'</option>';
                        }
                    }

                }
                $ret .= '</optgroup>';
            }
        }
        return $ret;
    }
    /**
     * Return the option items list.
     *
     * @return array The items.
     **/
    public function getItems(){
        if ( $this->isProperty('items') ){
            return  $this->getProperty('items');
        }
        if ( $this->isProperty('groups') ){
            return  $this->getProperty('groups');
        }
        return null;
    }
    public function getSelected(){
        if ($this->isProperty('items') ) {
            foreach($this->getProperty('items') as $item) {
                if ( is_array($item)) {
                    if ( $this->getValue() == $item[0] ) {
                        return array($item[0] => $item[1]);
                    }
                } else {
                    if ($this->getValue() == $item) {
                        return $item;
                    }
                }
            }
        }
        if ($this->isProperty('groups') ) {
            foreach ($this->getProperty('groups') as $group){
                foreach($group['items'] as $item) {
                    if ( is_array($item)) {
                        if ( $this->getValue() == $item[0] ) {
                            return array($item[0] => $item[1]);
                        }
                    } else {
                        if ($this->getValue() == $item) {
                            return $item;
                        }
                    }

                }
            }
        }
        return array();

    }
    /**
     * Set the Element value attibute.
     *
     * @param string $value The value attribute value.
     *
     * @return AbstractElelemnt__Form The configured Form instance.
     **/
   /* public function setValue($context){
        return $this;
    }*/
    /**
     * Return the form Element value.
     *
     * @return string The Element value attribute
     **/
    /*public function getValue($arrayFormat = false){
    }
    */
}