<?php
/**
 * Abstract element class.
 *
 * PHP version 5
 *
 * @package WX_Form
 * @subpackage Elements
 */
/**
 * Abstract element class.
 *
 * @package WX_Form
 * @subpackage Elements
 */
abstract class WX_Form_Form_Element_Abstract {
    protected $style = array();
    /**
     * Set html style parameter.
     *
     * Example backround-color:white addStyle('background-color','white')
     *
     * @param string $key   The style parameter name.
     * @param string $value The style parameter value.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function addStyle($key,$value){
        $this->style[$key] = $value;
        return $this;
    }
    /**
     * Return the Element style attribute.
     *
     * @return string The Element style attribute
     **/
    public function getStyle(){
        $style = '';
        foreach ( $this->style as $name => $value) {
            $style .= $name .':'.$value.';';
        }
        return ' style="'.$style.'" ';
    }
    /**
     * Set the Element width style attibute.
     *
     * @param string $value The type attribute value.
     *
     * @return Form_Element The configured Form instance.
     **/
    public function setWidth($width){
        $this->addStyle('width',$width);
        return $this;
    }

    /**
     * var array The properties.
     **/
    protected $properties = array();
    /**
     * Set a property to the element.
     *
     * @param string $key   The property name.
     * @param string $value The property value.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setProperty($key, $value){
        $this->properties[$key] = $value;        
        return $this;
    }
    /**
     * Get a property value.
     *
     * @param string $key The property name.
     *
     * @return string The property value or null if the property is not set.
     **/
    public function getProperty($key){
        if (isset($this->properties[$key])) {
            return $this->properties[$key];
        };
        return null;
    }
    public function getProperties(){
        if (count($this->properties)) {
            return $this->properties;
        };
        return array();
    }
    /**
     * Get the property enabled.
     *
     * @param string $key The property name.
     *
     * @return boolean True is proeprty is set.
     **/
    public function isProperty($key){
        return isset($this->properties[$key]);
    }

    /**
     * var The class of form.
     **/
    protected $_class;
	/**
     * Set The class value.
     *
     * @param string $value The class value.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setClass($value){
        $this->_class = $value;        
        return $this;
    }
    /**
     * Get The class value.
     *
     * @param string $key The class value.
     *
     * @return string The class value or 'form' if the class is not set.
     **/
    public function getClass(){
        if ( isset($this->_class)) {
            return $this->_class;
        };
        return 'form';
    }

    /**
     * var The feedback.
     **/
    protected $feedback;
	/**
     * Set The feedback value.
     *
     * @param string $value The feedback value.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setFeedback($value){
        $this->feedback = $value;        
        return $this;
    }
    /**
     * Get The feedback value.
     *
     * @param string $key The feedback value.
     *
     * @return string The feedback value or null if the feedback is not set.
     **/
    public function getFeedback(){
        if ( isset($this->feedback)) {
            return $this->feedback;
        };
        return false;
    }
    /**
     * Get the feedback enabled.
     *
     * @return boolean True is feedback is set.
     **/
    public function isFeedback(){
        return isset($this->isFeedback);
    }

    protected $attributes = array();
    /**
     * Return the attributes of the elemenet.
     *
     * @return array The element attibutes.
     **/
    public function getAttributes(){
        return $this->attributes;
    }
    /**
     * Set attributes of the element.
     *
     * @param array $attributes The attributes array to set the element.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setAttributes(array $attributes){
        $this->attributes = array_merge($this->attributes, $attributes);
        return $this;
    }
    /**
     * Set an attribute to the element.
     *
     * The attribute is a HTML attribute for the input HTML element.
     *
     * @param string $key   The attribute name.
     * @param string $value The attribute value.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setAttribute($key, $value){
        $this->attributes[$key] = $value;
        return $this;
    }
    /**
     * Get the attibute value.
     *
     * @param string $key The name of the attribute.
     *
     * @return string The value of the attribute.
     **/
    public function getAttribute($key){
        if (isset($this->attributes[$key])) {
            return $this->attributes[$key];
        };
        return null;
    }


    /**
     * Get the element ID.
     *
     * @return string The HTML id of the element.
     **/
    public function getId(){
        return $this->getAttribute('id');
    }
    /**
     * Set the element ID.
     *
     * @param string The ID.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setId($value){
        $this->setAttribute('id', $value);
        return $this;
    }
    /**
     * Get the name of the element.
     *
     * @return string The name.
     **/
    public function getName(){
        return $this->getAttribute('name');
    }
    /**
     * Set the element name.
     *
     * @param string $value The element name.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setName($value){
        $this->setAttribute('name', $value);
        return $this;
    }



    /**
     * var string The template name.
     **/
    protected $template;
    /**
     * Set the element template.
     *
     * @param string $value The template name.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setTemplate($value){
        $this->template = $value;
        return $this;
    }
    /**
     * Get the template of the element.
     *
     * @return string The name.
     **/
    public function getTemplate(){
        return $this->template;
    }
    /**
    * var string The tamplate directory name.
    **/
    protected $templateDirectory = '';
    /**
     * Set the element template directory.
     *
     * @param string $value The template directory name.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setTemplateDirectory($value){
        $this->templateDirectory = $value;
        return $this;
    }
    /**
     * Get the template directory of the element.
     *
     * @return string The name.
     **/
    public function getTemplateDirectory(){
        return $this->templateDirectory;
    }

    /**
     * Return the form Element value.
     *
     * @return string The Element value attribute
     **/
    abstract public function getValue($arrayFormat = false);
    /**
     * Set the Element value attibute.
     *
     * @param mixed $value The value attribute value.
     *
     * @return AbstractElelemnt__Form The configured Form instance.
     **/
    abstract public function setValue($context);
    /**
     * Try to show the element.
     *
     * @return void
     **/
    abstract public function render();
    /**
     * Validating the elements. And return the result boolean value.
     *
     * @param array  $context The input value for validating the element value.
     * @param string $value   The value of the element (using in checkbox group).
     * @param string $name    The name of the element (using in checkbox group).
     *
     * @return boolean The form value is valid.
     **/
    abstract public function isValid(array $context);
    /**
     * Return true is any error after the validation.
     *
     * @return boolean Is an error in element validation.
     **/
    abstract public function isError();

    /**
     * Enable element
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    abstract public function enable();
    /**
     * Disable element.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    abstract public function disable();
    /**
     * Set the element readonly.
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    abstract public function readonly();
    /**
     * Get the attribute string of the element.
     *
     * @return string The attribute string.
     **/
    protected function getAttributesString(){
        $ret = ' ';
        foreach ($this->attributes as $name => $value) {
//            $ret .= $name .'="'.addslashes($value).'" ';
            $ret .= $name .'="'.str_replace('"',"'",$value).'" ';
        }
        return $ret;
    }

    /**
     * var array The array of the error mesagess.
     **/
    public $messages = array();
    /**
     * Get the element's error messages after the validation.
     *
     * @return array The error messages.
     **/
    public function getMessages(){
        return $this->messages;
    }
    /**
     * Add the element's error messages of the element after the validation.
     *
     * @return array The error messages.
     **/
    public function addMessages(array $messages)
    {
        $this->messages = array_merge($this->messages,$messages);
    }
    /**
     * var array The array of the error codes.
     **/
    protected $codes = array();
    /**
     * Get the element's error codes after the validation.
     *
     * @return array The error codes.
     **/
    public function getCodes(){
        return $this->codes;
    }
    /**
     * Add the element's error codes of the element after the validation.
     *
     * @return array The error messages.
     **/
    public function addCodes(array $codes)
    {
        $this->codes = array_merge($this->codes,$codes);
    }
}
