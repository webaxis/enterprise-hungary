<?php
/**
 * Multiple Option Element class.
 *
 * PHP version 5
 *
 * @package    WX_Form_Form
 * @subpackage Elements
 */
/**
 * MultipleOption Element class.
 *
 * @package    WX_Form_Form
 * @subpackage Elements
 */
class WX_Form_Form_Element_MultipleOption extends WX_Form_Form_Element {
    /**
     * Construct the element.
     *
     * Set default template and template directory.
     *
     * @param WX_Form_Form_Configurator $configurator The default configuration object.
     *
     **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_MULTIPLEOPTION);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->multipleoptionTemplate);
    }
    /**
     * Get the options string.
     *
     * @return string The options string.
     **/
    public function getOptionsString(){
        $ret = '';
        if ( $this->isProperty('emptyText')) {
            if ( !$this->getValue()) {
                $ret .= '<option selected value="">'.$this->getProperty('emptyText').'</option>';
            } else {
                $ret .= '<option value="">'.$this->getProperty('emptyText').'</option>';
            }

        }
        $values = $this->getValue();
        if ($this->isProperty('items')) {
            foreach ($this->getProperty('items') as $item) {
                if ( is_array($values) and count($values) ) {
                    $found = false;
                    foreach ($values as $value) {
                        if ( is_array($item) ) {
                            if ( $value == $item[0] ) {
                                $ret .= '<option selected value="'.$item[0].'">'.$item[1].'</option>';
                                $found = true;
                            }
                        } else {
                            if ( $value == $item ) {
                                $ret .= '<option selected value="'.$item.'">'.$item.'</option>';
                                $found = true;
                            }
                        }

                    }
                    if ( !$found) {
                        if ( is_array($item) ) {
                            $ret .= '<option value="'.$item[0].'">'.$item[1].'</option>';
                        } else {
                            $ret .= '<option value="'.$item.'">'.$item.'</option>';
                        }
                    }

                } else {
                    if ( is_array($item) ) {
                        $ret .= '<option value="'.$item[0].'">'.$item[1].'</option>';
                    } else {
                        $ret .= '<option value="'.$item.'">'.$item.'</option>';
                    }
                }

            }
        }
        if ($this->isProperty('groups') ) {
            foreach ($this->getProperty('groups') as $group) {
                $ret .= '<optgroup label="'.$group['name'].'">';
                foreach ($group['items'] as $item) {
                    if (is_array($values) and  count($values) ) {
                        $found = false;
                        foreach ($values as $value) {
                            if ( is_array($item) ) {
                                if ( $value == $item[0] ) {
                                    $ret .= '<option selected value="'.$item[0].'">'.$item[1].'</option>';
                                    $found = true;
                                }
                            } else {
                                if ( $value == $item ) {
                                    $ret .= '<option selected value="'.$item.'">'.$item.'</option>';
                                    $found = true;
                               }
                            }
                        }
                        if ( !$found) {
                           if ( is_array($item) ) {
                               $ret .= '<option value="'.$item[0].'">'.$item[1].'</option>';
                            } else {
                               $ret .= '<option value="'.$item.'">'.$item.'</option>';
                            }
                        }
                    } else {
                        if ( is_array($item) ) {
                            $ret .= '<option value="'.$item[0].'">'.$item[1].'</option>';
                        } else {
                            $ret .= '<option value="'.$item.'">'.$item.'</option>';
                        }
                    }
                }
                $ret .= '</optgroup>';
            }
        }
        return $ret;
    }
    public function getSelected(){
        $values = $this->getValue();
        $ret = array();
        if ($this->isProperty('items') ) {
            foreach($this->getProperty('items') as $item) {
                if ( is_array($values) and count($values) ) {
                    foreach ($values as $value) {
                        if ( is_array($item)) {
                            if ( $value == $item[0] ) {
                                $ret[] = array($item[0] => $item[1]);
                            }
                        } else {
                            if ( $value == $item ) {
                                $ret[] = $item;
                            }
                        }
                    }
                }
            }
        }
        if ($this->isProperty('groups') ) {
            foreach ($this->getProperty('groups') as $group){
                foreach($group['items'] as $item) {
                    if ( is_array($values) and count($values) ) {
                        foreach ($values as $value) {
                            if ( is_array($item)) {
                                if ( $value == $item[0] ) {
                                    $ret[] = array($item[0] => $item[1]);;
                                }
                            } else {
                                if ( $value == $item ) {
                                    $ret[] = $item;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $ret;

    }
    /**
     * Return the option items list.
     *
     * @return array The items.
     **/
    public function getItems(){
        if ( $this->isProperty('items') ){
            return  $this->getProperty('items');
        }
        if ( $this->isProperty('groups') ){
            return  $this->getProperty('groups');
        }
        return null;
    }

    /**
     * Get the name of the element.
     *
     * @return string The name.
     **/
    public function getName(){
        return $this->getProperty('name');
    }
    /**
     * Set the element name.
     *
     * @param string $value The element name.
     *
     * @return WX_Form_Form_Element_Abstract The instance.
     **/
    public function setName($value){
        $this->setAttribute('name', $value.'[]');
        $this->setProperty('name', $value);
        return $this;
    }

    /**
     * Set the Element value attibute.
     *
     * @param string $value The value attribute value.
     *
     * @return AbstractElelemnt__Form The configured Form instance.
     **/
   /* public function setValue($context){
        return $this;
    }*/
    /**
     * Return the form Element value.
     *
     * @return string The Element value attribute
     **/
    /*public function getValue($arrayFormat = false){
    }
    */
}