<?php
/**
 * Textarea class.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Elements
 */
/**
 * Textarea input element.
 *
 * Valid JSON descriptor
 * <code>
 * {
 *    'name':'<the input name (required)>',
 *    'vtype':'text',
 *    'fieldLabel':'<the label of the field>',
 *    'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
 *    'class':'<the css class to input filed>',
 *    'id':'<the id of the input tag>'
 *    'templateDirectory':'<the directory of the templates>',
 *    'validators':[{ //Validators }],
 *    'properties':[{ //properties }]
 *    'template':'<the template file of the form>'
 * }
 * </code>
 *
 **/
class WX_Form_Form_Element_TextArea extends WX_Form_Form_Element {
    /**
     * Construct the element.
     *
     * Set type and template.
     **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_TEXTAREA);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->textareaTemplate);
    }

}