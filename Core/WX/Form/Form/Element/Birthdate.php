<?php
/**
 * Birthdate Element class.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Elements
 */
/**
 * Birthdate Element class.
 *
 * @package WX_Form_Form
 * @subpackage Elements
 */
class WX_Form_Form_Element_Birthdate extends WX_Form_Form_Element {
    /**
     * Construct the element.
     *
     * Set the tamplate.
     **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_OPTION);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->optionTemplate);
    }
	
    public function setValue($context){
        if ( isset($context[$this->getName()]) ){
			$value = $context[$this->getName()];
			if (is_array($value)) {
				$this->value = $value;
			} else {
				$this->value['year'] = substr($value,0,4);
				$this->value['month'] = substr($value,5,2);
				$this->value['day'] = substr($value,8,2);
			}
			$this->filter();
        }
        return $this;
    }
	
}