<?php
/**
 * Checkbox group class from JSON.
 *
 * PHP version 5
 *
 * @package	WX_Form
 * @subpackage Elements
 */

/**
 * Checkbox group Element.
 *
 * Valid JSON descriptor
 * <code>
 * {
 *	'vtype':'checkboxgroup',
 *	'label':'<the label of the group>',
 *	'description':'<the description of the group>',
 *	'name':'<the name (required)>',
 *	'templateDirectory':'<the directory of the templates>',
 *	'template':'<the template file of the form>',
 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
 *	'items': [],
 *	'properties':[{ //properties }]
 * }
 * </code>
 *
 * @package	WX_Form
 * @subpackage Elements
 *
 **/
class WX_Form_Form_Element_CheckBoxGroup extends WX_Form_Form_Element_Group {
	/**
	 * Construct the element.
	 *
	 * Set default template and template directory.
	 *
	 * @param WX_Form_Form_Configurator $configurator The default configuration object.
	 *
	 **/
	public function __construct(WX_Form_Form_Configurator $configurator){
		$this->setTemplateDirectory($configurator->templateDirectory);
		$this->setTemplate($configurator->checkgroupTemplate);
	}
	/**
	* var boolean The field is allowed empty.
	**/
	protected $allowBlank = true;
	/**
	 * Set the element allow blank parameter.
	 *
	 * @param boolean $value If true allow the element value blank otherwise not.
	 *
	 * @return WX_Form_Element_Abstract The instance.
	 **/
	public function setAllowBlank($value){
		$this->allowBlank = $value;
		return $this;
	}
	/**
	 * Return the allow blank parameter.
	 *
	 * @return boolean The parameter value.
	 **/
	public function getAllowBlank(){
		return $this->allowBlank;
	}
	/**
	 * var array The validator object store.
	 **/
	protected $validators = array();
	/**
	 * Add a validator object ot the form.
	 *
	 * @param WX_Form_Form_IValidator $validator The validator object.
	 *
	 * @return Element_Form The instance.
	 **/
	public function addValidator(WX_Form_Form_IValidator $validator){
		if (is_object($validator)) {
			$this->validators[] = $validator;
			return $this;
		}
		$this->validators[] = new $validator;
	}
	/**
	 * Return true is any error after the validation.
	 *
	 * @return boolean Is an error in element validation.
	 **/
	public function isError(){
		return count($this->messages) or count($this->codes) ? true : false;
	}
	/**
	 * Validating the group of the elements. And return the result boolean value.
	 *
	 * @param array  $context The input value for validating the element value.
	 * @param string $value   The value of the element (using in checkbox group).
	 * @param string $name	The name of the element (using in checkbox group).
	 *
	 * @return boolean The form value is valid.
	 **/
	public function isValid(array $context){
		//If allowBlank then valid
		if ( $this->getAllowBlank()) {
			return true;
		}
		$ret = true;
		$this->messages = array();
		$this->codes = array();
		foreach ($this->validators as $validator) {
			if (method_exists($validator, 'isValid')) {
				//@todo: Itt meg megfelelo parameteratadas kell
				$v = $validator->isValid($this->getValue(), $context, $this->getName());
				$ret = $ret && $v;
				if (!$v) {
					$this->messages = array_merge($this->messages, $validator->getMessages());
					$this->codes = array_merge($this->codes, $validator->getErrors());
				}
			}
		}
		$this->validated = true;
		return $ret;
	}

	/**
	 * var array The array of the error codes.
	 **/
	protected $codes = array();
	/**
	 * Get the error codes after the validation process.
	 *
	 * @return array The codes array.
	 **/
	public function getCodes(){
		return $this->codes;
	}
	/**
	 * Get the array contained the value of group's elements.
	 *
	 * @param boolean $arrayFormat Set true if you want the result in array format.
	 *
	 * @return array The group element value arrray.
	 **/

	public function getValue($arrayFormat = false){
		$ret = array();
		foreach ($this->elements as $element) {
			$r = $element->getValue();
			if (strlen($r)) {
				$ret[] = $r;
			}

		}
		return array($this->getName() => $ret);
	}

}