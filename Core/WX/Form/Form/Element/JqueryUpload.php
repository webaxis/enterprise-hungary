<?php

class WX_Form_Form_Element_JqueryUpload extends WX_Form_Form_Element {

	const GOOGLEMAPAUTOCOMPLETE_LAT = 'lat';

	const GOOGLEMAPAUTOCOMPLETE_LONG = 'long';

    /**
    * Constructor
    **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType('jqueryupload');
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->jqueryuploadTemplate);
    }

    public function setValue($context){
		if (isset($context['reportHash'])) {
			$this->value['reportHash'] = $context['reportHash'];
		}
        if (isset($context[$this->getName()])){
			$this->value[$this->getName()] = $context[$this->getName()];
		}
		if (isset($context[self::GOOGLEMAPAUTOCOMPLETE_LONG])){
			$this->value[self::GOOGLEMAPAUTOCOMPLETE_LONG] = $context[self::GOOGLEMAPAUTOCOMPLETE_LONG];
		}
		if (isset($context[self::GOOGLEMAPAUTOCOMPLETE_LAT])){
			$this->value[self::GOOGLEMAPAUTOCOMPLETE_LAT] = $context[self::GOOGLEMAPAUTOCOMPLETE_LAT];
		}
		if (isset($context['zoom'])){
			$this->value['zoom'] = $context['zoom'];
		}
		$this->filter();

        return $this;
    }
}