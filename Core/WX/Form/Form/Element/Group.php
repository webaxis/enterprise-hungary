<?php
/**
 * Group element of the form.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Elements
 **/

/**
 * Group element of the form.
 *
 * @package WX_Form_Form
 * @subpackage Elements
 **/
 class WX_Form_Form_Element_Group extends WX_Form_Form_Element_Abstract {
    /**
     * var string The label text of the element.
     **/
    protected $label;
    /**
     * Set the element label.
     *
     * @param string value
     *
     * @return WX_Form_Element_Abstract The instance.
     **/
    public function setLabel($value){
        $this->label = $value;
        return $this;
    }
    /**
     * Return the label string.
     *
     * @return string The label.
     **/
    public function getLabel(){
        return $this->label;
    }
    /**
     * var array The elements store.
     **/
    protected $elements = array();
    /**
     * The constructor.
     *
     * Set the default template to group.tpl.php.
     **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->groupTemplate);
    }
    /**
     * Add an element to the group.
     *
     * @param WX_Form_Form_Element_Abstract $value The gorup element.
     *
     * @return WX_Form_Form_Element_Group The instance.
     **/
    public function addElement(WX_Form_Form_Element_Abstract $value){
        //@TODO: Ha nem alapertelmezett a templateDirectory??
        if (isset($value)) {
            //$value->setTemplateDirectory($this->getTemplateDirectory());
            $this->elements[] = $value;
        }
        return $this;
    }
    /**
     * Get the group elements.
     *
     * @return array The gorup elements.
     **/
    public function getElements(){
        return $this->elements;
    }

    /**
     * Validating the group of the elements. And return the result boolean value.
     *
     * @param array  $context The input value for validating the element value.
     * @param string $value   The value of the element (using in checkbox group).
     * @param string $name    The name of the element (using in checkbox group).
     *
     * @return boolean The form value is valid.
     **/
    public function isValid(array $context){
        $ret = true;
        foreach ($this->elements as $element) {
            $ret = ($element->isValid($context) and $ret);
        }
        return $ret;
    }

    /**
     * Filtering the group of the elements.
     **/
    public function filter(){
        foreach ($this->elements as $element) {
            $element->filter();
        }
    }
    /**
     * Validating the form partiali. And return the result in JSON.
     *
     * @param array  $context The input value for validating the element value.
     * @param string $value   The value of the element (using in checkbox group).
     * @param string $name    The name of the element (using in checkbox group).
     *
     * @return Object The JSON response.
     **/
    public function isValidPartial($context, $name = ''){
        $ret = new StdClass();
        if ( $this->getName() == $name) {
            //Innen töröltem a $name paramétert
            if ($this->isValid($context)) {
                $ret->isValid = true;
            } else {
                $ret->isValid = false;
                $ret->errors = new StdClass();
                $ret->errors->messages = $this->getMessages();
                $ret->errors->codes = $this->getCodes();
            };
        } else {
            foreach ($this->elements as $element) {
                if ( method_exists($element, 'isValidPartial')) {
                    $ret = $element->isValidPartial($context, $name);
                } elseif ($element->getName() == $name) {
                    //Innen töröltem a $name paramétert
                    if ($element->isValid($context)) {
                        $ret->isValid = true;
                    } else {
                        $ret->isValid = false;
                        $ret->errors = new StdClass();
                        $ret->errors->messages = $element->getMessages();
                        $ret->errors->codes = $element->getCodes();
                    };
                }
            }
        }
        return $ret;
    }
    /**
     * Enable element
     *
     * @return WX_Form_Form_Element_Group The instance.
     **/
    public function enable(){
        foreach ($this->elements as $element) {
            $element->enable();
        }
        $this->setProperty('enable', true);
        unset($this->attributes['disabled']);
        return $this;
    }
    /**
     * Disable element.
     *
     * @return WX_Form_Form_Element_Group The instance.
     **/
    public function disable(){
        foreach ($this->elements as $element) {
            $element->disable();
        }
        $this->setProperty('enable', false);
        $this->setAttribute('disabled', '');
        return $this;
    }
    /**
     * Set the element readonly.
     *
     * @return WX_Form_Form_Element_Group The instance.
     **/
    public function readonly(){
        foreach ($this->elements as $element) {
            $element->readonly();
        }
        $this->setAttribute('readonly', '');
        return $this;
    }
    /**
     * Return true is any error after the validation.
     *
     * @return boolean Is an error in element validation.
     **/
    public function isError(){
        $ret = false;
        foreach ($this->elements as $element) {
            $ret = ($element->isError() or $ret);
        }
        return $ret;
    }
    /**
     * Render the group element.
     *
     * @return void
     **/
    public function render(){
        include($this->getTemplateDirectory().$this->getTemplate());
    }
    /**
     * Fetch the group element.
     *
     * @return void
     **/
    public function fetch(){
		ob_start();
        include($this->getTemplateDirectory().$this->getTemplate());
		$content = ob_get_contents();
		ob_end_clean();
		return $content;
    }
    /**
     * Get the array contained the value of group's elements.
     *
     * @return array The group element value arrray.
     **/
    public function getValue($arrayFormat = false){
        $ret = array();
        foreach ($this->elements as $element) {
            $ret = array_merge($ret, $element->getValue(true));
        }
        return $ret;
    }
    /**
     * Set the group's elements values from a context.
     *
     * @param array $context The context data.
     *
     * @return WX_Form_Form_Element_Group The instance.
     **/
    public function setValue($context){
        foreach ($this->elements as $element) {
            //@TODO Group Filter
            $element->setValue($context);
        }
        return $this;
    }
    /**
     * Render the group elements.
     *
     * @return void
     **/
    public function renderElements(){
        foreach ($this->elements as $element) {
            if (method_exists($element, 'render')) {
                $element->render();
            }
        }
    }
    /**
     * Get the element's error messages of the gorup element after the validation.
     *
     * @return array The error messages.
     **/
    public function getMessages(){
        $ret = $this->messages;
        foreach ($this->elements as $element) {
            $ret = array_merge($ret, $element->getMessages());
        }
        return $ret;
    }
    /**
     * Get the element's error codess of the gorup element after the validation.
     *
     * @return array The error codes.
     **/
    public function getCodes(){
        $ret = $this->codes;
        foreach ($this->elements as $element) {
            $ret = array_merge($ret, $element->getCodes());
        }
        return $ret;
    }
 }