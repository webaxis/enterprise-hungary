<?php
/**
 * Checkbox Element class.
 *
 * PHP version 5
 *
 * @package WX_Form
 * @subpackage Elements
 */
/**
 * Checkbox Element class.
 *
 * @package WX_Form
 * @subpackage Elements
 */
class WX_Form_Form_Element_CheckBox extends WX_Form_Form_Element_Checked{
    /**
     * Construct the element.
     *
     * Set the tamplate.
     **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_CHECKBOX);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->checkboxTemplate);
        $this->setProperty('checked',false);
    }
    /**
     * Set the Element value attibute.
     *
     * @param string $value The value attribute value.
     *
     * @return AbstractElelemnt__Form The configured Form instance.
     **/
    public function setValue($context){
        $name = $this->getName();
        $name = str_replace('[]','',$name);
        if (isset($context[$name])) {
            if (is_array($context[$name])) {
                if ( in_array( $this->value, $context[$name])) {
                    $this->checked();
                } else {
                    $this->unchecked();
                }
            } else {
                $this->value = $context[$name];
                $this->checked();
            }
        } else {
             //$this->value = '';
//            $this->unchecked();
        }
        return $this;
    }
    /**
     * Return the form Element value.
     *
     * @return string The Element value attribute
     **/
    public function getValue($arrayFormat = false){
        $name = $this->getName();
        $name = str_replace('[]','',$name);
        if ($arrayFormat) {
            return $this->isChecked() ? array($name => $this->value) : array($name => '');
        } else {
            return $this->isChecked() ? $this->value : '';
        }
    }
    /**
     * Return the form theoretically value.
     *
     * @return string The Element value attribute
     **/
    public function getTheoreticallyValue(){
        return $this->value;
    }
}