<?php

class WX_Form_Form_Element_Multiimage extends WX_Form_Form_Element {
    /**
    * Constructor set the type default imagemulti and template multiimage.tpl.php.
    **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_MULTIIMAGE);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->multiimageTemplate);
    }
}