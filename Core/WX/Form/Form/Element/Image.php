<?php
/**
 *
 * @package WX_Form_Form
 * @subpackage Elements
 *
 **/
/**
 * Image element.
 *
 * Valid JSON descriptor
 * <code>
 * {
 *    'name':'<the input name (required)>',
 *    'vtype':'image',
 *    'options':'[{
 *		'type' : '<type of the image>',
 *		'isTemp' : '<if the image is temp>',
 *		'size' : '<size of the image (eg. normal)>',
 *	   }],
 *    'fieldLabel':'<the label of the field>',
 *    'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
 *    'class':'<the css class to input filed>',
 *    'id':'<the id of the input tag>'
 *    'templateDirectory':'<the directory of the templates>',
 *    'template':'<the template file of the form>',
 *    'validators':[{ //Validators }],
 *    'properties':[{ //properties }]
 * }
 * </code>
 *
 * @package WX_Form_Form
 * @subpackage Elements
 **/
class WX_Form_Form_Element_Image extends WX_Form_Form_Element {
    /**
    * Constructor set the type default image and template image.tpl.php.
    **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_IMAGE);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->imageTemplate);
    }
}