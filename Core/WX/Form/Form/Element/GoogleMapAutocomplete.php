<?php

class WX_Form_Form_Element_GoogleMapAutocomplete extends WX_Form_Form_Element {

	const LAT = 'lat';

	const LONG = 'long';

	const ZOOM = 'zoom';

    /**
     * Constructor
     **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType('googlemapautocomplete');
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->googlemapautocompleteTemplate);
    }

    public function setValue($context){
		if (isset($context['lastSeenHere'])) {
			$this->value['lastSeenHere'] = $context['lastSeenHere'];
		}
        if (isset($context[$this->getName()])){
			$this->value[$this->getName()] = $context[$this->getName()];
		}
		if (isset($context[self::LONG])){
			$this->value[self::LONG] = $context[self::LONG];
		}
		if (isset($context[self::LAT])){
			$this->value[self::LAT] = $context[self::LAT];
		}
		if (isset($context[self::ZOOM])){
			$this->value[self::ZOOM] = $context[self::ZOOM];
		}
		$this->filter();

        return $this;
    }
}
