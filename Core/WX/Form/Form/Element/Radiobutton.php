<?php
/**
 * Radio Button class.
 *
 * PHP version 5
 *
 * @package    WX_Form_Form
 * @subpackage Elements
 */
/**
 * Radio Button class.
 *
 * PHP version 5
 *
 * @package    WX_Form_Form
 * @subpackage Elements
 */
class WX_Form_Form_Element_RadioButton extends WX_Form_Form_Element_Checked {
    /**
     * Construct the element.
     *
     * Set default template and template directory.
     *
     * @param WX_Form_Form_Configurator $configurator The default configuration object.
     *
     **/
    public function __construct(WX_Form_Form_Configurator $configurator){
        $this->setType(WX_Form_Form_Elements::T_RADIOBUTTON);
        $this->setTemplateDirectory($configurator->templateDirectory);
        $this->setTemplate($configurator->radioButtonTemplate);
        $this->setProperty('checked', false);
    }
    /**
     * Set the Element value attibute.
     *
     * @param mixed $context The value attribute value.
     *
     * @return RadioButtonElement_Form The configured Form instance.
     **/
    public function setValue($context){
        if (isset($context[$this->getName()])) {
            if ($this->value == $context[$this->getName()]) {
                $this->checked();
            } else {
                $this->unchecked();
            }
            //$this->value = $context[$this->getName()];

        } else {
            //$this->value = '';
            //$this->setProperty('checked',false);
        }
        return $this;
    }
    /**
     * Return the form Element value.
     *
     * @param boolean $arrayFormat True if you want to get the result in array.
     *
     * @return string The Element value attribute
     **/
    public function getValue($arrayFormat = false){
        if ($arrayFormat) {
            return $this->getProperty('checked') ? array($this->getName() => $this->value) : array($this->getName() => '');
        } else {
            return $this->getProperty('checked') ? $this->value : '';
        }
    }
    /**
     * Return the form theoretically value.
     *
     * @return string The Element value attribute
     **/
    public function getTheoreticallyValue(){
        return $this->value;
    }
}