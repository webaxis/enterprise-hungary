<?php
/**
 * Element class.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 * @subpackage Elements
 */
/**
 * Element class.
 *
 * @package WX_Form_Form
 * @subpackage Elements
 */
class WX_Form_Form_Element_Link extends WX_Form_Form_Element {
    /**
     * Construct the Element.
     *
     * Set the type and template.
     **/
    public function __construct(){
        $this->setType(WX_Form_Form_Elements::T_LINK);
        $this->setTemplate('link.tpl.php');
    }
}