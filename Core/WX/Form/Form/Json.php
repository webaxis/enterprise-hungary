<?php
/**
 * Form class from JSON.
 *
 * PHP version 5
 *
 * @package WX_Form_Form
 */

/**
 * Form class from JSON.
 *
 * Example form descriptor:
 * <code>
 *  {
 *   'url':'form2.php',
 *   'method':'post',
 *   'globalFilters':
 *	  [{
 *	  	'class':'WX_Form_Filter_StringTrim',
 *	  	'options':{
 *	  		'charlist':'. '
 *	  		}
 *	  }],
 *   'items':
 *	   [{
 *		   'vtype':'group',
 *		   'label':'User data',
 *		   'description':'Eza description askdjakj sdlkajs djal ksjdlajsd',
 *		   'items':
 *			   [{
 *				   'fieldLabel': 'First Name',
 *				   'name': 'first',
 *				   'allowBlank':false,
 *				   'readOnly':true,
 *				   'enable':false,
 *				   'class':'text_in'
 *				},{
 *				   'fieldLabel': 'Last Name',
 *				   'name': 'last'
 *				},{
 *				   'fieldLabel': 'Description',
 *				   'name': 'description',
 *				   'vtype':'textarea'
 *				},{
 *				   'fieldLabel': 'Elfogadom',
 *				   'name': 'privacypolicy',
 *				   'vtype':'checkbox'
 *				}]
 *		}, {
 *		   'vtype':'group',
 *		   'label':'User data',
 *		   'description':'Eza description askdjakj sdlkajs djal ksjdlajsd',
 *		   'items':
 *			   [{
 *				   'fieldLabel': 'Company',
 *				   'name': 'company',
 *				   'allowBlank':true,
 *				   'class':'text_in',
 *				   'validators':[{
 *					   'class':'WX_Form_Form_Validator_Notempty',
 *					   'message':'Nem lehet üres a mező',
 *					   'code':101
 *					 }]
 *				}, {
 *				   'fieldLabel': 'Email',
 *				   'name': 'email',
 *				   'vtype':'email',
 *				   'validators':[{
 *					   'class':'WX_Form_Form_Validator_Notempty'
 *					   }],
 *				  'filters':
 *					  [{
 *				  		'class':'Zend_Filter_StripTags',
 *					  	'options':{
 *					  		'allowTags':'p',
 *					  		'allowAttribs':'style'
 *					  	}
 *					  }]
 *			   }, {
 *				   'fieldLabel': 'Nő',
 *				   'name': 'email2',
 *				   'vtype':'radio'
 *			   }, {
 *				   'fieldLabel': 'Férfi',
 *				   'name': 'email3',
 *				   'vtype':'radio'
 *			   }]
 *		},{
 *		   'vtype':'radiogroup',
 *		   'label':'Nem',
 *		   'description':'Please check the gender!',
 *		   'name':'gender',
 *		   'allowBlank':false,
 *		   'error':{
 *			   'message':'Egyet meg kell adni',
 *			   'code':2
 *		   },
 *		   'items':
 *			   [{
 *				   'fieldLabel': 'Nő'
 *			   }, {
 *				   'fieldLabel': 'Férfi',
 *				   'value': 1
 *			   }]
 *		}],
 *	   'buttons': [{
 *		   'text': 'Save'
 *	   },{
 *		   'text': 'Cancel'
 *	   }]
 *   }
 * </code>
 *
 * @package WX_Form_Form
 */

class WX_Form_Form_Json extends WX_Form_Form_Form {
	/**
	 * var WX_Form_Form_Configurator The basic configurations data of the form.
	 **/
	private static $configuration;

	/**
	 * var array Contains the global filters
	 */
	private static $_globalFilters = array();


	/**
	 * Set the form default input type.
	 *
	 * @param string $value The defualt type (text|radio|checkbox|textarea).
	 *
	 * @return JSONForm_Form The instance.
	 **/
	public function setDefaultType($value){
		$this->setProperty('defaultType', $value);
		return $this;
	}
	/**
	 * Get the default type of the form input element.
	 *
	 * @return string The default type of the input element.
	 **/
	public function getDefaultType(){
		return $this->getProperty('defaultType') ? $this->getProperty('defaultType') : 'text';
	}
	/**
	 * Set the Element properties attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setProperties(&$desc,&$element){
		if ( isset($desc['properties'] )) {
			foreach ($desc['properties'] as $property) {
				if (isset($property['name'])) {
					$name = $property['name'];
				}
				if (isset($property['value'])) {
					$value = $property['value'];
				}
				if (isset($name) and isset($value)) {
					$element->setProperty($name, $value);
				} else {
					 throw new Exception('JSON:property error:'.json_encode($desc));
				}
			}

		}
	}
	/**
	 * Create an imput element instance by name from descriptor.
	 *
	 * The name parameter in description is required.
	 *
	 * @param mixed  &$desc		The reference of the description.
	 * @param string $elementclass The element class name.
	 *
	 * @return AbstractWX_Form_Form_Element The isntance.
	 **/
	private static function CreateElementFromDescriptor(&$desc, $elementclass){
		if (!isset($desc['name'])) {
			throw new Exception($elementclass.' element name property is required');
		}
		$e = new $elementclass(self::$configuration);
		$e->setName($desc['name']);
		return $e;
	}
	/**
	 * Set the Element attributes.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementAttributes(&$desc,&$element){
		if ( isset($desc['attributes'])) {
			$element->setAttributes($desc['attributes']);
		}
	}
	/**
	 * Set the Element ID attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementIdAttribute(&$desc,&$element){
		if ( isset($desc['id'])) {
			$element->setId($desc['id']);
		} else{
			$element->setId($element->getName());

		}
	}

	/**
	 * Set the Element Name attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementName(&$desc,&$element){
		if ( isset($desc['name'])) {
			$element->setName($desc['name']);
		}
	}

	/**
	 * Set the emptyText property of the Element.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementEmptyTextPropoerty(&$desc,&$element){
		if ( isset($desc['emptyText'])) {
			$element->setProperty('emptyText',$desc['emptyText']);
		}
	}
	/**
	 * Set the Element class attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementClassAttribute(&$desc,&$element){
		if ( isset($desc['class'])) {
			$element->setAttribute('class', $desc['class']);
		}
	}
	/**
	 * Set the Element allowblank attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementAllowBlankAttribute(&$desc,&$element){
		if ( isset($desc['allowBlank'])) {
			if (!is_bool($desc['allowBlank'])) {
				throw new Exception('The allowBlank parameter must be boolean.');
			}
			$element->setAllowBlank($desc['allowBlank']);
			if (!$element->getAllowBlank()) {
				if ( isset($desc['error']) ){
					$element->addValidator(new WX_Form_Form_Validator_Notempty($desc['error']));
				} else {
					$element->addValidator(new WX_Form_Form_Validator_Notempty());
				}
			}
		}
	}
	/**
	 * Set the Element validators.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementValidators(&$desc,&$element){
		if ( isset($desc['validators'])) {
			foreach ($desc['validators'] as $validator) {
				if (isset($validator['class'])) {
					$validatorInstance = new $validator['class']($validator);
					$element->addValidator($validatorInstance);
				}
			}
		}
	}
	/**
	 * Set the Element filters.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementFilters(&$desc,&$element) {

		//Set the global filters
		self::_setGlobalFilters($desc,$element);

		if ( isset($desc['filters'])) {
			foreach ($desc['filters'] as $filter) {
				if (isset($filter['class'])) {
					(!isset($filter['options'])) ? $filter['options'] = array() : '';
					//var_dump($filter['options']);die;

					//Zend_Configgá kell alakítani az options tömböt, mert pl.: StringTrim hibára fut ha tömböt kap
					$options = self::_createZendConfigFromArray($filter['options']);
					//var_dump($options);die;
					$filterInstance = new $filter['class']($options);
					$element->addFilter($filterInstance);
				}
			}
		}
	}

	/**
	 * Set up the global filters
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function _setGlobalFilters(&$desc,&$element)
	{
		//Except the password fileds, add the global filters
		if (!($element instanceof WX_Form_Form_Element_Password)) {
			foreach (self::$_globalFilters as $globalFilter) {
				$desc['filters'][] = $globalFilter;
			}
		}
	}

	/**
	 * Creates a Zend_Config Object from an Array
	 *
	 * @param array
	 *
	 * @return Zend_Config
	 */
	private static function _createZendConfigFromArray($options)
	{
		$config = new Zend_Config($options);
		return $config;
	}

	/**
	 * Set the Element template directory attribute.
	 *
	 * @param mixed  &$desc	The reference of the description.
	 * @param mixed  &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementTemplateDirectory(&$desc, &$element){
		if ( isset($desc['templateDirectory'])) {
			$element->setTemplateDirectory($desc['templateDirectory']);
		}
	}

	/**
	 * Set the Element template options attribute.
	 *
	 * @param mixed  &$desc	The reference of the description.
	 * @param mixed  &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementOptions(&$desc, &$element){
		if ( isset($desc['options'])) {
			$element->setOptions($desc['options']);
		}
	}
	/**
	 * Set the Element ID attribute.
	 *
	 * @param mixed  &$desc	The reference of the description.
	 * @param mixed  &$element The element to set from description.
	 *
	 * @return void
	 **/
	private static function setElementTemplate(&$desc,&$element){
		if ( isset($desc['template'])) {
			$element->setTemplate($desc['template']);
			//var_dump($element);
		}
	}
	/**
	 * Set the Element Description attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set  from description.
	 *
	 * @return void
	 *
	 **/
	private static function setElementDescription(&$desc, &$element){
		if ( isset($desc['description'])) {
			$element->setProperty('description', $desc['description']);
		}
	}

	/**
	 * Set the Element Label attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set  from description.
	 *
	 * @return void
	 *
	 **/
	private static function setElementLabel(&$desc, &$element){
		if ( isset($desc['fieldLabel'])) {
			$element->setLabel($desc['fieldLabel']);
		}
	}
	/**
	 * Set the Element default value attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set  from description.
	 *
	 * @return void
	 *
	 **/
	private static function setElementDefaultValue(&$desc, &$element){
		if ( isset($desc['defaultValue'])) {
			$element->initValue( $desc['defaultValue']);
		}
	}
	/**
	 * Set the Element enable attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set  from description.
	 *
	 * @return void
	 *
	 **/
	private static function setElementEnable(&$desc, &$element){
		if ( isset($desc['enable'])) {
			if ( !$desc['enable'] ){
				$element->disable();
			}

		}
	}

	/**
	 * Set the Element readonly attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set  from description.
	 *
	 * @return void
	 *
	 **/
	private static function setElementReadonly(&$desc, &$element){
		if ( isset($desc['readOnly'])) {
			if ( $desc['readOnly'] ){
				$element->readonly();
			}

		}
	}
	
	/**
	 * Set the Element default value attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set  from description.
	 *
	 * @return void
	 *
	 **/
	private static function setElementData(&$desc, &$element){
		if ( isset($desc['data'])) {
			$element->setData($desc['data']);
		}
	}
	
	/**
	 * Set the Element enable attribute.
	 *
	 * @param mixed &$desc	The reference of the description.
	 * @param mixed &$element The element to set  from description.
	 *
	 * @return void
	 *
	 **/
	private static function setElementHint(&$desc, &$element){
		if ( isset($desc['hint'])) {
			$element->setProperty('hint',$desc['hint']);
		}
	}
	
	/**
	 * Create a form instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'template':'<the template file of the form>',
	 *	'id':'idString'
	 *	'label':'<the label of the group>',
	 *	'description':'<the description of the group>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'url':'<the form submit url>',
	 *	'method':'<get|post>',
	 *	'defaultType':'the default input type of the form (if not set default is text)',
	 *	'items': [],
	 *	'buttons': []
	 * }
	 * </code>
	 *
	 * @param string			$jsonDescriptor The form descriptor JSON Format string.
	 * @param WX_Form_Form_Configurator $configurator   The form builder base configurations (default template directory and templates) .
	 *
	 **/
	public function __construct($jsonDescriptor, WX_Form_Form_Configurator $configurator = null){
		parent::__construct();
		//Try to decode the JSON Form format desriptor
		$jsonDescriptor = trim($jsonDescriptor);
		$jsonDescriptor = str_replace("'", '"', $jsonDescriptor);
		$desc = json_decode($jsonDescriptor, true);
		//The descriptor is not valid (syntax error etc..)
		if ( $desc == null ) {
			throw new Exception('JSON Error:'.$jsonDescriptor);
		}
		//If form configuration object no in the parameter create a default
		if (!isset($configurator) ) {
			self::$configuration = new WX_Form_Form_Configurator();
		} else {
			self::$configuration = $configurator;
		}
		$this->setTemplateDirectory(self::$configuration->templateDirectory);
		$this->setTemplate(self::$configuration->formTemplate);
		
		if ( isset($desc['feedback'] )) {
			$this->setFeedback($desc['feedback']);
		}
		
		if ( isset($desc['class'] )) {
			$this->setClass($desc['class']);
		}

		if ( isset($desc['method'] )) {
			$this->setMethod($desc['method']);
		} else {
			$this->setGetMethod();
		}
		self::setElementTemplateDirectory($desc, $this);
		self::setElementTemplate($desc, $this);
		if ( isset($desc['label'])) {
			$this->setProperty('label', $desc['label']);
		}
		self::setElementDescription($desc, $this);

		if ( isset($desc['url'] )) {
			$this->setAction($desc['url']);
		}
		if ( isset($desc['width'] )) {
			$this->setWidth($desc['width']);
		}
		if ( isset($desc['id'] )) {
			$this->setId($desc['id']);
			//$this->setName($desc['id']);
		}
		if ( isset($desc['defaultType'] )) {
			$this->setDefaultType($desc['defaultType']);
		} else {
			$this->setDefaultType('text');
		}
		self::setProperties($desc, $this);
		if ( isset($desc['globalFilters'])) {
			//var_dump($desc['globalFilters']);die;
			self::$_globalFilters = $desc['globalFilters'];
		}
		if ( isset($desc['items'] )) {
			foreach ($desc['items'] as $item) {
				if (!isset($item['vtype'])) {
					$item['vtype'] = $this->getDefaultType();
				} elseif($item['vtype'] == 'file') {
					$this->setPostMethod();
					$this->setAttribute('enctype','multipart/form-data');
				}
				if (!isset($item['templateDirectory']) && isset($desc['templateDirectory'])) {
					$item['templateDirectory'] = $this->getTemplateDirectory();
				}
				if (!isset($item['defaultType'])) {
					$item['defaultType'] = $this->getDefaultType();
				}
				//var_dump($item);
				$this->addElement(self::CreateElementByDescriptor($item));
			}

		}
		if ( isset($desc['buttons'] )) {
			foreach ($desc['buttons'] as $button) {
				if (!isset($button['vtype'])) {
					$button['vtype'] = 'button';
				}
				if (!isset($button['name'])) {
					if (isset($button['text'])) {
						$button['name'] =  strtolower($button['text']);
					} else {
						$button['name'] = 'send';
					}
				}
				if ( isset($button['text'])) {
					$button['value'] =  $button['text'];
				} else {
					$button['value'] =  $button['name'];
				}
				$this->addButton(self::CreateElementByDescriptor($button));
			}
		}
	}
	/**
	 * Create an element from JSON descriptor.
	 *
	 * @param array			 $descriptor   The descriptor array.
	 *
	 * @return AbstractWX_Form_Form_Element The Element instance.
	 **/
	public static function CreateElementByDescriptor(array $descriptor){

		switch($descriptor['vtype']) {
		case 'group':
			return self::CreateGroup($descriptor);
			break;
		case 'button':
			return self::CreateButton($descriptor);
			break;
		case 'textarea':
			return self::CreateTextArea($descriptor);
			break;
		case 'email':
			return self::CreateEmail($descriptor);
			break;
		case 'radio':
			if ( !isset($descriptor['value'])) {
				$descriptor['value'] = $descriptor['name'];
			}
			return self::CreateRadioButton($descriptor);
			break;
		case 'checkbox':
			if ( !isset($descriptor['value'])) {
				$descriptor['value'] = $descriptor['name'];
			}
			return self::CreateCheckBox($descriptor);
			break;
		case 'radiogroup':
			return self::CreateRadioGroup($descriptor);
			break;
		case 'option':
			return self::CreateOption($descriptor);
			break;
		case 'multipleoption':
			return self::CreateMultipleOption($descriptor);
			break;
		case 'checkboxgroup':
			return self::CreateCheckBoxGroup($descriptor);
			break;
		case 'file':
			return self::CreateFile($descriptor);
			break;
		case 'password':
			return self::CreatePassword($descriptor);
			break;
		case 'hidden':
			return self::CreateHidden($descriptor);
			break;
		case 'description':
			return self::CreateDescription($descriptor);
			break;
		case 'birthdate':
			return self::CreateBirthdate($descriptor);
			break;
		case 'googlemapautocomplete':
			return self::CreateGoogleMapAutocomplete($descriptor);
			break;
		case 'custom':
			return self::CreateCustom($descriptor);
			break;
		case 'link':
			return self::CreateLink($descriptor);
			break;
		case 'image':
			return self::CreateImage($descriptor);
			break;
		case 'multiimage':
			return self::CreateMultiimage($descriptor);
			break;
		default:
			return self::CreateText($descriptor);

		}
	}
	/**
	 * Create text input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'text',
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Text The text instance.
	 **/
	public static function CreateText(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Text');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementName($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementLabel($item, $e);
		self::setElementEnable($item, $e);
		self::setElementDescription($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementHint($item, $e);
		return $e;
	}

	/**
	 * Create hidden input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'hidden',
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Text The text instance.
	 **/
	public static function CreateHidden(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Hidden');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementName($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementLabel($item, $e);
		self::setElementEnable($item, $e);
		self::setElementDescription($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		return $e;
	}
	/**
	 * Create password input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'password',
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Text The text instance.
	 **/
	public static function CreatePassword(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Password');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementLabel($item, $e);
		self::setElementEnable($item, $e);
		self::setElementDescription($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		return $e;
	}


	/**
	 * Create description element instance from JSON. Free text in a paragraph.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'description',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Text The text instance.
	 **/
	public static function CreateDescription(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Description');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		//self::setElementValidators($item, $e);
		//self::setElementFilters($item, $e);
		//self::setElementLabel($item, $e);
		self::setElementEnable($item, $e);
		self::setElementDescription($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementDefaultValue($item, $e);
		//self::setElementAllowBlankAttribute($item, $e);
		return $e;
	}


	/**
	 * Create text input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'text',
	 *	'fieldLabel':'<the label of the field>',
	 *	'class':'<the css class to input filed>',
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_File The text instance.
	 **/
	public static function CreateFile(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_File');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementOptions($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementLabel($item, $e);
		self::setElementEnable($item, $e);
		self::setElementDescription($item, $e);
		self::setElementDefaultValue($item, $e);
		return $e;
	}
	/**
	 * Create email input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'email',
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Text The email instance.
	 **/
	public static function CreateEmail(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Text');
		if (isset($item['error'])) {
			$v = new WX_Form_Form_Validator_Email($item['error']);
		} else {
			$v = new WX_Form_Form_Validator_Email();
		}
		$e->addValidator($v);

		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementDescription($item, $e);

		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementLabel($item, $e);
		self::setElementEnable($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		return $e;
	}
	/**
	 * Create textarea element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'textarea',
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Textarea The text area instance.
	 **/
	public static function CreateTextArea(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Textarea');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementLabel($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementEnable($item, $e);
		self::setElementDescription($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		return $e;
	}
	/**
	 * Create option element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'option',
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>',
	 *	'emptyText':'Select one ...',
	 *	'items':[
	 *		 ['1','Metallica'],
	 *		 ['2','Deiciede']
	 *	]
	 * }
	 *
	 * // OR
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'option',
	 *	'emptyText':'Select one ...',
	 *	'groups':[
	 *		{
	 *			'name':'Metal',
	 *			'items':[
	 *				['1','Metallica'],
	 *				['2','Deiciede']
	 *			]
	 *		},{
	 *			'name':'Rap',
	 *			'items':[
	 *				['3','MC Hammer'],
	 *				['4','Ganxta']
	 *			]
	 *	   }
	 *	]
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Option The text area instance.
	 **/
	public static function CreateOption(array $item){
		if ( !isset($item['items']) and !isset($item['groups'])) {
			throw new Exception('JSON:items or groups missing in option:'.json_encode($item));
		}
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Option');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementLabel($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementDescription($item, $e);
		self::setElementEnable($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementEmptyTextPropoerty($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);

		if ( isset($item['items'] )) {
			$e->setProperty('items',$item['items']);
		}
		if ( isset($item['groups'] )) {
			$e->setProperty('groups',$item['groups']);
		}
		if ( isset($item['extraFields'] )) {
			$e->setProperty('extraFields',$item['extraFields']);
		}

		return $e;
	}
	/**
	 * Create multiple option element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'multipleoption',
	 *	'size':3,
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'filters':[{ //Filters }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>',
	 *	'emptyText':'Select one ...',
	 *	'items':[
	 *		 ['1','Metallica'],
	 *		 ['2','Deiciede']
	 *	]
	 * }
	 *
	 * // OR
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'multipleoption',
	 *	'size':3,
	 *	'emptyText':'Select one ...',
	 *	'groups':[
	 *		{
	 *			'name':'Metal',
	 *			'items':[
	 *				['1','Metallica'],
	 *				['2','Deiciede']
	 *			]
	 *		},{
	 *			'name':'Rap',
	 *			'items':[
	 *				['3','MC Hammer'],
	 *				['4','Ganxta']
	 *			]
	 *	   }
	 *	]
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Option The text area instance.
	 **/
	public static function CreateMultipleOption(array $item){
		if ( !isset($item['items']) and !isset($item['groups'])) {
			throw new Exception('JSON:items or groups missing in option:'.json_encode($item));
		}
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Multipleoption');
		$e->setAttribute('multiple','');

		if ( isset($item['size'] )) {
			$e->setAttribute('size',$item['size']);
		}
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementLabel($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementDescription($item, $e);
		self::setElementEnable($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementEmptyTextPropoerty($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);

		if ( isset($item['items'] )) {
			$e->setProperty('items',$item['items']);
		}
		if ( isset($item['groups'] )) {
			$e->setProperty('groups',$item['groups']);
		}

		return $e;
	}
	/**
	 * Create group element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'vtype':'group',
	 *	'label':'<the label of the group>',
	 *	'description':'<the description of the group>',
	 *	'id':'<the id of the input tag>'
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'defaultType':'the default input type of the form (if not set default is text)',
	 *	'items': [],
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $descriptor   The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Group The group instance.
	 **/
	public static function CreateGroup(array $descriptor){

		$e = new WX_Form_Form_Element_Group(self::$configuration);
		self::setElementTemplate($descriptor, $e);
		self::setElementTemplateDirectory($descriptor, $e);

		if ( isset($descriptor['label'])) {
			$e->setProperty('label', $descriptor['label']);
		}
		if ( isset($descriptor['name'])) {
			$e->setName($descriptor['name']);
		}
		if ( isset($descriptor['description'])) {
			$e->setProperty('description', $descriptor['description']);
		}
		if ( isset($descriptor['cloneGroup'])) {
			$e->setProperty('cloneGroup', $descriptor['cloneGroup']);
		}

		if ( isset($descriptor['items'] )) {
			foreach ($descriptor['items'] as $item) {
				if (!isset($item['vtype'])) {
					$item['vtype'] = $descriptor['defaultType'];
				}
				if (!isset($item['templateDirectory']) && isset($descriptor['templateDirectory'])) {
					$item['templateDirectory'] = $e->getTemplateDirectory();
				}
				$e->addElement(self::CreateElementByDescriptor($item));
			}

		}
		return $e;
	}
	/**
	 * Create radio group element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'vtype':'radiogroup',
	 *	'label':'<the label of the group>',
	 *	'description':'<the description of the group>',
	 *	'name':'<the name (required)>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'template':'<the template file of the form>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'items': [],
	 *	'properties':[{ //properties }]
	 * }
	 * </code>
	 *
	 * @param array			 $descriptor   The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Radiogroup The radio group instance.
	 **/
	public static function CreateRadioGroup(array $descriptor){
		//DX($descriptor);
		$e = new WX_Form_Form_Element_Radiogroup(self::$configuration);
		if (!isset($descriptor['name'])) {
			throw new Exception('WX_Form_Form_Element_Radiogroup element name property is required');
		}
		self::setElementTemplate($descriptor, $e);
		self::setElementTemplateDirectory($descriptor, $e);

		self::setElementLabel($descriptor, $e);
		
		if ( isset($descriptor['label'])) {
			$e->setProperty('label', $descriptor['label']);
		} elseif (isset($descriptor['fieldLabel'])) {
			$e->setProperty('label', $descriptor['fieldLabel']);
			$e->setProperty('fieldLabel', $descriptor['fieldLabel']);
		}

		if ( isset($descriptor['description'])) {
			$e->setProperty('description', $descriptor['description']);
		}
		if (isset($descriptor['name'])) {
			$e->setName($descriptor['name']);
		}
		if (isset($descriptor['allowBlank'])) {
			$e->setAllowBlank($descriptor['allowBlank']);
			if (!$e->getAllowBlank()) {
				if (isset($descriptor['error'])) {
					$v = new WX_Form_Form_Validator_Radiogroupnotempty($descriptor['error']);
				} else {
					$v = new WX_Form_Form_Validator_Radiogroupnotempty();
				}
				$e->addValidator($v);
			}
		}
		if ( isset($descriptor['items'] )) {
			foreach ($descriptor['items'] as $item) {
				if (!isset($item['vtype'])) {
					$item['vtype'] = 'radio';
					$item['name'] = $e->getName();
					if (!isset($item['fieldLabel'])) {
						throw new Exception('Radio button group item fieldLabel missing');
					}
					if (!isset($item['value'])) {
					  $item['value'] =  $item['fieldLabel'];
					}
				}
				if (isset($descriptor['readOnly'])) {
					$item['readOnly'] = $descriptor['readOnly'];
				}
				if (isset($descriptor['enable'])) {
					$item['enable'] = $descriptor['enable'];
				}
				if (!isset($item['templateDirectory']) && isset($descriptor['templateDirectory'])) {
					$item['templateDirectory'] = $e->getTemplateDirectory();
				}
				$radio = self::CreateElementByDescriptor($item);

				if (isset($descriptor['defaultValue'])) {
					if ($radio->getTheoreticallyValue() == $descriptor['defaultValue']) {
						$radio->checked();
					}
				}
				$e->addElement($radio);

				//$e->addElement(self::CreateElementByDescriptor($item, $configurator));
			}

		}
		return $e;
	}
	/**
	 * Create checkbox group element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'vtype':'checkboxgroup',
	 *	'label':'<the label of the group>',
	 *	'description':'<the description of the group>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'name':'<the name (required)>',
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'template':'<the template file of the form>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'items': [],
	 *	'properties':[{ //properties }]
	 * }
	 * </code>
	 *
	 * @param array			 $descriptor   The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Radiogroup The radio group instance.
	 **/
	public static function CreateCheckBoxGroup(array $descriptor){

		$e = new WX_Form_Form_Element_Checkboxgroup(self::$configuration);
		if (!isset($descriptor['name'])) {
			throw new Exception('WX_Form_Form_Element_Checkboxgroup element name property is required');
		}
		self::setElementTemplate($descriptor, $e);
		self::setElementTemplateDirectory($descriptor, $e);
		self::setElementLabel($descriptor, $e);

		if ( isset($descriptor['label'])) {
			$e->setProperty('label', $descriptor['label']);
		}
		self::setElementDescription($descriptor, $e);
		if (isset($descriptor['name'])) {
			$e->setName($descriptor['name']);
		}
		if (isset($descriptor['allowBlank'])) {
			$e->setAllowBlank($descriptor['allowBlank']);
			if (!$e->getAllowBlank()) {
				if (isset($descriptor['error'])) {
					$v = new WX_Form_Form_Validator_Radiogroupnotempty($descriptor['error']);
				} else {
					$v = new WX_Form_Form_Validator_Radiogroupnotempty();
				}
				$e->addValidator($v);
			}
		}
		if ( isset($descriptor['items'] )) {
			foreach ($descriptor['items'] as $item) {
				if (!isset($item['vtype'])) {
					$item['vtype'] = 'checkbox';
					$item['name'] = $e->getName().'[]';
					if (!isset($item['fieldLabel'])) {
						throw new Exception('Checkbox group item fieldLabel missing');
					}
					if (!isset($item['value'])) {
					  $item['value'] =  $item['fieldLabel'];
					}
				}
				if (isset($descriptor['readOnly'])) {
					$item['readOnly'] = $descriptor['readOnly'];
				}
				if (isset($descriptor['enable'])) {
					$item['enable'] = $descriptor['enable'];
				}
				if (!isset($item['templateDirectory']) && isset($descriptor['templateDirectory'])) {
					$item['templateDirectory'] = $e->getTemplateDirectory();
				}

				$check = self::CreateElementByDescriptor($item);
				if (isset($descriptor['defaultValue'])) {
					foreach ($descriptor['defaultValue'] as $defV) {
						if ($check->getTheoreticallyValue() == $defV) {
							$check->checked();
						}
					}
				}
				$e->addElement($check);

				//$e->addElement(self::CreateElementByDescriptor($item, $configurator));
			}

		}
		return $e;
	}
	/**
	 * Create submit input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'submit',
	 *	'class':'<the css class to input filed>',
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'properties':[{ //properties }],
	 *	'value':'value of the submit',
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Submit The submit instance.
	 **/
	public static function CreateButton(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Submit');
		//D($item);

		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		if ( isset($item['value'])) {
			$e->setAttribute('value', $item['value']);
		}
		if ( isset($item['type'])) {
			$e->setAttribute('type',$item['type']);
		} else {
			$e->setAttribute('type','submit');
		}
		self::setProperties($item, $e);
		return $e;
	}

	/**
	 * Create radio button input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'radio',
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'value':'<value>',
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Radiobutton The radio button instance.
	 **/
	public static function CreateRadioButton(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Radiobutton');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementLabel($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementEnable($item, $e);
		self::setElementReadonly($item, $e);
		if ( isset($item['value'])) {
			$e->initValue( $item['value']);
		} else {
			$e->initValue($e->getName());
		}
		return $e;
	}
	/**
	 * Create check box input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'checkbox',
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'value':'<value>',
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return CheckBox_Element_Form The check box instance.
	 **/
	public static function CreateCheckBox(array $item){
		if (isset($item['defaultValue'])) {
				throw new Exception('The defaultValue parameter not allowed here.');
		}
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Checkbox');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementDescription($item, $e);
		self::setElementEnable($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementLabel($item, $e);
		if (isset($item['allowBlank'])) {
			if (!is_bool($item['allowBlank'])) {
				throw new Exception('The allowBlank parameter must be boolean.');
			}
			$e->setAllowBlank($item['allowBlank']);

			if (!$e->getAllowBlank()) {
				if (isset($item['error'])) {
					$v = new WX_Form_Form_Validator_Radiogroupnotempty($item['error']);
				} else {
					$v = new WX_Form_Form_Validator_Radiogroupnotempty();
				}
				$e->addValidator($v);
			}
		}
		if ( isset($item['value'])) {
			$e->initValue( $item['value']);
		} else {
			$e->initValue($e->getName());
		}
		if ( isset($item['checked']) && $item['checked']) {
			$e->checked();
		}
		return $e;
	}
	
	public static function CreateBirthdate(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Birthdate');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementLabel($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementDescription($item, $e);
		self::setElementEnable($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementEmptyTextPropoerty($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementData($item, $e);

		if ( isset($item['items'] )) {
			$e->setProperty('items',$item['items']);
		}
		if ( isset($item['groups'] )) {
			$e->setProperty('groups',$item['groups']);
		}
		if ( isset($item['extraFields'] )) {
			$e->setProperty('extraFields',$item['extraFields']);
		}
		
		$thisYear = date('Y');
		$years = array();
		$years[] = '';
		for ($y = $thisYear; $y >= 1900; $y--) {
			$years[$y] = $y;
		}
		
		if (
			!is_array($e->data) ||
			count($e->data) != 12
		) {
			throw new Exception("CreateBirthdate: data parameter is not array or count != 12");
		}
		
		$month = $e->data;
		
		$months = array();
		$months[] = '';
		
		for ($m = 0,$i = 1; $m < 12; $m++,$i++) {
			$months[$i] = $month[$m];
		}
		
		$days = array();
		$days[] = '';
		for ($i=1;$i<=31;$i++){
			$days[$i] = $i;
		}
		$e->years = $years;
		$e->months = $months;
		$e->days = $days;
		
		return $e;
	}
	

	public static function CreateGoogleMapAutocomplete(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_GoogleMapAutocomplete');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementLabel($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementDescription($item, $e);
		self::setElementEnable($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementEmptyTextPropoerty($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementData($item, $e);

		if ( isset($item['items'] )) {
			$e->setProperty('items',$item['items']);
		}
		if ( isset($item['groups'] )) {
			$e->setProperty('groups',$item['groups']);
		}
		if ( isset($item['extraFields'] )) {
			$e->setProperty('extraFields',$item['extraFields']);
		}
		
		return $e;
	}

	public static function CreateCustom(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Custom');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementLabel($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementDescription($item, $e);
		self::setElementEnable($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementEmptyTextPropoerty($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementData($item, $e);
		
		return $e;
	}

	/**
	 * Create link (button) element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'link',
	 *	'class':'<the css class to input filed>',
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'properties':[{ //properties }],
	 *	'value':'value of the submit',
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Submit The submit instance.
	 **/
	public static function CreateLink(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Link');

		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementName($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementLabel($item, $e);

		/*
		if ( !isset($item['link'] )) {
			throw new Exception('Link proporty is required!');
		}
		$e->setProperty('link',$item['link']);
		*/

		if ( !isset($item['href'] )) {
			throw new Exception('Href proporty is required!');
		}
		$e->setProperty('href',$item['href']);

		return $e;
	}
	
	/**
	 * Create text input element instance from JSON.
	 *
	 * Valid JSON descriptor
	 * <code>
	 * {
	 *	'name':'<the input name (required)>',
	 *	'vtype':'text',
	 *    'options':'[{
	 *		'type' : '<type of the image>',
	 *		'isTemp' : '<if the image is temp>',
	 *		'size' : '<size of the image (eg. normal)>',
	 *	   }],
	 *	'fieldLabel':'<the label of the field>',
	 *	'allowBlank':'<the filed blank value is enabled (true|false if false the field is required field)>',
	 *	'class':'<the css class to input filed>',
	 *	'readOnly':<true|false>,
	 *	'enable':<false|true>,
	 *	'id':'<the id of the input tag>'
	 *	'templateDirectory':'<the directory of the templates>',
	 *	'validators':[{ //Validators }],
	 *	'properties':[{ //properties }]
	 *	'template':'<the template file of the form>'
	 * }
	 * </code>
	 *
	 * @param array			 $item		 The descriptor array.
	 *
	 * @return WX_Form_Form_Element_Text The text instance.
	 **/
	public static function CreateImage(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Image');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementOptions($item, $e);
		self::setElementName($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementLabel($item, $e);
		self::setElementEnable($item, $e);
		self::setElementDescription($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementHint($item, $e);
		return $e;
	}

	public static function CreateMultiimage(array $item){
		$e = self::CreateElementFromDescriptor($item, 'WX_Form_Form_Element_Multiimage');
		self::setElementTemplate($item, $e);
		self::setElementTemplateDirectory($item, $e);
		self::setElementAttributes($item, $e);
		self::setElementOptions($item, $e);
		self::setElementName($item, $e);
		self::setElementIdAttribute($item, $e);
		self::setElementClassAttribute($item, $e);
		self::setElementValidators($item, $e);
		self::setElementFilters($item, $e);
		self::setElementLabel($item, $e);
		self::setElementEnable($item, $e);
		self::setElementDescription($item, $e);
		self::setElementReadonly($item, $e);
		self::setElementDefaultValue($item, $e);
		self::setElementAllowBlankAttribute($item, $e);
		self::setElementHint($item, $e);
		return $e;
	}
}