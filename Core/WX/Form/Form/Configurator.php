<?php

class WX_Form_Form_Configurator {
    /**
     * var string The template directory default: form/templates/.
     **/
    public $templateDirectory = 'form/templates/html/';
    /**
     * var string The form template file name default: form.tpl.php.
     **/
    public $formTemplate = 'form.tpl.php';
    /**
     * var string The text input element template file name default: text.tpl.php.
     **/
    public $textTemplate = 'text.tpl.php';
    /**
     * var string The file input element template file name default: text.tpl.php.
     **/
    public $fileTemplate = 'file.tpl.php';
    /**
     * var string The textarea input element template file name default: textarea.tpl.php.
     **/
    public $textareaTemplate = 'textarea.tpl.php';
    /**
     * var string The form group element template file name default: group.tpl.php.
     **/
    public $groupTemplate = 'group.tpl.php';
    /**
     * var string The form radio group element template file name default: radiogroup.tpl.php.
     **/
    public $radiogroupTemplate = 'radiogroup.tpl.php';
    /**
     * var string The form checkbox group element template file name default: radiogroup.tpl.php.
     **/
    public $checkgroupTemplate = 'checkboxgroup.tpl.php';
    /**
     * var string The radio button element template file name default: radiobutton.tpl.php.
     **/
    public $radioButtonTemplate = 'radiobutton.tpl.php';
    /**
     * var string The check box element template file name default: checkbox.tpl.php.
     **/
    public $checkboxTemplate = 'checkbox.tpl.php';
    /**
     * var string The option element template file name default: option.tpl.php.
     **/
    public $multipleoptionTemplate = 'multipleoption.tpl.php';
    /**
     * var string The option element template file name default: option.tpl.php.
     **/
    public $optionTemplate = 'option.tpl.php';
    /**
     * var string The submit element template file name default: submit.tpl.php.
     **/
    public $submitTemplate = 'submit.tpl.php';
    /**
     * var string The password input element template file name default: password.tpl.php.
     **/
    public $passwordTemplate = 'password.tpl.php';
    /**
     * var string The hidden input element template file name default: hidden.tpl.php.
     **/
    public $hiddenTemplate = 'hidden.tpl.php';
    /**
     * var string The description  element template file name default: description.tpl.php.
     **/
    public $descriptionTemplate = 'description.tpl.php';
    /**
     * var string The description  element template file name default: googlemapautocomplete.tpl.php.
     **/
    public $googlemapautocompleteTemplate = 'googlemapautocomplete.tpl.php';
    /**
     * var string The description  element template file name default: googlemapautocomplete.tpl.php.
     **/
    public $jqueryuploadTemplate = 'jqueryupload.tpl.php';
    /**
     * var string The description  element template file name default: custom.tpl.php.
     **/
    public $customTemplate = 'custom.tpl.php';
    /**
     * var string The image input element template file name default: image.tpl.php.
     **/
    public $imageTemplate = 'image.tpl.php';
    /**
     * var string The image input element template file name default: multiimage.tpl.php.
     **/
    public $multiimageTemplate = 'multiimage.tpl.php';
}