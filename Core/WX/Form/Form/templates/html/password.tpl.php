<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>
	<div class="field">
		<input autocomplete="off" class="text <?=$this->getAttribute('class')?>" type="password" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>
