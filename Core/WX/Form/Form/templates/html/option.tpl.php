<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>
	<div class="field">
		<select class="select <?=$this->getAttribute('class')?>" <?=$this->getAttributesString()?>>
			<?=$this->getOptionsString(); ?>
		</select>
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>