<?php

class WX_Form_Form_Elements {
    const T_TEXT = 'text';
    const T_TEXTAREA = 'textarea';
    const T_SUBMIT = 'submit';
    const T_RADIOBUTTON = 'radio';
    const T_CHECKBOX = 'checkbox';
    const T_OPTION = 'option';
    const T_MULTIPLEOPTION = 'multipleoption';
    const T_FILE = 'file';
    const T_PASSWORD = 'password';
    const T_HIDDEN = 'hidden';
    const T_DESCRIPTION = 'description';
	const T_CUSTOM = 'custom';
	const T_LINK = 'link';
	const T_IMAGE = 'image';
	const T_MULTIIMAGE = 'multiimage';
}