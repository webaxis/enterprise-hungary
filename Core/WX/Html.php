<?php

class WX_Html {

	static public function get ($tpl, $params = array(), $scriptsPath = SCRIPTS_PATH){
		$view = new Zend_View;
		$view->setScriptPath($scriptsPath);
		
		foreach ($params as $key => $value){
			$view->assign($key, $value);
		}
		return $view->render($tpl);
	}
}