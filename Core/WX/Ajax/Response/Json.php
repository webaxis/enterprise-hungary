<?php

class WX_Ajax_Response_Json {

	public static function response($response = array()) {
		return json_encode($response);
	}

	public static function success($data = array()) {
		return json_encode(WX_Ajax_Response_Array::success($data));
	}

	public static function error($data = array()) {
		return json_encode(WX_Ajax_Response_Array::error($data));
	}

	public static function unexpectedError($data = array()) {
		return json_encode(WX_Ajax_Response_Array::unexpectedError($data));
	}

}

?>