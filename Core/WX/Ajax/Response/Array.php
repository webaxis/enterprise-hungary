<?php

class WX_Ajax_Response_Array {

	protected static function _response($status, $data) {
		$response = array(
			'status' => $status
		);
		if (!empty($data)) {
			$response['data'] = $data;
		}
		return $response;
	}

	public static function success($data = array()) {
		return self::_response('success', $data);
	}

	public static function error($data = array()) {
		return self::_response('error', $data);
	}

	public static function unexpectedError($data = array()) {
		if (!isset($data['errorCode'])) {
			WX_Exception::create('Please add error code e.g. 901');
		}
		return self::_response('unexpectedError', $data);
	}

}

?>