<?php

class WX_Content {
	
	protected $_identifier 			= false;

	protected $_dbTable				= 'content';

	protected $_contentColumnName	= 'content';
	protected $_headColumnName		= 'head';
	protected $_nameColumnName		= 'name';

	private $_model 				= null;

	public function __construct ($identifier = false) {
		$this->_identifier = $identifier;
	}

	public static function getInstance($identifier) {
		$class = get_called_class();
		return new $class($identifier);
	}

	public function getOne () {
		if (is_null($this->_model)) {
			$this->_setOne();
		}

		return $this->_model;
	}

	private function _setOne () {
		$modelName = APP_NAME . '_Model_' . ucfirst($this->_dbTable);

		$model = new $modelName;

		if (ctype_digit($this->_identifier)) {
			$model->find($this->_identifier);
		} else {
			$findFuncName = 'findOneBy' . ucfirst($this->_nameColumnName);
			$model->$findFuncName($this->_identifier);
		}

		$this->_model = $model;
	}
	
	public function getContent () {
		$row = $this->getOne();

		$localeClassName = APP_NAME . '_Locale';
		$iso = $localeClassName::getInstance()->getLanguage();

		$getFuncName = 'get' . ucfirst($this->_contentColumnName) . ucfirst(strtolower($iso));
		
		return $row->$getFuncName();
	}
	
	public function getHead () {
		$row = $this->getOne();

		$localeClassName = APP_NAME . '_Locale';
		$iso = $localeClassName::getInstance()->getLanguage();

		$getFuncName = 'get' . ucfirst($this->_headColumnName) . ucfirst(strtolower($iso));
		
		return $row->$getFuncName();
	}
}