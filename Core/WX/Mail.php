<?php

class WX_Mail {

	/**
	 * Singleton instance
	 *
	 * @var object
	 */
	protected static $WX_Mail;

	/**
	 * Create singleton instance
	 *
	 * @return object
	 */
	public static function getInstance() {
		if (self::$WX_Mail == null) {
			self::$WX_Mail = new self();
		}

		return self::$WX_Mail;
	}

	protected $_mailObject = null;

	protected $_descriptor = array();

	protected $_mailView = null;

	protected $_templateVars = array();

	protected $_hasEmbeddedElement = false;

	protected $_addTo = null;

	protected $_subject = null;

	protected $_addFromName = null;

	protected $_addFromEmail = null;

	protected $_addReplyToName = null;

	protected $_addReplyToEmail = null;

	protected $_language = null;

	public function send($descriptor, $mailParams, $templateVars = array()) {
		$this->_checkMailRequiredParams($mailParams);
		$this->_setDescriptor($descriptor);
		$this->_setTemplateVars($templateVars);
		$this->_initMailObject();
		$this->_createTemplate();
		$this->_sendMail();
	}

	protected function _checkMailRequiredParams($mailParams) {
		//@todo: megnezni, hogy alap parameterek megvannak e
		//@todo: validatorokat irni, hogy a parameterek helyesek e. Pl. email cím formátuma
		//@todo: ez csak ideiglenesen kell ide
		//@todo: bbc es egyeb is mehet oda
		$this->_addTo = $mailParams['addTo'];
		$this->_subject = $mailParams['subject'];
		$this->_addFromName = isset($mailParams['addFromName']) ? $mailParams['addFromName'] : false;
		$this->_addFromEmail = isset($mailParams['addFromEmail']) ? $mailParams['addFromEmail'] : false;
		$this->_addReplyToName = isset($mailParams['addReplyToName']) ? $mailParams['addReplyToName'] : false;
		$this->_addReplyToEmail = isset($mailParams['addReplyToEmail']) ? $mailParams['addReplyToEmail'] : false;
	}

	/**
	 * Read data from descriptor
	 *
	 * Descriptors desired location: .../Core/APP_NAME/Mail/Descriptor.php
	 */
	protected function _setDescriptor($descriptor) {
		$class = APP_NAME.'_Mail_Descriptor';

		if (!@class_exists($class)) {
			$class = 'WX_Mail_Descriptor';
		}

		if (!method_exists($class, $descriptor)) {
			$this->_descriptor = WX_Mail_Descriptor::defaults(strtolower($descriptor));
		} else {
			$method = ucfirst(strtolower($descriptor));
			$defaults = WX_Mail_Descriptor::defaults($method);
			$options = call_user_func(array($class, $method));
			$this->_descriptor = array_merge($defaults, $options);
		}
	}

	protected function _setTemplateVars($templateVars) {
		$this->_templateVars = $templateVars;
	}

	protected function _initMailObject() {
		$defaultSettingsObject = Zend_Registry::get('config')->resources->mail;

		$fromName = $defaultSettingsObject->defaultFrom->name;
		$fromEmail = $defaultSettingsObject->defaultFrom->email;
		$replyToName = $defaultSettingsObject->defaultReplyTo->name;
		$replyToEmail = $defaultSettingsObject->defaultReplyTo->email;
		
		if (isset($this->_descriptor['from']['name']))
			$fromName = $this->_descriptor['from']['name'];
		
		if (isset($this->_descriptor['from']['email']))
			$fromEmail = $this->_descriptor['from']['email'];
		
		if (isset($this->_descriptor['replyTo']['name']))
			$replyToName = $this->_descriptor['from']['name'];
		
		if (isset($this->_descriptor['replyTo']['email']))
			$replyToEmail = $this->_descriptor['from']['email'];
		
		if ($this->_addFromName)
			$fromName = $this->_addFromName;
			
		if ($this->_addFromEmail)
			$fromEmail = $this->_addFromEmail;
		
		if ($this->_addReplyToName)
			$replyToName = $this->_addReplyToName;
			
		if ($this->_addReplyToEmail)
			$replyToEmail = $this->_addReplyToEmail;

		$this->_mailObject = new Zend_Mail('UTF-8');
		$this->_mailObject->addHeader('X-Mailer', 'Zend Mail');
		$this->_mailObject->setFrom($fromEmail, $fromName);
		$this->_mailObject->setReplyTo($replyToEmail, $replyToName);
		//@TODO: BBC-vel lehet bővíteni
		$this->_mailObject->addTo($this->_addTo);
		$this->_mailObject->setSubject($this->_subject);
	}

	protected function _createTemplate() {
		$this->_language = Zend_Registry::get('language');
		$this->_initMailView();
		$this->_buildHtmlMode();
		$this->_buildTextMode();
	}

	protected function _initMailView() {
		$this->_mailView = new Zend_View();
		$this->_mailView->setScriptPath(MAIL_TEMPLATES_PATH . 'layouts/' . $this->_language . '/');
	}

	protected function _assignTemplateVars() {
		foreach ($this->_templateVars as $key => $value) {
			$this->_mailView->assign($key, $value);
		}
	}

	protected function _buildTextMode() {
		if (isset($this->_descriptor['layout']['text'])) {
			$template = WX_Html::get($this->_descriptor['template']['text'], $this->_templateVars, MAIL_TEMPLATES_PATH . 'templates/' . $this->_language . '/');
			$this->_mailView->assign('body', $template);
			
			$body = $this->_mailView->render($this->_descriptor['layout']['text']);

			$this->_mailObject->setBodyText($body);
		}
	}

	protected function _buildHtmlMode() {
		if (isset($this->_descriptor['layout']['html'])) {
			$template = WX_Html::get($this->_descriptor['template']['html'], $this->_templateVars, MAIL_TEMPLATES_PATH . 'templates/' . $this->_language . '/');
			$this->_mailView->assign('body', $template);
			
			$body = $this->_mailView->render($this->_descriptor['layout']['html']);
			
			if ($this->_descriptor['embedImages']) {
				$this->_embedImages($body);
			}
			
			$this->_mailObject->setBodyHtml($body);
		}
	}

	/**
	 * Embed images into mail
	 *
	 * @param string $html Mail html content
	 *
	 * @return string $html
	 */
	protected function _embedImages(&$html) {
		$xmlBody = new DomDocument();

		@$xmlBody->loadHTML($html);
		$imgs = $xmlBody->getElementsByTagName('img');

		$imgCount = 0;

		foreach ($imgs as $img) {
			$imgCount++;
			$imgUrl = $img->getAttribute('src');
			$imgExt = pathinfo($imgUrl, PATHINFO_EXTENSION);

			$imgId = sha1(time() . $imgCount . $imgUrl);
			$html = str_replace($imgUrl, 'cid:' . $imgId, $html);
			$imgBinary = file_get_contents($imgUrl);
			$imgPart = $this->_mailObject->createAttachment(
				$imgBinary,  
				mime_content_type($imgUrl), 
				Zend_Mime::DISPOSITION_INLINE, 
				Zend_Mime::ENCODING_BASE64
			);

			$imgPart->filename	= 'image' . $imgCount . $imgExt;
			$imgPart->id = $imgId;
		}
		return $html;
	}

	protected function _sendMail() {
		//DX($this);
		try {
			$this->_mailObject->send();
		} catch (Exception $ex) {
			WX_Exception::create($ex->getMessage());
		}
	}
}