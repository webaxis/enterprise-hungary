<?php

class WX_List2_Abstract {

	public $type = null;

	public $config = null;

	public $templateDirectory = null;

	public $template = null;
	
	public $descriptor = null;

	public function __construct ($descriptor) {
		if (isset($descriptor['templateDirectory'])) {
			$this->templateDirectory = $descriptor['templateDirectory'];
		}

		if (isset($descriptor['template'])) {
			$this->template = $descriptor['template'];
		}

		if ($this->descriptor === null) {
			$this->descriptor = $descriptor;
		}
	}

	public function renderJavascript ($template = false) {
		if (!$template) {
			$template = $this->template;
		}

		$template = 'javascript/' . $template;

		echo $this->fetch($template);
	}

	public function render ($template = false) {
		echo $this->fetch($template);
	}

	public function fetch ($template = false) {

		ob_start();
		include($this->getTemplate($template));
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	public function getType () {
		if ($this->type === null) {
			throw new Exception('Type must be defined at: ' . get_called_class(),1);
		}

		return $this->type;
	}

	public function getConfig () {
		if (!$this->config) {
			$config = new WX_List2_Config;
			$this->config = $config;
		}

		return $this->config;
	}

	// template directory can be a fill server path
	public function getTemplateDirectory () {
		if ($this->templateDirectory === null) {
			$config = $this->getConfig();
			return WX_PATH . $config->getTemplateDirectory();
		}

		return $this->templateDirectory;
	}

	# Bit messy but needed
	public function getTemplate ($template = false) {
		if (!$template) {
			$template = $this->template;
		}

		$config = $this->getConfig();

		if ($this->templateDirectory !== null) {
			# 1st - if directory was given manually - check if it exists in the <given>/<type>/ dir
			$pathToTemplate = $this->getTemplateDirectory() . $this->getType() . $template;

			if (!file_exists($pathToTemplate)) {
				# 2nd - if it's not in <given>/<type>/ try <given>/
				$pathToTemplate = $this->getTemplateDirectory() . $template;
			}

			if (!file_exists($pathToTemplate) && $this->templateDirectory !== null) {
				# 3rd - if it's given but not even in the <given> throw error
				throw new Exception('Template not found: ' . $pathToTemplate,1);
				return false;
			}
		} else {
			# 4th - if it was not given, check out <app>/<type>/ first
			$pathToTemplate = CORE_APP_PATH . $config->templateDirectory . $this->getType() . '/' . $template;
			if (!file_exists($pathToTemplate)) {
				# 5th - if it's not in <app>/<type>/ check out <app>/
				$pathToTemplate = CORE_APP_PATH . $config->templateDirectory . $template;
			}

			# 6th - if it was not in the app, check out <wx-list>/<type>/ first
			if (!file_exists($pathToTemplate)) {
				$pathToTemplate = $config->getTemplateDirectory() . $this->getType() . '/' . $template;
			}

			if (!file_exists($pathToTemplate)) {
				# 7th - if it's not in <wx-list>/<type>/ check out <wx-list>/
				$pathToTemplate = $config->getTemplateDirectory() . $template;
			}
		}

		# 8th - if it's not any of the places then you missed it
		if (!file_exists($pathToTemplate)) {
			throw new Exception('Template not found: ' . $pathToTemplate,1);
			return false;
		}

		return $pathToTemplate;
	}
}