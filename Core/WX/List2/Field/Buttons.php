<?php

class WX_List2_Field_Buttons extends WX_List2_Field_Abstract {

	public $template = 'buttons.phtml';

	public $width = '1%';

	public $style = 'text-align: right; white-space: nowrap;';
}