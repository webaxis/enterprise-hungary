<?php

class WX_List2_Button_Delete extends WX_List2_Button_Abstract {

	public $baseClass = 'wx-list-delete';

	public function __construct ($descriptor, $data = null, $index = null) {
		parent::__construct($descriptor, $data, $index);

		$this->domData = array(
			'id' => $this->getData('id')
		);
	}
}