<?php

class WX_List2_Source_ZendQuery extends WX_List2_Source_Db {

	public function getBaseQuery () {
		$query = $this->source;

		if ($this->prefilter !== null) {
			$query->where($this->prefilter);
		}

		$query->reset(Zend_Db_Select::ORDER)
			  ->reset(Zend_Db_Select::LIMIT_COUNT)
			  ->reset(Zend_Db_Select::LIMIT_OFFSET);

		return $query;
	}
}