<?php

class WX_List2_Source_Db extends WX_List2_Source_Abstract {

	public function getData () {
		$db = Zend_Registry::get('db');

		$query = $this->getBaseQuery();

		if ($this->limit !== null) {
			$query->limit($this->getLimit(), $this->getFrom());
		}

		if ($this->orderBy !== null) {
			if (is_array($this->orderBy)) {
				$order = array();

				foreach ($this->orderBy as $key => $one) {
					$order[] = $one . ' ' . $this->order[$key];
				}
			} else {
				$order = array("$this->orderBy $this->order");
			}
			$query->order($order);
		}
		
		return $db->query($query)->fetchAll();
	}

	public function getTotalCount () {
		$db = Zend_Registry::get('db');
		return count($db->query($this->getBaseQuery())->fetchAll());
	}

	public function getBaseQuery () {
		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from($this->source);

		if ($this->prefilter !== null) {
			$query->where($this->prefilter);
		}

		$query->reset(Zend_Db_Select::ORDER)
			  ->reset(Zend_Db_Select::LIMIT_COUNT)
			  ->reset(Zend_Db_Select::LIMIT_OFFSET);

		return $query;
	}
}