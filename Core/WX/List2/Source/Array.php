<?php

class WX_List2_Source_Array extends WX_List2_Source_Abstract {

	public $baseData = null;

	public function getData () {
		$return = $this->getBaseData();		

		if ($this->orderBy !== null) {
			sortBy($this->orderBy, $return, $this->order);
		}

		if ($this->limit !== null) {
			$return = array_slice($return, $this->getFrom(), $this->getLimit());
		}

		return $return;
	}

	public function getBaseData () {
		if ($this->baseData !== null) {
			return $this->baseData;
		}

		$return = $this->source;

		if ($this->prefilter !== null) {
			$temp = array();

			# convert single filter to an array that contains that filter
			if (is_array($this->prefilter) && !isset($this->prefilter[0])) {
				$this->prefilter = array($this->prefilter);
			}

			foreach ($return as $key => $row) {
				$match = true;

				foreach ($this->prefilter as $filterKey => $filter) {
					# strict filter - looser can be implemeneted later
					if ($row[key($filter)] != $filter[key($filter)]) {
						$match = false;
					}
				}

				if ($match) {
					$temp[$key] = $row;
				}
			}

			$return = $temp;
		}

		$this->baseData = $return;

		return $return;
	}

	public function getTotalCount () {
		return count($this->getBaseData());
	}
}