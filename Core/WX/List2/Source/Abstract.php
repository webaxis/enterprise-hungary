<?php

class WX_List2_Source_Abstract {

	public $source = null;

	public $prefilter = null;

	public $page = null;

	public $limit = null;

	public $fromZero = false;

	public $ahead = null;

	public $refresh = false;

	public $order = 'DESC';

	public $orderBy = null;

	public function __construct ($source, $options) {
		$this->source = $source;

		if (isset($options['order'])) {
			$this->order = $options['order'];
		}

		if (isset($options['orderBy'])) {
			$this->orderBy = $options['orderBy'];
		}
	}

	public function getFrom () {
		if ($this->fromZero) {
			$from = 0;
		} else {
			$from = ($this->page - 1) * $this->limit;
		}

		if ($this->ahead !== null) {
			$from = $from - $this->ahead;

			if ($from < 0) {
				$from = 0;
			}
		}

		return $from;
	}

	public function getData () { }

	public function getTotalCount () { }

	public function prefilter ($prefilter) {
		$this->prefilter = $prefilter;
	}

	public function ahead ($ahead) {
		$this->ahead = $ahead;
	}

	public function refresh () {
		$this->refresh = true;
	}

	public function fromZero () {
		$this->fromZero = true;
	}

	public function setPage ($page) {
		$this->page = $page;
	}

	public function getPage () {
		return $this->page;
	}

	public function setLimit ($limit) {
		$this->limit = $limit;
	}

	public function getLimit () {
		$limit = $this->limit;

		if ($this->refresh) {
			$limit = $limit * $this->page;
		}

		return $limit;
	}

	public function getOrder () {
		return $this->order;
	}

	public function getOrderBy () {
		return $this->orderBy;
	}
}