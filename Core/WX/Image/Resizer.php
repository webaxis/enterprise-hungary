<?php

require_once('3rdparty/php_image_magician.php');

/**
 * This class create resized images from source.
 *
 * It use a 3rdparty class: imageLib. Further information abuot this library:
 * http://phpimagemagician.jarrodoberto.com/
 *
 */
class WX_Image_Resizer {

	protected $_filename = '';

	protected $_extension = '';

	protected $_path = '';

	/**
	 * It contains image sizes.
	 *
	 * Example:
	 * $_imageSizes = array(
	 * 		'm' => array(
	 *			'width' => 100,
	 *			'height' => 100,
	 *			'method' => 'crop'
	 *		),
	 *      ...
	 */
	protected $_imageSizes = array();

	/**
	 * It contains generated images.
	 *
	 * Example:
	 * $_generatedImages = array(
	 *		'm' => '/www/var/uploads/jellyfish_s.jpg',
	 *		...
	 * );
	 */
	protected $_generatedImages = array();

	static protected $_quality = 100;

	/**
	 * Error container
	 */
	protected $_errors = array();

	public function __construct($file, $path, $imageSizes) {
		$pathInfo = pathinfo($file);
		$this->_filename = $pathInfo['filename'];
		$this->_extension = $pathInfo['extension'];
		$this->_path = $path;
		$this->_imageSizes = $imageSizes;
	}

	/**
	 * Generate images.
	 */
	public function generateImages() {
		if (count($this->_imageSizes)) {
			foreach ($this->_imageSizes as $size => $properties) {
				$image = self::generateImage($this->_filename, $this->_extension, $size, $this->_path, $properties);
				if ($image !== false) {
					$this->_generatedImages[$size] = $image;
				}
			}
		} else {
			error_log('You can\'t resize images because "_imageSizes" array is empty',0);
		}
	}

	static public function generateImage($filename, $extension, $size, $path, $properties) {
		$fileOriginal = $filename . '.' . $extension;
		$file = $filename . '_' . $size . '.' . $extension;
		# @TODO: ezt majd kivenni szebb osztalyba
		$resizer = new imageLib($path . $fileOriginal);
		$resizer->setForceStretch(false);
		$resizer->resizeImage($properties['width'], $properties['height'], $properties['method']);
		$resizer->saveImage($path . $file, self::$_quality);
		if ($resizer->getErrors()) {
			$errors = implode(',', $resizer->getErrors());
			error_log('Error az image resize. Path: "' . $path . '", file: "' . $file . '"; imageLib error: ' . $errors);
			return false;
		} else {
			return $file;
			//$this->_generatedImages[$size] = $file;
		}
	}
}


?>