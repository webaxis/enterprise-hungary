<?php

class WX_Image_Getter {

	protected $_image = '';

	protected $_imageSizes = array();

	protected $_defaultImage = array();

	public function __construct($url, $path, $image, array $imageSizes, $defaultImage = array()) {
		$this->_url = $url;
		$this->_path = $path;
		$this->_image = $image;
		$this->_imageSizes = $imageSizes;
		$this->_defaultImage = $defaultImage;
	}

	protected function _validateSize($size = false) {
		if ($size === false) {
			return;
		}
		if (!array_key_exists($size, $this->_imageSizes)) {
			$message = 'The following size is not exists in $_imageSizes: "'.$size.'"';
			WX_Exception::create($message);
		}
	}

	/**
	 * Get the image url with directories
	 *
	 * @param mixed $size Size - optional, if $size false get the original image
	 *
	 * @return string e.g uploads/jellyfish_s.jpg
	 */
	public function getImageUrl($size = false) {
		$this->_validateSize($size);
		$pattern = $this->_path.$this->_image;

		// without size
		if ($size === false) {
			$filePath = $this->_readImageFromPath($pattern.'.*');
			if ($filePath != false) {
				$file = $this->_convertFilePathToUrl($filePath);
			} elseif (isset($this->_defaultImage['url'])) {
				$file = $this->_defaultImage['url'];
			} else {
				$file = '';
			}
		// with size
		} else {
			$filePath = $this->_readImageFromPath($pattern.'_'.$size.'.*');
			if (!empty($filePath)) {
				$file = $this->_convertFilePathToUrl($filePath);
			// not found, try get original
			} else {
				$filePath = $this->_readImageFromPath($pattern.'.*');
				if (!empty($filePath)) {
					$generatedFile = $this->_generateImage($filePath, $size);
					if (!empty($generatedFile)) {
						$file = $this->_url . '/' . $generatedFile;
					} else {
						$file = $this->_convertFilePathToUrl($filePath);
					}
				} elseif (isset($this->_defaultImage['url'])) {
					$file = $this->_defaultImage['url'];
				} else {
					$file = '';
				}
			}
		}

		return $file;
	}

	protected function _generateImage($filePath, $size) {
		$fileInfo = pathinfo($filePath);
		$properties = $this->_imageSizes[$size];
		return WX_Image_Resizer::generateImage($fileInfo['filename'], $fileInfo['extension'], $size, $this->_path, $properties);
	}

	/**
	 * Get an image's path
	 *
	 * @param mixed $size Image size e.g. 's'; default false means the original file
	 *
	 * @return string /www/var/uploads/jellyfish_s.jpg
	 */
	public function getImagePath($size = false) {
		$this->_validateSize($size);
		$pattern = $this->_path.$this->_image;
		// without size
		if ($size === false) {
			$filePath = $this->_readImageFromPath($pattern.'.*');
			if ($filePath != false) {
				$file = $filePath;
			} elseif (isset($this->_defaultImage['path'])) {
				$file = $$this->_defaultImage['path'];
			} else {
				$file = '';
			}
		// with size
		} else {
			$filePath = $this->_readImageFromPath($pattern.'_'.$size.'.*');
			if ($filePath !== false) {
				$file = $filePath;
			// not found, try get original
			} else {
				$filePath = $this->_getImage($pattern.'.*');
				if ($filePath !== false) {
					$file = $filePath;
				} elseif (isset($this->_defaultImage['path'])) {
					$file = $this->_defaultImage['path'];
				} else {
					$file = '';
				}
			}
		}
		return $file;
	}

	public function getImageWithId($size = false) {
		$data = array(
			'id' => $this->_image,
			'url' => $this->getImageUrl($size)
		);
		return $data;
	}

	/**
	 * Convert file path to file url
	 *
	 * Example: /www/var/uploads/jellyfish.jpg => uploads/jellyfish.jpg
	 *
	 * @return string example: uploads/jellyfish.jpg
	 */
	protected function _convertFilePathToUrl($filePath) {
		$file = str_replace($this->_path, '', $filePath);
		return $this->_url.$file;
	}

	/**
	 * Read the first occurence by pattern
	 *
	 * @param string $pattern Pattern e.g. /www/var/uploads/jellyfish.*
	 *
	 * @return string e.g. /www/var/uploads/jellyfish.jpg
	 */
	protected function _readImageFromPath($pattern) {
		$file = WX_Directory_Reader::getFirstResultByPattern($pattern);
		if ($file === false) {
			$message = 'IMAGE GETTER FAILED: This file not found: "'.$pattern.'"';
			#error_log($message, 0);
		}
		return $file;
	}
}

?>