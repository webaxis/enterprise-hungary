<?php

/**
 * Base image handler class.
 *
 * ABOUT
 * =====
 * You have an image file somewhere: /var/www/uploads/subdirectory/jellyfish.jpg
 * You want the following things with this file:
 * - upload via form
 * - create resized files
 * - copy
 * - delete
 * - get url for html contents
 * - etc.
 * This class create an object from the file and wrap some class which you can 
 * perform the above mentioned operations.
 *
 * Wrap some other class:
 * - WX_Image_Getter
 * - WX_Image_Upload
 * - WX_Image_Delete
 * - WX_Image_Copy
 * - WX_Image_Resizer
 */
class WX_Image_Image {

	protected $_path = '';

	protected $_url = '';

	/** 
	 * Available mods: normal, hash 
	 */
	protected $_mode = 'hash';

	/**
	 * Image hash: e4gt67d99alse54lmfhg4432er4h4s1
	 */
	protected $_imageHash = '';

	protected $_descriptor = array();

	/**
	 * Subdirectory is the section between $_path/$_url and file.
	 * Examples (uppercase words): 
	 * - /var/www/uploads/SUBDIRECTORY/jellyfish.jpg
	 * - /var/www/uploads/SUBDIRECTORY_1/SUBDIRECTORY_2/jellyfish.jpg
	 */
	protected $_subDirectory = '';

	/**
	 * e.g. jellyfish.jpg
	 */
	protected $_file = '';

	/**
	 * e.g. jellyfish
	 */
	protected $_fileName = '';

	/**
	 * e.g. jpg
	 */
	protected $_fileExtension = '';

	/**
	 * Define the (original) file pattern. 
	 * Match with: /www/uploads/directory/JELLYfish123.jpg
	 */
	public static $filePattern = '/^([0-9a-zA-Z\/\_]+)([0-9a-zA-Z]{2})(\.[a-zA-Z]{3})$/i';

	/**
	 * Define the (resized) file pattern. 
	 * Match with: /www/uploads/directory/JELLYfish123.jpg
	 */
	public static $filePatternSize = '/^([0-9a-zA-Z\/]+)(\_[a-z]\.[a-zA-Z]{3})$/i';

	protected $_imageSizes = array();

	protected $_getter = null;

	protected $_uploader = null;

	protected $_delete = null;

	protected $_copy = null;

	protected $_resizer = null;

	protected $_getterClass = 'WX_Image_Getter';

	public function __construct($descriptor, $imageHash = false, $file = false) {
		$this->_descriptor = $descriptor;
		$this->_path = $descriptor['path'];
		$this->_url = $descriptor['url'];
		//Set sizes - if exists
		if (isset($descriptor['sizes'])) {
			$this->_imageSizes = $descriptor['sizes'];
		}
		// Set mode - if exists
		$mode = isset($descriptor['mode']) ? $descriptor['mode'] : 'hash';
		switch ($mode) {
			case 'normal' :
				$this->_mode = 'normal';
				if ($file !== false) {
					$this->_file = $file;
				}
				
				break;
			case 'hash' : 
				$this->_mode = 'hash';
				if ($imageHash !== false) {
					$this->_imageHash = $imageHash;
				}
				break;
			default :
				throw new Exception('Unknown mode: "'.$descriptor['mode'].'"');
				break;
		}
	}

	public function initUploader($fileArray) {
		$path = $this->_path;
		$settings = isset($this->_descriptor['uploader']) ? $this->_descriptor['uploader'] : array();
		switch ($this->_mode) {
			case 'normal' :
				$this->_uploader = new WX_Image_Upload($fileArray, $path, $settings);
				break;
			case 'hash' :
				$this->_imageHash = md5(rand(0,9999) . $fileArray['name'] . time());
				$this->_uploader = new WX_Image_Upload($fileArray, $path, $settings, $this->_imageHash);
				break;
		}
		return $this->_uploader;
	}

	public function initDelete() {
		switch ($this->_mode) {
			case 'normal' :
				$pattern = $this->_path.$this->getName();
				break;
			case 'hash' :
				$pattern = $this->_path.$this->_imageHash;
				break;
		}
		$imageSizes = !empty($this->_imageSizes) ? array_keys($this->_imageSizes) : array();
		$this->_delete = new WX_Image_Delete($pattern, $imageSizes);
		return $this->_delete;
	}

	public function initGetter($image = false) {
		if ($image === false) {
			switch ($this->_mode) {
				case 'normal' :
					if ($image === false) {
						$image = $this->getName();
					}
					break;
				case 'hash' :
					$image = $this->_imageHash;
					break;
			}
		}

		//check default image
		if (isset($this->_descriptor['defaultImage'])) {
			if (!isset($this->_descriptor['defaultImage']['url'])) {
				throw new Exception('Missing "url" from the descriptor\'s "defaultImage" branch!');
			}
			/*
			if (!isset($this->_descriptor['defaultImage']['path'])) {
				throw new Exception('Missing "path" from the descriptor\'s "defaultImage" branch!');
			}
			*/
			$defaultImage = $this->_descriptor['defaultImage'];
		} else {
			$defaultImage = array();
		}

		$this->_getter = new $this->_getterClass(
			$this->_url,
			$this->_path,
			$image,
			(!empty($this->_imageSizes) ? $this->_imageSizes : array()),
			$defaultImage
		);

		return $this->_getter;
	}

	/**
	 * Initialize the resizer
	 * 
	 * You can resize images by $_imageSizes descriptor
	 * 
	 * @return WX_Image_Resizer object
	 */
	public function initResizer() {
		$this->_validateInitResizerDescriptor();
		$this->_imageSizes = $this->_descriptor['sizes'];
		switch ($this->_mode) {
			case 'normal' :
				$file = $this->getFile();
				break;
			case 'hash' :
				$pattern = $this->_path.$this->_imageHash.'.*';
				$fileWithPath = WX_Directory_Reader::getFirstResultByPattern($pattern);
				$file = substr(strrchr($fileWithPath, "/"), 1);
				break;
		}
		$imageSizes = !empty($this->_imageSizes) ? $this->_imageSizes : array();
		$this->_resizer = new WX_Image_Resizer($file, $this->_path, $imageSizes);
		return $this->_resizer;
	}

	protected function _validateInitResizerDescriptor() {
		if (!isset($this->_descriptor['sizes'])) {
			throw new Exception('You must define "resizer" in the desciptor\'s "sizes" branch!');
		}
		if (!count($this->_descriptor['sizes'])) {
			throw new Exception('You must define at least 1 size in "sizes"');
		}
	}

	/**
	 * Initialize the copy
	 *
	 * You can copy the image to a target path
	 *
	 * @return WX_Image_Copy object
	 */
	public function initCopy() {
		switch ($this->_mode) {
			case 'normal' :
				$pattern = $this->_path.$this->getName();
				break;
			case 'hash' :
				$pattern = $this->_path.$this->_imageHash;
				break;
		}

		$this->_validateInitCopyDescriptor();
		$source = $this->_path;
		$target = $this->_descriptor['copy']['targetPath'];
		$imageSizes = !empty($this->_imageSizes) ? array_keys($this->_imageSizes) : array();
		$this->_copy = new WX_Image_Copy($target, $pattern, $imageSizes);
		return $this->_copy;
	}
	
	protected function _validateInitCopyDescriptor() {
		if (!isset($this->_descriptor['copy'])) {
			throw new Exception('You must define "copy" branch in the desciptor!');
		}
		if (!isset($this->_descriptor['copy']['targetPath'])) {
			throw new Exception('You must define "targetPath" in the desciptor\'s "copy" branch!');
		}
	}

	public function getUploader() {
		return $this->_uploader;
	}

	public function getDelete() {
		return $this->_delete;
	}

	public function getResizer() {
		return $this->_resizer;
	}

	public function getGetter() {
		return $this->_getter;
	}

	public function getCopy() {
		return $this->_copy;
	}

	public static function getFilePattern() {
		return self::$filePattern;
	}

	public static function getFilePatternSize() {
		return self::$filePatternSize;
	}

	public function getName() {
		return $this->_fileName;
	}

	public function getFile() {
		return $this->_fileName.'.'.$this->_fileExtension;
	}
}


?>