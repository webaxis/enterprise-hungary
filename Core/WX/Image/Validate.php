<?php

/**
 * Image validator class
 *
 * It can validate:
 * - maxFileSize
 * - allowedExtensions
 * - file quantity limit
 *
 * You can check errors with hasError() and get errors with getErrors
 */
class WX_Image_Validate {

	protected $_errors = array();

	/**
	 * -1 means: no limit
	 */
	protected $_maxFileSizeSettings = -1;

	/**
	 * -1 means: no restriction
	 */
	protected $_allowedExtensionsSettings =  -1;

	/**
	 * File quantity limit; default -1 means: no limit
	 */
	protected $_fileCountSettings = -1;

	/**
	 * Singleton instance
	 *
	 * @var object
	 */
	protected static $WX_Image_Validate;

	/**
	 * Create singleton instance
	 *
	 * @return object
	 */
	public static function getInstance($settings) {
		if (self::$WX_Image_Validate == null) {
			self::$WX_Image_Validate = new self($settings);
		}

		return self::$WX_Image_Validate;
	}

	public function __construct($settings) {
		if (isset($settings['size'])) {
			$this->_maxFileSizeSettings = $settings['size'];
		}
		if (isset($settings['extensions'])) {
			$this->_allowedExtensionsSettings = $settings['extensions'];
		}
		if (isset($settings['fileCount'])) {
			$this->_fileCountSettings = $settings['fileCount'];
		}
	}

	public function validateSize($size) {
		if ($this->_maxFileSizeSettings !== -1) {
			if ($size > $this->_maxFileSizeSettings['maxFileSize']) {
				$this->_errors[] = $this->_maxFileSizeSettings['errorMessage'];
			}
		}
	}

	public function validateExtension($extension) {
		if ($this->_allowedExtensionsSettings !== -1) {
			if (!in_array($extension, $this->_allowedExtensionsSettings['types'])) {
				$this->_errors[] = $this->_allowedExtensionsSettings['errorMessage'];
			}
		}
	}

	/**
	 * Count how many files - EXCLUDE resized images - available in the directory.
	 *
	 * @param string $path The path what we want to check in the following format: "/var/www/uploads/directory/"
	 *
	 * @return bool
	 */
	public function validateFileCount($path) {
		$pattern = $path.'*.*';
		if ($this->_fileCountSettings != -1) {
			$files = WX_Directory_Reader::getFilesByPattern($pattern);
			if (count($files)) {
				$count = 0;
				foreach ($files as $file) {
					//count only not sized images
					if (preg_match(WX_Image::getFilePattern(), $file)) {
						$count++;
					}
				}
				if ($count >= $this->_fileCountSettings['maxFileCount']) {
					$this->_errors[] = $this->_fileCountSettings['errorMessage'];
				}
			} else {
				return true;
			}
		}
		return true;
	}

	public function hasError() {
		if (count($this->_errors)) {
			return true;
		}
		return false;
	}

	public function getErrors() {
		return $this->_errors;
	}

}


?>