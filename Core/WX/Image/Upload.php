<?php

/**
 * Upload a form file to a target directory
 */
class WX_Image_Upload {

	protected $_name = '';

	protected $_extension = '';

	protected $_size = '';

	protected $_path = '';

	protected $_settings = array();

	/**
	 * File temp location; e.g: /tmp/php5Wx0aJ
	 */
	protected $_tmp = '';

	/**
	 * Validator object instance
	 */
	protected $_validator = null;

	/**
	 * Error container
	 */
	protected $_errors = array();

	/**
	 * Constructor.
	 *
	 * The parameter $fileArray is simple array (when you send only 
	 * one file via http). 
	 *
	 * It's look like this example:
	 *
	 *	<code>
	 *		'name' => 'file.png',
	 *		'type' => 'image/png',
	 *		'tmp_name' => '/tmp/php5Wx0aJ',
	 *		'error' => 0,
	 *		'size' => 15726
	 *	</code>
	 *
	 * @example http://php.net/manual/en/reserved.variables.files.php
	 *
	 * @param array $fileArray
	 * @param array $path Upload path
	 * @param array $settings It contains uploader and validator settings
	 */
	public function __construct($fileArray, $path, $settings, $imageHash = false) {
		$this->_path = $path;
		$this->_imageHash = $imageHash;
		$this->_settings = $settings;
		$this->_processFile($fileArray);
		$validationSettings = isset($settings['validation']) ? $settings['validation'] : array();
		$this->_validator = WX_Image_Validate::getInstance($validationSettings);
	}

	/**
	 * General file setting process
	 */
	protected function _processFile($fileArray) {
		$pathInfo = pathinfo($fileArray['name']);
		$this->_name = $fileArray['name'];
		$this->_extension = strtolower($pathInfo['extension']);
		$this->_size = filesize($fileArray['tmp_name']);
		$this->_tmp = $fileArray['tmp_name'];
	}

	/**
	 * Get the filename
	 *
	 * @return string
	 */
	public function getName() {
		return $this->_name;
	}

	/**
	 * Get the extension
	 *
	 * @return string
	 */
	public function getExtension() {
		return $this->_extension;
	}

	/**
	 * Upload file if valid
	 *
	 * @return bool
	 */
	public function upload() {
		if ($this->isValid() === false) {
			return false;
		}
		if ($this->_upload() === false) {
			return false;
		}
		return true;
	}

	/**
	 * Upload the file from the temp
	 *
	 * Create the directory also when it doesn't exists
	 */
	public function _upload() {
		if (!file_exists($this->_path)) {
			WX_Directory_Writer::getInstance()->makeDirectory($this->_path, 0777);
		}
		if ($this->_imageHash !== false) {
			$image = $this->_imageHash.'.'.$this->_extension;
		} else {
			$image = $this->_name.'.'.$this->_extension;
		}
		$this->_errors = array();

		if (!move_uploaded_file($this->_tmp, $this->_path.$image)) {
			$this->_errors[] = 'Hiba feltöltés közben, probáld újra';
			return false;
		}
		return true;
	}

	/**
	 * Validate criterias
	 *
	 * @return bool
	 */
	public function isValid() {
		$this->_validator->validateSize($this->_size);
		$this->_validator->validateExtension($this->_extension);
		$this->_validator->validateFileCount($this->_path);
		if ($this->_validator->hasError() === true) {
			$this->_errors = $this->_validator->getErrors();
			return false;
		}
		return true;
	}

	/**
	 * Check it has error
	 *
	 * @return bool
	 */
	public function hasError() {
		if (count($this->_errors)) {
			return true;
		}
		return false;
	}

	/**
	 * Get errors
	 *
	 * @return array
	 */
	public function getErrors() {
		return $this->_errors;
	}
}


?>