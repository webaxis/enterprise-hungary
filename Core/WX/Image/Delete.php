<?php

/**
 * This class delete a file or files by a pattern.
 */
class WX_Image_Delete {

	/**
	 * File with path BUT without extension; e.g /var/www/uploads/jellyfish
	 *
	 * @param string $pattern 
	 */
	protected $_pattern = '';

	/**
	 * Image sizes array
	 *
	 * Example:
	 *
	 * array('s', 'm', 'l' ....);
	 */
	protected $_imageSizes = array();

	/**
	 * @param string $pattern File with path BUT without extension; e.g /var/www/uploads/jellyfish
	 * @param array $imageSizes It contains the image sizes e.g array('s', 'm', 'l' ....);
	 */
	public function __construct($pattern, array $imageSizes = array()) {
		$this->_pattern = $pattern;
		$this->_imageSizes = $imageSizes;
	}

	public function delete() {
		$this->_delete();
	}

	public function deleteSize($size) {
		$this->_delete($size);
	}

	public function deleteAllSizes() {
		if (count($this->_imageSizes)) {
			foreach ($this->_imageSizes as $size) {
				$this->deleteSize($size);
			}
		}
	}

	public function deleteAll() {
		$this->deleteAllSizes();
		$this->delete();
	}

	public function deleteAllDirectoryAlso() {
		$this->deleteAll();
		$this->deleteDirectory();
	}

	protected function _delete($size = false) {
		if ($size === false) {
			$pattern = $this->_pattern.'.*';
		} else {
			if (in_array($size, $this->_imageSizes)) {
				$pattern = $this->_pattern.'_'.$size.'.*';
			} else {
				$message = 'The following size is not exists in $_imageSizes: '.$size;
				throw new Exception($message);
			}
		}
		$file = WX_Directory_Reader::getFirstResultByPattern($pattern);
		if ($file === false) {
			$message = 'IMAGE DELETE FAILED: This file not exists: "'.$file.'"';
			error_log($message, 0);
		} else {
			unlink($file);
		}
	}

	public function deleteDirectory() {
		# @TODO: ez nem a legszebb megoldas...
		$position = strripos($this->_pattern, '/');
		$directory = substr($this->_pattern, 0, $position);

		WX_Directory_Writer::getInstance()->deleteDirectory($directory);
	}

	public function emptyDirectory() {
		# @TODO: ez nem a legszebb megoldas...
		$position = strripos($this->_pattern, '/');
		$directory = substr($this->_pattern, 0, $position);

		WX_Directory_Writer::getInstance()->emptyDirectory($directory);
	}
}


?>