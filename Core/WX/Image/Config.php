<?php

class WX_Image_Config {

	protected static $_instances = array();

	protected $_path = UPLOAD_PATH;	

	protected $_url = UPLOAD_URL;

	protected $_maxFileSize = 5242880;

	protected $_uploadError = 'Hiba történt fa feltöltés közben, kérjük próbálja meg újból.';

	protected $_uploadErrorSize = 'A maximális fájlméret: 5 MB.';

	protected $_uploadErrorExtension = 'Engedélyezett kiterjesztések: ';

	protected $_validExtensions = array("jpg", "png", "gif", "jpeg");

	public static function getInstance() {
        $calledClass = get_called_class();
        if (!isset(self::$_instances[$calledClass])) {
            self::$_instances[$calledClass] = new $calledClass();
        }

        return self::$_instances[$calledClass];
	}

	public function get($property) {
		$property = '_'.$property;
		switch ($property) {
			case '_tempUrl' :
				return $this->_url . 'temp/';
				break;
			case '_tempPath' :
				return $this->_path . 'temp/';
				break;
			case '_uploadErrorExtension' :
				return $this->_uploadErrorExtension . implode(', ', $this->_validExtensions);
				break;
			case '_sizes' :
				if (property_exists($this, $property)) {
					return $this->$property;
				}
				return array();
				break;
			default :
				if (!property_exists($this, $property)) {
					throw new Exception('This property is not exists: '.$property);
				}
				return $this->$property;
				break;
		}
	}
}

?>