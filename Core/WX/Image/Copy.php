<?php

/**
 * Copy one file or files to a target directory
 */
class WX_Image_Copy {

	/**
	 * Image sizes array
	 *
	 * Example:
	 *
	 * array('s', 'm', 'l' ....);
	 */
	protected $_imageSizes = array();

	public function __construct($target, $pattern, $imageSizes = array()) {
		$this->_target = $target;
		$this->_pattern = $pattern;
		$this->_imageSizes = $imageSizes;
	}

	public function copy () {
		$this->_copy();
	}

	public function copySize ($size) {
		$this->_copy($size);
	}

	public function copyAllSizes () {
		foreach ($this->_imageSizes as $size) {
			$this->copySize($size);
		}
	}

	public function copyAll () {
		$this->copy();
		$this->copyAllSizes();
	}

	protected function _copy ($size = false) {
		if ($size === false) {
			$pattern = $this->_pattern.'*.*';
		} else {
			if (in_array($size, $this->_imageSizes)) {
				$pattern = $this->_pattern.'_'.$size.'.*';
			} else {
				$message = 'The following size is not exists in $_imageSizes: '.$size;
				throw new Exception($message);
			}
		}
		$file = WX_Directory_Reader::getFirstResultByPattern($pattern);
		if ($file === false) {
			$message = 'IMAGE COPY FAILED: This file is not exists: "'.$file.'"';
			error_log($message, 0);
		} else {
			$filename = substr(strrchr($file, "/"), 1);
			WX_Directory_Writer::getInstance()->makeDirectoryIfNotExists($this->_target);
			$target = $this->_target.$filename;
			copy($file, $target);
		}
	}
}


?>