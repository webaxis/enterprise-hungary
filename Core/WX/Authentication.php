<?php

class WX_Authentication {

	const DEFAULT_NAMESPACE 		= 'user';
	const DEFAULT_EXPIRATION_TIME 	= 86400;

	private $_namespace				= false;
	private $_adapter				= false;
	private $_authenicated			= false;

	public function __construct ($namespace = self::DEFAULT_NAMESPACE, $expire = self::DEFAULT_EXPIRATION_TIME) {
		$this->_namespace = $namespace;
		
		$session = new Zend_Session_Namespace($this->_namespace);
        $session->setExpirationSeconds($expire);
	}

	static public function getInstance ($namespace = self::DEFAULT_NAMESPACE, $expire = self::DEFAULT_EXPIRATION_TIME) {
		return new WX_Authentication($namespace, $expire);
	}

	public function setAdapter ($params) {
		$db = Zend_Registry::get('db');

		$adapter = new Zend_Auth_Adapter_DbTable($db);
        $adapter->setTableName($params['table']);
        $adapter->setIdentityColumn($params['identity']);
        $adapter->setCredentialColumn($params['credential']);

		$this->_adapter = $adapter;
	}

	public function setLoginDetails ($params, $hashPassword = false) {
		if (!$this->_adapter)
			throw new Exception('Adapter is not set yet.');

		$this->_adapter->setIdentity($params['identity']);
		
		if ($hashPassword) {
			$this->_adapter->setCredential($params['credential']);
		} elseif (isset($params['salt']) && !empty($params['salt'])) {
			$this->_adapter->setCredential(md5(APP_KEY . $params['credential']));
		} else {
			$this->_adapter->setCredential(md5($params['credential']));
		}
	}

	public function authenticate() {
		if (!$this->_adapter)
			throw new Exception('Adapter is not set yet.');

		$auth		= $this->_getAuthInstance();
		$result 	= $auth->authenticate($this->_adapter);

		if (!$result->isValid()) {
			$this->_authenicated = false;
		} else {
			$this->_authenicated = true;
		}
		
		return $this->_authenicated;
	}

	public function login () {
		if (!$this->_authenicated) {
			return false;
		}

		$auth		= $this->_getAuthInstance();
		$rowData 	= (array) $this->_adapter->getResultRowObject();

		$auth->getStorage()->write($rowData['id']);

		return true;
	}

	public function logout () {
		$auth		= $this->_getAuthInstance();
		$auth->clearIdentity();
	}

	public function isLoggedIn () {
		$auth		= $this->_getAuthInstance();
		$identity  	= $auth->getIdentity();
		
		if (!empty($identity))
			return true;

		return false;
	}

	public function getIdentity () {
		$auth		= $this->_getAuthInstance();
		$identity  	= $auth->getIdentity();

		if (!empty($identity))
			return $identity;

		return false;
	}

	public function setNamespace ($namespace) {
		$this->_namespace = $namespace;
	}

	private function _getAuthInstance () {
		$auth 		= Zend_Auth::getInstance();
		$auth->setStorage(new Zend_Auth_Storage_Session($this->_getNamespace()));

		return $auth;
	}

	private function _getNamespace () {
		return $this->_namespace;
	}

}