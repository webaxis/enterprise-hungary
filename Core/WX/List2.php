<?php

class WX_List2 extends WX_List2_Abstract {

	public $type = 'list';

	public $id = null;

	public $permission = null;

	public $ownerField = null;

	public $descriptor = null;

	public $init = true;

	public $page = 1;

	public $template = 'table.phtml'; // default view

	public $fields = null;

	public $buttons = null;

	public $buttonsLabel = '&nbsp;';

	public $table = null;

	public $source = null;

	public $sourceAdapter = null;

	public $prefilter = null;

	public $paginator = null;

	public $autoload = false;

	public $limit = 10; // default limit

	public $order = 'DESC'; // default order

	public $orderBy = null; // default order by

	public $refresh = false;

	public $convert = null;

	public $instantiate = null;

	# name of the descriptor
	public function __construct ($descriptor, $page = 1, $params = null) {
		if ($this->descriptor === null) {
			$descriptorClass = APP_NAME . '_List2_Descriptor_' . ucfirst($descriptor);
			$descriptorObj = new $descriptorClass;
			$this->descriptor = $descriptor = $descriptorObj->descriptor();
		}

		if (empty($page)) {
			$page = 1;
		}
		$this->page = $page;
		if (isset($descriptor['id'])) {
			$this->id = $descriptor['id'];
		}

		if (isset($descriptor['permission'])) {
			$this->permission = $descriptor['permission'];
		}

		if (isset($descriptor['ownerField'])) {
			$this->ownerField = $descriptor['ownerField'];
		}

		if (isset($descriptor['init'])) {
			$this->init = $descriptor['init'];
		}

		if (isset($descriptor['template'])) {
			$this->template = $descriptor['template'];
		}

		if (isset($descriptor['fields'])) {
			$this->fields = $descriptor['fields'];
		}

		if (isset($descriptor['buttons'])) {
			$this->buttons = $descriptor['buttons'];
		}

		if (isset($descriptor['buttonsLabel'])) {
			$this->buttonsLabel = $descriptor['buttonsLabel'];
		}

		if (isset($descriptor['table'])) {
			$this->table = $descriptor['table'];
		}

		if (isset($descriptor['source'])) {
			$this->source = $descriptor['source'];
		}

		if (!isset($descriptor['source']) && isset($descriptor['table'])) {
			$this->source = $descriptor['table'];
		}

		if (isset($descriptor['prefilter'])) {
			$this->prefilter = $descriptor['prefilter'];
		}

		if (isset($descriptor['paginator'])) {
			$this->paginator = $descriptor['paginator'];
		}

		if (isset($descriptor['autoload'])) {
			$this->autoload = $descriptor['autoload'];
		}

		if (isset($descriptor['limit'])) {
			$this->limit = $descriptor['limit'];
		}

		if (isset($descriptor['order'])) {
			$this->order = $descriptor['order'];
		}

		if (isset($descriptor['orderBy'])) {
			$this->orderBy = $descriptor['orderBy'];
		}

		if (isset($descriptor['convert'])) {
			$this->convert = $descriptor['convert'];
		}

		if (isset($descriptor['instantiate'])) {
			$this->instantiate = $descriptor['instantiate'];
		}

		if (isset($params['limit'])) {
			$this->limit = $params['limit'];
		}

		if (isset($params['order'])) {
			$this->order = $params['order'];
		}

		if (isset($params['orderBy'])) {
			$this->orderBy = $params['orderBy'];
		}

		parent::__construct($descriptor);
	}

	public function refresh () {
		$this->refresh = true;
	}

	public function getPaginator ($a = true) {
		if (!isset($this->paginator['type'])) {
			$this->paginator = [
				'type' => 'pages'
			];
		}
		
		$options = array(
			'limit' => $this->limit,
			'total' => $this->getSourceAdapter()->getTotalCount(),
			'page' => $this->page,
			'displayPages' => 3,
			'autoload' => $this->getAutoload(),
			'init' => $this->getInit(),
			'listId' => $this->getId(),
		);

		switch ($this->paginator['type']) {
			case 'more';
				$options['refresh'] = $this->refresh;

				$paginator = new WX_List2_Paginator_More($this->paginator, $options);
				break;

			case 'pages';
				$paginator = new WX_List2_Paginator_Pages($this->paginator, $options);
				break;
		}
		
		return $paginator;
	}

	public function getPaginatorType () {
		$this->getPaginator(false);

		return $this->paginator['type'];
	}

	public function getLabels () {
		return $this->getFieldInstances();
	}

	public function getRows () {
		$rows = array();

		$data = $this->getData();

		if ($this->getConvert() !== null) {
			$data = call_user_func($this->convert['converter'], $data);
		}

		foreach ($data as $key => $row) {
			$rows[$key] = $this->getFieldInstances($row);
		}

		return $rows;
	}

	# Row is the row filled with data
	public function getFieldInstance ($field, $row = null) {
		if (!isset($field['type'])) {
			$field['type'] = false;
		}

		$field['order'] = $this->order;
		$field['orderBy'] = $this->orderBy;
		$field['table'] = $this->table;

		$return = null;

		$value = $row !== null && isset($row[$field['name']]) ? $row[$field['name']] : null;

		switch ($field['type']) {
			case 'image':
				$return = new WX_List2_Field_Image($field, $value, $row);
				break;

			case 'order':
				$return = new WX_List2_Field_Order($field, $value, $row);
				break;

			default:
				$return = new WX_List2_Field_Td($field, $value, $row);
		}

		return $return;
	}

	public function getFieldInstances ($row = null) {
		$instances = array();

		foreach ($this->fields as $field) {
			$instances[] = $this->getFieldInstance($field, $row);
		}
		return $instances;
	}

	public function getData () {
		if ($this->isConvertBeforeOrder() && !$this->getSourceAdapter() instanceof WX_List2_Source_Array) {
			$source	= clone $this->getSourceAdapter(true, true);

			$this->source = call_user_func($this->convert['converter'], $source->getData());

			$source	= clone $this->getSourceAdapter(false, true);
		} else {
			$source	= clone $this->getSourceAdapter();
		}

		$source->setLimit($this->limit);
		$source->setPage($this->page);
 
		$data = $source->getData();
		
		if ($this->getInstantiate()) {
			$data = $this->convertToInstantiate($data);
		}

		return $data;
	}

	public function convertToInstantiate ($data) {
		$instantiate = $this->getInstantiate();
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = new $instantiate($one['id'], $one);
		}

		return $return;
	}

	public function isConvertBeforeOrder () {
		if ($this->convert !== null && isset($this->convert['convertBeforeOrder'])) {
			return $this->convert['convertBeforeOrder'];
		}

		return false;
	}

	public function getId () {
		return $this->id;
	}

	public function getPermission () {
		return $this->permission;
	}

	public function isPermitted ($recordId = false) {
		if ($this->getPermission() === null) {
			return true;
		}

		$class = APP_NAME . '_' . ucfirst($this->getPermission());

		if ($class::getInstance()->isLoggedIn()) {
			if ($this->getOwnerField() !== null && $recordId) {
				$db = Zend_Registry::get('db');
				$query = $db->select()
					->from($this->getTable())
					->where('id = "' . $recordId . '"');
				$results = $db->query($query)->fetchAll();
				if (isset($results[0]) && $results[0][$this->getOwnerField()] == $class::getInstance()->getId()) {
					return true;
				}

				return false;
			} else {
				return true;
			}
		}

		return false;
	}

	public function getOwnerField () {
		return $this->ownerField;
	}

	public function getInit () {
		return $this->init;
	}

	public function getPage () {
		return $this->page;
	}

	public function getFields () {
		return $this->fields;
	}

	public function getField ($name) {
		foreach ($this->fields as $field) {
			if ($field['name'] == $name) {
				return $field;
			}
		}
	}

	public function getFieldByType ($type) {
		foreach ($this->fields as $field) {
			if (isset($field['type']) && $field['type'] == $type) {
				return $field;
			}
		}
	}

	# $index = index of current row
	public function getButtons ($index = null) {

		$data = $this->getData();

		$buttons = array();

		foreach ($this->buttons as $button) {
			if (!isset($button['type'])) {
				$button['type'] = null;
			}

			$button['listId'] = $this->id;
			$button['table'] = $this->table;

			switch ($button['type']) {
				case 'delete':
					$instance = new WX_List2_Button_Delete($button, $data, $index);
					break;

				default:
					$instance = new WX_List2_Button_Abstract($button, $data, $index);
			}

			$buttons[] = $instance->fetch();
		}

		$buttons = new WX_List2_Field_Buttons(null, implode('&nbsp;',$buttons));

		return $buttons;
	}

	public function hasButtons () {
		if ($this->buttons === null) {
			return false;
		}
		
		return true;
	}

	public function getButtonsLabel () {
		$label = $this->buttonsLabel;

		return new WX_List2_Field_Th(array('label' => $label, 'orderDisabled' => true));
	}

	public function getTable () {
		return $this->table;
	}

	public function getSource () {
		return $this->source;
	}

	public function getSourceAdapter ($disableOrder = false, $forceRefresh = false) {
		if ($this->sourceAdapter === null || $forceRefresh) {
			$options = array();

			if (!$disableOrder) {
				$options['order'] = $this->order;
				$options['orderBy'] = $this->orderBy;
			}

			if (is_string($this->source)) {
				$sourceAdapter = new WX_List2_Source_Db($this->source, $options);
			} elseif (is_array($this->source)) {
				$sourceAdapter = new WX_List2_Source_Array($this->source, $options);
			} elseif ($this->source instanceof Zend_Db_Select) {
				$sourceAdapter = new WX_List2_Source_ZendQuery($this->source, $options);
			}
			$sourceAdapter->prefilter($this->prefilter);

			if ($this->refresh) {
				$sourceAdapter->fromZero();
				$sourceAdapter->refresh();
			}

			$this->sourceAdapter = $sourceAdapter;
		}

		return $this->sourceAdapter;
	}

	public function getAutoload () {
		return $this->autoload;
	}

	public function getLimit () {
		return $this->limit;
	}

	public function getConvert () {
		return $this->convert;
	}

	public function getInstantiate () {
		return $this->instantiate;
	}
}