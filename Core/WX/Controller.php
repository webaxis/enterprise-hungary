<?php

class WX_Controller extends Zend_Controller_Action {

	public function init () {
		$r = $this->_request;
		$this->view->controller = $r->getControllerName();
		$this->view->action = $r->getActionName();
	}

	public function wxlistAction () {
		$this->_helper->viewRenderer->setNoRender(true);
		$rp = $this->_request->getParams();
		$descriptor = $rp['descriptor'];
		
		$method = $descriptor;

		$config = Zend_Registry::get('config')->list;

		$path = $config->descriptorDirectory;
		$pathParts = explode('/',str_replace(trim("\ "), "/", $path));

		$className = '';
		foreach ($pathParts as $part) {
			if ($part == APP_NAME)
				$className = APP_NAME;
			else
				$className .= '_' . $part;
		}
		$className .= ucfirst($method);
		
		die(call_user_func(array($className,'get'),$rp)->render());
	}

}