<?php

function t ($txt, $params = array()) {
	$args = func_get_args();
	$result = call_user_func_array(array('WX_Translator','t'), $args);

	# @todo: ez anno azert volt benne, hogy jelezze, ha valami nincs leforditva
	if (substr_count($result, '!!!') == 2) {
		return $txt;
	}

	return $result;
}

function D($v) {
	print '<pre>';
	var_dump($v);
	print '</pre>';
}

function DX($v) {
	print '<pre>';
	var_dump($v);
	print '</pre>';
	die;
}
