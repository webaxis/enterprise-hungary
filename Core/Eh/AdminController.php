<?php

class Eh_AdminController extends WX_Controller_CMS {

	public function init () {
		parent::init();

		if (!Eh_Admin::getInstance()->isLoggedIn()) {
			$this->_redirect(Eh_Url::adminIndex());
		}
	}
}