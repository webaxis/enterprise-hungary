<?php

class Eh_Converter_Admin {

	static public function toDb ($data) {
		return array(
			'name' => $data['name'],
			'login' => $data['login'],
			'password' => !empty($data['password']) ? md5($data['password']) : false
		); 
	}

	static public function toForm ($data) {
		return array(
			'name' => $data['name'],
			'login' => $data['login']
		);
	}
}