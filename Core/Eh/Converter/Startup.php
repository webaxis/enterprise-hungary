<?php

class Eh_Converter_Startup {

	static public function adminList ($data) {
		$return = array();

		$order = new Eh_Order('startup');

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['logo'] = WX_Image::getInstance('startuplogo')->get('adminList', $one['logo']);
			$return[$key]['order'] = $order->setSearchParams(array('batch'=>$one['batch']))->createSelect($one['id'], $one['order']);
			$return[$key]['batch'] = Eh_Static::getBatch($one['batch']);
		}

		return $return;
	}

	static public function toForm($data) {
		if (!empty($data['images'])) {
			$images = json_decode($data['images'], true);
			$result = array();
			foreach ($images as $hash => $image) {
				$result[$hash] = array(
					'url'	=> WX_Image::getInstance('startupimages')->get('normal', $hash)
				);
			}

			$data['images'] = json_encode($result);
		}

		return $data;
	}

	static public function toList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$logo = WX_Image::getInstance('startuplogo')->get('normal', $one['logo']);

			$return[$one['batch']][$key] = $one;
			$return[$one['batch']][$key]['logo'] = strstr($logo, 'default') !== false ? null : $logo;
			$return[$one['batch']][$key]['link'] = Eh_Url::startup($one['id'], $one['name']);
		}

		return $return;
	}

	static public function toProfile ($one) {
		$return = array();

		$logo = WX_Image::getInstance('startuplogo')->get('normal', $one['logo']);
		
		$return = $one;
		$return['logo'] = strstr($logo, 'default') !== false ? null : $logo;

		if (!empty($one['images'])) {
			$images = json_decode($one['images'], true);
			$result = array();
			foreach ($images as $hash => $image) {
				$result[] = [
					'normal' => WX_Image::getInstance('startupimages')->get('normal', $hash),
					'thumbnail' => WX_Image::getInstance('startupimages')->get('thumbnail', $hash)
				];
			}

			$return['images'] = $result;
		}

		return $return;
	}
}