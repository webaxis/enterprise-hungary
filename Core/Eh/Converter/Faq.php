<?php

class Eh_Converter_Faq {

	static public function adminList ($data) {
		$return = array();

		$order = new Eh_Order('faq');

		foreach ($data as $one) {
			$return[] = array(
				'id'		=> $one['id'],
				'order'		=> $order->createSelect($one['id'], $one['order']),
				'question'	=> $one['question']
			);
		}

		return $return;
	}

}