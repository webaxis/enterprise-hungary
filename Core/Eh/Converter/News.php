<?php

class Eh_Converter_News {

	static public function adminList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['site'] = Eh_Static::getSite($one['site']);
			$return[$key]['lang'] = Eh_Static::getLanguage($one['lang']);
			$return[$key]['image'] = WX_Image::getInstance('news')->get('adminList', $one['image']);
		}

		return $return;
	}

	static public function toList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['image'] = WX_Image::getInstance('news')->get('list', $one['image']);
			$return[$key]['url'] = Eh_Url::news($one['id'], $one['title']);
		}

		return $return;
	}

	static public function toForm($data) {
		if (!empty($data['gallery'])) {
			$images = json_decode($data['gallery'], true);
			$result = array();
			foreach ($images as $hash => $image) {
				$result[$hash] = array(
					'url'	=> WX_Image::getInstance('newsgallery')->get('normal', $hash)
				);
			}

			$data['gallery'] = json_encode($result);
		}

		return $data;
	}

	static public function toProfile ($one) {
		$return = array();

		$image = WX_Image::getInstance('news')->get('normal', $one['image']);

		$return = $one;
		$return['image'] = strstr($image, 'default') !== false ? false : $image;
		$return['lead'] = nl2br($one['lead']);
		$return['date'] = date('Y', strtotime($one['date'])) . ' ' . Eh_Static::getMonth(date('n', strtotime($one['date']))) . ' ' . date('d', strtotime($one['date']));

		if (!empty($one['pressrelease'])) {
			$release = json_decode($one['pressrelease'], true);
			$releaseUrl = (!empty($one['pressrelease']) ? UPLOAD_TEMP_URL . 'pressrelease/' . $release['hash'] . '.' . $release['ext'] : (!empty($one['releaseUrl']) ? $one['releaseUrl'] : false));

			$return['pressrelease'] = $releaseUrl;
		}

		if (!empty($one['gallery'])) {
			$images = json_decode($one['gallery'], true);
			$result = array();
			foreach ($images as $hash => $image) {
				$result[] = [
					'normal' => WX_Image::getInstance('newsgallery')->get('normal', $hash),
					'thumbnail' => WX_Image::getInstance('newsgallery')->get('thumbnail', $hash)
				];
			}

			$return['gallery'] = $result;
		}

		return $return;
	}
}