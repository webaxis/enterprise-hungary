<?php

class Eh_Converter_Slide {

	static public function adminList ($data) {
		$return = array();

		$order = new Eh_Order('slide');

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['site'] = Eh_Static::getSite($one['site']);
			$return[$key]['lang'] = Eh_Static::getLanguage($one['lang']);
			$return[$key]['image'] = WX_Image::getInstance('slide')->get('adminList', $one['image']);
			$return[$key]['order'] = $order->setSearchParams(array('site'=>$one['site']))->createSelect($one['id'], $one['order']);
		}

		return $return;
	}

	static public function toList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['image'] = WX_Image::getInstance('slide')->get('normal', $one['image']);
			$return[$key]['link'] = strstr($one['link'],'http') !== false || empty($one['link']) ? $one['link'] : 'http://' . $one['link'];
			$return[$key]['buttonlink'] = strstr($one['buttonlink'],'http') !== false || empty($one['buttonlink']) ? $one['buttonlink'] : 'http://' . $one['buttonlink'];
		}

		return $return;
	}
}