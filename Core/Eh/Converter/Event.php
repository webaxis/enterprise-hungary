<?php

class Eh_Converter_Event {

	static public function adminList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['site'] = Eh_Static::getSite($one['site']);
			$return[$key]['lang'] = Eh_Static::getLanguage($one['lang']);
			$return[$key]['image'] = WX_Image::getInstance('event')->get('adminList', $one['image']);
		}

		return $return;
	}

	static public function toList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['image'] = WX_Image::getInstance('event')->get('normal', $one['image']);
			$return[$key]['month'] = Eh_Static::getMonth3(date('n',strtotime($one['date'])));
			$return[$key]['day'] = date('d',strtotime($one['date']));
			$return[$key]['link'] = strstr($one['link'],'http') !== false ? $one['link'] : 'http://' . $one['link'];
		}

		return $return;
	}
}