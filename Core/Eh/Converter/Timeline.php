<?php

class Eh_Converter_Timeline {

	static public function adminList ($data) {
		$return = array();

		$order = new Eh_Order('timeline');

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['order'] = $order->createSelect($one['id'], $one['order']);
		}

		return $return;
	}

	static public function toList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['month'] = @explode(' ', $one['label'])[0];
			$return[$key]['day'] = @explode(' ', $one['label'])[1];
		}

		return $return;
	}
}