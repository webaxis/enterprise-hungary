<?php

class Eh_Converter_Partner {

	static public function adminList ($data) {
		$return = array();

		$order = new Eh_Order('partner');

		foreach ($data as $key => $one) {
			$return[] = array(
				'id'		=> $one['id'],
				'logo'		=> WX_Image::getInstance('partner')->get('adminList', $one['logo']),
				'section'	=> @Eh_Static::getPartnersection($one['section']),
				'order'		=> $order->createSelect($one['id'], $one['order']),
			);
		}

		return $return;
	}

	static public function toList ($data) {
		$return = array();

		foreach (Eh_Static::getPartnersections() as $skey => $section) {
			$return[$skey] = array(
				'title' => $section,
				'partners' => array()
			);

			foreach ($data as $key => $one) {
				if ($one['section'] == $skey) {
					$return[$skey]['partners'][$key] = $one;
					$return[$skey]['partners'][$key]['logo'] = WX_Image::getInstance('partner')->get('normal', $one['logo']);
					$return[$skey]['partners'][$key]['link'] = strstr($one['link'],'http') !== false || empty($one['link']) ? $one['link'] : 'http://' . $one['link'];
				}
			}	
		}

		return $return;
	}
	
}