<?php

class Eh_Converter_Organization {

	static public function adminList ($data) {
		$return = array();

		$order = new Eh_Order('organization');

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['logo'] = WX_Image::getInstance('organization')->get('adminList', $one['logo']);
			$return[$key]['order'] = $order->createSelect($one['id'], $one['order']);
		}

		return $return;
	}

	static public function toList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['logo'] = WX_Image::getInstance('organization')->get('normal', $one['logo']);
			$return[$key]['link'] = strstr($one['link'],'http') !== false || empty($one['link']) ? $one['link'] : 'http://' . $one['link'];
		}

		return $return;
	}
}