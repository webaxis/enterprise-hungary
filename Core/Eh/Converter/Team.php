<?php

class Eh_Converter_Team {

	static public function adminList ($data) {
		$return = array();

		$order = new Eh_Order('team');

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['image'] = WX_Image::getInstance('team')->get('adminList', $one['image']);
			$return[$key]['order'] = $order->createSelect($one['id'], $one['order']);
		}

		return $return;
	}

	static public function toList ($data) {
		$return = array();

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['image'] = WX_Image::getInstance('team')->get('normal', $one['image']);
		}

		return $return;
	}
}