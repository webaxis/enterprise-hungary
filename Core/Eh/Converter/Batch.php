<?php

class Eh_Converter_Batch {

	static public function adminList ($data) {
		$return = array();

		$order = new Eh_Order('batch');

		foreach ($data as $key => $one) {
			$return[$key] = $one;
			$return[$key]['order'] = $order->createSelect($one['id'], $one['order']);
		}

		return $return;
	}
}