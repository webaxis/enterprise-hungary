<?php

class Eh_Order extends WX_Order {

	protected $_descriptor	= array(
		'batch' => array(
			'table'			=> 'batch',
			'orderColumn'	=> 'order'
		),
		'organization' => array(
			'table'			=> 'organization',
			'orderColumn'	=> 'order'
		),
		'faq' => array(
			'table'			=> 'faq',
			'orderColumn'	=> 'order'
		),
		'partner' => array(
			'table'			=> 'partner',
			'orderColumn'	=> 'order',
			'searchParams'	=> ['section']
		),
		'slide' => array(
			'table'			=> 'slide',
			'orderColumn'	=> 'order',
			'searchParams'  => ['site']
		),
		'timeline' => array(
			'table'			=> 'timeline',
			'orderColumn'	=> 'order'
		),
		'team' => array(
			'table'			=> 'team',
			'orderColumn'	=> 'order'
		),
		'startup' => array(
			'table'			=> 'startup',
			'orderColumn'	=> 'order',
			'searchParams'  => ['batch']
		),
	);

}