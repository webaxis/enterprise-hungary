<?php

# a $_localizationContent a kapcsolat a localization es a contentLanguage kozott

class Eh_ContentLanguage {

	static protected $_contentLanguageInstances = null;

	protected $_sessionNamespace = 'contentLanguage';

	protected $_defaultLanguageId = 0;

	protected $_languages = array(
		0 => 'mindegy',
		'hu' => 'magyar',
		'en' => 'angol'
	);

	// assign localization to contentLanguage
	protected $_localizationContent = array(
		'hu' => 'hu',
		'en' => 'en',
	);

	final public static function getInstance() {
		$calledClass = get_called_class();

		if (!isset(self::$_contentLanguageInstances[$calledClass])) {
			self::$_contentLanguageInstances[$calledClass] = new $calledClass();
		}

		return self::$_contentLanguageInstances[$calledClass];
	}

	public function __construct() {
		$session = new Zend_Session_Namespace($this->_sessionNamespace);
		if (!$session->contentLanguage == null) {
			Zend_Registry::set($this->_sessionNamespace, $session->contentLanguage);
		}
	}

	public function setByLocale($locale) {
		if (!array_key_exists($locale, $this->_localizationContent)) {
			$message = '"' . $id . '" key not exists in $_localizationContent array!';
			throw new Exception($message);
		}

		$id = $this->_localizationContent[$locale];
		$session = new Zend_Session_Namespace($this->_sessionNamespace);
		$session->contentLanguage = $id;
		Zend_Registry::set($this->_sessionNamespace, $id);
	}

	public function setLanguage($id) {
		if (!array_key_exists($id, $this->_languages)) {
			$message = '"' . $id . '" key not exists in $_langauges array!';
			throw new Exception($message);
		}
		$session = new Zend_Session_Namespace($this->_sessionNamespace);
		$session->contentLanguage = $id;
		
		Zend_Registry::set($this->_sessionNamespace, $id);
	}

	public function getLanguage() {
		if (Zend_Registry::isRegistered($this->_sessionNamespace)) {
			return Zend_Registry::get($this->_sessionNamespace);
		}
	}

	public function getLanguageById($id) {
		$language = WX_Tools::getValue($this->_languages, $id);
		if ($language == false) {
			$message = 'Unknown language ID: "' . $id . '"';
			throw new Exception($message);
		}

		return $language;
	}

	public function isSelected($id) {
		if (Zend_Registry::isRegistered($this->_sessionNamespace)) {
			$storedId = Zend_Registry::get($this->_sessionNamespace);
		} else {
			$storedId = $this->_defaultLanguageId;
		}

		if ($id === $storedId) {
			return true;
		}

		return false;
	}
}

?>