<?php

class Eh_Admin_ContentLanguage extends Eh_ContentLanguage {

	protected $_sessionNamespace = 'adminContentLanguage';

	public function getLanguageOptionItems() {
		$languages = [];
		foreach ($this->_languages as $id => $language) {
			if ($id != 0) { # skip "mindegy"
				$languages[] = [
					'id' 	=> $id, 
					'name'	=> $language
				];
			}
		}

		return $languages;
	}

}

?>