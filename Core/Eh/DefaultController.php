<?php

class Eh_DefaultController extends Eh_Controller {

	public function init () {
		//$this->view->partners = Eh_Converter_Partner::all(Eh_Model_Partner::getAll(['order' => ['order ASC']]));

		$this->view->controller = $this->_request->getControllerName();
		$this->view->requestParams = $this->_request->getParams();

		if (PROJECT_NAME == 'sc') {
			$this->view->partners = Eh_Converter_Partner::toList(Eh_Model_Partner::getInstance()->fetchListToArray('footer = 1', 'order ASC'));
		}

		parent::init();
	}

	public function regenerateogimgAction () {
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$site = PROJECT_NAME == 'eh' ? 1 : 2;

		$slide = WX_Image::getInstance('slide')->get('og', Eh_Model_Slide::getInstance()->fetchListToArray('site = ' . $site . ' || site = 3', 'order ASC', 1)[0]['image']);
		$logo = IMAGE_URL . 'og-logo-' . PROJECT_NAME . '.png';
		
		$png = imagecreatefrompng($logo);
		$jpeg = imagecreatefromjpeg($slide);

		list($slidewidth, $slideheight) = getimagesize($slide);
		list($logowidth, $logoheight) = getimagesize($logo);

		$logoLeft = ($slidewidth - $logowidth) / 2;
		$logoTop = ($slideheight - $logoheight) / 2;

		$out = imagecreatetruecolor($slidewidth, $slideheight);
		imagecopyresampled($out, $jpeg, 0, 0, 0, 0, $slidewidth, $slideheight, $slidewidth, $slideheight);
		imagecopyresampled($out, $png, $logoLeft, $logoTop, 0, 0, $logowidth, $logoheight, $logowidth, $logoheight);
		imagejpeg($out, IMAGE_PATH . 'og-img-' . PROJECT_NAME . '.jpg', 90);

		DX('<img src="' . IMAGE_URL . 'og-img-' . PROJECT_NAME . '.jpg">');
	}

}

?>