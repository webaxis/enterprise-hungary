<?php

class Eh_Image_Descriptor {

	static public function getDefaultImageUrl($descriptor) {
		$result = call_user_func_array(array('self', $descriptor), array());

		return $result['defaultImage']['url'];
	}

	static public function organization() {
		$descriptor = array(
			'folder' => 'organization',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'auto',
					'width' => 164,
					'height' => 164
				),
				'adminList' => array(
					'method' => 'auto',
					'width' => 100,
					'height' => 100
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default.png'
			)
		);

		return $descriptor;
	}

	static public function event() {
		$descriptor = array(
			'folder' => 'event',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'auto',
					'width' => 345,
					'height' => 240
				),
				'adminList' => array(
					'method' => 'auto',
					'width' => 100,
					'height' => 70
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default-event-'.PROJECT_NAME.'.png'
			)
		);

		return $descriptor;
	}

	static public function news() {
		$descriptor = array(
			'folder' => 'news',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'auto',
					'width' => 1400,
					'height' => 580
				),
				'list' => array(
					'method' => 'auto',
					'width' => 328,
					'height' => 247
				),
				'adminList' => array(
					'method' => 'auto',
					'width' => 100,
					'height' => 42
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default-news-'.PROJECT_NAME.'.png'
			)
		);

		return $descriptor;
	}

	static public function newsgallery() {
		$descriptor = array(
			'folder' => 'newsgallery',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'auto',
					'width' => 1024,
					'height' => 768
				),
				'thumbnail' => array(
					'method' => 'crop',
					'width' => 300,
					'height' => 225
				),
				'adminList' => array(
					'method' => 'auto',
					'width' => 100,
					'height' => 75
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default.png'
			)
		);

		return $descriptor;
	}

	static public function partner() {
		$descriptor = array(
			'folder' => 'partner',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'auto',
					'width' => 200,
					'height' => 200
				),
				'adminList' => array(
					'method' => 'auto',
					'width' => 200,
					'height' => 200
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default.png'
			)
		);

		return $descriptor;
	}

	static public function slide() {
		$descriptor = array(
			'folder' => 'slide',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'auto',
					'width' => 1400,
					'height' => 830
				),
				'adminList' => array(
					'method' => 'auto',
					'width' => 100,
					'height' => 59
				),
				'og' => array(
					'method' => 'crop',
					'width' => 1200,
					'height' => 630
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default.png'
			)
		);

		return $descriptor;
	}

	static public function startuplogo() {
		$descriptor = array(
			'folder' => 'startuplogo',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'auto',
					'width' => 220,
					'height' => 65
				),
				'adminList' => array(
					'method' => 'auto',
					'width' => 100,
					'height' => 59
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default.png'
			)
		);

		return $descriptor;
	}

	static public function startupimages() {
		$descriptor = array(
			'folder' => 'startupimages',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'auto',
					'width' => 1024,
					'height' => 768
				),
				'adminList' => array(
					'method' => 'auto',
					'width' => 400,
					'height' => 300
				),
				'thumbnail' => array(
					'method' => 'auto',
					'width' => 400,
					'height' => 156
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default.png'
			)
		);

		return $descriptor;
	}

	static public function team() {
		$descriptor = array(
			'folder' => 'team',
			'sizes'	=> array(
				'normal' => array(
					'method' => 'crop',
					'width' => 150,
					'height' => 150
				),
				'adminList' => array(
					'method' => 'crop',
					'width' => 100,
					'height' => 100
				),
			),
			'defaultImage' => array(
				'url' => IMAGE_URL . 'default.png'
			)
		);

		return $descriptor;
	}
}