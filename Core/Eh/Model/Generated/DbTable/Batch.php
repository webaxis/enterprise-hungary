<?php

/**
 * Add your description here
 *
 * @author Webaxis Developer Team
 * @copyright ZF model generator
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

class Eh_Model_Generated_DbTable_Batch extends Zend_Db_Table_Abstract
{
    /**
     * $_name - name of database table
     *
     * @var string
     */
	public $_name='batch';

    /**
     * $_id - this is the primary key of batch table
     *        int(11)
     *
     * @var int
     */
	protected $_id='id';

    /**
     * returns the primary key column name
     *
     * @var string
     */
    public function getPrimaryKeyName() 
    {
        return $this->_id;
    }

    /**
     * returns the number of rows in the table
     * @var int
     */
    public function countAllRows() 
    {
        $query = $this->select()->from($this->_name, 'count(*) as all_count');
        $numRows = $this->fetchRow($query);
        return $numRows['all_count'];
    }

    public function countByQuery($where='') 
    {

        if ($where)
            $where='where '.$where;

        $query = <<<SQL
            select count(*) as all_count from {$this->_name} $where
SQL;
        $row=$this->getAdapter()->query($query)->fetch();

        return $row['all_count'];
    }
}


