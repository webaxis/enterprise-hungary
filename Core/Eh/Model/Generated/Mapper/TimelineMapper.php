<?php

/**
 * Add your description here
 *
 * @author Webaxis Developer Team
 * @copyright ZF model generator
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

class Eh_Model_Generated_Mapper_TimelineMapper
{

    /**
     * $_dbTable - instance of Eh_Model_DbTable_Timeline
     *
     * @var Eh_Model_DbTable_Timeline
     */
    protected $_dbTable;

    /**
     * finds a row where $field equals $value
     *
     * @param string $field
     * @param mixed $value
     * @param Eh_Model_Timeline $cls
     */
    public function findOneByField($field, $value, $cls)
    {
            $table = $this->getDbTable();
            $select = $table->select();

            $row = $table->fetchRow($select->where("{$field} = ?", $value));
            if (0 == count($row)) {
                    return;
            }

            $cls->setId($row->id)
		->setLabel($row->label)
		->setTitle($row->title)
		->setDescription($row->description)
		->setOrder($row->order);
	    return $cls;
    }


    /**
     * returns an array, keys are the field names.
     *
     * @param new Eh_Model_Timeline $cls
     * @return array
     *
     */
    public function toArray($cls) 
    {
        $result = array(

            'id' => $cls->getId(),
            'label' => $cls->getLabel(),
            'title' => $cls->getTitle(),
            'description' => $cls->getDescription(),
            'order' => $cls->getOrder(),
            
        );
        return $result;
    }

    /**
     * finds rows where $field equals $value
     *
     * @param string $field
     * @param mixed $value
     * @param Eh_Model_Timeline $cls
     * @return array
     */
    public function findByField($field, $value, $cls)
    {
            $table = $this->getDbTable();
            $select = $table->select();
            $result = array();

            $rows = $table->fetchAll($select->where("{$field} = ?", $value));
            foreach ($rows as $row) {
                    $cls=new Eh_Model_Timeline();
                    $result[]=$cls;
                    $cls->setId($row->id)
		->setLabel($row->label)
		->setTitle($row->title)
		->setDescription($row->description)
		->setOrder($row->order);
            }
            return $result;
    }

    /**
     * sets the dbTable class
     *
     * @param Eh_Model_DbTable_Timeline $dbTable
     * @return Eh_Model_TimelineMapper
     *
     */
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    /**
     * returns the dbTable class
     *
     * @return Eh_Model_DbTable_Timeline
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Eh_Model_Generated_DbTable_Timeline');
        }
        return $this->_dbTable;
    }

    /**
     * saves current row
     *
     * @param Eh_Model_Timeline $cls
     *
     */

    public function save(Eh_Model_Timeline $cls)
    {
        $data = $cls->toArray();
        
        if (null === ($id = $cls->getId())) {
            unset($data['id']);
            $id=$this->getDbTable()->insert($data);
            $cls->setId($id);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }

    /**
     * finds row by primary key
     *
     * @param int $id
     * @param Eh_Model_Timeline $cls
     */

    public function find($id, Eh_Model_Timeline $cls)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }

        $row = $result->current();

        $cls->setId($row->id)
		->setLabel($row->label)
		->setTitle($row->title)
		->setDescription($row->description)
		->setOrder($row->order);
    }

    /**
     * fetches all rows
     *
     * @return array
     */
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Eh_Model_Timeline();
            $entry->setId($row->id)
                  ->setLabel($row->label)
                  ->setTitle($row->title)
                  ->setDescription($row->description)
                  ->setOrder($row->order)
                              ->setMapper($this);
            $entries[] = $entry;
        }
        return $entries;
    }

    /**
     * fetches all rows optionally filtered by where,order,count and offset
     *
     * @param string $where
     * @param string $order
     * @param int $count
     * @param int $offset
     *
     */
    public function fetchList($where=null, $order=null, $count=null, $offset=null)
    {
            $resultSet = $this->getDbTable()->fetchAll($where, $order, $count, $offset);
            $entries   = array();
            foreach ($resultSet as $row) {
                    $entry = new Eh_Model_Timeline();
                    $entry->setId($row->id)
                          ->setLabel($row->label)
                          ->setTitle($row->title)
                          ->setDescription($row->description)
                          ->setOrder($row->order)
                          ->setMapper($this);
                    $entries[] = $entry;
            }
            return $entries;
    }

    /**
     * fetches all rows optionally filtered by where,order,count and offset
     *
     * @param string $where
     * @param string $order
     * @param int $count
     * @param int $offset
     *
     */
    public function fetchListToArray($where=null, $order=null, $count=null, $offset=null)
    {
            $resultSet = $this->getDbTable()->fetchAll($where, $order, $count, $offset)->toArray();
            return $resultSet;
    }

}
