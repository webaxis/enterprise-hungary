<?php

/**
 * Add your description here
 *
 * @author Webaxis Developer Team
 * @copyright ZF model generator
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

class Eh_Model_Generated_Mapper_SlideMapper
{

    /**
     * $_dbTable - instance of Eh_Model_DbTable_Slide
     *
     * @var Eh_Model_DbTable_Slide
     */
    protected $_dbTable;

    /**
     * finds a row where $field equals $value
     *
     * @param string $field
     * @param mixed $value
     * @param Eh_Model_Slide $cls
     */
    public function findOneByField($field, $value, $cls)
    {
            $table = $this->getDbTable();
            $select = $table->select();

            $row = $table->fetchRow($select->where("{$field} = ?", $value));
            if (0 == count($row)) {
                    return;
            }

            $cls->setId($row->id)
		->setImage($row->image)
		->setTitle($row->title)
		->setSubtitle($row->subtitle)
		->setLink($row->link)
		->setButtontitle($row->buttontitle)
		->setButtonlink($row->buttonlink)
		->setForward($row->forward)
		->setOrder($row->order)
		->setSite($row->site)
		->setLang($row->lang);
	    return $cls;
    }


    /**
     * returns an array, keys are the field names.
     *
     * @param new Eh_Model_Slide $cls
     * @return array
     *
     */
    public function toArray($cls) 
    {
        $result = array(

            'id' => $cls->getId(),
            'image' => $cls->getImage(),
            'title' => $cls->getTitle(),
            'subtitle' => $cls->getSubtitle(),
            'link' => $cls->getLink(),
            'buttontitle' => $cls->getButtontitle(),
            'buttonlink' => $cls->getButtonlink(),
            'forward' => $cls->getForward(),
            'order' => $cls->getOrder(),
            'site' => $cls->getSite(),
            'lang' => $cls->getLang(),
            
        );
        return $result;
    }

    /**
     * finds rows where $field equals $value
     *
     * @param string $field
     * @param mixed $value
     * @param Eh_Model_Slide $cls
     * @return array
     */
    public function findByField($field, $value, $cls)
    {
            $table = $this->getDbTable();
            $select = $table->select();
            $result = array();

            $rows = $table->fetchAll($select->where("{$field} = ?", $value));
            foreach ($rows as $row) {
                    $cls=new Eh_Model_Slide();
                    $result[]=$cls;
                    $cls->setId($row->id)
		->setImage($row->image)
		->setTitle($row->title)
		->setSubtitle($row->subtitle)
		->setLink($row->link)
		->setButtontitle($row->buttontitle)
		->setButtonlink($row->buttonlink)
		->setForward($row->forward)
		->setOrder($row->order)
		->setSite($row->site)
		->setLang($row->lang);
            }
            return $result;
    }

    /**
     * sets the dbTable class
     *
     * @param Eh_Model_DbTable_Slide $dbTable
     * @return Eh_Model_SlideMapper
     *
     */
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    /**
     * returns the dbTable class
     *
     * @return Eh_Model_DbTable_Slide
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Eh_Model_Generated_DbTable_Slide');
        }
        return $this->_dbTable;
    }

    /**
     * saves current row
     *
     * @param Eh_Model_Slide $cls
     *
     */

    public function save(Eh_Model_Slide $cls)
    {
        $data = $cls->toArray();
        
        if (null === ($id = $cls->getId())) {
            unset($data['id']);
            $id=$this->getDbTable()->insert($data);
            $cls->setId($id);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }

    /**
     * finds row by primary key
     *
     * @param int $id
     * @param Eh_Model_Slide $cls
     */

    public function find($id, Eh_Model_Slide $cls)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }

        $row = $result->current();

        $cls->setId($row->id)
		->setImage($row->image)
		->setTitle($row->title)
		->setSubtitle($row->subtitle)
		->setLink($row->link)
		->setButtontitle($row->buttontitle)
		->setButtonlink($row->buttonlink)
		->setForward($row->forward)
		->setOrder($row->order)
		->setSite($row->site)
		->setLang($row->lang);
    }

    /**
     * fetches all rows
     *
     * @return array
     */
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Eh_Model_Slide();
            $entry->setId($row->id)
                  ->setImage($row->image)
                  ->setTitle($row->title)
                  ->setSubtitle($row->subtitle)
                  ->setLink($row->link)
                  ->setButtontitle($row->buttontitle)
                  ->setButtonlink($row->buttonlink)
                  ->setForward($row->forward)
                  ->setOrder($row->order)
                  ->setSite($row->site)
                  ->setLang($row->lang)
                              ->setMapper($this);
            $entries[] = $entry;
        }
        return $entries;
    }

    /**
     * fetches all rows optionally filtered by where,order,count and offset
     *
     * @param string $where
     * @param string $order
     * @param int $count
     * @param int $offset
     *
     */
    public function fetchList($where=null, $order=null, $count=null, $offset=null)
    {
            $resultSet = $this->getDbTable()->fetchAll($where, $order, $count, $offset);
            $entries   = array();
            foreach ($resultSet as $row) {
                    $entry = new Eh_Model_Slide();
                    $entry->setId($row->id)
                          ->setImage($row->image)
                          ->setTitle($row->title)
                          ->setSubtitle($row->subtitle)
                          ->setLink($row->link)
                          ->setButtontitle($row->buttontitle)
                          ->setButtonlink($row->buttonlink)
                          ->setForward($row->forward)
                          ->setOrder($row->order)
                          ->setSite($row->site)
                          ->setLang($row->lang)
                          ->setMapper($this);
                    $entries[] = $entry;
            }
            return $entries;
    }

    /**
     * fetches all rows optionally filtered by where,order,count and offset
     *
     * @param string $where
     * @param string $order
     * @param int $count
     * @param int $offset
     *
     */
    public function fetchListToArray($where=null, $order=null, $count=null, $offset=null)
    {
            $resultSet = $this->getDbTable()->fetchAll($where, $order, $count, $offset)->toArray();
            return $resultSet;
    }

}
