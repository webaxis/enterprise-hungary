<?php

/**
 * Add your description here
 *
 * @author Webaxis Developer Team
 * @copyright ZF model generator
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

class Eh_Model_Generated_Mapper_EventMapper
{

    /**
     * $_dbTable - instance of Eh_Model_DbTable_Event
     *
     * @var Eh_Model_DbTable_Event
     */
    protected $_dbTable;

    /**
     * finds a row where $field equals $value
     *
     * @param string $field
     * @param mixed $value
     * @param Eh_Model_Event $cls
     */
    public function findOneByField($field, $value, $cls)
    {
            $table = $this->getDbTable();
            $select = $table->select();

            $row = $table->fetchRow($select->where("{$field} = ?", $value));
            if (0 == count($row)) {
                    return;
            }

            $cls->setId($row->id)
		->setTitle($row->title)
		->setLink($row->link)
		->setImage($row->image)
		->setDate($row->date)
		->setSite($row->site)
		->setLang($row->lang);
	    return $cls;
    }


    /**
     * returns an array, keys are the field names.
     *
     * @param new Eh_Model_Event $cls
     * @return array
     *
     */
    public function toArray($cls) 
    {
        $result = array(

            'id' => $cls->getId(),
            'title' => $cls->getTitle(),
            'link' => $cls->getLink(),
            'image' => $cls->getImage(),
            'date' => $cls->getDate(),
            'site' => $cls->getSite(),
            'lang' => $cls->getLang(),
            
        );
        return $result;
    }

    /**
     * finds rows where $field equals $value
     *
     * @param string $field
     * @param mixed $value
     * @param Eh_Model_Event $cls
     * @return array
     */
    public function findByField($field, $value, $cls)
    {
            $table = $this->getDbTable();
            $select = $table->select();
            $result = array();

            $rows = $table->fetchAll($select->where("{$field} = ?", $value));
            foreach ($rows as $row) {
                    $cls=new Eh_Model_Event();
                    $result[]=$cls;
                    $cls->setId($row->id)
		->setTitle($row->title)
		->setLink($row->link)
		->setImage($row->image)
		->setDate($row->date)
		->setSite($row->site)
		->setLang($row->lang);
            }
            return $result;
    }

    /**
     * sets the dbTable class
     *
     * @param Eh_Model_DbTable_Event $dbTable
     * @return Eh_Model_EventMapper
     *
     */
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    /**
     * returns the dbTable class
     *
     * @return Eh_Model_DbTable_Event
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Eh_Model_Generated_DbTable_Event');
        }
        return $this->_dbTable;
    }

    /**
     * saves current row
     *
     * @param Eh_Model_Event $cls
     *
     */

    public function save(Eh_Model_Event $cls)
    {
        $data = $cls->toArray();
        
        if (null === ($id = $cls->getId())) {
            unset($data['id']);
            $id=$this->getDbTable()->insert($data);
            $cls->setId($id);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }

    /**
     * finds row by primary key
     *
     * @param int $id
     * @param Eh_Model_Event $cls
     */

    public function find($id, Eh_Model_Event $cls)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }

        $row = $result->current();

        $cls->setId($row->id)
		->setTitle($row->title)
		->setLink($row->link)
		->setImage($row->image)
		->setDate($row->date)
		->setSite($row->site)
		->setLang($row->lang);
    }

    /**
     * fetches all rows
     *
     * @return array
     */
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Eh_Model_Event();
            $entry->setId($row->id)
                  ->setTitle($row->title)
                  ->setLink($row->link)
                  ->setImage($row->image)
                  ->setDate($row->date)
                  ->setSite($row->site)
                  ->setLang($row->lang)
                              ->setMapper($this);
            $entries[] = $entry;
        }
        return $entries;
    }

    /**
     * fetches all rows optionally filtered by where,order,count and offset
     *
     * @param string $where
     * @param string $order
     * @param int $count
     * @param int $offset
     *
     */
    public function fetchList($where=null, $order=null, $count=null, $offset=null)
    {
            $resultSet = $this->getDbTable()->fetchAll($where, $order, $count, $offset);
            $entries   = array();
            foreach ($resultSet as $row) {
                    $entry = new Eh_Model_Event();
                    $entry->setId($row->id)
                          ->setTitle($row->title)
                          ->setLink($row->link)
                          ->setImage($row->image)
                          ->setDate($row->date)
                          ->setSite($row->site)
                          ->setLang($row->lang)
                          ->setMapper($this);
                    $entries[] = $entry;
            }
            return $entries;
    }

    /**
     * fetches all rows optionally filtered by where,order,count and offset
     *
     * @param string $where
     * @param string $order
     * @param int $count
     * @param int $offset
     *
     */
    public function fetchListToArray($where=null, $order=null, $count=null, $offset=null)
    {
            $resultSet = $this->getDbTable()->fetchAll($where, $order, $count, $offset)->toArray();
            return $resultSet;
    }

}
