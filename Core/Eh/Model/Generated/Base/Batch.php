<?php

/**
 * Add your description here
 *
 * @author Webaxis Developer Team
 * @copyright ZF model generator
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

class Eh_Model_Generated_Base_Batch extends WX_Model 
{

    /**
     * mysql var type int(11)
     *
     * @var int
     */
    protected $_fieldId;

    /**
     * mysql var type varchar(255)
     *
     * @var string
     */
    protected $_fieldTitle;

    /**
     * mysql var type varchar(255)
     *
     * @var string
     */
    protected $_fieldSubtitle;

    /**
     * mysql var type int(11)
     *
     * @var int
     */
    protected $_fieldOrder;


    protected $_mapper;

    protected $_columnsList=array(
    'id'=>'Id',
    'title'=>'Title',
    'subtitle'=>'Subtitle',
    'order'=>'Order',
    );

    /**
     *
     * @param array $data
     * @return Default_Model_
     */

    public function setColumnsList($data) 
    {
        $this->_columnsList=$data;
        return $this;
    }

    /**
     * returns columns list array
     *
     * @return array
     */
    public function getColumnsList() 
    {
        return $this->_columnsList;
    }

    /**
     * converts database column name to php setter/getter function name
     * @param string $column
     */
    public function columnName2Var($column) 
    {
        if (!isset($this->_columnsList[$column]))
            throw new Exception("column '$column' not found!");
        return $this->_columnsList[$column];
    }

    /**
     * converts database column name to php setter/getter function name
     * @param string $column
     */
    public function varName2Column($thevar) 
    {

        foreach ($this->_columnsList as $column=>$var)
            if ($var == $thevar)
                    return $column;
        return null;
    }

    
    /**
     * sets column id type int(11)
     *
     * @param int $data
     * @return Eh_Model_Batch
     *
     **/

    public function setId($data)
    {
        $this->_fieldId=$data;
        return $this;
    }

    /**
     * gets column id type int(11)
     * @return int
     */

    public function getId()
    {
        return $this->_fieldId;
    }
    
    /**
     * sets column title type varchar(255)
     *
     * @param string $data
     * @return Eh_Model_Batch
     *
     **/

    public function setTitle($data)
    {
        $this->_fieldTitle=$data;
        return $this;
    }

    /**
     * gets column title type varchar(255)
     * @return string
     */

    public function getTitle()
    {
        return $this->_fieldTitle;
    }
    
    /**
     * sets column subtitle type varchar(255)
     *
     * @param string $data
     * @return Eh_Model_Batch
     *
     **/

    public function setSubtitle($data)
    {
        $this->_fieldSubtitle=$data;
        return $this;
    }

    /**
     * gets column subtitle type varchar(255)
     * @return string
     */

    public function getSubtitle()
    {
        return $this->_fieldSubtitle;
    }
    
    /**
     * sets column order type int(11)
     *
     * @param int $data
     * @return Eh_Model_Batch
     *
     **/

    public function setOrder($data)
    {
        $this->_fieldOrder=$data;
        return $this;
    }

    /**
     * gets column order type int(11)
     * @return int
     */

    public function getOrder()
    {
        return $this->_fieldOrder;
    }
    
    /**
     * Recognize methods for Belongs-To cases:
     * findBy<field>()
     * findOneBy<field>()
     * Use the non-greedy pattern repeat modifier e.g. \w+?
     *
     * @param string $method
     * @param array  $args
     */

    public function __call($method, array $args)
    {
        $matches = array();
        $result=null;
        if (preg_match('/^find(One)?By(\w+)?$/', $method, $matches)) {
                $methods = get_class_methods($this);
                $check = 'set'.$matches[2];

                $fieldName=$this->varName2Column($matches[2]);

                if (!in_array($check, $methods)) {
                        throw new Exception("Invalid field {$matches[2]} requested for table");
                }
                if ($matches[1] != '') {
                    $result=$this->getMapper()->findOneByField($fieldName, $args[0], $this);
                }
                    else $result=$this->getMapper()->findByField($fieldName, $args[0], $this);
                return $result;
        }

        throw new Exception("Unrecognized method '$method()'");
    }

    /**
     * fetch all rows in a 3d array
     *
     * @return array
     */

    public function fetchAllToArray()
    {
        $resultSet = $this->getMapper()->getDbTable()->fetchAll()->toArray();
        return $resultSet;
    }

    /**
     *  __set() is run when writing data to inaccessible properties
     *  overloading it to support setting columns.
     *  example: class->column_name='foo' or class->ColumnName='foo'
     *           will execute the function class->setColumnName('foo')
     *
     * @param string $name
     * @param mixed $value
     *
     */

    public function __set($name, $value)
    {
       $name=$this->columnName2var($name);

                $method = 'set'.$name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception("name:$name value:".
                        $value." - Invalid {".
                        $this->_tbname."
                        } property"
                    );
                }
                $this->$method($value);
    }

    /**
     *  __get() is utilized for reading data from inaccessible properties
     *  overloading it to support getting columns value.
     *  example: $foo=class->column_name or $foo=class->ColumnName
     *           will execute the function $foo=class->getColumnName()
     *
     * @param string $name
     * @param mixed $value
     *
     */

    public function __get($name)
    {
        $name=$this->columnName2Var($name);

        $method = 'get'.$name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
                throw new Exception("name:$name  - Invalid ".
                $this->_tbname.
                " property"
                );
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
                $key = preg_replace_callback(
                    '/_(.)/', 
                    create_function(
                        '$matches',
                        'return ucfirst($matches[1]);'
                    ),
                    $key
                );
                $method = 'set' . ucfirst($key);
                if (in_array($method, $methods)) {
                        $this->$method($value);
                }
        }
        return $this;
    }

    /**
     * sets the mapper class
     *
     * @param Eh_Model_BatchMapper $mapper
     * @return Eh_Model_Batch
     */

    public function setMapper($mapper)
    {
        $this->_mapper = $mapper;
        return $this;
    }


    /**
     * returns the mapper class
     *
     * @return Eh_Model_BatchMapper
     *
     */

    public function getMapper()
    {
        if (null === $this->_mapper) {
            $this->setMapper(new Eh_Model_Generated_Mapper_BatchMapper());
        }
        return $this->_mapper;
    }


    /**
     * returns an array, keys are the field names.
     *
     * @return array
     */
    public function toArray() 
    {
        return $this->getMapper()->toArray($this);
    }

    /**
     * saves current loaded row
     *
     */

    public function save()
    {
        $this->getMapper()->save($this);
    }

    /**
     * finds row by id
     *
     * @param int $id
     * @return Eh_Model_Batch
     *
     */
    public function find($id)
    {
        $this->getMapper()->find($id, $this);
        return $this;
    }

    /**
     * fetchs all row
     * returns an array of Eh_Model_Batch
     *
     * @return array
     *
     */
    public function fetchAll()
    {
        return $this->getMapper()->fetchAll();
    }

    /**
     * fetchs all rows
     * optionally filtered by where, order, count and offset
     * returns an array of Eh_Model_Batch
     *
     * @return array
     *
     */
    public function fetchList($where=null, $order=null, $count=null, $offset=null)
    {
        return $this->getMapper()->fetchList($where, $order, $count, $offset);
    }

    /**
     * fetchs all rows
     * optionally filtered by where, order, count and offset
     * returns a 3d-array of the result
     *
     * @return array
     *
     */
    public function fetchListToArray($where=null, $order=null, $count=null, $offset=null)
    {
        return $this->getMapper()->fetchListToArray($where, $order, $count, $offset);
    }

    /**
     * deletes current row by deleting a row that matches the primary key
     *
     * @return int
     */

    public function deleteRowByPrimaryKey()
    {
        if (!$this->getId())
            throw new Exception('Primary Key does not contain a value');
        return $this->getMapper()->getDbTable()->delete('id = '.$this->getId());
    }

    /**
     * deletes current row by a where satetement
     *
     * @param string $where
     * @return int
     */
    public function delete($where)
    {
        return $this->getMapper()->getDbTable()->delete($where);
    }

    /**
     * returns the number of rows in the table
     * @var int
     */
    public function countAllRows() 
    {
        return $this->getMapper()->getDbTable()->countAllRows();
    }
    /**
     * returns the primary key column name
     *
     * @var string
     */
    public function getPrimaryKeyName() 
    {
        return $this->getMapper()->getDbTable()->countAllRows();
    }


    public function countByQuery($where='') 
    {
    return $this->getMapper()->getDbTable()->countByQuery($where);
    }
}

