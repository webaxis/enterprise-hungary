<?php

/**
 * Add your description here
 *
 * @author Webaxis Developer Team
 * @copyright ZF model generator
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

class Eh_Model_Generated_Base_Startup extends WX_Model 
{

    /**
     * mysql var type int(11)
     *
     * @var int
     */
    protected $_fieldId;

    /**
     * mysql var type varchar(255)
     *
     * @var string
     */
    protected $_fieldName;

    /**
     * mysql var type varchar(255)
     *
     * @var string
     */
    protected $_fieldLogo;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldIntro;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldUsp;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldDetails;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldProjectmanager;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldBusinessinformation;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldInvestmentinformation;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldProjectteam;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldIncubator;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldBusinesssummary;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldSolution;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldTargetmarket;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldCompetitors;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldProgress;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldBusinessmodel;

    /**
     * mysql var type text
     *
     * @var text
     */
    protected $_fieldImages;

    /**
     * mysql var type int(11)
     *
     * @var int
     */
    protected $_fieldBatch;

    /**
     * mysql var type int(11)
     *
     * @var int
     */
    protected $_fieldOrder;


    protected $_mapper;

    protected $_columnsList=array(
    'id'=>'Id',
    'name'=>'Name',
    'logo'=>'Logo',
    'intro'=>'Intro',
    'usp'=>'Usp',
    'details'=>'Details',
    'projectmanager'=>'Projectmanager',
    'businessinformation'=>'Businessinformation',
    'investmentinformation'=>'Investmentinformation',
    'projectteam'=>'Projectteam',
    'incubator'=>'Incubator',
    'businesssummary'=>'Businesssummary',
    'solution'=>'Solution',
    'targetmarket'=>'Targetmarket',
    'competitors'=>'Competitors',
    'progress'=>'Progress',
    'businessmodel'=>'Businessmodel',
    'images'=>'Images',
    'batch'=>'Batch',
    'order'=>'Order',
    );

    /**
     *
     * @param array $data
     * @return Default_Model_
     */

    public function setColumnsList($data) 
    {
        $this->_columnsList=$data;
        return $this;
    }

    /**
     * returns columns list array
     *
     * @return array
     */
    public function getColumnsList() 
    {
        return $this->_columnsList;
    }

    /**
     * converts database column name to php setter/getter function name
     * @param string $column
     */
    public function columnName2Var($column) 
    {
        if (!isset($this->_columnsList[$column]))
            throw new Exception("column '$column' not found!");
        return $this->_columnsList[$column];
    }

    /**
     * converts database column name to php setter/getter function name
     * @param string $column
     */
    public function varName2Column($thevar) 
    {

        foreach ($this->_columnsList as $column=>$var)
            if ($var == $thevar)
                    return $column;
        return null;
    }

    
    /**
     * sets column id type int(11)
     *
     * @param int $data
     * @return Eh_Model_Startup
     *
     **/

    public function setId($data)
    {
        $this->_fieldId=$data;
        return $this;
    }

    /**
     * gets column id type int(11)
     * @return int
     */

    public function getId()
    {
        return $this->_fieldId;
    }
    
    /**
     * sets column name type varchar(255)
     *
     * @param string $data
     * @return Eh_Model_Startup
     *
     **/

    public function setName($data)
    {
        $this->_fieldName=$data;
        return $this;
    }

    /**
     * gets column name type varchar(255)
     * @return string
     */

    public function getName()
    {
        return $this->_fieldName;
    }
    
    /**
     * sets column logo type varchar(255)
     *
     * @param string $data
     * @return Eh_Model_Startup
     *
     **/

    public function setLogo($data)
    {
        $this->_fieldLogo=$data;
        return $this;
    }

    /**
     * gets column logo type varchar(255)
     * @return string
     */

    public function getLogo()
    {
        return $this->_fieldLogo;
    }
    
    /**
     * sets column intro type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setIntro($data)
    {
        $this->_fieldIntro=$data;
        return $this;
    }

    /**
     * gets column intro type text
     * @return text
     */

    public function getIntro()
    {
        return $this->_fieldIntro;
    }
    
    /**
     * sets column usp type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setUsp($data)
    {
        $this->_fieldUsp=$data;
        return $this;
    }

    /**
     * gets column usp type text
     * @return text
     */

    public function getUsp()
    {
        return $this->_fieldUsp;
    }
    
    /**
     * sets column details type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setDetails($data)
    {
        $this->_fieldDetails=$data;
        return $this;
    }

    /**
     * gets column details type text
     * @return text
     */

    public function getDetails()
    {
        return $this->_fieldDetails;
    }
    
    /**
     * sets column projectmanager type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setProjectmanager($data)
    {
        $this->_fieldProjectmanager=$data;
        return $this;
    }

    /**
     * gets column projectmanager type text
     * @return text
     */

    public function getProjectmanager()
    {
        return $this->_fieldProjectmanager;
    }
    
    /**
     * sets column businessinformation type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setBusinessinformation($data)
    {
        $this->_fieldBusinessinformation=$data;
        return $this;
    }

    /**
     * gets column businessinformation type text
     * @return text
     */

    public function getBusinessinformation()
    {
        return $this->_fieldBusinessinformation;
    }
    
    /**
     * sets column investmentinformation type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setInvestmentinformation($data)
    {
        $this->_fieldInvestmentinformation=$data;
        return $this;
    }

    /**
     * gets column investmentinformation type text
     * @return text
     */

    public function getInvestmentinformation()
    {
        return $this->_fieldInvestmentinformation;
    }
    
    /**
     * sets column projectteam type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setProjectteam($data)
    {
        $this->_fieldProjectteam=$data;
        return $this;
    }

    /**
     * gets column projectteam type text
     * @return text
     */

    public function getProjectteam()
    {
        return $this->_fieldProjectteam;
    }
    
    /**
     * sets column incubator type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setIncubator($data)
    {
        $this->_fieldIncubator=$data;
        return $this;
    }

    /**
     * gets column incubator type text
     * @return text
     */

    public function getIncubator()
    {
        return $this->_fieldIncubator;
    }
    
    /**
     * sets column businesssummary type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setBusinesssummary($data)
    {
        $this->_fieldBusinesssummary=$data;
        return $this;
    }

    /**
     * gets column businesssummary type text
     * @return text
     */

    public function getBusinesssummary()
    {
        return $this->_fieldBusinesssummary;
    }
    
    /**
     * sets column solution type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setSolution($data)
    {
        $this->_fieldSolution=$data;
        return $this;
    }

    /**
     * gets column solution type text
     * @return text
     */

    public function getSolution()
    {
        return $this->_fieldSolution;
    }
    
    /**
     * sets column targetmarket type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setTargetmarket($data)
    {
        $this->_fieldTargetmarket=$data;
        return $this;
    }

    /**
     * gets column targetmarket type text
     * @return text
     */

    public function getTargetmarket()
    {
        return $this->_fieldTargetmarket;
    }
    
    /**
     * sets column competitors type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setCompetitors($data)
    {
        $this->_fieldCompetitors=$data;
        return $this;
    }

    /**
     * gets column competitors type text
     * @return text
     */

    public function getCompetitors()
    {
        return $this->_fieldCompetitors;
    }
    
    /**
     * sets column progress type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setProgress($data)
    {
        $this->_fieldProgress=$data;
        return $this;
    }

    /**
     * gets column progress type text
     * @return text
     */

    public function getProgress()
    {
        return $this->_fieldProgress;
    }
    
    /**
     * sets column businessmodel type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setBusinessmodel($data)
    {
        $this->_fieldBusinessmodel=$data;
        return $this;
    }

    /**
     * gets column businessmodel type text
     * @return text
     */

    public function getBusinessmodel()
    {
        return $this->_fieldBusinessmodel;
    }
    
    /**
     * sets column images type text
     *
     * @param text $data
     * @return Eh_Model_Startup
     *
     **/

    public function setImages($data)
    {
        $this->_fieldImages=$data;
        return $this;
    }

    /**
     * gets column images type text
     * @return text
     */

    public function getImages()
    {
        return $this->_fieldImages;
    }
    
    /**
     * sets column batch type int(11)
     *
     * @param int $data
     * @return Eh_Model_Startup
     *
     **/

    public function setBatch($data)
    {
        $this->_fieldBatch=$data;
        return $this;
    }

    /**
     * gets column batch type int(11)
     * @return int
     */

    public function getBatch()
    {
        return $this->_fieldBatch;
    }
    
    /**
     * sets column order type int(11)
     *
     * @param int $data
     * @return Eh_Model_Startup
     *
     **/

    public function setOrder($data)
    {
        $this->_fieldOrder=$data;
        return $this;
    }

    /**
     * gets column order type int(11)
     * @return int
     */

    public function getOrder()
    {
        return $this->_fieldOrder;
    }
    
    /**
     * Recognize methods for Belongs-To cases:
     * findBy<field>()
     * findOneBy<field>()
     * Use the non-greedy pattern repeat modifier e.g. \w+?
     *
     * @param string $method
     * @param array  $args
     */

    public function __call($method, array $args)
    {
        $matches = array();
        $result=null;
        if (preg_match('/^find(One)?By(\w+)?$/', $method, $matches)) {
                $methods = get_class_methods($this);
                $check = 'set'.$matches[2];

                $fieldName=$this->varName2Column($matches[2]);

                if (!in_array($check, $methods)) {
                        throw new Exception("Invalid field {$matches[2]} requested for table");
                }
                if ($matches[1] != '') {
                    $result=$this->getMapper()->findOneByField($fieldName, $args[0], $this);
                }
                    else $result=$this->getMapper()->findByField($fieldName, $args[0], $this);
                return $result;
        }

        throw new Exception("Unrecognized method '$method()'");
    }

    /**
     * fetch all rows in a 3d array
     *
     * @return array
     */

    public function fetchAllToArray()
    {
        $resultSet = $this->getMapper()->getDbTable()->fetchAll()->toArray();
        return $resultSet;
    }

    /**
     *  __set() is run when writing data to inaccessible properties
     *  overloading it to support setting columns.
     *  example: class->column_name='foo' or class->ColumnName='foo'
     *           will execute the function class->setColumnName('foo')
     *
     * @param string $name
     * @param mixed $value
     *
     */

    public function __set($name, $value)
    {
       $name=$this->columnName2var($name);

                $method = 'set'.$name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception("name:$name value:".
                        $value." - Invalid {".
                        $this->_tbname."
                        } property"
                    );
                }
                $this->$method($value);
    }

    /**
     *  __get() is utilized for reading data from inaccessible properties
     *  overloading it to support getting columns value.
     *  example: $foo=class->column_name or $foo=class->ColumnName
     *           will execute the function $foo=class->getColumnName()
     *
     * @param string $name
     * @param mixed $value
     *
     */

    public function __get($name)
    {
        $name=$this->columnName2Var($name);

        $method = 'get'.$name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
                throw new Exception("name:$name  - Invalid ".
                $this->_tbname.
                " property"
                );
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
                $key = preg_replace_callback(
                    '/_(.)/', 
                    create_function(
                        '$matches',
                        'return ucfirst($matches[1]);'
                    ),
                    $key
                );
                $method = 'set' . ucfirst($key);
                if (in_array($method, $methods)) {
                        $this->$method($value);
                }
        }
        return $this;
    }

    /**
     * sets the mapper class
     *
     * @param Eh_Model_StartupMapper $mapper
     * @return Eh_Model_Startup
     */

    public function setMapper($mapper)
    {
        $this->_mapper = $mapper;
        return $this;
    }


    /**
     * returns the mapper class
     *
     * @return Eh_Model_StartupMapper
     *
     */

    public function getMapper()
    {
        if (null === $this->_mapper) {
            $this->setMapper(new Eh_Model_Generated_Mapper_StartupMapper());
        }
        return $this->_mapper;
    }


    /**
     * returns an array, keys are the field names.
     *
     * @return array
     */
    public function toArray() 
    {
        return $this->getMapper()->toArray($this);
    }

    /**
     * saves current loaded row
     *
     */

    public function save()
    {
        $this->getMapper()->save($this);
    }

    /**
     * finds row by id
     *
     * @param int $id
     * @return Eh_Model_Startup
     *
     */
    public function find($id)
    {
        $this->getMapper()->find($id, $this);
        return $this;
    }

    /**
     * fetchs all row
     * returns an array of Eh_Model_Startup
     *
     * @return array
     *
     */
    public function fetchAll()
    {
        return $this->getMapper()->fetchAll();
    }

    /**
     * fetchs all rows
     * optionally filtered by where, order, count and offset
     * returns an array of Eh_Model_Startup
     *
     * @return array
     *
     */
    public function fetchList($where=null, $order=null, $count=null, $offset=null)
    {
        return $this->getMapper()->fetchList($where, $order, $count, $offset);
    }

    /**
     * fetchs all rows
     * optionally filtered by where, order, count and offset
     * returns a 3d-array of the result
     *
     * @return array
     *
     */
    public function fetchListToArray($where=null, $order=null, $count=null, $offset=null)
    {
        return $this->getMapper()->fetchListToArray($where, $order, $count, $offset);
    }

    /**
     * deletes current row by deleting a row that matches the primary key
     *
     * @return int
     */

    public function deleteRowByPrimaryKey()
    {
        if (!$this->getId())
            throw new Exception('Primary Key does not contain a value');
        return $this->getMapper()->getDbTable()->delete('id = '.$this->getId());
    }

    /**
     * deletes current row by a where satetement
     *
     * @param string $where
     * @return int
     */
    public function delete($where)
    {
        return $this->getMapper()->getDbTable()->delete($where);
    }

    /**
     * returns the number of rows in the table
     * @var int
     */
    public function countAllRows() 
    {
        return $this->getMapper()->getDbTable()->countAllRows();
    }
    /**
     * returns the primary key column name
     *
     * @var string
     */
    public function getPrimaryKeyName() 
    {
        return $this->getMapper()->getDbTable()->countAllRows();
    }


    public function countByQuery($where='') 
    {
    return $this->getMapper()->getDbTable()->countByQuery($where);
    }
}

