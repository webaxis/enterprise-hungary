<?php
/**
 * Add your description here
 *
 * @author Webaxis Developer Team
 * @copyright ZF model generator
 * @license http://framework.zend.com/license/new-bsd     New BSD License
 */

class Eh_Model_Translateeh extends Eh_Model_Generated_Base_Translateeh
{
/**
 * Implement your methods here
 */
	static public function deleteAll() {
		$db = Zend_Registry::get('db');
		$query = 'TRUNCATE TABLE translateeh';

		return count($db->query($query));
	}

	static public function saveTag ($parts, $language = 'hu') {
		$model = new Eh_Model_Translateeh();

		if (!empty($parts) && !empty($parts[0])) {
			$tag = $parts[0];
			$text = (array_key_exists(1, $parts)) ? $parts[1] : '';

			$model->findOneByLabel($tag);
			$model->setLabel($tag);
			$fn = 'set'.ucfirst(nl2br($language));
			$model->$fn($text);

			$model->save();
		}
	}
}

