<?php

class Eh_CMS_Descriptor {

	static public function adminadmin () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminadmin',
				'listId' => 'adminadmin-list',
				'template' => array(
					'title' => 'Admin felhasználók'
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminAdmin() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadása'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminadmin',
				'formId' => 'adminadmin-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminAdmin()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Felhasználó hozzáadása',
						'edit' => 'Felhasználó szerkesztése'
					)
				),
				'data' => array(
					'dbTable' => 'admin',
					'params' => array(
						'name',
						'login',
						'password'
					),
					'toFormConverter' => array('admin','toForm'),
					'toDbConverter' => array('admin','toDb'),
				)
			)
		);
	}

	static public function adminbatch () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminbatch',
				'listId' => 'adminbatch-list',
				'template' => array(
					'title' => 'Batchek'
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminBatch() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadása'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminbatch',
				'formId' => 'adminbatch-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminBatch()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Batch hozzáadása',
						'edit' => 'Batch szerkesztése'
					)
				),
				'data' => array(
					'dbTable' => 'batch',
					'params' => array(
						'title',
						'subtitle',
						'order' => array(
							'type' => 'order',
							'orderDescriptorName' => 'batch',
							'orderColumn' => 'order'
						),
					)
				)
			)
		);
	}

	static public function adminorganization () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminorganization',
				'listId' => 'adminorganization-list',
				'template' => array(
					'title' => 'Diákszervezetek'
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminOrganization() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadása'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminorganization',
				'formId' => 'adminorganization-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminOrganization()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Szervezet hozzáadása',
						'edit' => 'Szervezet szerkesztése'
					)
				),
				'data' => array(
					'dbTable' => 'organization',
					'params' => array(
						'logo' => [
							'type' => 'image',
							'imageDescriptorName' => 'organization'
						],
						'title',
						'description',
						'link',
						'order' => array(
							'type' => 'order',
							'orderDescriptorName' => 'organization',
							'orderColumn' => 'order'
						),
					)
				)
			)
		);
	}

	static public function admincontent () {
		return array(
			'list' => array(
				'listDescriptorName' => 'admincontent',
				'listId' => 'admincontent-list',
				'template' => array(
					'title' => 'Tartalmak'
				)
			),
			'form' => array(
				'type' => 'wysiwyg',
				'template' => array(
					'title' => array(
						'add' => 'Tartalom szerkesztése',
						'edit' => 'Tartalom szerkesztése'
					)
				),
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminContent()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'data' => array(
					'dbTable' => 'content',
					'columnName' => 'content'
				)
			)
		);
	}

	static public function adminevent () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminevent',
				'listId' => 'adminevent-list',
				'template' => array(
					'title' => 'Események',
	    			'directory' => CORE_APP_PATH . 'CMS/templates/admin',
					'template' => 'list-with-language-selector.phtml'
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminEvent() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadása'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminevent',
				'formId' => 'adminevent-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminEvent()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Esemény hozzáadása',
						'edit' => 'Esemény szerkesztése'
					)
				),
				'data' => array(
					'dbTable' => 'event',
					'params' => array(
						'title',
						'link',
						'image' => [
							'type' => 'image',
							'imageDescriptorName' => 'event'
						],
						'date',
						'site',
						'lang',
					)
				)
			)
		);
	}

	static public function adminfaq () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminfaq',
				'listId' => 'adminfaq-list',
				'template' => array(
					'title' => 'GYIK'
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminFaq() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadása'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminfaq',
				'formId' => 'adminfaq-form',
				'template' => array(
					'title' => array(
						'add' => 'Kérdés hozzáadása',
						'edit' => 'Kérdés szerkesztése'
					)
				),
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminFaq()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'data' => array(
					'dbTable' => 'faq',
					'params' => array(
						'question',
						'answer',
						'order' => array(
							'type' => 'order',
							'orderDescriptorName' => 'faq',
							'orderColumn' => 'order'
						),
					)
				)
			)
		);
	}

	static public function adminnews () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminnews',
				'listId' => 'adminnews-list',
				'template' => array(
					'title' => 'Hírek',
	    			'directory' => CORE_APP_PATH . 'CMS/templates/admin',
					'template' => 'list-with-language-selector.phtml'
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminNews() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadása'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminnews',
				'formId' => 'adminnews-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminNews()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Hír hozzáadása',
						'edit' => 'Hír szerkesztése'
					),
					'directory' => CORE_APP_PATH . 'CMS/templates/',
					'template' => 'admin/form-news-multiimage.phtml'
				),
				'data' => array(
					'dbTable' => 'news',
					'params' => array(
						'title',
						'lead',
						'image' => [
							'type' => 'image',
							'imageDescriptorName' => 'news'
						],
						'description',
						'date',
						'pressrelease',
						'site',
						'lang',
						'gallery' => array(
							'type' => 'images',
							'imageDescriptorName' => 'newsgallery'
						),
					),
					'toFormConverter' => array('news','toForm'),
				)
			)
		);
	}
	
	static public function adminpartner () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminpartner',
				'listId' => 'adminpartner-list',
				'template' => array(
					'title' => 'Partnerek',
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminPartner() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadás'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminpartner',
				'formId' => 'adminpartner-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminPartner()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Partner hozzáadása',
						'edit' => 'Partner szerkesztése'
					)
				),
				'data' => array(
					'dbTable' => 'partner',
					'params' => array(
						'title',
						'logo' => array(
							'type' => 'image',
							'imageDescriptorName' => 'partner'
						),
						'link',
						'footer',
						'order' => array(
							'type' => 'order',
							'orderDescriptorName' => 'partner',
							'orderColumn' => 'order'
						),
						'section',
					),
				)
			)
		);
	}
	
	static public function adminslide () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminslide',
				'listId' => 'adminslide-list',
				'template' => array(
					'title' => 'Slideok',
	    			'directory' => CORE_APP_PATH . 'CMS/templates/admin',
					'template' => 'list-with-language-selector.phtml'
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminSlide() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadás'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminslide',
				'formId' => 'adminslide-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminSlide()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Slide hozzáadása',
						'edit' => 'Slide szerkesztése'
					)
				),
				'data' => array(
					'dbTable' => 'slide',
					'params' => array(
						'image' => array(
							'type' => 'image',
							'imageDescriptorName' => 'slide'
						),
						'order' => array(
							'type' => 'order',
							'orderDescriptorName' => 'slide',
							'orderColumn' => 'order'
						),
						'title',
						'subtitle',
						'link',
						'buttontitle',
						'buttonlink',
						'forward',
						'site',
						'lang',
					),
				)
			)
		);
	}
	
	static public function adminstartup () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminstartup',
				'listId' => 'adminstartup-list',
				'template' => array(
					'title' => 'Startupok',
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminStartup() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadás'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminstartup',
				'formId' => 'adminstartup-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminStartup()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Startup hozzáadása',
						'edit' => 'Startup szerkesztése'
					),
					'directory' => CORE_APP_PATH . 'CMS/templates/',
					'template' => 'admin/form-startup-multiimage.phtml'
				),
				'data' => array(
					'dbTable' => 'startup',
					'params' => array(
						'logo' => array(
							'type' => 'image',
							'imageDescriptorName' => 'startuplogo'
						),
						'name',
						'intro',
						'usp',
						'details',
						'batch',
						'projectmanager',
						'businessinformation',
						'investmentinformation',
						'projectteam',
						'incubator',
						'businesssummary',
						'solution',
						'targetmarket',
						'competitors',
						'progress',
						'businessmodel',
						'images' => array(
							'type' => 'images',
							'imageDescriptorName' => 'startupimages'
						),
						'order' => array(
							'type' => 'order',
							'orderDescriptorName' => 'startup',
							'orderColumn' => 'order'
						),
					),
					'toFormConverter' => array('startup','toForm'),
				)
			)
		);
	}
	
	static public function adminteam () {
		return array(
			'list' => array(
				'listDescriptorName' => 'adminteam',
				'listId' => 'adminteam-list',
				'template' => array(
					'title' => 'Csapat',
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminTeam() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadás'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'adminteam',
				'formId' => 'adminteam-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminTeam()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Csapattag hozzáadása',
						'edit' => 'Csapattag szerkesztése'
					)
				),
				'data' => array(
					'dbTable' => 'team',
					'params' => array(
						'name',
						'image' => array(
							'type' => 'image',
							'imageDescriptorName' => 'team'
						),
						'description',
						'email',
						'linkedin',
						'order' => array(
							'type' => 'order',
							'orderDescriptorName' => 'team',
							'orderColumn' => 'order'
						),
					),
				)
			)
		);
	}
	
	static public function admintimeline () {
		return array(
			'list' => array(
				'listDescriptorName' => 'admintimeline',
				'listId' => 'admintimeline-list',
				'template' => array(
					'title' => 'Timeline',
				),
				'buttons' => array(
					array(
						'url' => Eh_Url::adminTimeline() . '/add',
						'class' => 'btn large gradient-gray',
						'label' => '+ Új hozzáadás'
					)
				)
			),
			'form' => array(
				'formDescriptorName' => 'admintimeline',
				'formId' => 'admintimeline-form',
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminTimeline()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					),
				),
				'template' => array(
					'title' => array(
						'add' => 'Elem hozzáadása',
						'edit' => 'Elem szerkesztése'
					)
				),
				'data' => array(
					'dbTable' => 'timeline',
					'params' => array(
						'label',
						'title',
						'description',
						'order' => array(
							'type' => 'order',
							'orderDescriptorName' => 'timeline',
							'orderColumn' => 'order'
						),
					),
				)
			)
		);
	}

	static public function admintranslateeh () {
		return array(
			'type' => 'translate',
			'options' => [
				'fileGeneratorClass' => 'Admin_TranslateehController',
				'fileGeneratorMethod' => 'generateFiles'
			],
			'list' => array(
				'listDescriptorName' => 'admintranslateeh',
				'listId' => 'admintranslateeh-list',
				'template' => array(
					'title' => 'Szövegek',
					'directory' => CORE_APP_PATH . 'CMS/templates',
					'template' => 'translateeh-list.phtml'
				)
			),
			'form' => array(
				'formDescriptorName' => 'admintranslate',
				'formId' => 'admintranslate-form',
				'template' => array(
					'title' => array(
						'add' => 'Szöveg szerkesztése',
						'edit' => 'Szöveg szerkesztése'
					)
				),
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminTranslateeh()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					)
				),
				'data' => array(
					'dbTable' => 'translateeh',
					'params' => array(
						'hu',
						'en',
					),
				)
			)
		);
	}

	static public function admintranslatesc () {
		return array(
			'type' => 'translate',
			'options' => [
				'fileGeneratorClass' => 'Admin_TranslatescController',
				'fileGeneratorMethod' => 'generateFiles'
			],
			'list' => array(
				'listDescriptorName' => 'admintranslatesc',
				'listId' => 'admintranslatesc-list',
				'template' => array(
					'title' => 'Szövegek',
					'directory' => CORE_APP_PATH . 'CMS/templates',
					'template' => 'translatesc-list.phtml'
				)
			),
			'form' => array(
				'formDescriptorName' => 'admintranslate',
				'formId' => 'admintranslate-form',
				'template' => array(
					'title' => array(
						'add' => 'Szöveg szerkesztése',
						'edit' => 'Szöveg szerkesztése'
					)
				),
				'buttons' => array(
					array(
						'label' => 'Mégsem',
						'class' => 'btn large gradient-gray',
						'url' => Eh_Url::adminTranslatesc()
					),
					array(
						'label' => 'Mentés',
						'class' => 'btn large colored',
						'id' => 'save-button',
					)
				),
				'data' => array(
					'dbTable' => 'translatesc',
					'params' => array(
						'hu',
					),
				)
			)
		);
	}
}