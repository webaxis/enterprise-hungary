<?php

class Eh_List_Descriptors_Admintimeline {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Timeline::getAllQuery(['order'=>'order ASC']),
			'converter'		=> 'Eh_Converter_Timeline::adminList',
			'id' 			=> 'admintimeline-list',
			'limit' 		=> 25,
			'newMode'		=> true,
			'structure'		=> array(
				'fields' => [
					'label' => array(
						'label' => 'Címke',
					),
					'title' => [
						'label' => 'Cím',
					],
					'order' => [
						'label' => 'Sorrend',
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminTimeline(),
						],
					],
					'delete' => []
				]
			)
		);

		return new WX_List_Create($params, $descriptor);
	}
}