<?php

class Eh_List_Descriptors_Adminnews {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$language = Eh_Admin_ContentLanguage::getInstance()->getLanguage();
		$where = !empty($language) ? 'lang like "'.$language.'"' : null;
		
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_News::getAllQuery(['order'=>'date DESC', 'where' => $where]),
			'converter'		=> 'Eh_Converter_News::adminList',
			'id' 			=> 'adminnews-list',
			'limit' 		=> 25,
			'newMode'		=> true,
			'structure'		=> array(
				'fields' => [
					'image' => array(
						'label' => 'Kép',
						'type' => 'image'
					),
					'title' => array(
						'label' => 'Cím'
					),
					'date' => [
						'label' => 'Dátum',
					],
					'site' => [
						'label' => 'Oldal',
					],
					'lang' => [
						'label' => 'Nyelv',
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminNews(),
						],
					],
					'delete' => []
				]
			)
		);

		return new WX_List_Create($params, $descriptor);
	}
}