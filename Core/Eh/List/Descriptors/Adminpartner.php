<?php

class Eh_List_Descriptors_Adminpartner {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$options = ['order' => ['order ASC']];
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Partner::getAllQuery($options),
			'converter'		=> 'Eh_Converter_Partner::adminList',
			'id' 			=> 'adminpartner-list',
			'limit' 		=> 25,
			'newMode' 		=> true,
			'structure'		=> [
				'fields' => [
					'logo'	=> [
						'label'	=> 'Kép',
						'type'	=> 'image'
					],
					'section' => [
						'label' => 'Szekció',
					],
					'order' => [
						'label' => 'Sorrend',
						'class'	=> 'pull-right'
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminPartner(),
						],
					],
					'delete' => []
				]
			]
		);

		return new WX_List_Create($params, $descriptor);
	}
}