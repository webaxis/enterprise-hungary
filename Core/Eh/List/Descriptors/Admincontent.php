<?php

class Eh_List_Descriptors_Admincontent {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Content::getAllQuery(),
			'id' 			=> 'admincontent-list',
			'limit' 		=> 10,
			'newMode'		=> true,
			'structure'		=> [
				'fields' => [
					'title' => [
						'label' => 'Tartalom neve'
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminContent(),
						],
					],
					#'delete' => []
				]
			]
		);
		return new WX_List_Create($params, $descriptor);
	}
}