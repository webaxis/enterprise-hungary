<?php

class Eh_List_Descriptors_Adminbatch {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Batch::getAllQuery(['order'=>'order ASC']),
			'converter'		=> 'Eh_Converter_Batch::adminList',
			'id' 			=> 'adminbatch-list',
			'limit' 		=> 25,
			'newMode'		=> true,
			'structure'		=> array(
				'fields' => [
					'title' => array(
						'label' => 'Név'
					),
					'order' => [
						'label' => 'Sorrend',
						'class'	=> 'pull-right'
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminBatch(),
						],
					],
					'delete' => []
				]
			)
		);

		return new WX_List_Create($params, $descriptor);
	}
}