<?php

class Eh_List_Descriptors_Adminslide {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$language = Eh_Admin_ContentLanguage::getInstance()->getLanguage();
		$where = !empty($language) ? 'lang like "'.$language.'"' : null;
		
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Slide::getAllQuery(['order'=>['site ASC','order ASC'], 'where' => $where]),
			'converter'		=> 'Eh_Converter_Slide::adminList',
			'id' 			=> 'adminslide-list',
			'limit' 		=> 25,
			'newMode'		=> true,
			'structure'		=> array(
				'fields' => [
					'image' => array(
						'label' => 'Kép',
						'type' => 'image'
					),
					'site' => [
						'label' => 'Oldal',
					],
					'order' => [
						'label' => 'Sorrend',
					],
					'site' => [
						'label' => 'Nyelv',
					],
					'lang' => [
						'label' => 'Nyelv',
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminSlide(),
						],
					],
					'delete' => []
				]
			)
		);

		return new WX_List_Create($params, $descriptor);
	}
}