<?php

class Eh_List_Descriptors_Adminadmin {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Admin::getAllQuery(),
			'id' 			=> 'adminadmin-list',
			'limit' 		=> 10,
			'newMode'		=> true,
			'structure'		=> array(
				'fields' => [
					'name' => array(
						'label' => 'Név'
					),
					'login' => array(
						'label' => 'Felhasználónév'
					)
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminAdmin(),
						],
					],
					'delete' => []
				]
			)
		);

		return new WX_List_Create($params, $descriptor);
	}
}