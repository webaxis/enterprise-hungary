<?php

class Eh_List_Descriptors_Adminorganization {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Organization::getAllQuery(['order'=>'order ASC']),
			'converter'		=> 'Eh_Converter_Organization::adminList',
			'id' 			=> 'adminorganization-list',
			'limit' 		=> 25,
			'newMode'		=> true,
			'structure'		=> array(
				'fields' => [
					'logo' => array(
						'label' => 'Logo',
						'type' => 'image'
					),
					'title' => array(
						'label' => 'Cím'
					),
					'order' => [
						'label' => 'Sorrend',
						'class' => 'pull-right'
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminOrganization(),
						],
					],
					'delete' => []
				]
			)
		);

		return new WX_List_Create($params, $descriptor);
	}
}