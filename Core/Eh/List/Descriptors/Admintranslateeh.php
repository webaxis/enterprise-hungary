<?php

class Eh_List_Descriptors_Admintranslateeh
{
	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$where = self::_buildWhere($params);

		$options = [
			'where' => $where
		];

		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Translateeh::getAllQuery($options),
			'id' 			=> 'admintranslateeh-list',
			'limit' 		=> 100,
			'template' 		=> 'admintranslateeh.phtml',
			'structure'		=> array(
				'label' => array(
					'label' => 'Címke'
				),
				'hu' => array(
					'label' => 'HU'
				),
				'en' => array(
					'label' => 'EN'
				),
			)
		);
		return new WX_List_Create($params, $descriptor);
	}

	static protected function _buildWhere($params) {

		if (array_key_exists('query', $params)) {

			$whereArray = [];
			$whereArray[] = 'label LIKE "%'.$params['query'].'%"';

			$languages = Eh_Locale::getInstance()->getAvailableLanguages();
			foreach ($languages as $language) {
				$whereArray[] = strtolower($language).' LIKE "%'.$params['query'].'%"';
			}

			$where =implode(' OR ', $whereArray);

			return $where;
		}


	}
}