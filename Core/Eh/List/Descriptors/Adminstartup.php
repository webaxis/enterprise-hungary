<?php

class Eh_List_Descriptors_Adminstartup {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Startup::getAllQuery(['order'=>['batch DESC','order ASC']]),
			'converter'		=> 'Eh_Converter_Startup::adminList',
			'id' 			=> 'adminstartup-list',
			'limit' 		=> 25,
			'newMode'		=> true,
			'structure'		=> array(
				'fields' => [
					'logo' => array(
						'label' => 'Logo',
						'type' => 'image'
					),
					'name' => [
						'label' => 'Név',
					],
					'batch' => [
						'label' => 'Batch',
					],
					'order' => [
						'label' => 'Sorrend',
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminStartup(),
						],
					],
					'delete' => []
				]
			)
		);

		return new WX_List_Create($params, $descriptor);
	}
}