<?php

class Eh_List_Descriptors_Adminfaq {

	/**
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get($params) {
		$options = ['order' => 'order ASC'];
		$descriptor = array(
			'dbSelect'		=> Eh_Model_Faq::getAllQuery($options),
			'converter'		=> 'Eh_Converter_Faq::adminList',
			'id'			=> 'adminfaq-list',
			'limit'			=> 25,
			'newMode'		=> true,
			'structure'		=> [
				'fields' => [
					'question'	=> [
						'label' => 'Kérdés'
					],
					'order' => [
						'label' => 'Sorrend',
						'class'	=> 'pull-right'
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminFaq(),
						],
					],
					'delete' => []
				]
			]
		);

		return new WX_List_Create($params, $descriptor);
	}
}