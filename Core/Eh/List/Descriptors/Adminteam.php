<?php

class Eh_List_Descriptors_Adminteam {

	/*
	 * Must get @params that must be passed to WX_List_Create as first arg
	 */
	static public function get ($params) {
		$descriptor = array(
			'dbSelect' 		=> Eh_Model_Team::getAllQuery(['order'=>'order ASC']),
			'converter'		=> 'Eh_Converter_Team::adminList',
			'id' 			=> 'adminteam-list',
			'limit' 		=> 25,
			'newMode'		=> true,
			'structure'		=> array(
				'fields' => [
					'image' => array(
						'label' => 'Kép',
						'type' => 'image'
					),
					'name' => [
						'label' => 'Név',
					],
					'order' => [
						'label' => 'Sorrend',
					]
				],
				'buttons' => [
					'edit' => [
						'variables' => [
							'url' => Eh_Url::adminTeam(),
						],
					],
					'delete' => []
				]
			)
		);

		return new WX_List_Create($params, $descriptor);
	}
}