<?php

class Eh_Router extends WX_Router {

	protected function _resolve() {

		$this->setLanguages(Eh_Locale::getInstance()->getAvailableLanguages());


		$module = PROJECT_NAME;

		# FRONT
		$this->addRoute(array('/','GET','index','index', $module))
			 ->addRoute(array('en','GET','index','langen', $module))
			 ->addRoute(array('hu','GET','index','langhu', $module))
			 ->addRoute(array('regenerateogimg','GET','index','regenerateogimg', $module))
			 ->addRoute(array('URL_NEWS/:news','GET','news','index', $module));

		if ($module == 'sc') {
			$this
			->addRoute(array('URL_ABOUT','GET','content','about', $module))

			->addRoute(array('URL_TEAM','GET','team','index', $module))

			->addRoute(array('URL_STARTUP','GET','startup','list', $module))
			->addRoute(array('URL_STARTUP/:startup','GET','startup','index', $module))

			->addRoute(array('URL_PARTNER','GET','partner','index', $module))

			->addRoute(array('URL_INCUBE','GET','incube','index', $module))

			->addRoute(array('URL_FAQ','GET','faq','index', $module))
			
			->addRoute(array('jelentkezes','GET','index','apply', $module));
		}

		$this
			->addRoute(array('wx/list','GET,POST','ajax','list'))
			->addRoute(array('wx/list/more','GET,POST','ajax','listmore'));

		# ADMIN

		$this
		->addRoute(array('changecontentlanguage','GET','ajax','changecontentlanguage', 'default'))

		->addRoute(array('admin','GET,POST','index','index','admin'))
		->addRoute(array('admin/logout','GET,POST','logout','index','admin'))

		->addRoute(array('admin/admin','GET,POST','admin','list','admin'))
		->addRoute(array('admin/admin/add','GET,POST','admin','edit','admin'))
		->addRoute(array('admin/admin/edit/id/:id','GET,POST','admin','edit','admin'))

		->addRoute(array('admin/batch','GET,POST','batch','list','admin'))
		->addRoute(array('admin/batch/add','GET,POST','batch','edit','admin'))
		->addRoute(array('admin/batch/edit/id/:id','GET,POST','batch','edit','admin'))

		->addRoute(array('admin/organization','GET,POST','organization','list','admin'))
		->addRoute(array('admin/organization/add','GET,POST','organization','edit','admin'))
		->addRoute(array('admin/organization/edit/id/:id','GET,POST','organization','edit','admin'))

		->addRoute(array('admin/content','GET,POST','content','list','admin'))
		->addRoute(array('admin/content/edit/id/:id','GET,POST','content','edit','admin'))

		->addRoute(array('admin/event','GET,POST','event','list','admin'))
		->addRoute(array('admin/event/add','GET,POST','event','edit','admin'))
		->addRoute(array('admin/event/edit/id/:id','GET,POST','event','edit','admin'))

		->addRoute(array('admin/faq','GET,POST','faq','list','admin'))
		->addRoute(array('admin/faq/add','GET,POST','faq','edit','admin'))
		->addRoute(array('admin/faq/edit/id/:id','GET,POST','faq','edit','admin'))

		->addRoute(array('admin/news','GET,POST','news','list','admin'))
		->addRoute(array('admin/news/add','GET,POST','news','edit','admin'))
		->addRoute(array('admin/news/edit/id/:id','GET,POST','news','edit','admin'))

		->addRoute(array('admin/partner','GET,POST','partner','list','admin'))
		->addRoute(array('admin/partner/add','GET,POST','partner','edit','admin'))
		->addRoute(array('admin/partner/edit/id/:id','GET,POST','partner','edit','admin'))

		->addRoute(array('admin/slide','GET,POST','slide','list','admin'))
		->addRoute(array('admin/slide/add','GET,POST','slide','edit','admin'))
		->addRoute(array('admin/slide/edit/id/:id','GET,POST','slide','edit','admin'))

		->addRoute(array('admin/startup','GET,POST','startup','list','admin'))
		->addRoute(array('admin/startup/add','GET,POST','startup','edit','admin'))
		->addRoute(array('admin/startup/edit/id/:id','GET,POST','startup','edit','admin'))

		->addRoute(array('admin/team','GET,POST','team','list','admin'))
		->addRoute(array('admin/team/add','GET,POST','team','edit','admin'))
		->addRoute(array('admin/team/edit/id/:id','GET,POST','team','edit','admin'))

		->addRoute(array('admin/timeline','GET,POST','timeline','list','admin'))
		->addRoute(array('admin/timeline/add','GET,POST','timeline','edit','admin'))
		->addRoute(array('admin/timeline/edit/id/:id','GET,POST','timeline','edit','admin'))

		->addRoute(array('admin/translateeh','GET,POST','translateeh','list','admin'))
		->addRoute(array('admin/translateeh/importtranslatefile','GET,POST','translateeh','importtranslatefile','admin'))
		->addRoute(array('admin/translateeh/exporttranslatefile','GET,POST','translateeh','exporttranslatefile','admin'))
		->addRoute(array('admin/translateeh/edit/id/:id','GET,POST','translateeh','edit','admin'))

		->addRoute(array('admin/translatesc','GET,POST','translatesc','list','admin'))
		->addRoute(array('admin/translatesc/importtranslatefile','GET,POST','translatesc','importtranslatefile','admin'))
		->addRoute(array('admin/translatesc/exporttranslatefile','GET,POST','translatesc','exporttranslatefile','admin'))
		->addRoute(array('admin/translatesc/edit/id/:id','GET,POST','translatesc','edit','admin'))

		# SANDBOX

		->addRoute(array('sandbox','GET','sandbox','index'))

		;
	}
}
