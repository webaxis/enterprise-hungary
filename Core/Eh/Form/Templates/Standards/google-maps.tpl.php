<script>
	$(document).ready(function(){
		mapAutoComplete.init('map-canvas', '<?=$this->getAttribute('id');?>');
	});
</script>
<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>
	<div class="field">
		<input autocomplete="off" class="text <?=$this->getAttribute('class')?>" type="text" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
		<div class="map" id="map-canvas">
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>
