<div class="row<?=$this->getMessages()?' error':''?>">
	<?php include('label.tpl.php');?>
	<div class="field">
		<textarea class="textarea <?=$this->getAttribute('class')?>" <?=$this->getAttributesString()?>><?=$this->getValue()?></textarea>
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>