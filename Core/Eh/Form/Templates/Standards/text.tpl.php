<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>
	<div class="field">
		<input autocomplete="off" class="text <?=$this->getAttribute('class')?>" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
	</div>
	<?php include('error_row.tpl.php');?>
	<?php include('description.tpl.php');?>
</div>
