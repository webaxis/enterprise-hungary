<script>
	$(document).ready(function(){
		if ($('#<?=$this->getAttribute('id')?>').val() == '') {
			openingHours = [
                {isActive: false},
                {isActive: false},
                {isActive: false},
                {isActive: false},
                {isActive: false},
                {isActive: false},
                {isActive: false}
            ]
		} else {
			openingHours = $.parseJSON($('#<?=$this->getAttribute('id')?>').val());
		}

		$("#opening-hours-container").businessHours({
			operationTime : openingHours,
			weekdays: ['Hétfő', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat', 'Vasárnap'],
			postInit: function() {
				$('.operationTimeFrom, .operationTimeTill').timepicker({
					'timeFormat': 'H:i',
					'step': 15
				});
			},
			inputSelector : '#<?=$this->getAttribute('id')?>'
		 });
	});
</script>
<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>
	<div class="field">
		<div id="opening-hours-container"></div>
		<input type="hidden" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
	</div>
	<?php include('description.tpl.php');?>
</div>
