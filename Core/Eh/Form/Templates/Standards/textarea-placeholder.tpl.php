<div class="row<?=$this->getMessages()?' error':''?>">
	<div class="field">
		<textarea class="textarea <?=$this->getAttribute('class')?>" placeholder="<?=$this->getLabel()?>" <?=$this->getAttributesString()?>><?=$this->getValue()?></textarea>
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>