<fieldset class="form-group" clone-group="<?=$this->getProperty('cloneGroup')?>">
	<legend class="title">
		<?=$this->getProperty('label')?>
	</legend>
	<div class="description">
		<?=$this->getProperty('description')?>
	</div>
	<div class="elements">
		<?=$this->renderElements()?>
	</div>
</fieldset>
