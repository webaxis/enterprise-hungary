<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>
	<div class="full-width">
		<div class="button-container push-left">
			<span>Képek feltöltése</span>
			<input id="files-<?=$this->getId()?>" type="file" multiple="multiple">
		</div>
		<div id="progress-<?=$this->getId();?>" class="progress-bar-container push-left"></div>
	</div>

	<div id="images-container" class="multi-uploader-image-container push-left full-width label-push"></div>
	<input type="hidden" name="<?=$this->getId();?>" id="<?=$this->getId()?>" value="<?=$this->getValue();?>" />

	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>