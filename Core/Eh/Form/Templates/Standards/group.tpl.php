<fieldset class="form-group">
	<legend class="title">
		<?=$this->getProperty('label')?>
	</legend>
	<div class="description">
		<?=$this->getProperty('description')?>
	</div>
	<div class="elements">
		<?=$this->renderElements()?>
	</div>
</fieldset>
