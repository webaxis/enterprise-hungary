<div class="row half <?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>
	<div class="field">
		<input type="password" autocomplete="off" class="text <?=$this->getAttribute('class')?>" type="text" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>
