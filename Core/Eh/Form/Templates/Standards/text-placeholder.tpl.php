<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<div class="field">
		<input autocomplete="off" placeholder="<?=$this->getLabel()?>" class="text <?=$this->getAttribute('class')?>" type="text" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>
