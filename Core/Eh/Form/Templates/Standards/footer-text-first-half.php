<div class="row <?=$this->getMessages() ? ' error' : ''?>">
	<div class="field">
		<input autocomplete="off" placeholder="<?=$this->getLabel();?>" class="half first <?=$this->getAttribute('class')?>" type="text" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
	</div>
