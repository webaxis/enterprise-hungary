<?php
	$captcha = WX2_Tools::createCaptcha(6, 'fonts/wfg/Neris-Light.ttf', 50, 140, 3600);
?>
<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<div class="field captcha">
		<div class="captcha-image">
			<?=$captcha->render();?>
		</div>
		<input type="hidden" class="captcha-image" name="<?=$this->getAttribute('id')?>-id" id="<?=$this->getAttribute('id')?>-id" value="<?=$captcha->getId();?>"/>
		<input id="<?=$this->getAttribute('id')?>-input" placeholder="<?=$this->getLabel();?>" name="<?=$this->getAttribute('id')?>-input" autocomplete="off" class="text captcha-input <?=$this->getAttribute('class')?>" type="text" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
	</div>
</div>
