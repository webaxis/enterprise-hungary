<?
	$msg = $this->getMessages();
	$msg = $msg?$msg[0]:array();
	$fieldValue = $this->getValue();
?>
	<div class="row">
		<?php include('label.tpl.php');?>
		<div class="field">
			<select class="select-year <?=isset($msg[1])?'error':''?>" name="<?=$this->getName()?>[year]" id="<?=$this->getId()?>-year">
				<? foreach ($this->years as $key => $value) : ?>
					<option value="<?=$key;?>" <?=isset($fieldValue['year'])&&$fieldValue['year']==$key?'selected="selected"':''?>> <?=$value;?></option>
				<? endforeach; ?>
			</select>
			<select class="select-month <?=isset($msg[2])?'error':''?>" name="<?=$this->getName()?>[month]" id="<?=$this->getId()?>-month">
				<? foreach ($this->months as $key => $value) : ?>
					<option value="<?=$key;?>" <?=isset($fieldValue['month'])&&$fieldValue['month']==$key?'selected="selected"':''?>> <?=$value;?></option>
				<? endforeach; ?>
			</select>
			<select class="select-day <?=isset($msg[3])?'error':''?>" name="<?=$this->getName()?>[day]" id="<?=$this->getId()?>-day">
				<? foreach ($this->days as $key => $value) : ?>
					<option value="<?=$key;?>" <?=isset($fieldValue['day'])&&$fieldValue['day']==$key?'selected="selected"':''?>> <?=$value;?></option>
				<? endforeach; ?>
			</select>
		</div>
		<?php include('error_row.tpl.php');?>
		<?php include('description.tpl.php');?>
	</div>
	