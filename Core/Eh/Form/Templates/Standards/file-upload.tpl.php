<?
	$rp = Zend_Controller_Front::getInstance()->getRequest()->getParams();
	$id = !empty($rp['id']) ? $rp['id'] : null;

	$options = $this->getOptions();
	$contentId = isset($options['contentId']) ? $options['contentId'] : '';
	$allowedExtensions = !empty($options['allowedExtensions']) ? $options['allowedExtensions'] : false;
	$errorNotAllowedExt = !empty($options['errorNotAllowedExt']) ? $options['errorNotAllowedExt'] : 'Nem engedélyezett file kiterjesztés.';
	$maxFileSize = !empty($options['maxFileSize']) ? $options['maxFileSize'] : 5242880; # 5 MB
	$errorFileTooLarge = !empty($options['errorFileTooLarge']) ? $options['errorFileTooLarge'] : 'Fájl mérete túl nagy.'; # 5 MB

	# Ha van constraint az extions-onokre, at kell alakitani javascript tombbe
	if ($allowedExtensions != false) {
		$allowedExtensions = WX_Tools::toJsArray($allowedExtensions);
	}

?>

<script>
	$(document).ready(function(){
		var containerSelector = 'div[data-id="file-<?=$this->getId();?>"]';

		<? if ( $this->getValue() != '') : ?>
			var file = JSON.parse('<?=html_entity_decode($this->getValue());?>');

			var link = $('#file-<?=$this->getId();?>-name a.file-link');
			link.attr('href', '<?=UPLOAD_URL;?>' + file.type + '/' + file.hash + '.' + file.ext);
			link.html(file.file);

			$('#file-<?=$this->getId();?>-name').show();
			$('#file-<?=$this->getId();?>-loader').hide();

			$('#file-<?=$this->getId();?>-delete').unbind('click').bind('click', function(){
				var answer = confirm('Amennyiben törlöd a fájlt, később nem tudod visszaállítani. Folytatod?');
				if (answer == true) {
					var formData = new FormData;
					formData.append('type', file.type);
					formData.append('ext', file.ext);
					formData.append('hash', file.hash);
					<? if ($id != null) : ?>
						formData.append('id', '<?=$id;?>');
						formData.append('dbModel', '<?=$options['dbModel'];?>');
						formData.append('dbField', '<?=$options['dbField'];?>');
					<? endif; ?>
					wx.ajax('/ajax/deletefile', formData, {
						isImageAjax : true, <? # @todo : ez a formData miatt kell ?>
						afterSuccess : function(response) {
							$('#file-<?=$this->getId();?>-name').hide();
							var link = $('#file-<?=$this->getId();?>-name a.file-link');
							link.attr('href', 'javascript:void();');
							link.html('');
							$('#<?=$this->getId()?>').val('');
						}
					});
				}
			});
		<? else : ?>
			$('#file-<?=$this->getId();?>-name').hide();
			$('#file-<?=$this->getId();?>-loader').hide();
		<? endif; ?>

		$(containerSelector + ' input[type="file"]').unbind('change').bind('change', function() {
			var file = $(this).prop("files")[0];
			var errors = [];

			var ext = $(this).val().split('.').pop().toLowerCase();
			if (<?=$allowedExtensions;?> == false) {
				size = file.size;
				if (size > <?=$maxFileSize;?>) {
					errors.push('<?=$errorFileTooLarge;?>');
				}
			} else {
				if ($.inArray(ext, <?=$allowedExtensions;?>) != -1) {
					size = file.size;
					if (size > <?=$maxFileSize;?>) {
						errors.push('<?=$errorFileTooLarge;?>');
					}
				} else {
					errors.push('<?=$errorNotAllowedExt;?>');
				}
			}

			if (errors.length === 0) {
				var formData = new FormData;
				formData.append('upload-file', this.files[0]);
				formData.append('id', '<?=$contentId;?>');
				formData.append('type', '<?=$options['type'];?>');
				formData.append('directoryPath', '<?=str_replace('\\','/',$options['uploadDirectoryTemp']);?>');
				<? /*formData.append('downloadScript', '<?=$options['downloadScript'];?>'); */ ?>

				$('#file-<?=$this->getId();?>-loader').show();

				setTimeout(function(){
					wx.ajax('/ajax/uploadfile', formData, {
						isImageAjax : true, <? # @todo : ez a formData miatt kell ?>
						afterSuccess : function(response) {
							if (response.status == 'success') {
								var link = $('#file-<?=$this->getId();?>-name a.file-link');
								link.attr('href', response.data.downloadLink);
								link.html(response.data.file);

								$('#file-<?=$this->getId();?>-loader').hide();
								$('#file-<?=$this->getId();?>-name').show();

								var file = {
									'type' : '<?=$options['type'];?>',
									'file' : response.data.file,
									'name' : response.data.name,
									'ext'  : response.data.ext,
									'hash' : response.data.hash
								};

								$('#<?=$this->getId()?>').val(JSON.stringify(file));

								$('#file-<?=$this->getId();?>-delete').unbind('click').bind('click', function(){
									var formData = new FormData;
									formData.append('type', '<?=$options['type'];?>');
									formData.append('ext', response.data.ext);
									formData.append('hash', response.data.hash);
									<? if ($contentId != '') : ?>
										formData.append('db', '<?=$contentId;?>');
									<? endif; ?>

									wx.ajax('/ajax/deletefile', formData, {
										isImageAjax : true, <? # @todo : ez a formData miatt kell ?>
										afterSuccess : function(response) {
											$('#file-<?=$this->getId();?>-name').hide();
											var link = $('#file-<?=$this->getId();?>-name a.file-link');
											link.attr('href', 'javascript:void();');
											link.html('');
											$('#<?=$this->getId()?>').val('');
										}
									});
								});

							} else if (response.status == 'error') {
								alert(response.data);
							}
						}
					});
				},500)
			} else {
				alert(errors);
			}
		});
	});
</script>

<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>
	<div class="field" data-id="file-<?=$this->getId();?>">

		<div class="file-uploader-container">
			<span class="" id="file-<?=$this->getId();?>-name">
				<a href="javascript:void(0);" id="file-<?=$this->getId();?>-delete">[x]</a>
				<a href="javascript:void(0);" class="file-link" target="_blank"></a>
			</span>

			<span class="loader" id="file-<?=$this->getId();?>-loader"><img src="<?=IMAGE_URL?>wx/image-loader.gif" /> Feltöltés folyamatban...</span>
		</div>
		<div class="clearfix"></div>
		<form method="post" enctype="multipart/form-data" action="javascript:void(0);">
			<div class="button-container">
				<span>Feltöltés</span>
				<input type="file" name="upload">
				<input name="hash" type="hidden" value="" />
			</div>

		<input type="hidden" name="<?=$this->getId()?>" id="<?=$this->getId()?>" value="<?=$this->getValue();?>" />
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>
