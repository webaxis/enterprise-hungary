<div class="row<?=$this->getMessages() ? ' error' : ''?>">
	<?php include('label.tpl.php');?>

	<? if (!isset($this->getOptions()['type'])) : ?>
		<? $message = 'Missing "type" from image descriptor\'s "options" branch at "' . $this->getId() . '"!'; ?>
		<? WX_Exception::create($message); ?>
	<? endif; ?>
	<? $type = $this->getOptions()['type']; ?>

	<div class="field image-uploader" data-id="<?=$this->getId();?>" data-type="<?=$type;?>"> <? # this wrapper is required! ?>
		<? $hasDefaultImage = isset($this->getOptions()['defaultImage']) ? true : false; ?>
		<? $displaySize = isset($this->getOptions()['displaySize']) ? $this->getOptions()['displaySize'] : false; ?>
		<? if ($this->getValue() !== '') {
				$imageSrc = WX_Image::getInstance($type)
					->getUploadedOrTemp($displaySize, $this->getValue());
			} else {
				$imageSrc = false;
			}
		?>
		<? $hide = $imageSrc == false ? 'display:none;' : '';  ?> 
		<? $hideDefault = $imageSrc == false ? '' : 'display:none;';  ?> 
		<img class="image" id="<?=$this->getId();?>-image" data-default="<?=$hasDefaultImage;?>" src="<?=$imageSrc;?>" style="<?=$hide;?>"/>
		<? if ($hasDefaultImage) { ?>
			<img class="default-image" id="<?=$this->getId();?>-image-default" src="<?=$this->getOptions()['defaultImage'];?>" style="<?=$hideDefault;?>"/>
		<? } ?>
		<div class="loader" style="display:none;"><img src="" /></div>
		<div class="clear"></div>
		<div class="image-uploader-container" id="<?=$this->getId();?>-container"></div>
		<a class="image-uploader-delete btn large gradient-gray" href="" style="<?=$hide;?>">Kép törlése</a>
		<input type="hidden" name="<?=$this->getId()?>" id="<?=$this->getId()?>" value="<?=$this->getValue();?>" />
	</div>
	<?php include('description.tpl.php');?>
	<?php include('error_row.tpl.php');?>
</div>