<div class="row <?=$this->getAttribute('class')?>">
	<div class="radio-field">
		<input class="radio <?=$this->getAttribute('class')?>" id="<?=$this->getId()?>-<?=$this->value;?>" <?=$this->getAttributesString()?> value="<?=$this->value;?>" <?=$this->getChecked();?>/>
	</div>
	<label for="<?=$this->getId();?>-<?=$this->value;?>" class="for-radio">
		<?=$this->getLabel();?><?=(!$this->getAllowBlank()) ? '<sub>*</sub>' : '';?>
	</label>
	<?php include('error_row.tpl.php');?>
</div>
