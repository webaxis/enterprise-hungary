<?
    $items = $this->getMessages();
	if ($items) {
		$result = '<span class="help-block error">';
		foreach ($items as $key => $item) {
			if (is_array($item)) {
				$i = 0;
				foreach ($item as $k => $v) {
					$i++;
					$result .= $v;
					$result .= $i<count($item)?'<br />':'';
				}
			} else {
				$result .= $item;
				$result .= $key<count($items)-1?'<br />':'';
			}
		}
		$result .= '</span>';
		echo $result;
	}
?>