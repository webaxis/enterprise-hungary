<div class="row checkbox-row <?=$this->getAttribute('class')?>">
	<input class="checkbox <?=$this->getAttribute('class')?>" id="<?=$this->getId()?>-<?=$this->value;?>" <?=$this->getAttributesString()?> value="<?=$this->value;?>" <?=$this->getChecked();?>/>
	<label for="<?=$this->getId();?>-<?=$this->value;?>" class="for-checkbox">
		<?=$this->getLabel();?><?=(!$this->getAllowBlank()) ? '<sub>*</sub>' : '';?>
	</label>
	<?php include('error_row.tpl.php');?>
</div>