<?php include('label.tpl.php');?>
			<div class="field <?=$this->getMessages() ? ' has-error' : ''?>">
				<select class="form-control select day <?=$this->getAttribute('class')?>" <?=$this->getAttributesString()?>>
					<?=$this->getOptionsString(); ?>
				</select>
			</div>

			<?php if (array_key_exists('description', $this->properties) && !empty($this->properties['description'])) : ?>
			<div class="field">
				<span class="info-icon" data-toggle="tooltip" data-placement="top" title="<?=$this->properties['description']?>"></span>
			</div>
			<? endif; ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>



