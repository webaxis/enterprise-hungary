<div class="row<?=$this->getMessages() ? ' has-error' : ''?>">
	<div class="col-lg-12 form-group">
		<?php include('label.tpl.php');?>
		<input autocomplete="off" placeholder="<?=$this->getLabel()?>" class="form-control <?=$this->getAttribute('class')?>" type="password" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
        <?php include('description.tpl.php');?>
        <?php include('error_row.tpl.php');?>
	</div>
</div>