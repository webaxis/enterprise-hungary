<div class="row<?=$this->getMessages() ? ' has-error' : ''?>">
	<div class="col-lg-12 form-group">
		<?php include('label.tpl.php');?>
		<input autocomplete="off" class="form-control <?=$this->getAttribute('class')?>" placeholder="<?=$this->getLabel();?>" value="<?=$this->getValue();?>" <?=$this->getAttributesString()?>/>
		<?php include('error_row.tpl.php');?>
        <?php include('description.tpl.php');?>
	</div>
</div>



