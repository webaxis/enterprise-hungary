<div class="row<?=$this->getMessages() ? ' has-error' : ''?>">
	<div class="col-lg-12 form-group">
		<textarea class="form-control <?=$this->getAttribute('class')?>" <?=$this->getAttributesString()?> placeholder="<?=$this->getLabel();?>"><?=$this->getValue()?></textarea>
		<?php include('error_row.tpl.php');?>
        <?php include('description.tpl.php');?>
		<div class="clearfix"></div>
	</div>
</div>