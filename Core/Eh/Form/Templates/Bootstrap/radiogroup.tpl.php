<div class="row radiogroup <?=$this->getMessages() ? ' has-error' : ''?>">
	<div class="col-lg-12 form-group">
		<?php include('label.tpl.php'); ?>
		<div class="radio-options">
			<?=$this->renderElements()?>
			<?php if (array_key_exists('description', $this->properties) && !empty($this->properties['description'])) : ?>
			<div class="row radio-row-info">
				<span class="info-icon" data-toggle="tooltip" data-placement="top" title="<?=$this->properties['description']?>"></span>
			</div>
			<? endif; ?>
		</div>
		<div class="clearfix"></div>
		<?php include('error_row.tpl.php');?>
	</div>
</div>



