<div class="row radio-row <?= $this->getAttribute('class') ?>">
	<input class="radio <?= $this->getAttribute('class') ?>" id="<?= $this->getId() ?>-<?= $this->value; ?>" <?= $this->getAttributesString() ?> value="<?= $this->value; ?>" <?= $this->getChecked(); ?>/>
	<label for="<?= $this->getId(); ?>-<?= $this->value; ?>" class="for-radio">
		<?= $this->getLabel(); ?><?= (!$this->getAllowBlank()) ? '<sub>*</sub>' : ''; ?>
	</label>
	<?php include('error_row.tpl.php'); ?>
</div>

