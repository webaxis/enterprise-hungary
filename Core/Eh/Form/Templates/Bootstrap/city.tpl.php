<div class="row city-row">
	<div class="col-lg-12 form-group">
		<?php include('label.tpl.php');?>
		<div class="select-container">
			<div class="field <?=$this->getMessages() ? ' has-error' : ''?>">
				<select class="city-autocomplete form-control select year <?=$this->getAttribute('class')?>" <?=$this->getAttributesString()?>> 
					<?=$this->getOptionsString(); ?>
				</select>
				<input type="hidden" id="city-id" name="city-id" value="<?= $this->getValue(); ?>" />
			</div>
		</div>
	</div>
</div>
