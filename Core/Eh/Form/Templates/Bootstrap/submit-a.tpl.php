<div class="row<?=$this->getMessages() ? ' has-error' : ''?>">
	<div class="col-lg-12 form-group text-right">
		<a href="javascript:void(0);" class="btn btn-primary btn-block" <?=$this->getAttributesString()?>><?= $this->getLabel(); ?></a>
	</div>
</div>