<?php

class Eh_Admin extends WX_User
{
	protected $_namespace		= 'admin';

	protected $_table			= 'admin';
	protected $_identity		= 'login';
	protected $_credential		= 'password';

	protected $_useSalt 		= false;

	protected $_expiration		= 86400;

	protected $_fields = array(
		'groups' => array(
			'activation' => array(
				'enabled' => false
			)
		)
	);

	public function getId () {
		return $this->getLoggedInParam('id');
	}
}