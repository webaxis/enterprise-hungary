<?php

class Eh_Static {

	public static $partnersections = array(
		1 => 'Egyetemi partnerek',
		2 => 'Szakmai partnerek',
	);

	public static $languages = array(
		'hu' => 'Magyar',
		'en' => 'Angol',
	);

	public static $sites = array(
		1 => 'Enterprise Hungary',
		2 => 'Startup Campus',
		3 => 'Mindkettő'
	);

	public static $bool = array(
		1 => 'Igen',
		2 => 'Nem',
	);

	public static $month3 = array(
		1 => 'Jan',
		2 => 'Feb',
		3 => 'Már',
		4 => 'Ápr',
		5 => 'Máj',
		6 => 'Jún',
		7 => 'Júl',
		8 => 'Aug',
		9 => 'Sze',
		10 => 'Okt',
		11 => 'Nov',
		12 => 'Dec',
	);

	public static $month = array(
		1 => 'Január',
		2 => 'Február',
		3 => 'Március',
		4 => 'Április',
		5 => 'Május',
		6 => 'Június',
		7 => 'Július',
		8 => 'Augusztus',
		9 => 'Szeptember',
		10 => 'Október',
		11 => 'November',
		12 => 'December',
	);

	static public function getPartnersectionsSelect () {
		$return = array();

		foreach (self::$partnersections as $key => $one) {
			$return[] = array($key, $one);
		}

		return $return;
	}

	static public function getPartnersections () {
		$return = array();

		foreach (self::$partnersections as $key => $one) {
			$return[$key] = $one;
		}

		return $return;
	}

	static public function getPartnersection ($section) {
		return t(self::$partnersections[$section]);
	}

	static public function getLanguagesSelect () {
		$return = array();

		foreach (self::$languages as $key => $one) {
			$return[] = array($key, $one);
		}

		return $return;
	}

	static public function getLanguages () {
		return self::$languages;
	}

	static public function getLanguage ($lang) {
		return self::$languages[$lang];
	}

	static public function getMonth ($key) {
		return self::$month[$key];
	}

	static public function getMonth3 ($key) {
		return self::$month3[$key];
	}
	
	static public function getSite ($key) {
		return self::$sites[$key];
	}

	static public function getSites () {
		return self::$sites;
	}

	static public function getSitesSelect ($includeBoth = true) {
		$return = array();

		foreach (self::$sites as $key => $value) {
			if ($key < 3 || $includeBoth) {
				$return[] = array($key, $value);
			}
		}

		return $return;
	}

	static public function getBatchesSelect () {
		$return = array();

		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from('batch')
					->order('order DESC');

		foreach ($db->query($query)->fetchAll() as $key => $value) {
			$return[] = array($value['id'], $value['title']);
		}

		return $return;
	}

	static public function getBatch ($id) {
		$return = array();

		$db = Zend_Registry::get('db');
		$query = $db->select()
					->from('batch')
					->where('id = "'.$id.'"');
		$result = $db->query($query)->fetch();
		return $result['title'];
	}

	static public function getBoolSelect () {
		$return = array();

		foreach (self::$bool as $key => $value) {
			$return[] = array($key, $value);
		}

		return $return;
	}
}