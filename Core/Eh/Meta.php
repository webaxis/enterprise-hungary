<?php

class Eh_Meta {

	static public function get ($request, $view) {
		$pageTitle = t('PAGE_TITLE');

		$pageTitlePrefix 	= false;

		$metaTitle 			= t('META_TITLE');
		$metaDescription 	= t('META_DESCRIPTION');
		$metaTags 			= t('META_TAGS');

		$ogTitle 			= t('DEFAULT_OG_TITLE');
		$ogDescription 		= t('DEFAULT_OG_DESCRIPTION');
		$ogImage 			= IMAGE_URL . 'og-img-' . PROJECT_NAME . '.jpg';

		switch ($request['controller']) {
			case 'news':
				$object = Eh_Model_News::getInstance()->find(explode('-', $request['news'])[0])->toArray();

				$pageTitlePrefix = $object['title'];
				$metaDescription = $ogDescription = WX_Tools::truncate($object['lead'], 240);

				if (!empty($object['image'])) {
					$ogImage = WX_Image::getInstance('news')->get('normal', $object['image']);
				}

				break;

			case 'content':

				switch ($request['action']) {
					case 'about':
						$pageTitlePrefix = t('TAB_PROGRAM_TITLE');
						break;
				}

				break;

			case 'team':
				$pageTitlePrefix = t('TAB_TEAM_TITLE');
				break;

			case 'startup':

				switch ($request['action']) {
					case 'index':
						$object = Eh_Model_Startup::getInstance()->find(explode('-', $request['startup'])[0])->toArray();

						$pageTitlePrefix = $object['name'];
						$metaDescription = $ogDescription = WX_Tools::truncate($object['intro'], 240);

						if (!empty($object['logo'])) {
							$ogImage = WX_Image::getInstance('startuplogo')->get('normal', $object['logo']);
						}
						break;

					case 'list':
						$pageTitlePrefix = t('TAB_STARTUPS_TITLE');
						break;
				}

				break;

			case 'partner':
				$pageTitlePrefix = t('TAB_PARTNERS_TITLE');
				break;

			case 'incube':
				$pageTitlePrefix = t('TAB_INCUBE_TITLE');
				break;

			case 'faq':
				$pageTitlePrefix = t('TAB_FAQ_TITLE');
				break;
		}

		if ($pageTitlePrefix) {
			$pageTitle = $ogTitle = $metaTitle = $pageTitlePrefix . ' | ' . $pageTitle;
		}

		return array(
			'pageTitle' 		=> $pageTitle,
			'meta' => [
				'title' 		=> $metaTitle,
				'description' 	=> $metaDescription,
				'tags' 			=> $metaTags,
			],
			'og' => [
				'title' 		=> $ogTitle,
				'description' 	=> $ogDescription,
				'image' 		=> $ogImage,
			],
		);
	}
}
