<?php

class Eh_Url {

	# FRONT

	static public function index() {
		return BASE_HOST;
	}
	
	static public function news($id = false, $name = false) {
		$news = '';

		if ($id) {
			$news = '/' . $id . '-';

			if (!$name) {
				$name = Eh_Model_News::find($id)->toArray()['name'];
			}
			
			$news .= WX_Tools::linkRewrite($name);
		}

		return BASE_HOST . t('URL_NEWS') . $news;
	}
	
	static public function about() {
		return BASE_HOST . t('URL_ABOUT');
	}
	
	static public function team() {
		return BASE_HOST . t('URL_TEAM');
	}
	
	static public function startup($id = false, $name = false) {
		$startup = '';

		if ($id) {
			$startup = '/' . $id . '-';

			if (!$name) {
				$name = Eh_Model_News::find($id)->toArray()['name'];
			}
			
			$startup .= WX_Tools::linkRewrite($name);
		}

		return BASE_HOST . t('URL_STARTUP') . $startup;
	}
	
	static public function partner() {
		return BASE_HOST . t('URL_PARTNER');
	}
	
	static public function incube() {
		return BASE_HOST . t('URL_INCUBE');
	}
	
	static public function faq() {
		return BASE_HOST . t('URL_FAQ');
	}

	# ADMIN

	static public function admin() {
		return self::adminAdmin();
	}

	static public function adminIndex() {
		return ADMIN_URL;
	}

	static public function adminAdmin () {
		return ADMIN_URL . 'admin';
	}

	static public function adminBatch () {
		return ADMIN_URL . 'batch';
	}

	static public function adminOrganization () {
		return ADMIN_URL . 'organization';
	}

	static public function adminContent () {
		return ADMIN_URL . 'content';
	}

	static public function adminEvent () {
		return ADMIN_URL . 'event';
	}

	static public function adminFaq () {
		return ADMIN_URL . 'faq';
	}

	static public function adminNews () {
		return ADMIN_URL . 'news';
	}

	static public function adminPartner () {
		return ADMIN_URL . 'partner';
	}

	static public function adminSlide () {
		return ADMIN_URL . 'slide';
	}

	static public function adminStartup () {
		return ADMIN_URL . 'startup';
	}

	static public function adminTeam () {
		return ADMIN_URL . 'team';
	}

	static public function adminTimeline () {
		return ADMIN_URL . 'timeline';
	}

	static public function adminTranslateeh () {
		return ADMIN_URL . 'translateeh';
	}

	static public function adminTranslatesc () {
		return ADMIN_URL . 'translatesc';
	}

	static public function adminLogout () {
		return ADMIN_URL . 'logout';
	}

	static public function adminEdit ($id = false) {
		return 'edit' . ($id != false ? '/id/' . $id : '');
	}

}