<?php

class Eh_Locale extends WX_Locale {

	protected $_defaultLanguage 	= 'hu';

	protected $_defaultRegion 		= 'HU';

	protected $_availableLanguages = array(
		'hu',
		'en',
	);

}