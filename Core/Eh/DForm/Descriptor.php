<?php

class Eh_DForm_Descriptor {

	static public function adminlogin () {
		$descriptor = array(
			'id' => 'adminlogin-form',
			'url' => '',
			'templateDirectory' => CORE_APP_PATH . 'Form/Templates/Adminlogin/',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminlogin',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'login',
							'label' => 'Felhasználónév'
						),
						array(
							'type' => 'password',
							'name' => 'password',
							'label' => 'Jelszó'
						)
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminadmin () {
		$descriptor = array(
			'id' => 'adminadmin-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminadmin',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'name',
							'label' => 'Név',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'name' => 'login',
							'label' => 'Felhasználónév',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'type' => 'password',
							'name' => 'password',
							'label' => 'Jelszó'
						)
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminbatch () {
		$descriptor = array(
			'id' => 'adminbatch-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminbatch',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'title',
							'label' => 'Név',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'description' => 'pl. Batch #1'
						),
						array(
							'name' => 'subtitle',
							'label' => 'Címke',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'description' => 'pl. 2015 Q1'
						)
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminorganization () {
		$descriptor = array(
			'id' => 'adminorganization-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminorganization',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'type' => 'image',
							'options' => array(
								'type' => 'organization',
								'size' => 'adminList',
								'displaySize' => 'adminList'
							),
							'name' => 'logo',
							'label' => 'Logo',
							'description' => 'Javasolt felbontás: 164x164px'
						),
						array(
							'name' => 'title',
							'label' => 'Név',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'name' => 'description',
							'label' => 'Leírás',
							'type' => 'textarea',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'name' => 'link',
							'label' => 'Link',
							'allowBlank' => true,
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminevent () {
		$descriptor = array(
			'id' => 'adminevent-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminevent',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'title',
							'label' => 'Cím',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'name' => 'link',
							'label' => 'Link',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'type' => 'image',
							'options' => array(
								'type' => 'event',
								'size' => 'adminList',
								'displaySize' => 'adminList'
							),
							'name' => 'image',
							'label' => 'Kép',
							'allowBlank' => true,
							'description' => 'Javasolt felbontás: 345x240px'
						),
						array(
							'name' => 'date',
							'label' => 'Dátum',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'decorators' => array(
								'datepicker'
							),
						),
						array(
							'type' => 'option',
							'name' => 'site',
							'label' => 'Weboldal',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'items' => Eh_Static::getSitesSelect()
						),
						array(
							'type' => 'option',
							'name' => 'lang',
							'label' => 'Nyelv',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'items' => Eh_Static::getLanguagesSelect()
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminfaq () {
		$descriptor = array(
			'id' => 'adminfaq-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminfaq',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'question',
							'label' => 'Kérdés',
							'error' => array(
								'message' => t('FORM_ERROR_MUST_FILL_IN')
							)
						),
						array(
							'name' => 'answer',
							'label' => 'Válasz',
							'type' => 'Textarea',
							'error' => array(
								'message' => t('FORM_ERROR_MUST_FILL_IN')
							)
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminnews () {
		$descriptor = array(
			'id' => 'adminnews-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminnews',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'title',
							'label' => 'Cím',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'type' => 'image',
							'options' => array(
								'type' => 'news',
								'size' => 'adminList',
								'displaySize' => 'adminList'
							),
							'name' => 'image',
							'label' => 'Kép',
							'allowBlank' => true,
							'description' => 'Javasolt felbontás: 1400x580px'
						),
						array(
							'type' => 'textarea',
							'name' => 'lead',
							'label' => 'Lead',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'type' => 'textarea',
							'name' => 'description',
							'label' => 'Tartalom',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'class' => 'ckeditor-content editor-news'
						),
						array(
							'type' => 'multiimage',
							'options' => array(
								'type' => 'newsgallery',
								'size' => 'adminList',
							),
							'name' => 'gallery',
							'label' => 'Galéria',
							'allowBlank' => true,
							'description' => 'Javasolt felbontás: 1024x768px'
						),
						array(
							'name' => 'date',
							'label' => 'Dátum',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'decorators' => array(
								'datepicker'
							),
						),
						array(
							'type'	=> 'file',
							'name' => 'pressrelease',
							'label' => 'Sajtóközlemény',
							'template' => 'file-upload.tpl.php',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'options' => [
								'type' => 'release',
								'dbModel' => 'Eh_Model_News',
								'dbField' => 'pressrelease',
								'uploadDirectoryTemp'		=> UPLOAD_TEMP_PATH . 'pressrelease',
								'maxFileSize' 				=> 20971520, # 20 MB
								'errorFileTooLarge' 		=> 'Fájl mérete túl nagy. Maximálisan engedélyezett méret: 20 MB',
								'allowedExtensions' 		=> ['doc', 'docx'],
								'errorNotAllowedExt' 		=> 'Nem engedélyezett file kiterjesztés. Engélyezett kiterjesztések: .doc, .docx'
							],
							'description' => 'Engedélyezett fájl formátumok: .doc, .docx'
						),
						array(
							'type' => 'option',
							'name' => 'site',
							'label' => 'Weboldal',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'items' => Eh_Static::getSitesSelect()
						),
						array(
							'type' => 'option',
							'name' => 'lang',
							'label' => 'Nyelv',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'items' => Eh_Static::getLanguagesSelect()
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminpartner () {

		$descriptor = array(
			'id' => 'adminpartner-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminpartner',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'link',
							'label' => 'Link',
							'allowBlank' => true,
						),
						array(
							'name' => 'logo',
							'label' => 'Kép',
							'type' => 'image',
							'options' => array(
								'type' => 'partner',
								'size' => 'adminList',
								'displaySize' => 'adminList'
							),
							'description' => '<br/>Áttetsző hátterű png feltöltése javasolt! Javasolt felbontás: 180x180px (a feltöltő automatikusan ekkora keretek közé méretezi a képet).'
						),
						array(
							'type' => 'option',
							'name' => 'section',
							'label' => 'Szekció',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'items' => Eh_Static::getPartnersectionsSelect()
						),
						array(
							'type' => 'option',
							'name' => 'footer',
							'label' => 'Láblécben megjelenik',
							'allowBlank' => true,
							'items' => Eh_Static::getBoolSelect()
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminslide () {
		$descriptor = array(
			'id' => 'adminslide-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminslide',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'type' => 'image',
							'options' => array(
								'type' => 'slide',
								'size' => 'adminList',
								'displaySize' => 'adminList'
							),
							'name' => 'image',
							'label' => 'Slide',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'description' => 'Javasolt felbontás: 1400x830px'
						),
						array(
							'name' => 'title',
							'label' => 'Főcím',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
						),
						array(
							'name' => 'subtitle',
							'label' => 'Alcím',
							'allowBlank' => true,
							'description' => 'Csak a Startup Campus esetén érhető el.'
						),
						array(
							'name' => 'link',
							'label' => 'Link',
							'allowBlank' => true
						),
						array(
							'name' => 'buttontitle',
							'label' => 'Gomb felirat',
							'allowBlank' => true,
							'description' => 'Csak a Startup Campus esetén érhető el.',
						),
						array(
							'name' => 'buttonlink',
							'label' => 'Gomb link',
							'allowBlank' => true,
							'description' => 'Csak a Startup Campus esetén érhető el.',
						),
						array(
							'type' => 'option',
							'name' => 'forward',
							'label' => 'Tovább gomb megjelenítése',
							'allowBlank' => true,
							'items' => Eh_Static::getBoolSelect()
						),
						array(
							'type' => 'option',
							'name' => 'site',
							'label' => 'Weboldal',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'items' => Eh_Static::getSitesSelect(false)
						),
						array(
							'type' => 'option',
							'name' => 'lang',
							'label' => 'Nyelv',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'items' => Eh_Static::getLanguagesSelect()
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminstartup () {
		$descriptor = array(
			'id' => 'adminstartup-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminstartup',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'name',
							'label' => 'Név',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'type' => 'image',
							'options' => array(
								'type' => 'startuplogo',
								'size' => 'adminList',
								'displaySize' => 'adminList'
							),
							'name' => 'logo',
							'label' => 'Logo',
							'allowBlank' => true,
							'description' => 'Javasolt felbontás: 220x65px'
						),
						array(
							'type' => 'multiimage',
							'options' => array(
								'type' => 'startupimages',
								'size' => 'adminList',
							),
							'name' => 'images',
							'label' => 'Images',
							'allowBlank' => true,
							'description' => 'Javasolt felbontás: 1024x768px'
						),
						array(
							'type' => 'textarea',
							'name' => 'intro', 
							'label' => 'Intro',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'description' => 'Ez jelenik meg a listában.'
						),
						array(
							'type' => 'option',
							'name' => 'batch',
							'label' => 'Batch',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'items' => Eh_Static::getBatchesSelect()
						),
						array(
							'type' => 'textarea',
							'name' => 'usp',
							'label' => 'USP',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'type' => 'textarea',
							'name' => 'details',
							'label' => 'Details',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'description' => 'Company, address, CEO, email, phone, url, etc.'
						),
						array(
							'type' => 'textarea',
							'name' => 'projectmanager',
							'label' => 'Project Manager',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'businessinformation',
							'label' => 'Business Information',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'investmentinformation',
							'label' => 'Investment Information',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'projectteam',
							'label' => 'Project Team',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'incubator',
							'label' => 'Incubator',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'businesssummary',
							'label' => 'Business Summary',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'solution',
							'label' => 'Solution (Product/Services) & Impact',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'targetmarket',
							'label' => 'Target Market',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'competitors',
							'label' => 'Competitors',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'progress',
							'label' => 'Progress',
							'allowBlank' => true,
						),
						array(
							'type' => 'textarea',
							'name' => 'businessmodel',
							'label' => 'Business Model',
							'allowBlank' => true,
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function adminteam () {
		$descriptor = array(
			'id' => 'adminteam-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'adminteam',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'type' => 'image',
							'options' => array(
								'type' => 'team',
								'size' => 'adminList',
								'displaySize' => 'adminList'
							),
							'name' => 'image',
							'label' => 'Kép',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'name' => 'name',
							'label' => 'Név',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'name' => 'description',
							'label' => 'Titulus / leírás',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'name' => 'email',
							'label' => 'Email',
							'allowBlank' => true
						),
						array(
							'name' => 'linkedin',
							'label' => 'LinkedIn link',
							'allowBlank' => true
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function admintimeline () {
		$descriptor = array(
			'id' => 'admintimeline-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'admintimeline',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'label',
							'label' => 'Címke',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'description' => 'pl. AUG 23'
						),
						array(
							'name' => 'title',
							'label' => 'Cím',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
						array(
							'type' => 'textarea',
							'name' => 'description',
							'label' => 'Leírás',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							)
						),
					)
				)
			)
		);

		return $descriptor;
	}

	static public function admintranslate () {
		$descriptor = array(
			'id' => 'admintranslate-form',
			'url' => '',
			'method' => 'POST',
			'globalFilters' => array(
				array(
					'class' => 'WX_Form_Filter_StringTrim',
					'options' => array(
						'charlist' => ' '
					)
				)
			),
			'groups' => array(
				array(
					'id' => 'admintranslate',
					'label' => '',
					'template' => 'empty-group.tpl.php',
					'items' => array(
						array(
							'name' => 'label',
							'label' => 'Címke',
							'error' => array(
								'message' => 'Mező kitöltése kötelező'
							),
							'enable' => false
						),
						array(
							'type' => 'hidden',
							'name' => 'label'
						),
					)
				)
			)
		);

		$languages = Eh_Locale::getInstance()->getLanguages();

		foreach ($languages as $lang) {
			$descriptor['groups'][0]['items'][] = array(
				'name' => strtolower($lang['language']),
				'label' => ucfirst($lang['name']),
				'type' => 'textarea',
				'error' => array(
					'message' => 'Mező kitöltése kötelező'
				)
			);
		}

		return $descriptor;
	}
}