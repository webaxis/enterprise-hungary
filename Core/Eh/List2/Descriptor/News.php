<?php

class Eh_List2_Descriptor_News {

	public function descriptor () {
		return array(
			'id' => 'news',
			'template' => 'news.phtml',
			'table' => 'news',
			'paginator' => [
				'type' => 'more',
				'template' => 'news.phtml'
			],
			'orderBy' => 'date',
			'order' => 'DESC',
			'limit' => 3,
			'prefilter' => '(site = 1 || site = 3) && lang = "' . Eh_Locale::getInstance()->getLanguage() . '"'
		);
	}
}