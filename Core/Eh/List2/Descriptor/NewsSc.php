<?php

class Eh_List2_Descriptor_NewsSc {

	public function descriptor () {
		return array(
			'id' => 'newsSc',
			'template' => 'news.phtml',
			'table' => 'news',
			'paginator' => [
				'type' => 'more',
				'template' => 'news.phtml'
			],
			'orderBy' => 'date',
			'order' => 'DESC',
			'limit' => 3,
			'prefilter' => 'site = 2 || site = 3'
		);
	}
}